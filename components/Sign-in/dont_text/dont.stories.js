import DontText from "."

export default {
    title: "Sign/Don't"
}
const Template = (argument) => <DontText {...argument} />
export const doText = Template.bind({})
doText.args = {
    dont_signUp: "Don’t have an account?",
    sign_Up: "Sign UP"
}
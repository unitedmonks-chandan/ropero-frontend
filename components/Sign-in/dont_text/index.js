import React from 'react'
const DontPage = ({ dont_signUp, sign_Up }) => {
    return (
        <div className=' w-full flex justify-center dontText'>
            <p className='text-xs'> {dont_signUp}
                <button className=' border-0 bg-transparent ' type='submit'>
                    <span className=' text-gray-800'> {sign_Up}</span>
                </button>
            </p>
        </div>
    )
}

export default DontPage
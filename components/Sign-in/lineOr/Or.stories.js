import OR from "./index"


export default {
    title: "Sign/line"
}
const Template = (argument) => <OR {...argument} />
export const OrLine = Template.bind({})
OrLine.args = {
    or_text: "Or"
}
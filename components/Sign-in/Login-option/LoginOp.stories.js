import LoginOption from "./index"
import Google from "../../../public/assets/images/Sign-in/google.png"
export default {
    title: "Sign/Option"
}
const Template = (argument) => <LoginOption {...argument} />
export const loginOption = Template.bind({})
loginOption.args = {
    social_icon: Google,
    signOption_Text: "Sign in with Google"
}
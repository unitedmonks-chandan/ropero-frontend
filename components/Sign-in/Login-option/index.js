import React from 'react'

const LoginOption = ({ social_icon, signOption_Text }) => {
    return (
        <>
            <div className='flex  Login_option w-full md:w-1/3 lg:w-1/3'>
                <div className="w-full">

                    <button className=" h-10 flex mx-auto text-black " type="submit">
                        <div className='mx-auto'>
                            <img className='py-2' src={social_icon} alt='google_image' />
                        </div>
                        <p className=' mx-auto py-2'>{signOption_Text}</p>
                    </button>

                </div>
            </div>

        </>



    )
}

export default LoginOption
import Heading from "."

export default {
    title: "Sign/Heading"
}
const Template = (argument) => <Heading {...argument} />
export const signTitle = Template.bind({})
signTitle.args = {
    Heading_text: "SIGN IN"
}
import React from 'react'
// import "./style.css"
const Heading = ({ Heading_text }) => {
    return (
        <div className=' w-full heading_sign flex justify-center py-10'>
            <h1 className='text-4xl'>{Heading_text}</h1>
        </div>
    )
}

export default Heading;
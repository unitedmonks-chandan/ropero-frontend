import React from 'react'

const Button = ({ btn_Text }) => {
    return (

        <div className='flex justify-center'>
            <div className="w-full px-2 md:w-1/3 SignIN_btn sm:mx-4 ">

                <button className="w-full h-10 px-3   text-black " type="submit"> {btn_Text}</button>
            </div>
        </div>
    )
}

export default Button;
import Button from "./index"

export default {
    title: "Sign/button"
}
const Template = (argument) => <Button {...argument} />
export const SignIn = Template.bind({})
SignIn.args = {
    btn_Text: "SIGN IN"
}
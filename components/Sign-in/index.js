import React from 'react'
import Heading from './title/index';
import InputField from "./InputField/index";
import Forgot from "./ForgotText/index"
import Button from "./button/index";
import OR from "./lineOr/index"
import LoginOption from "./Login-option/index";
import DontPage from "./dont_text/index";

const SignPage = (props) => {
    const { Container, child_contain, inputStyle, fot, orstyle, logStyle, logStyle1, dont,
        signText, email, EmailType, TypeName, TypePassword, password, PasswordN, sign, forgot, google, facebook,
        goggle_text, facebook_text, dnt, sign_Up, or } = props.props;

    return (

        <div className={Container} >
            <Heading Heading_text={signText} />

            <div className={child_contain}>
                <InputField
                    labelText={email}
                    InputType={EmailType}
                    name={TypeName}
                />
            </div>

            <div className={inputStyle}>
                <InputField
                    labelText={password}
                    InputType={TypePassword}
                    name={PasswordN} />
            </div>
            <div className={fot}>
                <Forgot
                    ForgotText={forgot} />
            </div>

            <Button
                btn_Text={sign}
            />
            <div className={orstyle}>
                <OR or_text={or} />
            </div>
            <div className={logStyle}>
                <LoginOption
                    social_icon={google}
                    signOption_Text={goggle_text} />
            </div>

            <div className={logStyle1}>
                <LoginOption
                    social_icon={facebook}
                    signOption_Text={facebook_text} />
            </div>

            <div className={dont}>
                <DontPage
                    dont_signUp={dnt}
                    sign_Up={sign_Up} />
            </div>

        </div >
    )
}

export default SignPage;
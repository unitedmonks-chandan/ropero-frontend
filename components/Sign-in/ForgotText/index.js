import React from 'react'
// import "./style.css"
const Forgot = ({ ForgotText }) => {
    return (
        <div className='w-full flex justify-center forggotText'>
            <button className=' border-0 bg-transparent ' type='submit'>
                <p className='text-xl'>{ForgotText}</p>
            </button>
        </div>
    )
}

export default Forgot;

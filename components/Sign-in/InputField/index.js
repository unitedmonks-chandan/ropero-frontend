import React from 'react'
// import "./style.css"
const InputField = ({ labelText, InputType, name }) => {
    return (
        <div className="w-full px-2 md:w-1/3 SignInput sm:mx-4 ">
            <label
                className="block mt-10 mb-4 text-sm md:text-xl lg:text-2xl xl:text-2xl font-light"
                htmlFor="">{labelText}</label>
            <input
                className="w-full h-10 px-3 bg-transparent"
                type={InputType} name={name}
                autoComplete="off"
                required />
        </div>
    )
}

export default InputField;

import Google from "../../public/assets/images/Sign-in/google.png";
import Facebook from "../../public/assets/images/Sign-in/facebook.png";

import SignPage from "./index"

export default {
    title: "SSS/page"
}
const Template = (argument) => <SignPage {...argument} />
export const sigN = Template.bind({})
sigN.args = {
    props: {
        Container: 'h-full bg-gray-100 lg:mx-28 xl:mx-40 2xl:mx-32 rounded-2xl',
        child_contain: ' flex justify-center item-center',
        inputStyle: 'flex justify-center item-center',
        fot: 'py-3',
        orstyle: 'my-2',
        logStyle: 'my-3 flex justify-center mx-6',
        logStyle1: 'my-3 flex justify-center mx-6 ',
        dont: 'py-16',
        signText: "SIGN IN",
        email: "Email*",
        EmailType: "email",
        TypeName: "email",
        password: "Password*",
        TypePassword: "password",
        PasswordN: "password",
        sign: "SIGN IN",
        forgot: "Forgot Password?",
        google: Google,
        goggle_text: "Sign in with Google",
        facebook: Facebook,
        facebook_text: "Sign in with Facebook",
        dnt: " Don't have an account? ",
        sign_Up: "Sign Up",
        or: "Or"
    }
}
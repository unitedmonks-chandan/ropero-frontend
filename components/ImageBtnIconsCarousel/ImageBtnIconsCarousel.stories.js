import ImageBtnIconsCarousel from "./ImageBtnIconsCarousel";
import img1 from "../../public/assets/images/zimba/best-sellers1.jpg";
import img2 from "../../public/assets/images/zimba/best-sellers2.jpg";
import heartSvg from "../../public/assets/svg/red-heart-fill.svg";
import bagSvg from "../../public/assets/svg/bag.svg";

export default {
  title: "Roperro / Carouse with Image,Btn & Icons",
};

const Template = (args) => <ImageBtnIconsCarousel {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  customSpaceBetween: 20,
  customStyle: " relative zimba-best-sellers-slider hide-arrow ",
  carouselData: [
    {
      customSlide: "flex",
      btnIcons: "bg-lightCream px-12 grid place-items-center relative -ml-1",
      image: img1,
      btnData: {
        container: "inline-flex flex-col items-start gap-8",
        position: "",
        headingStyle: "text-midGray font-primary text-left",
        heading1: "LINEN JACKET",
        heading1Style: "text-3xl mb-1",
        heading2: "₹ 5000",
        heading2Style: "text-2xl mb-2",
        heading3:
          "Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.",
        heading3Style: "text-lg",
        btnLink: "/view-all",
        btn: "inline-block bg-black text-white text-lg font-primary px-10 py-1",
        size: "",
        btnText: "View",
      },
      icons: [
        {
          icon: heartSvg,
          iconSize: "icon-size",
        },
        {
          icon: bagSvg,
          iconSize: "icon-size",
        },
      ],
      iconsPosition: "flex gap-4 absolute right-20 bottom-24",
    },
    {
      customSlide: "flex",
      btnIcons: "bg-lightCream px-12 grid place-items-center relative -ml-1",
      image: img2,
      btnData: {
        container: "inline-flex flex-col items-start gap-8",
        position: "",
        headingStyle: "text-midGray font-primary text-left",
        heading1: "LINEN JACKET",
        heading1Style: "text-3xl mb-1",
        heading2: "₹ 5000",
        heading2Style: "text-2xl mb-2",
        heading3:
          "Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.",
        heading3Style: "text-lg",
        btnLink: "/view-all",
        btn: "inline-block bg-black text-white text-lg font-primary px-10 py-1",
        size: "",
        btnText: "View",
      },
      icons: [
        {
          icon: heartSvg,
          iconSize: "icon-size",
        },
        {
          icon: bagSvg,
          iconSize: "icon-size",
        },
      ],
      iconsPosition: "flex gap-4 absolute right-20 bottom-24",
    },
    {
      customSlide: "flex",
      btnIcons: "bg-lightCream px-12 grid place-items-center relative -ml-1",
      image: img2,
      btnData: {
        container: "inline-flex flex-col items-start gap-8",
        position: "",
        headingStyle: "text-midRed font-primary text-left",
        heading1: "LINEN JACKET",
        heading1Style: "text-3xl mb-1",
        heading2: "₹ 5000",
        heading2Style: "text-2xl mb-2",
        heading3:
          "Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.",
        heading3Style: "text-lg",
        btnLink: "/view-all",
        btn: "inline-block bg-white text-black text-lg font-primary px-10 py-1",
        size: "",
        btnText: "View",
      },
      icons: [
        {
          icon: heartSvg,
          iconSize: "icon-size",
        },
        {
          icon: bagSvg,
          iconSize: "icon-size",
        },
      ],
      iconsPosition: "flex gap-4 absolute right-20 bottom-24",
    },
    {
      customSlide: "flex",
      btnIcons: "bg-lightCream px-12 grid place-items-center relative -ml-1",
      image: img1,
      btnData: {
        container: "inline-flex flex-col items-start gap-8",
        position: "",
        headingStyle: "text-midRed font-primary text-left",
        heading1: "LINEN JACKET",
        heading1Style: "text-3xl mb-1",
        heading2: "₹ 5000",
        heading2Style: "text-2xl mb-2",
        heading3:
          "Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.",
        heading3Style: "text-lg",
        btnLink: "/view-all",
        btn: "inline-block bg-white text-black text-lg font-primary px-10 py-1",
        size: "",
        btnText: "View",
      },
      icons: [
        {
          icon: heartSvg,
          iconSize: "icon-size",
        },
        {
          icon: bagSvg,
          iconSize: "icon-size",
        },
      ],
      iconsPosition: "flex gap-4 absolute right-20 bottom-24",
    },
  ],
};

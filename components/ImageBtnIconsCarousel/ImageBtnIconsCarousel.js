import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation, Autoplay } from "swiper";
import SwiperCore from "swiper";
// import "../CarouselCustomSyling/carousel-custom-styling.css";

import LinkButtonWithHeading from "../NewButtons/LinkButtonWithHeading/LinkButtonWithHeading";

SwiperCore.use([Pagination])

function  ImageBtnIconsCarousel({
  customStyle,
  carouselData,
  customSlidesPerView,
  customSlidesPerGroup,
  customSpaceBetween,
}) {
  console.log(customSlidesPerView);
  return (
    <div className={customStyle}>
      <Swiper
        slidesPerView={customSlidesPerView ? customSlidesPerView : 2}
        slidesPerGroup={customSlidesPerGroup ? customSlidesPerGroup : 2}
        spaceBetween={customSpaceBetween ? customSpaceBetween : 8}
        // autoplay={{
        //   delay: 2500,
        //   disableOnInteraction: false,
        // }}
        pagination={{
          clickable: true,
        }}
        loop={true}
        navigation
        modules={[Pagination]}
        className={`mySwiper`}
      >
        {carouselData &&
          carouselData.map(
            (
              { customSlide, image, imageTailwind, altTag, btnIcons, btnData, icons, iconsPosition },
              i
            ) => {
              return (
                <SwiperSlide key={i}>
                  <div className={customSlide}>
                    <div className={imageTailwind}>
                      <img src={image} alt={altTag} style={{ width: '100%' }} />
                    </div>
                    <div className={btnIcons}>
                      <LinkButtonWithHeading {...btnData} />
                      <div className={iconsPosition}>
                        {icons &&
                          icons.map(({ icon, iconSize }, i) => {
                            return (
                              <img
                                src={icon}
                                alt="icon"
                                className={iconSize}
                                key={i}
                              />
                            );
                          })}
                      </div>
                    </div>
                  </div>
                </SwiperSlide>
              );
            }
          )}
      </Swiper>
    </div>
  );
}

export default ImageBtnIconsCarousel;

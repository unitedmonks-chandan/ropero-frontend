import Trendy from "./index";

import TrendyImage from '../../public/assets/images/trendy/TRENDY.jpg';
import imageOne from '../../public/assets/images/trendy/imageOne.jpg';
import imageTwo from '../../public/assets/images/trendy/imageTwo.jpg';
import imageThree from '../../public/assets/images/trendy/imageThree.jpg';
import bag from '../../public/assets/images/trendy/bag.jpg';

export default {
    title: "Roperro/Trendy"
}


const Template = args => <Trendy {...args} />;

export const TrendyMonday = Template.bind({});

TrendyMonday.args = {
    props: {
            image: {
                textImage: {
                imageSource: TrendyImage,
                altTag: "Trendy Text",
                tailwindChild: 'ml-16 w-2/5',
                tailwind: 'flex justify-end'
            },
            
            
            image1: {
                imageSource: imageOne,
                altTag: "Boy in Black Jacket"
            },
            
            image2: {
                imageSource: imageTwo,
                altTag: "Girl with Black Bag"
            },
            imagebag: {
                imageSource: bag,
                altTag: "Black Bag"
            },

            imageTall: {
                imageSource: imageThree,
                altTag: "Girl with White Bag",
                tailwind: 'row-span-2'
            },
            
            tailwind: 'grid grid-cols-3 -mt-6'
        },

        text: {
            heading: {
                text: 'trendy',
                tailwind: 'uppercase text-8xl text-hoverYellow font-cinzel text-center mt-9'
            },
            subHeading: {
                text: 'monday',
                tailwind: 'uppercase text-7xl text-colorBlack0 text-center -mt-12'
            },
            para: {
                text: "The online store presents selected successful collections",
                tailwind: 'text-2xl text-colorGray38 text-center mt-2.5 mb-auto'
            },
            link: {
                text: 'shop now',
                tailwind: 'w-64 h-16 font-medium bg-colorBlack0 text-white text-2xl font-medium flex justify-center items-center uppercase',
                linkHref: '/'
            },

            tailwind: 'mt-9 ml-28 mr-28 mb-20 flex flex-col items-center'
        }
}
}
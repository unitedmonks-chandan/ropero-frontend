import Heading from '../Typography/Heading';
import Para from '../Typography/Para';
import RoperroLink from '../RoperroLink';


const Trendy = (props) => {

    const { image, text } = props.props;
    const { textImage, image1, image2, imagebag, imageTall } = image;
    const { heading, subHeading, para, link } = text;

    return (
        <div>
            <div className={textImage.tailwind}>
                <img src={textImage.imageSource} alt={textImage.altTag}
                    className={image.textImage.tailwindChild}
                />
                <img src={image.textImage.imageSource} alt={image.textImage.altTag}
                    className={image.textImage.tailwindChild}
                />
            </div>
            <div className={image.tailwind}>
                <img src={image1.imageSource} alt={image1.altTag} />
                <img src={image2.imageSource} alt={image2.altTag} />
                <img src={imageTall.imageSource} alt={imageTall.altTag}
                    className={imageTall.tailwind}
                />
                <img src={imagebag.imageSource} alt={imagebag.altTag} />

                <div className={text.tailwind}>
                    <Heading text={heading.text} tailwind={heading.tailwind} />
                    <Heading text={subHeading.text} tailwind={subHeading.tailwind} />
                    <Para text={para.text} tailwind={para.tailwind} />
                    <RoperroLink text={link.text} tailwind={link.tailwind} linkHref={link.linkHref} />
                </div>
            </div>
        </div>
    );
};

export default Trendy;
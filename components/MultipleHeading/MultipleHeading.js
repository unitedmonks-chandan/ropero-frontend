import React from "react";

function MultipleHeading({
  container,
  containerPosition,
  logo,
  logoSize,
  heading1,
  heading1Style,
  heading2,
  heading2Style,
  heading3,
  heading3Style,
  heading4,
  heading4Style,
}) {
  return (
    <div
      className={`${container} ${containerPosition ? containerPosition : ""}`}
    >
      {logo && <img src={logo} alt="" className={logoSize} />}
      {heading1 && <p className={heading1Style}>{heading1}</p>}
      {heading2 && <p className={heading2Style}>{heading2}</p>}
      {heading3 && <p className={heading3Style}>{heading3}</p>}
      {heading4 && <p className={heading4Style}>{heading4}</p>}
    </div>
  );
}

export default MultipleHeading;

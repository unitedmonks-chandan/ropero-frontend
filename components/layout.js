import Footer from '../components/Footer'
import NewNavbar from '../components/NewHeader/Navbar/index'

const Layout = ({ children }) => {
  let footer_data = {
    logo: {
      iconLogo: {
        logoFooter: '/assets/svg/roperro-white-logo.svg',
      },
      textLogo: {
        svg: '/assets/svg/roperro-white-text-logo.svg',
        customCss: 'ropero-text-logo-footer',
      },
    },

    links: {
      houseOfRoperro: {
        headingText: 'house of roperro',
        linksArray: [
          { linkText: 'Career', address: '/' },
          { linkText: ' Contact', address: '/contact' },
        ],
      },
      usefulLinks: {
        headingText: 'useful links',
        linksArray: [
          { linkText: 'FAQ', address: '/faqs' },
          { linkText: 'Shipping', address: '/Shipping' },
          { linkText: 'Return Payment', address: '/' },
          { linkText: 'Refund Cancellation', address: '/' },
          { linkText: 'Terms & Conditions', address: '/' },
        ],
      },
    },

    advantage: {
      headingText: 'roperro advantages',
      advantageArray: [
        {
          advantageText: 'Free Delivery',
          svg: '/assets/svg/delivery-van-svg-icon.svg',
        },
        {
          advantageText: 'Free Returns',
          svg: '/assets/svg/free-return-svg-icon.svg',
        },
        {
          advantageText: 'Free Gift Wrapping',
          svg: '/assets/svg/gift-wrap-svg-icon.svg',
        },
      ],
    },

    email: {
      emailLabelText: 'subscribe to our newsletter',
      emailInputPlaceholder: 'Enter Your Email',
      buttonText: 'subscribe',
      buttonTailwind:
        'w-40 h-12 text-lg font-medium text-colorBlack0 bg-hoverYellow uppercase mt-4',
    },

    largeIcons: {
      qualityProductIcon: '/assets/svg/quality-svg-icon.svg',
      easyReturnIcon: '/assets/svg/easy-return-svg-icon.svg',
    },

    copyrightText: '&copy; 2022 All Rights Reserved ROPERO',

    SocialMediaIcons: [
      {
        icon: '/assets/svg/facebook-svg-icon.svg',
        iconCssClass: 'facebook-icon',
      },
      {
        icon: '/assets/svg/instagram-svg-icon.svg',
        iconCssClass: 'instagram-icon',
      },
      {
        icon: '/assets/svg/twitter-svg-icon.svg',
        iconCssClass: 'twitter-icon',
      },
    ],
  }

  let header_data = {
    iconsNav: [
      {
        icon: '/assets/svg/user-new.svg',
        mobileView: 'hide',
        // NavbarIconNavigation: '/SignIn',
        NavbarIconNavigation: '#',
      },
      {
        icon: '/assets/svg/heart-new.svg',
        mobileView: 'show',
        // NavbarIconNavigation: '/Favorite',
        NavbarIconNavigation: '#',
      },
      {
        icon: '/assets/svg/shopping-bag-new.svg',
        mobileView: 'hide',
        // NavbarIconNavigation: '/my-orders',
        NavbarIconNavigation: '#',
      },
    ],

    searchIcon: '/assets/svg/search-svg-icon.svg',
    menuItems: [
      {
        name: 'Brands',
        menuItemNavigation: '/',
        subMenu: {
          subMenuItems: [
            {
              item: 'Lerida',
              subMenuItemNavigation: '/brands/lerida',
            },
            {
              item: 'Migear',
              subMenuItemNavigation: '/brands/migear',
            },
            {
              item: 'Arkar',
              subMenuItemNavigation: '/brands/arkar',
            },
            {
              item: 'Monada',
              subMenuItemNavigation: '/brands/monadaa',
            },
            {
              item: 'Zimba',
              subMenuItemNavigation: '/brands/zimba',
            },
          ],
        },
      },
      {
        name: 'Men',
        menuItemNavigation: '#',
        subMenu: {
          subMenuItems: [
            // {
            //   item: 'Lerida',
            //   subMenuItemNavigation: '/brands/lerida',
            // },
            // {
            //   item: 'Migear',
            //   subMenuItemNavigation: '/brands/migear',
            // },
            // {
            //   item: 'Arkar',
            //   subMenuItemNavigation: '/brands/arkar',
            // },
            // {
            //   item: 'Monada',
            //   subMenuItemNavigation: '/brands/monada',
            // },
            // {
            //   item: 'Zimba',
            //   subMenuItemNavigation: '/brands/zimba',
            // },
          ],
        },
      },
      {
        name: 'Women',
        menuItemNavigation: '#',
        subMenu: {
          subMenuItems: [
            // {
            //   item: 'Lerida',
            //   subMenuItemNavigation: '/brands/lerida',
            // },
            // {
            //   item: 'Migear',
            //   subMenuItemNavigation: '/brands/migear',
            // },
            // {
            //   item: 'Arkar',
            //   subMenuItemNavigation: '/brands/arkar',
            // },
            // {
            //   item: 'Monada',
            //   subMenuItemNavigation: '/brands/monada',
            // },
            // {
            //   item: 'Zimba',
            //   subMenuItemNavigation: '/brands/zimba',
            // },
          ],
        },
      },
      {
        name: 'Collections',
        menuItemNavigation: '#',
        subMenu: {
          subMenuItems: [
            // {
            //   item: 'Lerida',
            //   subMenuItemNavigation: '/brands/lerida',
            // },
            // {
            //   item: 'Migear',
            //   subMenuItemNavigation: '/brands/migear',
            // },
            // {
            //   item: 'Arkar',
            //   subMenuItemNavigation: '/brands/arkar',
            // },
            // {
            //   item: 'Monada',
            //   subMenuItemNavigation: '/brands/monada',
            // },
            // {
            //   item: 'Zimba',
            //   subMenuItemNavigation: '/brands/zimba',
            // },
          ],
        },
      },
      {
        name: 'Sale',
        menuItemNavigation: '#',
        subMenu: {
          subMenuItems: [
            // {
            //   item: "Lerida",
            //   subMenuItemNavigation: "/brands/lerida",
            // },
            // {
            //   item: "Migear",
            //   subMenuItemNavigation: "/brands/migear",
            // },
            // {
            //   item: "Arkar",
            //   subMenuItemNavigation: "/brands/arkar",
            // },
            // {
            //   item: "Monada",
            //   subMenuItemNavigation: "/brands/monada",
            // },
            // {
            //   item: "Zimba",
            //   subMenuItemNavigation: "/brands/zimba",
            // },
          ],
        },
      },
    ],
  }

  return (
    <>
      <NewNavbar
        menuItems={header_data.menuItems}
        iconsNav={header_data.iconsNav}
        searchIcon={header_data.searchIcon}
      />
      <div>{children}</div>
      <Footer props={footer_data} />
    </>
  )
}
export default Layout

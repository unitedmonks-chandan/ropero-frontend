import CartItem from "./index";
import itemOne from '../../public/assets/images/cart/itemOne.jpg';

import RupeeSVGIcon from '../../public/assets/svg/checkout/rupee-svg-icon.svg';
import MinusSVGIcon from '../../public/assets/svg/checkout/minus-svg-icon.svg';
import PlusSVGIconBlack from '../../public/assets/svg/checkout/plus-svg-icon-black.svg';

export default {
    title: "Roperro/Cart Item"
}

const Template = args => <CartItem {...args} />;

export const GreySweatshirt = Template.bind({});

GreySweatshirt.args = {

    tailwind: 'flex items-start',
    css: '',

    image: {
        imageSource: itemOne,
        altTag: 'Girl in Grey Sweatshirt',
        imageTailwind: '',
        imageCss: '',
        imageWrapperTailwind: '',
        imageWrapperCss: ''
    },

    textContainer: {
        tailwind: 'ml-6',
        css: '',

        heading: {
            text: 'Light Grey Hoody Sweatshirt',
            tailwind: 'text-base font-medium text-colorGray30',
            css: ''
        },



        price: {
            tailwind: 'flex items-center mt-3',
            css: '',

            icon: {
                currency: RupeeSVGIcon,
                tailwind: 'w-2 h-3 mr-1',
                css: ''
            },

            cost: {
                cost: 5000,
                tailwind: 'text-base font-medium text-colorBlack5',
                css: ''
            }
        },

        color: {
            value: 'Grey',
            tailwind: 'text-normal font-normal text-colorGray30 mt-2.5',
            css: ''
        },

        size: {
            value: 'Medium',
            tailwind: 'text-normal font-normal text-colorGray30 mt-2.5',
            css: ''
        },

        quantity: {
            value: 2,
            tailwind: 'text-normal font-normal text-colorGray30 mt-2.5',
            css: ''
        },

        buttons: {
            tailwind: 'flex items-center mt-1',
            css: '',

            minus: {
                svg: MinusSVGIcon,
                tailwind: 'w-5 h-5 bg-colorGray88 flex items-center justify-center rounded-full',
                css: ''
            },

            quantityInBetweenbuttons: {
                tailwind: 'ml-1.5 mr-1.5',
                css: ''
            },

            plus: {
                svg: PlusSVGIconBlack,
                tailwind: 'w-5 h-5 bg-hoverYellow flex items-center justify-center rounded-full',
                css: ''
            }
        }
    },

    handleInitialTotal: () => {
        // check OrderSummery index.js for defination
    },
    
    handleSubTotalOnQuantityChange: () => {
        // check OrderSummery index.js for defination
    }
}
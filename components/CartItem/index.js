import {useState} from 'react';
import { ReactSVG } from 'react-svg';
import Heading from '../Typography/Heading';
import Para from '../Typography/Para';

const CartItem = (props) => {

    // Data coming from parent 
    const { 
        tailwind, css, image, textContainer, 
        handleInitialTotal,
        handleSubTotalOnQuantityChange,
    } = props;

    // Destructuring data
    const { heading, price, color, size, quantity, buttons } = textContainer;
    const { minus, quantityInBetweenbuttons, plus } = buttons;

    // Change quantity of item
    const [changedQuantity, setChangedQuantity] = useState(quantity.value); 

    
    // Holds initial price sum of all quantity of this product before increase or decrease quantity
    const amountSumInitialValue = price.cost.cost * quantity.value;
    
    // Holds price sum of all quantity of this product after increase and decrease quantity
    const [amountSum, setAmountSum] = useState(amountSumInitialValue);

    // To increase the product quantity
    const incrementQuantity = () => {
        setChangedQuantity(changedQuantity + 1);
        setAmountSum(amountSum + price.cost.cost);
        handleAmountOnQuantityChange(+price.cost.cost);
    }

    // To decrease product quantity
    const decrementQuantity = () => {
        if(changedQuantity > 1){
            setChangedQuantity(changedQuantity -1);
            setAmountSum(amountSum - price.cost.cost);
            handleAmountOnQuantityChange(-price.cost.cost);
        }
    }

    // Passing initial price sum to parent
    const InitialTotal = () => {
       return handleInitialTotal(amountSumInitialValue);
    }
    InitialTotal();


    const handleAmountOnQuantityChange = (updateValueby) => {
        handleSubTotalOnQuantityChange(updateValueby);
    }

    

    return (
        <div className={`${tailwind} ${css}`}>
            <div className={`${image.imageWrapperTailwind} ${image.imageWrapperCss}`}>
                <img src={image.imageSource} alt={image.altTag}
                    className={`${image.imageTailwind} ${image.imageCss}`}
                />
            </div>

            <div className={`${textContainer.tailwind} ${css}`}>
                <Heading text={heading.text} tailwind={heading.tailwind} css={heading.css} />

                <div className={`${price.tailwind} ${price.css}`}>
                    <ReactSVG src={price.icon.currency}
                        className={`${price.icon.tailwind} ${price.icon.css}`}
                    />
                    <span className={`${price.cost.tailwind} ${price.cost.css}`}>{price.cost.cost}</span>
                </div>

                <Para text={`Color: ${color.value}`} tailwind={color.tailwind} css={color.css} />
                <Para text={`Size: ${size.value}`} tailwind={size.tailwind} css={size.css} />
                <Para text={`Quantity: ${changedQuantity}`} tailwind={quantity.tailwind} css={quantity.css} />

                <div className={`${buttons.tailwind} ${buttons.css}`}>
                    <button className={minus.tailwind}
                        onClick={()=>decrementQuantity()}
                    >
                        <ReactSVG src={minus.svg} />
                    </button>

                    <Para
                        text={changedQuantity}
                        tailwind={quantityInBetweenbuttons.tailwind}
                        css={quantityInBetweenbuttons.css}
                    />

                    <button className={plus.tailwind} 
                        onClick={()=>incrementQuantity()}
                    >
                        <ReactSVG src={plus.svg} />
                    </button>
                </div>
            </div>
        </div>
    );
};

export default CartItem;
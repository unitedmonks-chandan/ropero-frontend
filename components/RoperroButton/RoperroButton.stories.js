import RoperroButton from './index';

export default {
    title: 'Roperro/Button'
}

const Template = args => <RoperroButton {...args} />;

export const ShopNowBlack = Template.bind({});

ShopNowBlack.args = {
    buttonText: 'subscribe',
    tailwind: "w-40 h-12 font-medium"
}


export const ShopNowGolden = Template.bind({});

ShopNowGolden.args = {
    buttonText: 'shop now',
    tailwind: 'w-64 h-16 font-medium',
    css: '' 
}
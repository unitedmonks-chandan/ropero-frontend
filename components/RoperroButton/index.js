

const RoperroButton = (props) => {
    const { buttonText, tailwind, css } = props;

    return (
        <button
            className={`${tailwind} ${css}`}
        >
            {buttonText}
        </button >
    );
};

export default RoperroButton;
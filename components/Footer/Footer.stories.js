import Footer from './index';

// importing logo
import LogoFooterSVGIcon from "../../public/assets/svg/roperro-white-logo.svg";
import LogoTextFooterSVGIcon from "../../public/assets/svg/roperro-white-text-logo.svg";


// importing advantage icons
import DeliveryVanSVGIcon from '../../public/assets/svg/delivery-van-svg-icon.svg';
import GiftWrapSVGIcon from '../../public/assets/svg/gift-wrap-svg-icon.svg';
import FreeReturnSVGIcon from '../../public/assets/svg/free-return-svg-icon.svg';

// large icons  
import QualitySVGIcon from '../../public/assets/svg/quality-svg-icon.svg';
import EasyReturnSVGIcon from '../../public/assets/svg/easy-return-svg-icon.svg';



// importing social media icons
import FacebookSVGIcon from '../../public/assets/svg/facebook-svg-icon.svg';
import InstagramSVGIcon from '../../public/assets/svg/instagram-svg-icon.svg';
import TwitterSVGIcon from '../../public/assets/svg/twitter-svg-icon.svg';

export default {
    title: 'Ropero/ Footer'
}

const Template = args => <Footer {...args} />;

export const RoperoFooter = Template.bind({});

RoperoFooter.args = {

    props: {

        logo: {
    
            iconLogo: {
                logoFooter: LogoFooterSVGIcon,
            },
            textLogo: {
                svg: LogoTextFooterSVGIcon,
                customCss: 'ropero-text-logo-footer'
            }
    
        },
    
    
        links: {
            houseOfRoperro: {
                headingText: 'house of roperro',            
                linksArray: [
                    {linkText: 'Career', address: "/"},
                    {linkText: ' Contact', address: "/"},
                ]
            },
            usefulLinks: {
                headingText: 'useful links',            
                linksArray: [
                    {linkText: 'FAQ', address: "/"},
                    {linkText: 'Shipping', address: "/"},
                    {linkText: 'Return Payment', address: "/"},
                    {linkText: 'Refund Cancellation', address: "/"},
                    {linkText: 'Terms & Conditions', address: "/"}
                ]
            }
        },
    
        advantage: {
            headingText: 'roperro advantages',
            advantageArray: [
                {advantageText: 'Free Delivery', svg: DeliveryVanSVGIcon},
                {advantageText: 'Free Returns', svg: FreeReturnSVGIcon},
                {advantageText: 'Free Gift Wrapping', svg: GiftWrapSVGIcon}
            ]
        },
    
        email: {
            emailLabelText: 'subscribe to our newsletter',
            emailInputPlaceholder: 'Enter Your Email',
            buttonText: 'subscribe',
            buttonTailwind: 'w-40 h-12 text-lg font-medium text-colorBlack0 bg-hoverYellow uppercase mt-4', 
        },
    
        largeIcons: {
            qualityProductIcon: QualitySVGIcon,
            easyReturnIcon: EasyReturnSVGIcon
        },
    
        copyrightText: '&copy; 2022 All Rights Reserved ROPERO',
    
        SocialMediaIcons: [
            {icon: FacebookSVGIcon, iconCssClass: 'facebook-icon'},
            {icon: InstagramSVGIcon, iconCssClass: 'instagram-icon'},
            {icon: TwitterSVGIcon, iconCssClass: 'twitter-icon'}
        ]
    }

}
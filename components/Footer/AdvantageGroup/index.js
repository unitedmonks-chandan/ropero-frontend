import Advantage from "../../Typography/Advantage"
import FooterLinkHeading from "../../Typography/FooterLinkHeading";


const AdvantageGroup = (props) => {

    // data coming from parents
    const {headingText, advantageArray} = props;

    // It renders list of advantages i.e. list of icon with text line
    const renderAdvantage = () => {
        return advantageArray.map((item, i) => <Advantage key={i} svg={item.svg} 
            text={item.advantageText}
        />)
    }

    // Entry point renders heading and renderAdvantage which returns list of icon with text
    return (
        <div className="advantage-group">
            <FooterLinkHeading headingText={headingText} />
            {renderAdvantage()}
        </div>
    );
};

export default AdvantageGroup;


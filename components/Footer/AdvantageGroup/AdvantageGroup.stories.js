import AdvantageGroup from "./index";
import DeliveryVanSVGIcon from '../../../public/assets/svg/delivery-van-svg-icon.svg';
import GiftWrapSVGIcon from '../../../public/assets/svg/gift-wrap-svg-icon.svg';
import FreeReturnSVGIcon from '../../../public/assets/svg/free-return-svg-icon.svg';

export default {
    title: "Ropero/Footer Link Group"
}

const Template = args => <AdvantageGroup {...args} />

export const UsefulLinks = Template.bind({});

UsefulLinks.args = {
    headingText: 'roperro advantages',
    advantageArray: [
        {advantageText: 'Free Delivery', svg: DeliveryVanSVGIcon},
        {advantageText: 'Free Returns', svg: FreeReturnSVGIcon},
        {advantageText: 'Free Gift Wrapping', svg: GiftWrapSVGIcon}
    ]
}
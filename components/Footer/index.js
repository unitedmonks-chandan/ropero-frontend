import LogoFooter from '../Typography/LogoFooter';

import RoperoTextLogo from '../Typography/RoperoTextLogo';
import EmailLabel from '../Typography/EmailLabel';
import EmailInput from '../Typography/EmailInput';
import RoperroButton from '../RoperroButton';

import GenuineProductIcon from '../Typography/GenuineProductIcon';
import EasyReturnIcon from '../Typography/EasyReturnIcon';
import LegalText from '../Typography/LegalText';
import SocialMediaIconsGroup from './SocialMediaIconsGroup';


import FooterLinkGroup from './FooterLinkGroup';
import AdvantageGroup from './AdvantageGroup';


const Footer = (props) => {

    const { links, advantage, email, copyrightText, SocialMediaIcons, logo, largeIcons } = props.props;
    const { houseOfRoperro, usefulLinks } = links;

    return (
        <div className="footer">
            <div className="footer__content">
                <div className="footer__content__logo">

                    <LogoFooter logoFooter={logo.iconLogo.logoFooter} />
                    <RoperoTextLogo
                        svg={logo.textLogo.svg}
                        customCss={logo.textLogo.customCss}
                    />

                </div>
                <div className="footer__content__link-and-email-box">
                    <div className="footer__links__container">
                        <FooterLinkGroup
                            headingText={houseOfRoperro.headingText}
                            linksArray={houseOfRoperro.linksArray}
                        />
                        <FooterLinkGroup
                            headingText={usefulLinks.headingText}
                            linksArray={usefulLinks.linksArray}
                        />
                        <AdvantageGroup
                            headingText={advantage.headingText}
                            advantageArray={advantage.advantageArray}
                        />
                    </div>
                    <div className="footer__email__container">
                        <div className="footer__email">
                            <EmailLabel emailLabelText={email.emailLabelText} />
                            <EmailInput emailInputPlaceholder={email.emailInputPlaceholder} />
                            <RoperroButton buttonText={email.buttonText} 
                                tailwind={email.buttonTailwind}
                            />
                        </div>
                        <GenuineProductIcon qualityProductIcon={largeIcons.qualityProductIcon} />
                        <EasyReturnIcon easyReturnIcon={largeIcons.easyReturnIcon} />
                    </div>
                </div>
            </div>
            <hr />
            <div className="footer__legal-text__container">
                <LegalText copyrightText={copyrightText} />
                <SocialMediaIconsGroup SocialMediaIcons={SocialMediaIcons} />
            </div>
        </div>
    );
};

export default Footer;


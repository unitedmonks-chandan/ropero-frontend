import FooterLinkGroup from "./index";

export default {
    title: "Ropero/Footer Link Group"
}

const Template = args => <FooterLinkGroup {...args} />

export const UsefulLinks = Template.bind({});

UsefulLinks.args = {
    headingText: 'useful links',
    linksArray: [
        {linkText: 'FAQ', address: "/"},
        {linkText: 'Shipping', address: "/"},
        {linkText: 'Return Payment', address: "/"},
        {linkText: 'Refund Cancellation', address: "/"},
        {linkText: 'Terms & Conditions', address: "/"}
    ]
}

import FooterLinkHeading from '../../Typography/FooterLinkHeading';
import FooterLink from '../../Typography/FooterLink';

const FooterLinkGroup = (props) => {
    const { headingText, linksArray } = props;

    const renderLinks = () => {
        return linksArray?.map((item, i) => <FooterLink linkText={item.linkText} address={item.address} key={i} />)
    }

    return (
        <div className="footer-link-group">
            <FooterLinkHeading headingText={headingText} />
            {renderLinks()}
        </div>
    );
};

export default FooterLinkGroup;
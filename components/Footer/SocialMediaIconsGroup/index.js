import {ReactSVG} from 'react-svg';

const SocialMediaIconsGroup = (props) => {

    // data coming from parents
    const {SocialMediaIcons} = props;

    // It renders list/row of icons
    const renderIcons = () => {
        return SocialMediaIcons.map((item, i) => <ReactSVG src={item.icon} 
        className={item.iconCssClass} 
        key={i} />)
    }

    // Entry point call renderIcons  
    return (
        <div className="social-media-icons-group">
            {renderIcons()}
        </div>
    );
};

export default SocialMediaIconsGroup;

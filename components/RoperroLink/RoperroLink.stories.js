import RoperroLink from './index';

export default {
    title: 'Roperro/Link'
}

const Template = args => <RoperroLink {...args} />;

export const ShopNow = Template.bind({});

ShopNow.args = {
    text: 'shop now',
    linkHref: '/',
    tailwind: 'font-medium text-2xl uppercase text-colorBlack0',
    css: '',
    backgroundTailwind: 'flex items-center justify-center w-64 h-16 bg-hoverYellow'
}

export const BackToShopping = Template.bind({});

BackToShopping.args = {
    text: 'back to shopping',
    linkHref: '/',
    tailwind: 'text-2xl uppercase  text-hoverYellow',
    css: '',
    backgroundTailwind: 'w-80 h-16 bg-white border-2 border-solid border-hoverYellow flex items-center justify-center'
}

export const MakePayment = Template.bind({});

MakePayment.args = {
    text: 'make payment',
    linkHref: '/',
    tailwind: 'text-2xl uppercase text-white ',
    css: '',
    backgroundTailwind: 'w-80 h-16 bg-hoverYellow border-2 border-solid border-hoverYellow flex items-center justify-center'
}
import Link from 'next/link';

const RoperroLink = (props) => {

    const { text, backgroundTailwind, linkHref, tailwind, css } = props;

    return (
        <div className={backgroundTailwind}>
            <Link
                href={linkHref}
                >
                <a className={`${tailwind} ${css}`}>
                    {text}
                </a>
            </Link >
        </div>
    );
};

export default RoperroLink;
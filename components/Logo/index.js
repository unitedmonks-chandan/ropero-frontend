import React from 'react';

const Logo = (props) => {
    const { size = 'logo__large' } = props;
    return (
        <>
            <div className={`logo ${size}`}>TANN TRIM</div>
            {/* <div className="logo logo__small" {...args}>TANN TRIM</div> */}
        </>
    );
};

export default Logo;
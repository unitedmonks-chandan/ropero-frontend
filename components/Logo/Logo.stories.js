import Logo from './index';

export default {
    title: 'Logo'
}

const Template = (args) => <Logo {...args} />

export const LogoLarge = Template.bind({});
LogoLarge.args = {
    size: 'logo__large'
};

export const LogoSmall = Template.bind({});
LogoSmall.args = {
    size: 'logo__small'
}
import OurCollection from "../../OurCollection/OurCollection";
import womenImg from "../../../public/assets/images/arkar/ourCollectionWomen.jpg";
import menImg from "../../../public/assets/images/arkar/ourCollectionMen.jpg";
import lifestyleImg from "../../../public/assets/images/arkar/ourCollectionLifestyle.jpg";
import jacketsImg from "../../../public/assets/images/arkar/ourCollectionJackets.jpg";

export default {
  title: "Roperro / Arkar / Our Collection",
};

const Template = (args) => <OurCollection {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  stripContainer: "font-primary text-center py-10 arkar-strip-bg bg-cover",
  stripText1: "Our Collection",
  stripText1Style: "text-white text-6xl mb-6",
  stripText2:
    "The online store presents selected successful collections, fashionable ",
  stripText2Style: "text-lightGray text-lg text-center",
  stripText3:
    "women’s clothing which is sewn exclusively from quality material.",
  stripText3Style: "text-lightGray text-lg text-center",
  display: "grid grid-cols-1 lg:grid-cols-2",
  shopNowCompsData: [
    {
      bgImg: womenImg,
      colSpan: "1",
      altTag: "Woman in blue jacket",
      btnData: {
        container: "",
        position: "absolute left-16 bottom-16",
        headingStyle: "text-4xl text-white uppercase mb-3",
        heading1: "For",
        heading2: "Women",
        btnContainer: "",
        btnLink: "/arkar/women",
        btn: "bg-white text-black px-6 py-1 uppercase",
        size: "",
        btnText: "Shop now",
      },
    },
    {
      bgImg: menImg,
      altTag: "Man in brown jacket",
      colSpan: "1",
      btnData: {
        container: "",
        position: "absolute left-16 bottom-16",
        headingStyle: "text-4xl text-white uppercase mb-3",
        heading1: "For",
        heading2: "Men",
        btnContainer: "",
        btnLink: "/arkar/men",
        btn: "bg-white text-black px-6 py-1 uppercase",
        size: "",
        btnText: "Shop now",
      },
    },
    {
      bgImg: lifestyleImg,
      altTag: "Woman in black coat", 
      colSpan: "1",
      btnData: {
        container: "",
        position: "absolute left-16 bottom-16",
        headingStyle: "text-4xl text-white uppercase mb-3",
        heading1: "Best in women",
        heading2: "lifeStyle",
        btnContainer: "",
        btnLink: "/arkar/lifestyle",
        btn: "bg-white text-black px-6 py-1 uppercase",
        size: "",
        btnText: "Shop now",
      },
    },
    {
      bgImg: jacketsImg,
      altTag: "Man in jacket",
      colSpan: "1",
      btnData: {
        container: "",
        position: "absolute left-16 bottom-16",
        headingStyle: "text-4xl text-white uppercase mb-3",
        heading1: "Custom varsity",
        heading2: "Jackets",
        btnContainer: "",
        btnLink: "/arkar/jackets",
        btn: "bg-white text-black px-6 py-1 uppercase",
        size: "",
        btnText: "Shop now",
      },
    },
  ],
};

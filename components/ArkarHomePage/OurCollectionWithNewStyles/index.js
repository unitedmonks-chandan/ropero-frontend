import OurCollection from "../../OurCollection/OurCollection";
import NewStyles from "../NewStylesy/NewStylesy";

const OurCollectionWithNewStyles = (props) => {

    const { containerStyle, ourCollection, newStylesy } = props;

    return (
        <div className={containerStyle}>
            <OurCollection
                stripContainer={ourCollection.stripContainer}
                stripText1={ourCollection.stripText1}
                stripText2={ourCollection.stripText2}
                stripText3={ourCollection.stripText3}
                stripText1Style={ourCollection.stripText1Style}
                stripText2Style={ourCollection.stripText2Style}
                stripText3Style={ourCollection.stripText3Style}
                display={ourCollection.display}
                shopNowCompsData={ourCollection.shopNowCompsData}
            />
            <NewStyles
                container={newStylesy.container}
                textData={newStylesy.textData}
                display={newStylesy.display}
                btnData={newStylesy.btnData}
                carouselData={newStylesy.carouselData}
            />
        </div>
    )
};

export default OurCollectionWithNewStyles;
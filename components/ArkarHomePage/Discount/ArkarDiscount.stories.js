import ArkarDiscount from "./ArkarDiscount";
import arkarDiscountImg from "../../../public/assets/images/arkar/discount.jpg";
import arkarLogo from "../../../public/assets/images/arkar/arkar-logo.svg";

export default {
  title: "Roperro / Arkar / Discount",
};

const Template = (args) => <ArkarDiscount {...args} />;

export const Design1 = Template.bind({});
Design1.args = {
  image: arkarDiscountImg,
  container: "bg-midRed text-white font-primary px-16 pt-8 pb-10",
  containerPosition: "absolute left-16 heading-position",
  logo: arkarLogo,
  logoSize: "w-24 mb-7",
  heading1: "HOUSE OF ROPERRO",
  heading1Style: "text-lg mb-1",
  heading2: "Leather",
  heading2Style: "text-4xl",
  heading3: "Collection",
  heading3Style: "text-5xl mb-5",
  heading4: "50% off",
  heading4Style: "text-5xl",
};

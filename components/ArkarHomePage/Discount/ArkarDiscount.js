import React from "react";
import MultipleHeading from "../../MultipleHeading/MultipleHeading";

function ArkarDiscount({
  image,
  container,
  containerPosition,
  logo,
  logoSize,
  heading1,
  heading1Style,
  heading2,
  heading2Style,
  heading3,
  heading3Style,
  heading4,
  heading4Style,
}) {
  return (
    <div className="relative">
      <img src={image} alt="" />
      <MultipleHeading
        container={container}
        containerPosition={containerPosition}
        logo={logo}
        logoSize={logoSize}
        heading1={heading1}
        heading1Style={heading1Style}
        heading2={heading2}
        heading2Style={heading2Style}
        heading3={heading3}
        heading3Style={heading3Style}
        heading4={heading4}
        heading4Style={heading4Style}
      />
    </div>
  );
}

export default ArkarDiscount;

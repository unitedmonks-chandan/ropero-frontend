import React from "react";


function BestSeller({
  display,
  img1,
  img2,
  img3,
  img4,
  img5,
  colSpan,
  mainHeadingStyle,
  text,
  textStyle,
}) {
  const { mainHeading, textData } = text;

  return (
    <div className={display}>
      <img src={img1.imageSource} alt={img1.altTag} />
      <img src={img2.imageSource} alt={img2.altTag} />
      <div className={textStyle}>
        <p className={mainHeadingStyle}>{mainHeading}</p>
        {textData.map((text, i) => (
          <p key={i}>{text}</p>
        ))}
      </div>
      <img src={img3.imageSource} alt={img3.altTag} className={colSpan} />
      <img src={img4.imageSource} alt={img4.altTag} />
      <img src={img5.imageSource} alt={img5.altTag} />
    </div>
  );
}

export default BestSeller;

import BestSeller from "./BestSeller";
import img1 from "../../../public/assets/images/arkar/best-seller1.jpg";
import img2 from "../../../public/assets/images/arkar/best-seller2.jpg";
import img3 from "../../../public/assets/images/arkar/best-seller3.jpg";
import img4 from "../../../public/assets/images/arkar/best-seller4.jpg";
import img5 from "../../../public/assets/images/arkar/best-seller5.jpg";

export default {
  title: "Roperro / Arkar / Best Seller",
};

const Template = (args) => <BestSeller {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  display: "bg-black pt-12 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 md:gap-x-4 md:gap-y-3 place-items-center",
  img1,
  img2,
  img3,
  img4,
  img5,
  colSpan: "md:col-span-2 md:self-stretch",
  mainHeadingStyle: "text-white text-3xl font-primary uppercase mb-2",
  textStyle: "md:col-span-2 text-white text-lg px-36 py-32",
  text: {
    mainHeading: "Best Seller",
    textData: [
      "The online store presents selected",
      "successful collections, fashionable",
      "women’s clothing which is sewn",
      "exclusively from quality material.",
    ],
  },
};

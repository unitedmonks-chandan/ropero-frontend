import NewStylesy from "./NewStylesy";
import img1 from "../../../public/assets/images/arkar/new-stylesy1.jpg";
import img2 from "../../../public/assets/images/arkar/new-stylesy2.jpg";
import img3 from "../../../public/assets/images/arkar/new-stylesy3.jpg";

export default {
  title: "Roperro / Arkar / New Stylesy",
};

const Template = (args) => <NewStylesy {...args} />;

export const design1 = Template.bind({});

design1.args = {
  container: "py-20 brownImgBg bg-center",
  textData: {
    display: "flex gap-16 items-center pl-24 mb-16",
    headings: ["New", "Stylesy"],
    headingStyle: "text-midRed text-6xl uppercase",
    paras: [
      "The online store presents selected successful",
      "collections, fashionable women’s clothing",
      "which is sewn exclusively from quality material. ",
    ],
    paraStyle: "text-white text-lg",
  },
  btnData: {
    btnLink: "/arkar/view-all",
    btn: "bg-midRed text-white text-lg uppercase px-7 self-end justify-self-center",
    text: "View All",
    size: "",
  },
  display: "grid grid-cols-4",
  carouselData: {
    customStyle: "single-arrow-slider pl-20 relative w-full col-span-3",
    images: [
      {
        img: img1,
        altTag: 'model'
      
      },
      {
        img: img2,
        altTag: 'model'
      },
      {
        img: img3,  
        altTag: 'model'
      },
      {
        img: img2, 
        altTag: 'model' 
      },
      {
        img: img3, 
        altTag: 'model' 
      },
      {
        img: img1,
        altTag: 'model'
      }
    ]
  },
};

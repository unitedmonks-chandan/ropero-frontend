import React from "react";
import MultipleHeadingWithMultiplePara from "../../MultipleHeadingWithMultiplePara/MultipleHeadingWithMultiplePara";
import LinkButton from "../../NewButtons/LinkButton/LinkButton";
import NewCarousel from "../../NewCarousel/NewCarousel";

function NewStyles({ container, textData, display, btnData, carouselData }) {
  return (
    <div className={container}>
      <MultipleHeadingWithMultiplePara {...textData} />
      <div className={display}>
        <LinkButton {...btnData} />
        <NewCarousel {...carouselData} />
      </div>
    </div>
  );
}

export default NewStyles;

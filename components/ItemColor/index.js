import SelectColor from '../SelectColor/SelectColor.js';


const AvailableColors = ({colors, showMore}) => {
    let render = "none";
    if(showMore) {
        render = "block"
    }

    return (
        <div className="item-colors">
            {colors.map((color, i) => <SelectColor hexCode={color} key={i} />)}
            <span style={{ marginLeft: "8px", display: render }}>& more</span>
        </div>
    );
};

export default AvailableColors;

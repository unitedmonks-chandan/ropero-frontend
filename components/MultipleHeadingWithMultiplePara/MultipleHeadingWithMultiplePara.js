import React from "react";

function MultipleHeadingWithMultiplePara({
  display,
  headings,
  headingStyle,
  paras,
  paraStyle,
}) {
  return (
    <div className={display}>
      <div className={headingStyle}>
        {headings &&
          headings.map((heading, index) => <p key={index}>{heading}</p>)}
      </div>
      <div className={paraStyle}>
        {paras && paras.map((para, index) => <p key={index}>{para}</p>)}
      </div>
    </div>
  );
}

export default MultipleHeadingWithMultiplePara;

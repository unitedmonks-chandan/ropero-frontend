import MultipleHeadingWithMultiplePara from "./MultipleHeadingWithMultiplePara";

export default {
  title: "Roperro / Multiple Headings with Paras",
};

const Template = (args) => <MultipleHeadingWithMultiplePara {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  display: "flex gap-12 items-center",
  headingStyle: "text-midRed text-5xl uppercase",
  headings: ["New", "Stylesy"],
  paras: [
    "The online store presents selected successful",
    "collections, fashionable women’s clothing",
    "which is sewn exclusively from quality material. ",
  ],
};

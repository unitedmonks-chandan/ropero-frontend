import HeroNewCarousel from "../../HeroNewCarousel/HeroNewCarousel";
import hero1 from "../../../public/assets/images/zimba/hero1.jpg";
import hero2 from "../../../public/assets/images/monadaa-comp.png";
import logo1 from "../../../public/assets/svg/zimba/logo.svg";

export default {
  title: "Roperro / Zimba / Hero",
};

const Template = (args) => <HeroNewCarousel {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  customStyle: "single-right-arrow-slider zimba-hero-slider relative h-screen",
  carouselData: [
    {
      image: hero1,
      btnData: {
        position:
          "absolute zimba-hero-btn-position z-50 flex flex-col gap-8 items-center justify-center",
        logo: logo1,
        logoSize: "slider-logo-size",
        btnLink: "/zimba",
        btn: "bg-white text-black text-lg font-primary px-7 py-1 uppercase",
        btnText: "Shop now",
      },
    },
    {
      image: hero2,
      btnData: {
        position:
          "absolute zimba-hero-btn-position z-50 flex flex-col gap-8 items-center justify-center",
        logo: logo1,
        logoSize: "slider-logo-size",
        btnLink: "/zimba/view-all",
        btn: "bg-midRed text-white text-lg font-primary px-7 py-1 uppercase",
        btnText: "View all",
      },
    },
  ],
};

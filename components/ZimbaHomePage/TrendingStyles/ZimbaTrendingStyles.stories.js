import HeroNewCarousel from "../../HeroNewCarousel/HeroNewCarousel";
import img1 from "../../../public/assets/images/zimba/trending-styles.jpg";
// import bgLogo from '../../../public/assets/svg/zimba/logo2.svg';
import logo1 from "../../../public/assets/svg/zimba/trending-styles.svg";

export default {
  title: "Roperro / Zimba / Trending Styles",
};

const Template = (args) => <HeroNewCarousel {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  customStyle:
    "single-right-arrow-slider zimba-hero-slider zimba-trending-styles-slider relative bg-black pr-24 py-32",
  carouselData: [
    {
      image: img1,
      altTag: 'Woman with Grey bag',
      btnData: {
        position:
          "flex flex-col gap-8 items-start justify-center w-96  pl-24 2xl:p-0 mr-32",
        logo: logo1,
        logoSize: "slider-logo-size",
        btnLink: "/zimba/view-all",
        btn: "bg-white text-black text-lg font-primary px-7 py-1 uppercase",
        btnText: "View all",
      },
    },
    {
      image: img1,
      altTag: 'Woman with Grey bag',
      btnData: {
        position:
          "flex flex-col gap-8 items-start justify-center w-96  pl-24 2xl:p-0 mr-32",
        logo: logo1,
        logoSize: "slider-logo-size",
        btnLink: "/zimba",
        btn: "bg-midRed text-white text-lg font-primary px-7 py-1 uppercase",
        btnText: "Shop now",
      },
    },
  ],
};

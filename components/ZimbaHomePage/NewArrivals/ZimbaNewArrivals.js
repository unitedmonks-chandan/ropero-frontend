import React from "react";
import NewCarousel from "../../NewCarousel/NewCarousel";
import LinkButtonWithHeading from "../../NewButtons/LinkButtonWithHeading/LinkButtonWithHeading";
// import "./bg-style.css";

function ZimbaNewArrivals({
  topBg,
  midBg,
  bottomBg,
  heading,
  headingStyle,
  images,
  customStyle,
  image,
  imagePosition,
  btnData,
}) {
  return (
    <div>
      <div className={topBg}>
        <p className={headingStyle}>{heading}</p>
      </div>
      <div className={midBg}>
        <NewCarousel images={images} customStyle={customStyle} />
      </div>
      <div className={bottomBg}>
        <img src={image} className={imagePosition} alt="" />
        <LinkButtonWithHeading {...btnData} />
      </div>
    </div>
  );
}

export default ZimbaNewArrivals;

import ZimbaNewArrivals from "./ZimbaNewArrivals";
import img1 from "../../../public/assets/images/zimba/new-arrivals1.jpg";
import img2 from "../../../public/assets/images/zimba/new-arrivals2.jpg";
import img3 from "../../../public/assets/images/zimba/new-arrivals3.jpg";
// import img4 from "../../../public/assets/images/zimba/summer-collection.jpg";
import img4 from "../../../public/assets/images/zimba/summer-collection-removebg.png";

export default {
  title: "Roperro / Zimba / New Arrivals",
};

const Template = (args) => <ZimbaNewArrivals {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  topBg: "top-bg",
  heading: "New Arrivals",
  headingStyle:
    "font-cinzel text-7xl text-white pt-20 text-center uppercase mb-96",
  midBg: "bg-black",
  images: [
    {img: img1, altTag: 'Woman in black leather coat'},
    {img: img2, altTag: 'Grey black purse'},
    {img: img3, altTag: 'Man in brown jacket'},
    {img: img2, altTag: 'Grey black purse'},
    {img: img3, altTag: 'Man in brown jacket'},
    {img: img1, altTag: 'Woman in black leather coat'}
  ],
  customStyle:
    "relative pl-52 pr-56 hide-inbuilt-arrow left-white-arrow right-white-arrow reset-swiper-container zimba-new-arrivals-slider hide-pagination-bullets",
  bottomBg: "bottom-bg grid place-items-center pt-2 pb-52 relative",
  image: img4,
  imagePosition: "absolute bottom-0 left-24",
  btnData: {
    container: "",
    position: "relative top-20",
    heading1: "HOUSE OF ZIMBA",
    heading1Style: "text-white text-xl font-primary uppercase mb-2",
    heading2: "Summer",
    heading2Style: "font-cinzel text-5xl text-white uppercase",
    heading3: "Collection",
    heading3Style: "font-cinzel text-5xl text-white uppercase mb-8",
    btnLink: "/shop-now",
    btn: "inline-block bg-lightOrange text-black text-lg font-primary px-7 py-1 uppercase",
    size: "",
    btnText: "Shop Now",
  },
};

import OurCollection from "../../OurCollection/OurCollection";
import womenImg from "../../../public/assets/images/zimba/our-collection1.jpg";
import menImg from "../../../public/assets/images/zimba/our-collection2.jpg";

export default {
  title: "Roperro / Zimba / Our Collection",
};

const Template = (args) => <OurCollection {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  stripContainer: "font-primary text-center py-10 zimba-strip-bg bg-cover",
  stripText1: "Our Collection",
  stripText1Style: "text-white text-6xl mb-6",
  stripText2:
    "The online store presents selected successful collections, fashionable ",
  stripText2Style: "text-lightGray text-lg text-center",
  stripText3:
    "women’s clothing which is sewn exclusively from quality material.",
  stripText3Style: "text-lightGray text-lg text-center",
  display: "grid grid-cols-1 lg:grid-cols-2",
  shopNowCompsData: [
    {
      bgImg: womenImg,
      altTag: "Woman in brown coat",
      colSpan: "1",
      btnData: {
        container: "",
        position:
          "zimba-our-collection-btn bottom-20 flex flex-col justify-center items-center",
        headingStyle: "text-5xl text-white uppercase mb-12",
        heading1: "Women",
        btnContainer: "",
        btnLink: "/zimba/women",
        btn: "bg-white text-black px-6 py-1 uppercase",
        size: "",
        btnText: "Shop now",
      },
    },
    {
      bgImg: menImg,
      altTag: "Man in black leather jacket",
      colSpan: "1",
      btnData: {
        container: "",
        position:
          "zimba-our-collection-btn bottom-20 flex flex-col justify-center items-center",
        headingStyle: "text-5xl text-white uppercase mb-12",
        heading1: "Men",
        btnContainer: "",
        btnLink: "/zimba/men",
        btn: "bg-white text-black px-6 py-1 uppercase",
        size: "",
        btnText: "Shop now",
      },
    },
  ],
};

import React from "react";

import ImageBtnIconsCarousel from "../../ImageBtnIconsCarousel/ImageBtnIconsCarousel";
import MultipleHeading from "../../MultipleHeading/MultipleHeading";

function ZimbaBestSellers({ textData, carouselData }) {
  return (
    <div className="bg-black py-20">
      <MultipleHeading {...textData} />
      <ImageBtnIconsCarousel {...carouselData} />
    </div>
  );
}

export default ZimbaBestSellers;

import ZimbaBestSellers from "./ZimbaBestSellers";
import img1 from "../../../public/assets/images/zimba/best-sellers1.jpg";
import img2 from "../../../public/assets/images/zimba/best-sellers2.jpg";
import bagSvg from "../../../public/assets/svg/bag.svg";

import heartSvg from "../../../public/assets/svg/red-heart-fill.svg";


export default {
  title: "Roperro / Zimba / Best Sellers",
};

const Template = (args) => <ZimbaBestSellers {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  textData: {
    container: "font-primary text-center pb-12",
    heading1: "best sellers",
    heading1Style: "bg-black text-white text-6xl uppercase mb-4",
    heading2:
      "The online store presents selected successful collections, fashionable",
    heading2Style: "text-gray100 text-xl uppercase",
    heading3:
      "women’s clothing which is sewn exclusively from quality material.",
    heading3Style: "text-gray100 text-xl uppercase mb-8",
  },
  carouselData: {
    customSpaceBetween: 20,
    customStyle:
      " relative zimba-best-sellers-slider hide-arrow white-pagination-bullet golden-pagination-bullet-active reset-swiper-container",
    carouselData: [
      {
        customSlide: "flex",
        btnIcons: "bg-lightCream px-12 grid place-items-center relative -ml-1",
        image: img1,
        altTag: "Woman in White Coat",
        btnData: {
          container: "inline-flex flex-col items-start gap-8",
          position: "",
          headingStyle: "text-midGray font-primary text-left",
          heading1: "LINEN JACKET",
          heading1Style: "text-3xl mb-1",
          heading2: "₹ 5000",
          heading2Style: "text-2xl mb-2",
          heading3:
            "Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.",
          heading3Style: "text-lg",
          btnLink: "/view-all",
          btn: "inline-block bg-black text-white text-lg font-primary px-10 py-1 mt-6",
          size: "",
          btnText: "View",
        },
        icons: [
          {
            icon: heartSvg,
            iconSize: "icon-size",
          },
          {
            icon: bagSvg,
            iconSize: "icon-size",
          },
        ],
        iconsPosition: "flex gap-4 absolute right-20 bottom-24",
      },
      {
        customSlide: "flex",
        btnIcons: "bg-lightCream px-12 grid place-items-center relative -ml-1",
        image: img2,
        btnData: {
          container: "inline-flex flex-col items-start gap-8",
          position: "",
          headingStyle: "text-midGray font-primary text-left",
          heading1: "LINEN JACKET",
          heading1Style: "text-3xl mb-1",
          heading2: "₹ 5000",
          heading2Style: "text-2xl mb-2",
          heading3:
            "Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.",
          heading3Style: "text-lg",
          btnLink: "/view-all",
          btn: "inline-block bg-black text-white text-lg font-primary px-10 py-1 mt-6",
          size: "",
          btnText: "View",
        },
        icons: [
          {
            icon: heartSvg,
            iconSize: "icon-size",
          },
          {
            icon: bagSvg,
            iconSize: "icon-size",
          },
        ],
        iconsPosition: "flex gap-4 absolute right-20 bottom-24",
      },
      {
        customSlide: "flex",
        btnIcons: "bg-lightCream px-12 grid place-items-center relative -ml-1",
        image: img2,
        altTag: "Man in Green Shirt",
        btnData: {
          container: "inline-flex flex-col items-start gap-8",
          position: "",
          headingStyle: "text-midRed font-primary text-left",
          heading1: "LINEN JACKET",
          heading1Style: "text-3xl mb-1",
          heading2: "₹ 5000",
          heading2Style: "text-2xl mb-2",
          heading3:
            "Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.",
          heading3Style: "text-lg",
          btnLink: "/view-all",
          btn: "inline-block bg-white text-black text-lg font-primary px-10 py-1 mt-6",
          size: "",
          btnText: "View",
        },
        icons: [
          {
            icon: heartSvg,
            iconSize: "icon-size",
          },
          {
            icon: bagSvg,
            iconSize: "icon-size",
          },
        ],
        iconsPosition: "flex gap-4 absolute right-20 bottom-24",
      },
      {
        customSlide: "flex",
        btnIcons: "bg-lightCream px-12 grid place-items-center relative -ml-1",
        image: img1,
        btnData: {
          container: "inline-flex flex-col items-start gap-8",
          position: "",
          headingStyle: "text-midRed font-primary text-left",
          heading1: "LINEN JACKET",
          heading1Style: "text-3xl mb-1",
          heading2: "₹ 5000",
          heading2Style: "text-2xl mb-2",
          heading3:
            "Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.",
          heading3Style: "text-lg",
          btnLink: "/view-all",
          btn: "inline-block bg-white text-black text-lg font-primary px-10 py-1 mt-6",
          size: "",
          btnText: "View",
        },
        icons: [
          {
            icon: heartSvg,
            iconSize: "icon-size",
          },
          {
            icon: bagSvg,
            iconSize: "icon-size",
          },
        ],
        iconsPosition: "flex gap-4 absolute right-20 bottom-24",
      },
    ],
  },
};

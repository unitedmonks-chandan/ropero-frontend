import Myaccount from "./index";

export default {
    title: "Myaccount/Complate-UI",

}
const container = arguments_ => <Myaccount {...arguments_} />
export const myaccount = container.bind({})

myaccount.args = {
    props: {
        main_container: 'md:mx-24 lg:mx-48 xl:mx-56 my-50 ',
        child_container: 'w-full h-full',
        title: "MY ACCOUNT",
        descriptionText: "Please tell us more about your inquiry using the form below and click to submit your information.",
        ac_container: 'grid grid-cols-2 lg:gap-x-20 2xl:gap-32 md:gap-x-10 md:mx-10 lg:mx-12 xl:mx-24 2xl:mx-40',
        firstname: "First Name*",
        text: "text",
        changename: "Change Name",
        phone: "Phone*",
        number: "number",
        changeNum: "Change Phone",
        password: "Password*",
        passwordType: "password",
        changepassword: "Change Password",
        confirmpassword: "Confirm Password*",
        changepassword2: "Change Password*",
        forggot: "Forgotten Your Password?",
        lastname: "Last Name*",
        lastNamechange: "Change Last Name",
        email: "Email*",
        EmailType: "email",
        changeEmail: "Change Email",
        gender: "Gender*",
        ac_checkbox: 'grid grid-cols-2 gap-x-0 gap-y-0  mx-2',
        male: "Male",
        female: "Female",
        btn_style: 'mt-28 my-10 ',
        btnText: "SUBMIT"
    }
}




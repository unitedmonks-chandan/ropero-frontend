import React from 'react';
import Heading from "../AC_component/Title/index"
import Description from "../AC_component/Description/index"
import CustomInput from "../AC_component/Input/index"
import Forgot from '../AC_component/forgot_text/index';
import Checkbox from "../AC_component/Checkbox/index"
import Button from '../AC_component/Button/index';
import Gender from '../AC_component/Gender/index';


const Myaccount = (props) => {
    const { main_container, child_container, title, descriptionText, ac_container, firstname, text, changename, phone, number, changeNum, password,
        passwordType, changepassword, changepassword2, confirmpassword, forggot, lastname, lastNamechange, email, EmailType, changeEmail, gender, ac_checkbox,
        male, female, btn_style, btnText } = props.props;

    return (
        <>
            <section className={main_container}>
                <div className={child_container}>
                    <div className="sets">
                        <Heading text={title} />

                        <Description description={descriptionText} />
                    </div>

                    <div className={ac_container}>

                        <div>
                            <CustomInput
                                labelText={firstname}
                                TypeAttributes={text}
                                placeholderText={changename} />

                            <CustomInput
                                labelText={phone}
                                TypeAttributes={number}
                                placeholderText={changeNum} />

                            <CustomInput
                                labelText={password}
                                TypeAttributes={passwordType}
                                placeholderText={changepassword} />

                            <CustomInput
                                labelText={confirmpassword}
                                TypeAttributes={passwordType}
                                placeholderText={changepassword2} />

                            <Forgot
                                forgot_text={forggot} />
                        </div>

                        <div>
                            <CustomInput
                                labelText={lastname}
                                TypeAttributes={text}
                                placeholderText={lastNamechange} />

                            <CustomInput
                                labelText={email}
                                TypeAttributes={EmailType}
                                placeholderText={changeEmail} />

                            <Gender headText={gender} />

                            <div className={`${ac_checkbox}`}>

                                <div>
                                    <Checkbox labelText={male} />

                                </div>

                                <div>
                                    <Checkbox labelText={female} />
                                </div>

                            </div>

                            <div className={btn_style}>
                                <Button text_button={btnText} />
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </>
    )
}
export default Myaccount;
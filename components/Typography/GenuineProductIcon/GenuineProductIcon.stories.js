import GenuineProductLogo from "./index";
import QualitySVGIcon from '../../../public/assets/svg/quality-svg-icon.svg';

export default {
    title: 'Ropero/Genuine Product Logo'
}

const Template = (args) => <GenuineProductLogo {...args} /> //creating a template

export const Logo = Template.bind({})

//Passing the props to the component

Logo.args = {
    qualityProductIcon: QualitySVGIcon
}

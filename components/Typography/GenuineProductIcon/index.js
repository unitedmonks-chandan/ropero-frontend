import { ReactSVG } from 'react-svg';

const GenuineProductIcon = ({qualityProductIcon}) => {
    return (
        <ReactSVG src={qualityProductIcon} className="genuine-product-icon" />
    );
};

export default GenuineProductIcon;
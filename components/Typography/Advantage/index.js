import { ReactSVG } from 'react-svg';


const Advantage = ({ svg, text }) => {
    return (
        <div className="advantage">
            <ReactSVG src={svg} />
            <p>{text}</p>
        </div>
    );
};

export default Advantage;
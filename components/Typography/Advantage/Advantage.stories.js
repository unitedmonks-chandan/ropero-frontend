import RoperoAdvantage from './index'; //importing the RoperoAdvantage component

import DeliveryVanSVGIcon from '../../../public/assets/svg/delivery-van-svg-icon.svg';

export default {
    //title for our storybook
    title: "Ropero/Advantage"
}

const Template = (args) => <RoperoAdvantage {...args} /> //creating a template

export const Advantage = Template.bind({})

//Passing the props to the component

Advantage.args = {
    text: 'Free Delivery',
    svg: DeliveryVanSVGIcon
}
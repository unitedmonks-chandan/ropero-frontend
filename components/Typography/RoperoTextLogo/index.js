import Link from 'next/link';
import { ReactSVG } from 'react-svg';


const RoperoTextLogo = ({svg, customCss='', tailwindCss, iconTailwindCss=''}) => {

    return (
        <div className={`${customCss} ${tailwindCss}`}>
            <Link href="/" >
                <a>
                    <ReactSVG src={svg}
                        className={iconTailwindCss}
                    />
                </a>
            </Link>
        </div>
    );
};

export default RoperoTextLogo;



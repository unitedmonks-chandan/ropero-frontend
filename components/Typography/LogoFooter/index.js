import Link from 'next/link';
import { ReactSVG } from 'react-svg';



const LogoFooter = ({ logoFooter }) => {

    return (
        <div className="logo-footer">
            <Link href="/" >
                <a>
                    <ReactSVG src={logoFooter} />
                </a>
            </Link>
        </div>
    );
};

export default LogoFooter;



import RoperoFooterLogo from './index'; //importing the Logo component


export default {
    title: "Ropero/Footer Logo" //title for our storybook
}

const Template = (args) => <RoperoFooterLogo {...args} /> //creating a template

export const Logo = Template.bind({})

//Passing the props to the component

Logo.args = {
}


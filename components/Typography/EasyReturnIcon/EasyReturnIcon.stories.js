import EasyReturnLogo from './index';
import EasyReturnSVGIcon from '../../../public/assets/svg/easy-return-svg-icon.svg';

export default {
    title: 'Ropero/Easy Return Logo'
}

const Template = args => <EasyReturnLogo {...args} />

export const Logo = Template.bind({});

Logo.args = {
    easyReturnIcon: EasyReturnSVGIcon
}
import {ReactSVG} from 'react-svg';


const EasyReturnIcon = ({easyReturnIcon}) => {
    return (
        <ReactSVG src={easyReturnIcon} className="easy-return-icon" />
    );
};

export default EasyReturnIcon;
import FooterLink from './index'; //importing the FooterLink component


export default {
    title: "Ropero/Footer Link" //title for our storybook
}

const Template = (args) => <FooterLink {...args} /> //creating a template

export const Link = Template.bind({})

//Passing the props to the component

Link.args = {
    linkText: 'FAQ',
    address: '/'
}
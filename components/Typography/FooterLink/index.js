import Link from 'next/link';

const FooterLink = ({ linkText, address }) => {
    return (
        <div className="footer-link">
            <Link href={address} >
                <a>{linkText}</a>
            </Link>
        </div>
    );
};

export default FooterLink;
import BeforeHeading from './index';

export default {
    title: "Roperro Typography/Before Heading"
}


const Template = args => <BeforeHeading {...args} />

export const HouseOfRoperro = Template.bind({});

HouseOfRoperro.args = {
    text: 'House of Roperro',
    tailwind: 'uppercase text-2xl text-colorGray38'
}
import React from 'react';


const ProductName = ({ children, color = '$color-off-white87' }) => {
  return (
    <div className="product-name" style={{ color: color }}>
      {children}
    </div>
  );
};

export default ProductName;

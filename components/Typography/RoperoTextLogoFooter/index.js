import Link from 'next/link';
import { ReactSVG } from 'react-svg';


const RoperoTextLogoFooter = ({textLogoFooter}) => {

    return (
        <div className="ropero-text-logo-footer">
            <Link href="/" >
                <a>
                    <ReactSVG src={textLogoFooter}
                    />
                </a>
            </Link>
        </div>
    );
};

export default RoperoTextLogoFooter;



import RoperoTextLogoFooter from './index'; //importing the Logo component
import LogoTextFooterSVGIcon from '../../../public/assets/svg/roperro-white-text-logo.svg';


export default {
    title: "Ropero/Footer Logo" //title for our storybook
}

const Template = (args) => <RoperoTextLogoFooter {...args} /> //creating a template

export const LogoText2 = Template.bind({})

//Passing the props to the component

LogoText2.args = {
    textLogoFooter: LogoTextFooterSVGIcon
}
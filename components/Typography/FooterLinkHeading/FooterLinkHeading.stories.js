import FooterLinkHeading from './index'; //importing the FooterLinkHeading component

export default {
    title: 'Ropero/Footer Link Heading',  //title for our storybook
}


const Template = (args) => <FooterLinkHeading {...args} /> //creating a template

export const HouseOfRopero = Template.bind({})

//Passing the props to the component

HouseOfRopero.args ={
    text: 'house of ropero'
}
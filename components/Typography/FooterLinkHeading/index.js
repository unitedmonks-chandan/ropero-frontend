
const FooterLinkHeading = (props) => {

    const { headingText } = props;

    return (
        <p className="footer-link-heading">{headingText}</p>
    )
}

export default FooterLinkHeading;
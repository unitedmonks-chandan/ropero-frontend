

const EmailLabel = (props) => {
    const {emailLabelText} = props;
    return (
        <p className="email-label">
            {emailLabelText}
        </p>
    );
};

export default EmailLabel;
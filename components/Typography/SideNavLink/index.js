
import React, { useState } from 'react';
import RoperroLink from '../../RoperroLink';
import { ReactSVG } from 'react-svg';

const SideNavLink = (props) => {

    const { tailwind, link, icon, active, onClick } = props;

    const handleDisplay= (active) => {
        if(active) {
            return {display: 'block'}
        } else {
            return {display: 'none'}
        }
    }

    return (
        <div className={tailwind} onClick={onClick} >
            <RoperroLink
                text={link.text}
                linkHref={link.linkHref}
                tailwind={`${link.tailwind}`}
                css={active? `${link.css} ${link.onClickCss}`: `${link.css}`}
            />

            <ReactSVG 
                src={icon.svg} 
                className={active? `${icon.tailwind} block`: `${icon.tailwind} hidden`}
            />
        </div>
    );
};

export default SideNavLink;
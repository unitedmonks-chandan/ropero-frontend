import SideNavLink from "./index";
import RightArrowBlackSVGIcon from '../../../svgs/Ropero/RightArrowBlackSVGIcon.svg';

export default {
    title: "Roperro/Side Navbar Link"
};

const Template = args => <SideNavLink {...args} />;

export const Delivered = Template.bind({});

Delivered.args = {
    tailwind: ' flex items-baseline justify-between',
        
    link: {
        text: 'Delivered',
        linkHref: '/',
        tailwind: ' text-2xl font-light text-colorGray24',
        css: '',
        onClickCss: 'color-golden45 bold600'
    },
    
    icon: {
        svg: RightArrowBlackSVGIcon,
        tailwind: ' w-3 h-5',
    }
}



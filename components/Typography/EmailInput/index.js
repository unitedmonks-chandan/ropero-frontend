

const EmailInput = (props) => {
    const {emailInputPlaceholder} = props;
    return (
        <input type="email" className="email-input" placeholder={emailInputPlaceholder} />
    );
};

export default EmailInput;

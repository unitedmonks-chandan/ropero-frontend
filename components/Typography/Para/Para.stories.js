import Para from './index';

export default {
    title: "Roperro Typography/Para"
}


const Template = args => <Para {...args} />

export const LeatherCollection = Template.bind({});

LeatherCollection.args = {
    text: "The online store presents selected successful collections, fashionable women’s clothing which is sewn exclusively from quality material.",
    
    tailwind: 'text-2xl text-colorGray38',
    css: ''
}
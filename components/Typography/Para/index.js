


const Para = ({ text, tailwind, css }) => {
    return (
        <p className={`${tailwind} ${css}`}>{text}</p>
    );
};

export default Para;
import { ReactSVG } from 'react-svg';
import React from 'react';
import IndianRupeeSVGIcon from '../../../svgs/IndianRupeeSVGIcon.svg';

const ProductPrice = ({ price, specialPrice }) => {
    const percentOff = Math.round(((price - specialPrice) / price) * 100);

    return (
        <div className="product-price">
            <div className="icon__small" >
                <ReactSVG src={IndianRupeeSVGIcon} />
            </div>
            <span className="sales-price">{specialPrice}</span>
            <span className="strikethroughPrice">{price !== specialPrice && price > specialPrice ? price : ""}</span>
            <span className="percent-off">{percentOff > 0 ? ("(" + percentOff + "% off)") : ""}</span>
        </div>
    );
};

export default ProductPrice;

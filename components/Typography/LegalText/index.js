import React from 'react';

const LegalText = ({copyrightText}) => {
    return (
        <p className="legal-text">
            {copyrightText}
        </p>
    );
};

export default LegalText;


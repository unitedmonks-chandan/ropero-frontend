import { ReactSVG } from 'react-svg';

const CardNumber = ({ dot, lastFourDigit, digitTailwind }) => {

    const { dotMargin = '3.9px', dotGroupMargin = '13.3px' } = dot;

    const renderDotsGroup = () => {
        return (
            <div style={{ display: 'flex' }} className={dot.dotGroupMargin}>
                <div style={{ display: 'flex' }}>
                    <ReactSVG src={dot.svg} style={{ marginRight: dotMargin }} />
                    <ReactSVG src={dot.svg} style={{ marginRight: dotMargin }} />
                    <ReactSVG src={dot.svg} style={{ marginRight: dotMargin }} />
                    <ReactSVG src={dot.svg} style={{ marginRight: dotMargin }} />
                </div>
            </div>
        )
    }



    return (
        <div style={{ display: 'flex', alignItems: 'baseline' }} >
            {renderDotsGroup()}
            {renderDotsGroup()}
            {renderDotsGroup()}
            <div className={lastFourDigit.tailwind}>{lastFourDigit.digits}</div>
        </div>
    );
};

export default CardNumber;
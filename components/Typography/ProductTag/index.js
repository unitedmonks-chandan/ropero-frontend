import React from 'react';


const ProductTag = ({ children }) => {
    return (
        <div className="product-tag">
            {children}
        </div>
    );
};

export default ProductTag;
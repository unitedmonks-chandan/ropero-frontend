import Heading from './index';

export default {
    title: "Roperro Typography/Heading"
}


const Template = args => <Heading {...args} />

export const LeatherCollection = Template.bind({});

LeatherCollection.args = {
    text: 'new leather collections',
    tailwind: 'uppercase text-8xl text-hoverYellow font-cinzel'
}

export const Checkout = Template.bind({});

Checkout.args = {
    text: 'checkout',
    tailwind: 'uppercase font-semibold text-4xl text-hoverYellow'
}

export const ShippingAddress = Template.bind({});

ShippingAddress.args = {
    text: '1. Shipping Address',
    tailwind: 'font-semibold text-3xl text-colorGray30'
}
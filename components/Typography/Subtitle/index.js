import React from 'react';


const SubTitle = ({children}) => {
    return (
      <div className="product-subtitle">
        {children}
      </div>
    );
};

export default SubTitle;

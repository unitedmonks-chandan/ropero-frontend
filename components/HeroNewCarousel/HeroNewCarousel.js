import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation, Autoplay } from "swiper";
import SwiperCore from "swiper";
// import "./double-arrow-slider.css";
import LinkButtonWithLogo from "../NewButtons/LinkButtonWithLogo/LinkButtonWithLogo";

function HeroNewCarousel({
  customStyle,
  customSlidesPerView,
  customSlidesPerGroup,
  customSpaceBetween,
  carouselData,
}) {
  SwiperCore.use([Navigation, Pagination]);


  return (
    <div className={customStyle}>
      <Swiper
        slidesPerView={customSlidesPerView ? customSlidesPerView : 1}
        slidesPerGroup={customSlidesPerGroup ? customSlidesPerGroup : 1}
        spaceBetween={customSpaceBetween ? customSpaceBetween : 8}
        // autoplay={{
        //   delay: 2500,
        //   disableOnInteraction: false,
        // }}
        pagination={{
          clickable: true,
        }}
        loop={true}
        navigation
        modules={[Pagination]}
        className={`mySwiper`}
      >
        {carouselData &&
          carouselData.map(({ image, altTag, btnData }, i) => {
            return (
              <SwiperSlide key={i} style={{color: 'Grey'}}>
                <LinkButtonWithLogo {...btnData} />
                <img src={image} alt={altTag} />
              </SwiperSlide>
            );
          })}
      </Swiper>
    </div>
  );
}

export default HeroNewCarousel;

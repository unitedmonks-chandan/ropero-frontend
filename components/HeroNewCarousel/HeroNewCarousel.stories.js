import HeroNewCarousel from "./HeroNewCarousel";

import img1 from "../../public/assets/images/arkar/hero.jpg";
import img2 from "../../public/assets/images/monadaa-comp.png";
import arkarlogo from "../../svgs/arkar-logo-new.svg";

export default {
  title: "Roperro / Arkar / Hero New Carousel",
};

const Template = (args) => <HeroNewCarousel {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  customStyle: " arker-hero-slider relative h-screen",
  carouselData: [
    {
      image: img1,
      altTag: 'model',
      btnData: {
        position: "absolute arkar-hero-btn-position z-50 flex flex-col gap-8",
        logo: arkarlogo,
        logoSize: "slider-logo-size",
        btnLink: "/arkar",
        btn: "bg-midRed text-white text-lg font-primary px-7 py-1 uppercase self-center",
        btnText: "Shop now",
      },
    },
    {
      image: img2,
      altTag: 'model',
      btnData: {
        position: "absolute arkar-hero-btn-position z-50 flex flex-col gap-8",
        logo: arkarlogo,
        logoSize: "slider-logo-size",
        btnLink: "/arkar/view-all",
        btn: "bg-white text-black text-lg font-primary px-7 py-1 uppercase self-center",
        btnText: "View all",
      },
    },
  ],
};

import SecondComp from "./index";

import ourBrandsSvg from "../../../public/assets/svg/home/our-brands.svg";
import leridaLogo from "../../../public/assets/svg/home/lerida-logo.svg";
import zimbaLogo from "../../../public/assets/svg/home/zimba-logo.svg";
import monadaaLogo from "../../../public/assets/svg/home/monadaa-logo.svg";
import arkarLogo from "../../../public/assets/svg/home/arkar-logo.svg";
import migearLogo from "../../../public/assets/svg/home/migear-logo.svg";

import leridaBg from "../../../public/assets/images/lerida-comp.png";
import zimbaBg from "../../../public/assets/images/zimba-comp.png";
import monadaaBg from "../../../public/assets/images/zimba-comp.png";
import arkarBg from "../../../public/assets/images/arkar-comp.png";
import migearBg from "../../../public/assets/images/migear-comp.png";


export default {
  title: "Roperro / Homepage / Our Brands",
  component: SecondComp,
};

const Template = (args) => <SecondComp {...args} />;
export const Design1 = Template.bind({});

Design1.args = {
  bg1: "bg-black w-full flex flex-col gap-4 items-center justify-center h-48",
  font: "text-xl font-primay text-hoverYellow",
  blackBoxLogo: ourBrandsSvg,
  blackBoxLogoSize: "w-96",
  display: "grid grid-cols-1 lg:grid-cols-2",
  text: "From The House Of Roperro",
  blackContainer: true,
  // logoPosition: "shopNow-logo",
  shopNowCompsData: [
    {
      logoSize: "w-32",
      logoPosition: "shopNow-logo",
      btnLink: "/lerida",
      shopNowBtn:
        "bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary",
      logo: leridaLogo,
      btnText: "Shop now",
      position: "absolute shopNow-comp right-10 inline-block",
      bgImg: leridaBg,
      colSpan: "",
    },
    {
      logoSize: "w-20",
      logoPosition: "shopNow-logo",
      btnLink: "/zimba",
      shopNowBtn:
        "bg-black text-white px-5 py-2 flext items center justify-center uppercase text-xl font-primary",
      logo: zimbaLogo,
      btnText: "Shop now",
      position: "absolute shopNow-comp right-10 inline-block",
      bgImg: zimbaBg,
      colSpan: "",
    },
    {
      logoSize: "w-80",
      btnLink: "/monadaa",
      shopNowBtn:
        "bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary self-center",
      logo: monadaaLogo,
      btnText: "Shop now",
      position:
        "absolute shopNow-comp-type2 flex flex-col gap-8 justify-center",
      bgImg: monadaaBg,
      colSpan: "col-span-full",
      logoPosition: "",
    },
    {
      logoSize: "w-32",
      logoPosition: "shopNow-logo",
      btnLink: "/arkar",
      shopNowBtn:
        "bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary",
      logo: arkarLogo,
      btnText: "Shop now",
      position: "absolute shopNow-comp right-10 inline-block",
      bgImg: arkarBg,
      colSpan: "",
    },
    {
      logoSize: "w-32",
      logoPosition: "shopNow-logo",
      btnLink: "/migear",
      shopNowBtn:
        "bg-black text-white px-5 py-2 flext items center justify-center uppercase text-xl font-primary",
      logo: migearLogo,
      btnText: "Shop now",
      position: "absolute shopNow-comp right-10 inline-block",
      bgImg: migearBg,
      colSpan: "",
    },
  ],
};

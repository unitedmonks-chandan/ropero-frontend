import React from "react";
import { ReactSVG } from "react-svg";
import ShopNow from "../ShopNow/ShopNow";
// import leridaLogo from "../../../svgs/lerida-logo.svg";
// import leridaBg from "../../../assets/images/lerida-comp.png";
// import zimbaLogo from "../../../svgs/zimba-logo.svg";
// import zimbaBg from "../../../assets/images/zimba-comp.png";
// import monadaaLogo from "../../../svgs/monadaa-logo.svg";
// import monadaaBg from "../../../assets/images/monadaa-comp.png";
// import arkarLogo from "../../../svgs/arkar-logo.svg";
// import arkarBg from "../../../assets/images/arkar-comp.png";
// import migearLogo from "../../../svgs/migear-logo.svg";
// import migearBg from "../../../assets/images/migear-comp.png";

function SecondComp({
  bg1,
  font,
  text,
  blackBoxLogo,
  blackBoxLogoSize,
  display,
  blackContainer,
  shopNowCompsData,
}) {
  return (
    <div className="w-full">
      {blackContainer && (
        <div className={bg1}>
          <p className={font}>{text}</p>
          <ReactSVG src={blackBoxLogo} className={blackBoxLogoSize}></ReactSVG>
        </div>
      )}
      <div className={display}>
        {/* <ShopNow
          logoSize="w-32"
          logoPosition="shopNow-logo"
          btnLink="/lerida"
          shopNowBtn="bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary"
          logo={leridaLogo}
          btnText="Shop now"
          position="absolute shopNow-comp right-10 inline-block"
          bgImg={leridaBg}
          colSpan="1"
        />
        <ShopNow
          logoSize="w-20"
          logoPosition="shopNow-logo"
          btnLink="/zimba"
          shopNowBtn="bg-black text-white px-5 py-2 flext items center justify-center uppercase text-xl font-primary"
          logo={zimbaLogo}
          btnText="Shop now"
          position="absolute shopNow-comp right-10 inline-block"
          bgImg={zimbaBg}
          colSpan="1"
        />
        <ShopNow
          logoSize="w-80"
          btnLink="/monadaa"
          shopNowBtn="bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary self-center"
          logo={monadaaLogo}
          btnText="Shop now"
          position="absolute shopNow-comp-type2 flex flex-col gap-8 justify-center"
          bgImg={monadaaBg}
          colSpan="col-span-full"
          logoPosition=""
        />
        <ShopNow
          logoSize="w-32"
          logoPosition="shopNow-logo"
          btnLink="/arkar"
          shopNowBtn="bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary"
          logo={arkarLogo}
          btnText="Shop now"
          position="absolute shopNow-comp right-10 inline-block"
          bgImg={arkarBg}
          colSpan="1"
        />
        <ShopNow
          logoSize="w-32"
          logoPosition="shopNow-logo"
          btnLink="/migear"
          shopNowBtn="bg-black text-white px-5 py-2 flext items center justify-center uppercase text-xl font-primary"
          logo={migearLogo}
          btnText="Shop now"
          position="absolute shopNow-comp right-10 inline-block"
          bgImg={migearBg}
          colSpan="1"
        /> */}

        {/* Method 2 */}
        {shopNowCompsData &&
          shopNowCompsData.map(
            (
              {
                colSpan,
                bgImg,
                altTag,
                position,
                logo,
                logoSize,
                logoPosition,
                btnLink,
                shopNowBtn,
                btnText,
              },
              index
            ) => (
              <ShopNow
                key={index}
                colSpan={colSpan}
                bgImg={bgImg}
                altTag={altTag}
                position={position}
                logo={logo}
                logoSize={logoSize}
                logoPosition={logoPosition}
                btnLink={btnLink}
                shopNowBtn={shopNowBtn}
                btnText={btnText}
              />
            )
          )}
      </div>
    </div>
  );
}

export default SecondComp;

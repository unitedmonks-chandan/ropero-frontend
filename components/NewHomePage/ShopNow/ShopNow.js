import React from "react";
import Link from "next/link";

function ShopNow({
  colSpan,
  bgImg,
  altTag,
  position,
  logo,
  logoSize,
  logoPosition,
  btnLink,
  shopNowBtn,
  btnText,
}) {
  return (
    <div className={`relative w-full ${colSpan}`}>
      <img src={bgImg} className="w-full" alt={altTag} />
      <div className={position}>
        <img src={logo} alt="logo" className={`${logoSize} ${logoPosition}`} />
        <Link href={btnLink}>
          <a className={shopNowBtn}>{btnText}</a>
        </Link>
      </div>
    </div>
  );
}

export default ShopNow;

import ShopNow from "./ShopNow";
import leridaLogo from "../../../public/assets/svg/home/lerida-logo.svg";
import monadaaLogo from "../../../public/assets/svg/home/monadaa-logo.svg";
import leridaBg from "../../../public/assets/images/lerida-comp.png";
import monadaaBg from "../../../public/assets/images/monadaa-comp.png";

export default {
  title: "Shop Now",
};

const Template = (args) => <ShopNow {...args} />;

export const ShopNowCompType1 = Template.bind({});

ShopNowCompType1.args = {
  logoSize: "w-40",
  btnLink: "/lerida",
  shopNowBtn:
    "bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary",
  logo: leridaLogo,
  btnText: "Shop now",
  position: "absolute shopNow-comp right-10 inline-block",
  bgImg: leridaBg,
  altTag: "Woman in creme color shirt",
  colSpan: "1",
  logoPosition: "shopNow-logo",
};

export const ShopNowCompType2 = Template.bind({});

ShopNowCompType2.args = {
  logoSize: "w-80",
  btnLink: "/monadaa",
  shopNowBtn:
    "bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary self-center",
  logo: monadaaLogo,
  btnText: "Shop now",
  position: "absolute shopNow-comp-type2 flex flex-col gap-8 justify-center",
  bgImg: monadaaBg,
  colSpan: "col-span-2",
  logoPosition: "",
};

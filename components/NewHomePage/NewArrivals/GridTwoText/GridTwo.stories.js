import GridTwo from "./GridTwoText";

export default {
  title: "Grid Two With Text & Logo",
};

const Template = (args) => <GridTwo {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  spacing: "py-16 pl-12 pr-60 2xl:pr-64",
  display:
    "grid grid-cols-1 xl:grid-cols-2 xl:max-w-screen-lg gap-10 xl:gap-14",
  subHeadingFont: "text-xl font-primary",
  subHeading: "HOUSE OF ROPERRO",
  headingFont: "text-hoverYellow text-8xl uppercase font-primary",
  heading1: "New",
  heading2: "Arrivals",
  spanDisplay: "block",
  paraFont: "text-lg font-primary self-center",
  para: "The online store presents selected suc- cessful collections, fashionable women’s clothing which is sewn exclusively from quality material.",
};

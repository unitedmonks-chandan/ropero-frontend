import React from "react";
import Link from "next/link";

function TwoImagesTwoBtnWithHeading({
  containerCl,
  image1,
  altTag1,
  image2,
  altTag2,
  positionCl,
  headingCl,
  heading1,
  heading2,
  btnContainerCl,
  btn1Link,
  btn1Cl,
  btn1Text,
  btn2Link,
  btn2Cl,
  btn2Text,
}) {
  return (
    <div className={`relative ${containerCl}`}>
      <img src={image1} alt={altTag1} className="w-full max-w-full" />
      <img src={image2} alt={altTag2} className="w-full max-w-full" />
      <div className={positionCl}>
        <div className={headingCl}>
          <p>{heading1}</p>
          <p>{heading2}</p>
        </div>
        <div className={btnContainerCl}>
          <Link href={btn1Link}>
            <a className={btn1Cl}>{btn1Text}</a>
          </Link>
          <Link href={btn2Link}>
            <a className={btn2Cl}>{btn2Text}</a>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default TwoImagesTwoBtnWithHeading;

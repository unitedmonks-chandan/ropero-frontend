import TwoImagesTwoBtnWithHeading from "./TwoImagesTwoBtnWithHeading";
import image1 from "../../../../public/assets/images/winter-collection-img1.png";
import image2 from "../../../../public/assets/images/winter-collection-img2.png";

export default {
  title: "Roperro / Homepage / Winter Collection",
};

const Template = (args) => <TwoImagesTwoBtnWithHeading {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  containerCl: "grid grid-cols-1 xl:grid-cols-2",
  image1: image1,
  altTag1: 'Man in black orange sweatshirt',
  image2: image2,
  altTag2: 'Woman in light brown jacket',
  positionCl: "absolute two-btn-position flex flex-col gap-12",
  headingCl:
    "text-5xl font-primary text-black flex gap-2 flex-col justify-center items-center uppercase",
  heading1: "Winter",
  heading2: "collection",
  btnContainerCl: "flex flex-col gap-6 justify-center items-center",
  btn1Link: "/men",
  btn1Cl: "px-7 py-1 border-2 border-black flex items-center uppercase",
  btn1Text: "Shop for him",
  btn2Link: "/women",
  btn2Cl:
    "px-7 py-1 border-2 border-transparent bg-hoverYellow flex items-center uppercase",
  btn2Text: "Shop for her",
};

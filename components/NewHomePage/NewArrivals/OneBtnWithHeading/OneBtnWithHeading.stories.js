import OneBtnWithHeading from "./OneBtnWithHeading";

export default {
  title: "one btn with heading",
};

const Template = (args) => <OneBtnWithHeading {...args} />;

export const type1 = Template.bind({});

type1.args = {
  containerCl: "flex flex-col gap-6",
  headingCl:
    "flex flex-col text-3xl text-black text-primary uppercase font-primary",
  heading1: "Trendy",
  heading2: "Styles",
  btnLink: "/view-all",
  btnStyle:
    "bg-black text-white flex items-center justify-center px-16 py-2 self-start",
  btnText: "View All",
};

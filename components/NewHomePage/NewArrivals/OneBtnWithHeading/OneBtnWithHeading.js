import React from "react";
import Link from "next/link";

function OneBtnWithHeading({
  containerCl,
  headingCl,
  heading1,
  heading2,
  btnLink,
  btnStyle,
  btnText,
}) {
  return (
    <div>
      <div className={containerCl}>
        <div className={headingCl}>
          <p>{heading1}</p>
          <p>{heading2}</p>
        </div>
        <Link href={btnLink}>
          <a className={btnStyle}>{btnText}</a>
        </Link>
      </div>
    </div>
  );
}

export default OneBtnWithHeading;

import NewArrival from "./NewArrival";
import img1 from "../../../public/assets/images/new-arrivals-img1.png";
import img2 from "../../../public/assets/images/new-arrivals-img2.png";
import img3 from "../../../public/assets/images/new-arrivals-img3.png";
import winterImage1 from "../../../public/assets/images/winter-collection-img1.png";
import winterImage2 from "../../../public/assets/images/winter-collection-img2.png";

export default {
  title: "Roperro / Homepage / New arrivals",
};

const Template = (args) => <NewArrival {...args} />;

export const NewArrival1 = Template.bind({});

NewArrival1.args = {
  spacing: "py-16 pl-24 2xl:pl-44 pr-52 2xl:pr-60",

  firstCompData: [
    {
      spacing: "",
      // spacing: "py-16 pl-12 pr-60 2xl:pr-64",
      display:
        "grid grid-cols-1 xl:grid-cols-2 xl:max-w-screen-lg gap-10 xl:gap-14",
      subHeadingFont: "text-xl font-primary",
      subHeading: "HOUSE OF ROPERRO",
      headingFont: "text-hoverYellow text-8xl uppercase font-primary",
      heading1: "New",
      heading2: "Arrivals",
      spanDisplay: "block",
      paraFont: "text-lg font-primary self-center",
      para: "The online store presents selected suc- cessful collections, fashionable women’s clothing which is sewn exclusively from quality material.",
    },
  ],
  btnData: {
    container: "pt-44 pl-8",
    position: "",
    headingStyle: "text-2xl text-black font-cinzel font-medium uppercase mb-4",
    heading1: "Trendy",
    heading2: "Styles",
    btnLink: "/view-all",
    btn: "inline-block bg-black text-white text-lg font-primary px-7",
    size: "",
    btnText: "View All",
  },
  display: "grid grid-cols-4 mt-12",
  carouselData: {
    customSpaceBetween: 0.1,
    customStyle:
      "relative w-full col-span-3 hide-inbuilt-arrow right-black-arrow hide-pagination-bullets",
    images: [
      {img: img1, altTag: 'Woman in light brown dress'},
      {img: img2, altTag: 'Woman in blue sweatshirt'},
      {img: img3,  altTag: 'Woman in black coat'},
      {img: img2, altTag: 'Woman in blue sweatshirt'},
      {img: img3,  altTag: 'Woman in black coat'},
      {img: img1, altTag: 'Woman in light brown dress'},
    ],
  },
  winterCollectionData: [
    {
      containerCl: "grid grid-cols-1 xl:grid-cols-2",
      image1: winterImage1,
      image2: winterImage2,
      positionCl: "absolute two-btn-position flex flex-col gap-12",
      headingCl:
        "text-5xl font-primary text-black flex gap-2 flex-col justify-center items-center uppercase",
      heading1: "Winter",
      heading2: "collection",
      btnContainerCl: "flex flex-col gap-6 justify-center items-center",
      btn1Link: "/men",
      btn1Cl: "px-7 py-1 border-2 border-black flex items-center uppercase",
      btn1Text: "Shop for him",
      btn2Link: "/women",
      btn2Cl:
        "px-7 py-1 border-2 border-transparent bg-hoverYellow flex items-center uppercase",
      btn2Text: "Shop for her",
    },
  ],
};

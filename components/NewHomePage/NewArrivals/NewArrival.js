import React from "react";
import GridTwoText from "./GridTwoText/GridTwoText";
import TwoImagesTwoBtnWithHeading from "./TwoImagesTwoBtnWithHeading/TwoImagesTwoBtnWithHeading";
import LinkButtonWithHeading from "../../NewButtons/LinkButtonWithHeading/LinkButtonWithHeading";
import NewCarousel from "../../NewCarousel/NewCarousel";

function NewArrival({
  spacing,
  firstCompData,
  display,
  carouselData,
  btnData,
  winterCollectionData,
}) {
  return (
    <div>
      <div className={spacing}>
        {firstCompData &&
          firstCompData.map(
            (
              {
                spacing,
                display,
                subHeadingFont,
                subHeading,
                headingFont,
                heading1,
                heading2,
                paraFont,
                para,
              },
              index
            ) => (
              <GridTwoText
                key={index}
                spacing={spacing}
                display={display}
                subHeadingFont={subHeadingFont}
                subHeading={subHeading}
                headingFont={headingFont}
                heading1={heading1}
                heading2={heading2}
                paraFont={paraFont}
                para={para}
              />
            )
          )}
        <div className={display}>
          <NewCarousel {...carouselData} />
          <LinkButtonWithHeading {...btnData} />
        </div>
      </div>

      <div>
        {winterCollectionData &&
          winterCollectionData.map(
            (
              {
                containerCl,
                image1,
                altTag1,
                image2,
                altTag2,
                positionCl,
                headingCl,
                heading1,
                heading2,
                btnContainerCl,
                btn1Link,
                btn1Cl,
                btn1Text,
                btn2Link,
                btn2Cl,
                btn2Text,
              },
              index
            ) => (
              <TwoImagesTwoBtnWithHeading
                key={index}
                containerCl={containerCl}
                image1={image1}
                altTag1={altTag1}
                image2={image2}
                altTag2={altTag2}
                positionCl={positionCl}
                headingCl={headingCl}
                heading1={heading1}
                heading2={heading2}
                btnContainerCl={btnContainerCl}
                btn1Link={btn1Link}
                btn1Cl={btn1Cl}
                btn1Text={btn1Text}
                btn2Link={btn2Link}
                btn2Cl={btn2Cl}
                btn2Text={btn2Text}
              />
            )
          )}
      </div>
    </div>
  );
}

export default NewArrival;

import OurCollection from "../../OurCollection/OurCollection";
import womenImg from "../../../public/assets/images/lerida/for-her.jpg";
import menImg from "../../../public/assets/images/lerida/for-him.jpg";

export default {
  title: "Roperro / Lerida / Our Collection",
};

const Template = (args) => <OurCollection {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  display: "grid grid-cols-1",
  shopNowCompsData: [
    {
      bgImg: womenImg,
      colSpan: "1",
      btnData: {
        container: "",
        position:
          "lerida-our-collection-btn bottom-72 flex flex-col justify-center items-center",
        headingStyle: "text-5xl text-white uppercase mb-4 font-cinzel",
        heading1: "For her",
        btnContainer: "",
        btnLink: "/lerida/women",
        btn: "bg-white text-black font-primary font-medium px-7 py-2 uppercase",
        size: "",
        btnText: "Shop now",
      },
    },
    {
      bgImg: menImg,
      colSpan: "1",
      btnData: {
        container: "",
        position:
          "lerida-our-collection-btn bottom-72 flex flex-col justify-center items-center",
        headingStyle: "text-5xl text-white uppercase mb-12 font-cinzel",
        heading1: "For him",
        btnContainer: "",
        btnLink: "/lerida/men",
        btn: "bg-white text-black font-primary font-medium px-7 py-2 uppercase",
        size: "",
        btnText: "Shop now",
      },
    },
  ],
};

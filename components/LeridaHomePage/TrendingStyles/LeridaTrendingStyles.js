import ImageBtnIconsCarousel from "../../ImageBtnIconsCarousel/ImageBtnIconsCarousel";

function LeridaTrendingStyles({
  container,
  heading1,
  heading2,
  headingStyle,
  carouselData,
  image,
  imageSize,
}) {
  return (
    <div className={container}>
      <div className={headingStyle}>
        <p>{heading1}</p>
        <p>{heading2}</p>
      </div>
      <ImageBtnIconsCarousel {...carouselData} />
      <img src={image} alt="" className={imageSize} />
    </div>
  );
}

export default LeridaTrendingStyles;

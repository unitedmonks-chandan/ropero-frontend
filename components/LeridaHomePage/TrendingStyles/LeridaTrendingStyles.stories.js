import LeridaTrendingStyles from "./LeridaTrendingStyles";
import img1 from "../../../public/assets/images/lerida/bag.jpg";
import img2 from "../../../public/assets/images/lerida/trending-styles.jpg";
import heartSvg from "../../../public/assets/svg/red-heart-fill.svg";
import bagSvg from "../../../public/assets/svg/bag.svg";

export default {
  title: "Roperro / Lerida / Trending Styles",
};

const Template = (args) => <LeridaTrendingStyles {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  container: "bg-lightGray2 pt-32 pb-12 pr-20 grid grid-cols-3",
  heading1: "Trending",
  heading2: "Styles",
  headingStyle:
    "text-gray200 text-6xl 2xl:text-9xl font-cinzel uppercase self-center rotate-negative-90",
  image: img2,
  imageSize: "",
  //   heading2Style: "ml-28",
  carouselData: {
    customSlidesPerView: 1,
    customSlidesPerGroup: 1,
    customSpaceBetween: 0.1,
    customStyle:
      "relative pb-32 hide-inbuilt-arrow left-black-arrow right-black-arrow hide-pagination-bullets lerida-trending-styles-slider",
    carouselData: [
      {
        customSlide: "self-start relative",
        btnIcons: "absolute rotate-negative-90 -right-12 bottom-32",
        image: img1,
        btnData: {
          container: "",
          position: "",
          heading1: "BROWN LEATHER PURSE",
          heading1Style: "text-lg text-midGray ",
          btnLink: "/",
          btn: "hidden",
        },
        icons: [
          {
            icon: "",
            iconSize: "",
          },
          {
            icon: "",
            iconSize: "",
          },
        ],
        iconsPosition: "hidden",
      },
      {
        customSlide: "self-start relative",
        btnIcons: "absolute rotate-negative-90 -right-12 bottom-32",
        image: img1,
        btnData: {
          container: "",
          position: "",
          heading1: "BROWN LEATHER PURSE",
          heading1Style: "text-lg text-hoverYellow",
          btnLink: "/",
          btn: "hidden",
        },
        icons: [
          {
            icon: "",
            iconSize: "",
          },
          {
            icon: "",
            iconSize: "",
          },
        ],
        iconsPosition: "hidden",
      },
    ],
  },
};

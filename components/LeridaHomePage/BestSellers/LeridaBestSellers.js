import React from "react";

import ImageBtnIconsCarousel from "../../ImageBtnIconsCarousel/ImageBtnIconsCarousel";
import MultipleHeading from "../../MultipleHeading/MultipleHeading";

function LeridaBestSellers({ spacing, textData, carouselData }) {
  return (
    <div className={spacing}>
      <MultipleHeading {...textData} />
      <ImageBtnIconsCarousel {...carouselData} />
    </div>
  );
}

export default LeridaBestSellers;

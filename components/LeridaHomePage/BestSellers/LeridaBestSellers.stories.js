import LeridaBestSellers from "./LeridaBestSellers";
import img1 from "../../../public/assets/images/lerida/best-sellers1.jpg";
import img2 from "../../../public/assets/images/lerida/best-sellers2.jpg";
import img3 from "../../../public/assets/images/lerida/best-sellers3.jpg";
import heartSvg from "../../../public/assets/svg/red-heart-fill.svg";
import bagSvg from "../../../public/assets/svg/bag.svg";

export default {
  title: "Roperro / Lerida / Best Sellers",
};

const Template = (args) => <LeridaBestSellers {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  spacing: "pt-20",
  textData: {
    container: "font-primary text-center pb-16",
    heading1: "HOUSE OF LERIDA",
    heading1Style:
      "text-midGray text-2xl font-primary font-medium uppercase mb-2",
    heading2: "Best Sellers",
    heading2Style: " text-black text-6xl font-cinzel uppercase",
  },
  carouselData: {
    customSlidesPerView: 3,
    customSlidesPerGroup: 3,
    customSpaceBetween: 0.1,
    customStyle:
      "relative pb-32 hide-inbuilt-arrow hide-left-hidden-arrow right-black-arrow golden-pagination-bullet lerida-best-sellers-slider reset-swiper-container",
    carouselData: [
      {
        customSlide: "flex flex-col",
        btnIcons: "relative border border-midGray",
        image: img1,
        btnData: {
          container: "",
          position: "",
          headingStyle:
            "text-midGray text-2xl text-left font-primary flex flex-col justify-center items-center gap-2 py-4 ",
          heading1: "LEATHER JACKET",
          heading1Style: "font-medium",
          heading2: "₹ 5000",
          heading2Style: "",
          btnLink: "/",
          btn: "hidden",
        },
        icons: [
          {
            icon: heartSvg,
            iconSize: "icon-size",
          },
          {
            icon: bagSvg,
            iconSize: "icon-size",
          },
        ],
        iconsPosition: "flex gap-6 absolute right-10 bottom-4",
      },
      {
        customSlide: "flex flex-col",
        btnIcons: "relative border border-midGray",
        image: img2,
        btnData: {
          container: "",
          position: "",
          headingStyle:
            "text-midGray text-2xl text-left font-primary flex flex-col justify-center items-center gap-2 py-4 ",
          heading1: "TURTENECK SWEATSHIRT",
          heading1Style: "font-medium",
          heading2: "₹ 2000",
          heading2Style: "",
          btnLink: "/",
          btn: "hidden",
        },
        icons: [
          {
            icon: heartSvg,
            iconSize: "icon-size",
          },
          {
            icon: bagSvg,
            iconSize: "icon-size",
          },
        ],
        iconsPosition: "flex gap-6 absolute right-10 bottom-4",
      },
      {
        customSlide: "flex flex-col",
        btnIcons: "relative border border-midGray",
        image: img1,
        btnData: {
          container: "",
          position: "",
          headingStyle:
            "text-midGray text-2xl text-left font-primary flex flex-col justify-center items-center gap-2 py-4 ",
          heading1: "FORMAL GREY SUIT",
          heading1Style: "font-medium",
          heading2: "₹ 8000",
          heading2Style: "",
          btnLink: "/",
          btn: "hidden",
        },
        icons: [
          {
            icon: heartSvg,
            iconSize: "icon-size",
          },
          {
            icon: bagSvg,
            iconSize: "icon-size",
          },
        ],
        iconsPosition: "flex gap-6 absolute right-10 bottom-4",
      },
      {
        customSlide: "flex flex-col",
        btnIcons: "relative border border-midGray",
        image: img2,
        btnData: {
          container: "",
          position: "",
          headingStyle:
            "text-midGray text-2xl text-left font-primary flex flex-col justify-center items-center gap-2 py-4 ",
          heading1: "LEATHER JACKET",
          heading1Style: "font-medium",
          heading2: "₹ 5000",
          heading2Style: "",
          btnLink: "/",
          btn: "hidden",
        },
        icons: [
          {
            icon: heartSvg,
            iconSize: "icon-size",
          },
          {
            icon: bagSvg,
            iconSize: "icon-size",
          },
        ],
        iconsPosition: "flex gap-6 absolute right-10 bottom-4",
      },
    ],
  },
};

import React from "react";
import LinkButtonWithHeading from "../../NewButtons/LinkButtonWithHeading/LinkButtonWithHeading";
import NewCarousel from "../../NewCarousel/NewCarousel";

function LeridaNewArrivals({
  container,
  display,
  btnData,
  carouselData,
  heading,
  headingStyle,
}) {
  return (
    <div className={container}>
      <div className={display}>
        <LinkButtonWithHeading {...btnData} />
        <NewCarousel {...carouselData} />
      </div>
      <p className={headingStyle}>{heading}</p>
    </div>
  );
}

export default LeridaNewArrivals;

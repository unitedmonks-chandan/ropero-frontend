import LeridaNewArrivals from "./LeridaNewArrivals";
import img1 from "../../../public/assets/images/lerida/new-arrivals1.jpg";
import img2 from "../../../public/assets/images/lerida/new-arrivals2.jpg";

export default {
  title: "Roperro / Lerida / New arrivals",
};

const Template = (args) => <LeridaNewArrivals {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  container: "bg-black py-24 pl-16",
  display: "grid grid-cols-3 gap-16",
  btnData: {
    container: "pt-44 pl-8",
    position: "",
    headingStyle: "text-5xl text-white font-cinzel uppercase mb-6",
    heading1: "New",
    heading2: "Arrivals",
    heading3:
      "The online store presents selected successful collections, fashionable women’s clothing which is sewn exclusively from high quality material",
    heading3Style: "text-lg text-gray100 font-primary mt-6",
    btnLink: "/view-all",
    btn: "inline-block bg-lightOrange text-black text-lg font-primary px-7 font-medium uppercase",
    size: "",
    btnText: "Explore",
  },
  carouselData: {
    customSlidersPerView: 2,
    customSlidesPerGroup: 2,
    customSpaceBetween: 0.1,
    customStyle:
      "relative w-full col-span-2 hide-inbuilt-arrow hide-left-hidden-arrow right-white-arrow2 hide-pagination-bullets lerida-new-arrivals-slider",
    images: [img1, img2, img2, img1],
  },
  heading: "LEATHER JACKETS",
  headingStyle:
    "text-xl text-white font-primary font-medium text-center uppercase mt-10",
};

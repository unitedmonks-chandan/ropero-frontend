import HeroNewCarousel from "../../HeroNewCarousel/HeroNewCarousel";
import hero1 from "../../../public/assets/images/lerida/hero.jpg";
import logo1 from "../../../public/assets/svg/lerida/logo.svg";
import logo2 from "../../../public/assets/svg/lerida/logo2.svg";

export default {
  title: "Roperro / Lerida / Hero",
};

const Template = (args) => <HeroNewCarousel {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  customStyle:
    "relative h-screen hide-inbuilt-arrow hide-left-hidden-arrow right-white-arrow2 white-pagination-bullet golden-pagination-bullet-active lerida-hero-slider",
  carouselData: [
    {
      image: hero1,
      btnData: {
        position:
          "absolute lerida-hero-btn-position z-50 flex flex-col gap-8 items-center justify-center",
        logo: logo1,
        logoSize: "slider-logo-size",
        btnLink: "/lerida",
        btn: "bg-white text-black text-lg font-primary px-7 py-1 uppercase hidden",
        btnText: "Shop now",
      },
    },
    {
      image: hero1,
      btnData: {
        position:
          "absolute lerida-hero-btn-position z-50 flex flex-col gap-8 items-center justify-center",
        logo: logo2,
        logoSize: "slider-logo-size",
        btnLink: "/lerida/view-all",
        btn: "bg-midRed text-white text-lg font-primary px-7 py-1 uppercase hidden",
        btnText: "View all",
      },
    },
  ],
};

import ImageBtnIconsCarousel from "../../ImageBtnIconsCarousel/ImageBtnIconsCarousel";
import img1 from "../../../public/assets/images/lerida/discount.jpg";
import img2 from "../../../public/assets/images/lerida/discount.jpg";
import heartSvg from "../../../public/assets/svg/red-heart-fill.svg";
import bagSvg from "../../../public/assets/svg/bag.svg";

export default {
  title: "Roperro / Lerida / Discount",
};

const Template = (args) => <ImageBtnIconsCarousel {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  customSlidesPerView: 1,
  customSlidesPerGroup: 1,
  customSpaceBetween: 0.1,
  customStyle:
    " relative h-screen hide-inbuilt-arrow hide-left-hidden-arrow right-white-arrow2 hide-pagination-bullets lerida-discount-slider",
  carouselData: [
    {
      customSlide: "flex",
      btnIcons: "absolute left-52 bottom-56",
      image: img1,
      btnData: {
        container: "inline-flex flex-col items-start gap-8",
        position: "",
        headingStyle: "text-white font-primary text-left text-7xl",
        heading1: "HOUSE OF LERIDA",
        heading1Style: "text-xl font-medium uppercase mb-2",
        heading2: "Best of",
        heading3: "Jewellery",
        heading4: "50% off",
        heading4Style: "text-lightOrange font-roboto mt-4",
        btnLink: "/view-all",
        btn: "hidden",
      },
    },
    {
      customSlide: "flex",
      btnIcons: "absolute left-52 bottom-56",
      image: img1,
      btnData: {
        container: "inline-flex flex-col items-start gap-8",
        position: "",
        headingStyle: "text-lightOrange font-primary text-left text-7xl",
        heading1: "HOUSE OF LERIDA",
        heading1Style: "text-xl font-medium uppercase mb-2",
        heading2: "Best of",
        heading3: "Jewellery",
        heading4: "49% off",
        heading4Style: "text-white font-roboto mt-4",
        btnLink: "/view-all",
        btn: "hidden",
      },
    },
  ],
};

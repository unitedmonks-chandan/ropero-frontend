// import Carousel from '../Carousel';
// import RoperroLink from '../RoperroLink';
// import RoperoTextLogo from '../Typography/RoperoTextLogo';


// const Hero = (props) => {


//     const { carousel, container, link, logoData, tailwind, css } = props;


//     return (
//         <div className={`${tailwind} ${css}`}>
//             <Carousel images={carousel.images} customCss={carousel.customCss} />
//             <div className={container}>
//                 <RoperoTextLogo
//                     svg={logoData.svg}
//                     tailwindCss={logoData.tailwindCss}
//                     iconTailwindCss={logoData.iconTailwindCss}
//                     customCss={logoData.customCss}
//                 />

//                 <RoperroLink
//                     text={link.text}
//                     tailwind={link.tailwind}
//                     css={link.css}
//                     linkHref={link.linkHref}
//                 />
//             </div>
//         </div>
//     );
// };

// export default Hero;


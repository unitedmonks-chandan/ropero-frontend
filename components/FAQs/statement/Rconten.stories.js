import RightConten from "./index"
import arrow from "../../../public/assets/images/FAQs/Vector.png"

export default {
    title: "Faqs/Right"
}

const Template = argument => <RightConten {...argument} />
export const rightConten = Template.bind({})
rightConten.args = {
    downArrow: arrow,
    ques_text: "What Payments Are Accepted?",
    ans_text: "VISA, Mastercard, American Express, JCB, Discover, UnionPay, PayPal and Klarna Your billing address must match the information associated with your payment method. Please contact your financial institution for assistance if your payment is declined."
}
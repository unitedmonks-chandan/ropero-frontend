
import React, { useState } from 'react'

const RightConten = ({ downArrow, ques_text, ans_text }) => {
    const [show, setShow] = useState(true);
    return (<>

        <div className='child_right-ques py-8 lg:mx-0 md:pr-14 2xl:mx-5 text-left flex'>
            <p className='text-[25]'>{ques_text} <button className='w-6 md:h-3 xl:h-4 h-4 mt-1 ml-1' onClick={() => setShow(!show)} ><img className='w-6 md:h-3 xl:h-4 h-4 relative top-1 ml-1' src={downArrow} alt='arrow' /></button></p>

        </div>

        <div className='child_right-ans lg:mx-0 xl:mx-0 2xl:mx-6'>
            {show ?
                <div className='text-left md:pr-14 lg:pr-0 xl:pr-28 2xl:pr-80'>
                    <span className=' text-[20]'> {ans_text}</span>
                </div> : null
            }
        </div>
    </>
    )
}

export default RightConten;

import SideNavLink from '../../Typography/SideNavLink';
import { useState } from 'react';

const SideNav = (props) => {

    const { tailwind, css, heading, linksContainer } = props;
    const { links, defaultSelectedLink } = linksContainer;

    const [selected, setSelected] = useState(defaultSelectedLink);


    const renderLinks = () => {
        return links.map((item, i) => <SideNavLink
            key={i}
            tailwind={item.tailwind}
            link={item.link}
            icon={item.icon}
            active={item.link.text === selected}
            onClick={() => setSelected(item.link.text)}
        />
        )
    }

    return (
        <div className={`${tailwind} ${css}`}>


            <div className={`${linksContainer.tailwind} ${linksContainer.css}`}>

                {renderLinks()}

            </div>
        </div>
    );
};
export default SideNav;
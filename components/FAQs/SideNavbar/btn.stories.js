
import SideNav from "./index"

import arrow from "../../../public/assets/svg/RightArrowBlackSVGIcon.svg"

export default {
    title: "faqs/button"
}
const Template = argument => <SideNav {...argument} />
export const Item = Template.bind({});

Item.args = {

    tailwind: 'bg-colorGray98 pl-28',
    css: 'width-486px pt-12',

    heading: {
        text: 'Order Status',
        tailwind: 'text-3xl text-colorBlack21 font-semibold pb-12 border-b border-solid border-colorGray69 text-right pr-24',
        css: ''
    },

    linksContainer: {
        tailwind: 'pr-12 mt-12 ml-16 pb-12',
        css: '',

        links: [
            {
                tailwind: ' flex items-baseline justify-between mb-11',

                link: {
                    text: 'Delivered',
                    linkHref: '/',
                    tailwind: ' text-2xl font-light text-colorGray24',
                    css: '',
                    onClickCss: 'color-golden45 bold600'
                },

                icon: {
                    svg: arrow,
                    tailwind: ' w-3 h-5',
                }
            },

            {
                tailwind: ' flex items-baseline justify-between mb-11',

                link: {
                    text: 'In Transit',
                    linkHref: '/',
                    tailwind: ' text-2xl font-light text-colorGray24',
                    css: '',
                    onClickCss: 'color-golden45 bold600'
                },

                icon: {
                    svg: arrow,
                    tailwind: ' w-3 h-5',
                }
            },


            {
                tailwind: ' flex items-baseline justify-between mb-11',

                link: {
                    text: 'Cancelled',
                    linkHref: '/',
                    tailwind: ' text-2xl font-light text-colorGray24',
                    css: '',
                    onClickCss: 'color-golden45 bold600'
                },

                icon: {
                    svg: arrow,
                    tailwind: ' w-3 h-5',
                }
            },


            {
                tailwind: ' flex items-baseline justify-between',

                link: {
                    text: 'Returned',
                    linkHref: '/',
                    tailwind: ' text-2xl font-light text-colorGray24',
                    css: '',
                    onClickCss: 'color-golden45 bold600'
                },

                icon: {
                    svg: arrow,
                    tailwind: ' w-3 h-5',
                }
            },
        ]

    }
}
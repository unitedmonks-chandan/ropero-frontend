import React, { useState } from 'react'

const RightConten1 = ({ downArrow1, ques_text, ans_text }) => {
    const [show, setShow] = useState(true);
    return (<>

        <div className='child_right-ques1 py-8 lg:mx-0 md:pr-14 2xl:mx-5 text-left flex'>
            <p className='text-[25] flex'>{ques_text}

            </p>
            <button onClick={() => setShow(!show)} ><img className='w-6 h-4 mt-1 ml-1' src={downArrow1} alt='arrow' /></button>
        </div>

        <div className='child_right-ans1 lg:mx-0 xl:mx-0 2xl:mx-6'>
            {
                show ?
                    <div className='text-left md:pr-14 lg:pr-0 xl:pr-28 2xl:pr-80'>
                        <span className=' text-[20]'> {ans_text}</span>
                    </div> : null
            }
        </div>
    </>
    )
}

export default RightConten1;
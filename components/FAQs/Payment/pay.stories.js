import PaymentInfo from "./index"

export default {
    title: "FAQS/payment"
}
const Tempalte = argument => <PaymentInfo {...argument} />
export const payment = Tempalte.bind({})

payment.args = {
    props: {
        ques1: "What Payments Are Accepted?",
        ans1: "VISA, Mastercard, American Express, JCB, Discover, UnionPay, PayPal and Klarna Your billing address must match the information associated with your payment method. Please contact your financial institution for assistance if your payment is declined.",
        ques2: "I’ve Already Paid For The Order But It Hasn’t Been Shipped Yet",
        ans2: "Standard orders and pre-orders are debited only at the time of shipment. You may see a temporary authorization on your account for up to $1, but we do not collect payment until the product has been shipped. Made-to-Order products require payment in advance and you will be debited at the time of order placement.",
        ques3: "I’ve cancelled The Order But I Haven’t Received Credit",
        ans3: "If the order has never been shipped and you cancelled it before shipping, the amount has not been debited to your credit card but only authorized by your bank. No charges have been made as the products are charged only at the time of shipment.",
        ques4: "What Is Roperro ?",
        ans4: "Roperro allows customers to pay for their purchases over time. Orders placed with Roperro will show up as “Roperro” on your credit or debit card statements. Through Roperro, you may pay for most items on Ferragamo.com on bi-weekly basis, excluding pre-orders, custom or personalized items. Items may not be less than $100 or exceed $10,000. Use of Roperro is subject to credit check and approval by Roperro. A down payment may be required. Payment options may vary based on your purchase. Roperro loans are made by WebBank, member FDIC. CA residents: Loans made or arranged pursuant to a California Finance Lenders Law license.",
    }
}
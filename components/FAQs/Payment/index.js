import React from 'react'
import RightConten from '../statement/index';
import RightConten1 from '../statement1/index';

const PaymentInfo = (props) => {
    console.log(props);
    const { ques1, ans1, ques2, ans2, ques3, ans3, ques4, ans4 } = props.props;
    return (
        <div>

            <RightConten
                ques_text={ques1}
                ans_text={ans1} />

            <RightConten
                ques_text={ques2}
                ans_text={ans2} />

            <RightConten
                ques_text={ques3}
                ans_text={ans3} />

            <RightConten1
                ques_text={ques4}
                ans_text={ans4}
            />
        </div>
    )
}

export default PaymentInfo;

const Frequently = ({ conten_text }) => {

    return (
        <div className='Conten_container h-full md:mx-0 lg:mx-0  xl:pr-0 2xl:mx-0'>
            <div className='child_conten-contain py-8 text-left'>
                <span className='md:text-base lg:text-xl xl:text-xl'>{conten_text}</span>
            </div>

        </div >
    )
}

export default Frequently;
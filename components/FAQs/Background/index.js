import React from 'react'
// import "./Faqsbg.css"

const FaqsBg = (props) => {
    const { logo } = props.props;
    const list = ['Our Customer Care is available to answer your questions and can be reached via',
        'Phone: 1 - 866 - 337 - 7242 | Email: customercare@roperro.com',
        'Monday - Friday 9 am - 9 pm(IST), excluding holidays.'];

    const Textlist = () => {
        return list.map((item, i) => {
            return (<div key={i}>
                {item}
            </div>
            )

        })
    }
    return (
        <div className='faqs_container w-full h-full'>
            <div className='flex justify-center align-center pt-24 faqs_conten'>
                <img src={logo} className='w-36' />
            </div>
            <div>
                <div className='text-center md:px-16 xl:py-14 lg:px-52 xl:mx-28 2xl:mx-32 faqs_para text-[18]'>
                    <span className='block'>{Textlist()}</span>
                </div>
            </div>
        </div >
    )
}

export default FaqsBg;
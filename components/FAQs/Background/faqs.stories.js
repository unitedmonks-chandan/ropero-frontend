import FaqsBg from "./index"
import logo from "../../../public/assets/images/FAQs/Group.png"
export default {
    title: "FAQS/bg"
}
const Template = argument => <FaqsBg {...argument} />
export const FAQSlogo = Template.bind({})
FAQSlogo.args = {
    props: {
        logo: logo,
        info_text: "Our Customer Care is available to answer your questions and can be reached via: ",
        info_tex2: "Phone: 1 - 866 - 337 - 7242 | Email: customercare@roperro.com",
        info_tex3: "Monday - Friday 9 am - 9 pm(IST), excluding holidays."
    }

}
import React, { useState } from 'react';
import FaqsBg from "./Background/index";
import Frequently from "./conten/index";
import RightConten from "./statement/index";
import RightConten1 from "./statement1/index";

import SideNav from "./SideNavbar/index"


const FaqsPage = (props) => {
    const { imglogo, container, HeadNav, tailwind, css, linksContainer, arrow, arrow1,
        ques1, ans1, ques2, ans2, ques3, ans3, ques4, ans4 } = props.props;


    console.log(props);

    const [show, setShow] = useState(false);

    return (
        <>
            {/* <FaqsBg logo={imglogo} /> */}

            <div className={container}>
                <div>
                    <div className='Conten_container text-center' >
                        <Frequently
                            conten_text={HeadNav}
                        />
                        <div className='flex '>

                            {/* <button onClick={() => setShow(!show)} > */}

                            <SideNav
                                tailwind={tailwind}
                                css={css}
                                linksContainer={linksContainer} />
                            {/* </button> */}

                        </div>
                    </div>
                </div>
                <div className='py-12'>
                    {/* {
                        show ? <div> */}
                    <RightConten
                        downArrow={arrow}
                        ques_text={ques1}
                        ans_text={ans1} />
                    <RightConten
                        downArrow={arrow}
                        ques_text={ques2}
                        ans_text={ans2} />
                    <RightConten
                        downArrow={arrow}
                        ques_text={ques3}
                        ans_text={ans3} />
                    <RightConten1
                        downArrow1={arrow1}
                        ques_text={ques4}
                        ans_text={ans4}
                    />
                    {/* </div> : null
                    } */}
                </div>
            </div>
        </>
    )
}

export default FaqsPage;
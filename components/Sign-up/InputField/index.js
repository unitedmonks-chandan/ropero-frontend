import React from 'react'

const InputField = ({ labelText }) => {
    return (
        <div className='flex justify-around z-10 '>

            <div className=" w-full md:w-1/1  px-2 SignUpInput ">
                <label
                    className="block mt-10 mb-4 text-sm md:text-xs lg:text-xs xl:text-xl font-light"
                    htmlFor="">{labelText}</label>
                <input className="w-full px-3 h-10 bg-transparent" type="text" id="email" />

            </div>
        </div>
    )
}

export default InputField;
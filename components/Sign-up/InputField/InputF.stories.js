import InputField from "."

export default {
    title: "Sign-up/input"
}
const Template = (argument) => <InputField {...argument} />
export const inputText = Template.bind({})
inputText.args = {
    labelText: "Email*"
}
import React from 'react'

const Button = ({ btn_text }) => {
    return (
        <div className="flex  md:mx-2 lg:mx-2 signUp_btn">
            <div className="w-full">
                <button
                    className="mt-10 text-white  border-0 px-10 
                text-sm md:text-xl lg:text-xl xl:text-xl">{btn_text} </button>
            </div>
        </div>
    )
}

export default Button
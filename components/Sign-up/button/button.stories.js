import Button from "./index"

export default {
    title: "Sign-up/button"
}
const Template = (argument) => <Button {...argument} />
export const button = Template.bind({})
button.args = {
    btn_text: "SUBMIT"
}
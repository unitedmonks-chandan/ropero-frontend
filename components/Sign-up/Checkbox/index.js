import React from 'react'

const CheckBox = ({ labelText }) => {
    return (
        <div className="Contact-input_group sm:my-5 md:my-8 lg:my-4 xl:my-7 2xl:my-7 ">
            <div className="child_item">
                <label className="content sm:text-xs text-sm md:text-xs lg:text-xs xl:text-sm">
                    <span className="sp1 w-28 sm:text-xs text-sm md:text-xs lg:text-xs xl:text-sm font-light">{labelText}</span>
                    <input className='' type="radio" name='option' />
                    <span className="checkmark"></span>
                </label>
            </div>
        </div>
    )
}

export default CheckBox
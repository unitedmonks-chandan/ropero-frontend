import React from 'react'

const Heading = ({ Heading_text }) => {
    return (
        <div className=' w-full signup-heading_sign flex justify-center py-10'>
            <h1 className='text-4xl'>{Heading_text}</h1>
        </div>
    )
}

export default Heading;
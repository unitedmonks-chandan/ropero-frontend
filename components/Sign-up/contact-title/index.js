import React from 'react'

const Contact = ({ contact_text }) => {
    return (
        <div className=' w-full signUp_contact-Text'>
            <h6 className='text-xl'>{contact_text}</h6>
        </div>

    )
}

export default Contact
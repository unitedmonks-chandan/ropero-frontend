import Contact from "./index"

export default {
    title: "Sign-up/contact-us"
}
const Template = (argument) => <Contact {...argument} />
export const contacts = Template.bind({})
contacts.args = {
    contact_text: "Contact Details"
}
import SignUp from "./index"

export default {
    title: "Sign-Up/Main-page"
}
const Template = (argument) => <SignUp {...argument} />

export const signUp = Template.bind({})
signUp.args = {
    props: {
        signtext: "SIGN UP",
        container: "md:mx-16 lg:mx-32 xl:mx-52 2xl:mx-56",
        child_signUp: " grid grid-cols-2 md:gap-x-20 lg:gap-x-40 2xl:gap-x-48",
        contact_details: "Contact Details",
        name: "Name",
        lastName: "Last Name",
        phone: "Phone",
        email_address: "Email Address",
        container2: "md:mx-16 lg:mx-32 xl:mx-52 2xl:mx-56 mt-20 py-14 bg-gray-100 ",
        address_details: "Address Details",
        address_line1: "Address line 1*",
        city: "City",
        phonecode: "Pincode",
        address_line2: "Address line 2*",
        state: "State",
        checkdiv: "grid grid-cols-2 mx-2 mt-20 lg:mt-20 md:mt-16 ",
        homeads: "Home Address",
        officeadd: "Office Address",
        submit: "SUBMIT"
    }

}
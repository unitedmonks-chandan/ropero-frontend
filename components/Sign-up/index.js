import React from 'react';
import Heading from "./title/index";
import Contact from './contact-title/index';
import InputField from "./InputField/index";
import CheckBox from "./Checkbox/index";
import Button from "./button/index"
const SignUp = (props) => {

    const { signtext, container, child_signUp, contact_details, address_details, name, phone,
        lastName, email_address, container2, address_line1, city, phonecode, address_line2, state, checkdiv,
        homeads, officeadd, submit } = props.props;
    return (<>
        <Heading Heading_text={signtext} />

        <div className={container}>
            <div className='ml-2'>
                <Contact contact_text={contact_details} />
            </div>
            <div className={child_signUp}>

                <div>
                    <InputField labelText={name} />
                    <InputField labelText={phone} />
                </div>

                <div>
                    <InputField labelText={lastName} />
                    <InputField labelText={email_address} />
                </div>

            </div>
        </div>

        <div className=' bg-gray-100'>
            <div className={container2}>
                <div className='ml-2'>
                    <Contact contact_text={address_details} />
                </div>
                <div className={child_signUp}>

                    <div>
                        <InputField labelText={address_line1} />
                        <InputField labelText={city} />
                        <InputField labelText={phonecode} />
                    </div>

                    <div>
                        <InputField labelText={address_line2} />
                        <InputField labelText={state} />
                        <div className={checkdiv}>
                            <div>
                                <CheckBox labelText={homeads} />
                            </div>
                            <div>
                                <CheckBox labelText={officeadd} />
                            </div>
                        </div>

                    </div>

                </div>
                <Button btn_text={submit} />
            </div>

        </div>
    </>
    )
}

export default SignUp;
import HeadingPara from "./HeadingPara";

export default {
  title: "Heading and Paragraph for Contact Us",
};

const Template = (args) => <HeadingPara {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  headingText: "Get In Touch",
  paraText: "We will respond to every email within 24 hours, Monday to Friday, excluding holidays. You can also call us at our toll-free number +0000-000-0000.",
};

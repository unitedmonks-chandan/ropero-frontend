import React from "react";
// import './headingpara.css';

function HeadingPara({
  headingText,
  paraText,
}) {
  return (
    <div className="bg000">

        <h1 className="headingStyle">{headingText}</h1>
        <p className="paraStyle">{paraText}</p>

    </div>
  );
}

export default HeadingPara;

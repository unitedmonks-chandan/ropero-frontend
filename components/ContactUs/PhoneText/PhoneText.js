import React from "react";
import './phonetext.css';

function PhoneText({
  image,
  phoneText1,
  phoneText2,
  phoneHeading
}) {
  return (
    <div>


    <div className="bg000">
                          <div className="md:flex md:items-start md:-mx-4">
                              <span className="img-span">
                              <img src={image} alt="" className="PhoneIcon" />
                              </span>
  
                              <div className="mt-4 md:mx-4 md:mt-0">
                                  <h3 className="phone-head">{phoneHeading}</h3>
  
                                  <p className="phone-text">
                                  {phoneText1}<br/>
                                  {phoneText2}
                                  </p>
                                  
                              </div>
                          </div>
                        
            </div>


  </div>


  );
}

export default PhoneText;

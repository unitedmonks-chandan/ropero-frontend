import PhoneText from "./PhoneText";
import image1 from "../../../assests/images/phone-cs.svg";
export default {
  title: "Phone Text",
};

const Template = (args) => <PhoneText {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  image: image1,
  phoneText1:"1800 0000 000",
  phoneText2:"1800 0000 000",
  phoneHeading:"Phone"
};

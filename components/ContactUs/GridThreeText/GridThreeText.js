import React from "react";

function GridThreeText({
  spacing,
  display,
  subHeadingFont,
  subHeading,
  headingFont,
  heading1,
  heading2,
  paraFont,
  para,
}) {
  return (
    <div className={spacing}>
      <div>
        <p className={subHeadingFont}>{subHeading}</p>
        <p className={headingFont}>{heading1}</p>
      </div>
      <div className={display}>
        <p className={headingFont}>{heading2}</p>
        <p className={paraFont}>{para}</p>
      </div>
    </div>
  );
}

export default GridThreeText;

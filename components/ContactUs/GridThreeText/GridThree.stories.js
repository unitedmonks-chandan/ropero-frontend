import GridThree from "./GridThreeText";

export default {
  title: "Grid Three With Text & Logo",
};

const Template = (args) => <GridThree {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  spacing: "py-16 pl-12 pr-60 2xl:pr-64",
  display:
    "grid grid-cols-1 xl:grid-cols-2 xl:max-w-screen-lg gap-10 xl:gap-14",
  subHeadingFont: "text-xl font-primary",
  subHeading: "HOUSE OF RO",
  headingFont: "text-red-500 text-4xl uppercase font-primary",
  heading1: "New",
  heading2: "Arrivals",
  spanDisplay: "block",
  paraFont: "text-lg font-primary self-center",
  para: "The online store presents selected suc- cessful collections, which is sewn exclusively from quality material.",
};

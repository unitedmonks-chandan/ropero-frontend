import BannerMain from "./BannerMain";
import image1 from "../../../assests/images/customer-s-bg.svg";
import image2 from "../../../assests/images/cs-text.svg";

export default {
  title: "Banner for Customer Service Page",
};

const Template = (args) => <BannerMain {...args} />;

export const withoutArrow = Template.bind({});

withoutArrow.args = {
  image: image1,
  imagetext: image2,
};

import Image from 'next/image';
import MenuItem from './MenuItem';
import Navbar from './Navbar';
import NavIconButtons from './NavIconButtons';
import NavItem from './NavItem';



const Header = (props) => {

    const { menuItems, iconsNav } = props.props;
    return (
        <div className="header">
            <div className="header__content">
                <div className="header__content__logo">
                    <Navbar menuItems={menuItems} iconsNav={iconsNav} />
                </div>
            </div>
        </div>
    );
};

export default Header;


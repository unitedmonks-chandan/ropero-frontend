import Link from "next/link";
import MenuItem from '../MenuItem';


const NavItem = ({ navItems }) => {

    const renderNavItems = () => {
        return (
            navItems?.map(navItem => {
                return <li
                    className="navbar__list-item" key={navItem.name}>
                    <Link href={navItem.menuItemNavigation}>
                        <a>
                            {navItem.name}
                        </a>
                    </Link>
                    <MenuItem menu={navItem} />
                </li>
            })
        );
    };

    return (
        <>

            {renderNavItems()}
        </>
    );
};

export default NavItem;



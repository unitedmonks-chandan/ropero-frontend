import React, { useState } from 'react';
import Link from 'next/link';
import { ReactSVG } from 'react-svg';
import Logo from '../../Logo/index';
import NavIconButtons from '../NavIconButtons';
import NavItem from '../NavItem/index';
import MenuBarsSVGIcon from '../../../svgs/MenuBarsSVGIcon.svg';
import MenuCloseSVGIcon from '../../../svgs/MenuCloseSVGIcon.svg';


const Navbar = ({ menuItems, iconsNav }) => {

    const [NavIcons, setNavIcons] = useState(iconsNav);

    // CSS class controls the menu icon in mobile view bars or X 
    const [menuIcon, setMenuIcon] = useState(MenuBarsSVGIcon);

    // CSS class show and hide the menu items in mobile view when bars icon or times icon is clicked 
    const [isActive, setIsActive] = useState('inactive');


    const menu = () => {
        // Changes the menu icon states 
        menuIcon === MenuBarsSVGIcon ? setMenuIcon(MenuCloseSVGIcon) : setMenuIcon(MenuBarsSVGIcon);
        // Show and hide the menu items
        isActive === 'inactive' ? setIsActive('active') : setIsActive('inactive');
    };

    const navItems = menuItems;


    return (
        <div className="navbar" >
            <div className="navbar__logo-and-icons-container">
                {/* click changes the state of menu icon bars <=> X and show/hide the menu item  */}
                <div className="navbar__icon navbar__icon-menu icon__large" onClick={menu} >
                    <ReactSVG src={menuIcon} />
                </div>

                <div className="navbar__logo-box">
                    <Link href="/">
                        <a>
                            <Logo size="logo logo__large" />
                            <Logo size="logo logo__small" />
                        </a>
                    </Link>
                </div>

                <div className="navbar__icons-container" >
                    <NavIconButtons
                        icons={NavIcons}
                        btn="icon btn__icon__white"
                        size="btn__icon__medium"
                    />
                </div>
            </div>


            <div className={`navbar__items-container ${isActive}`}>
                <div className="navbar__items">
                    {/*Navbar items like Apparel, Home, Bags etc and their submenu  */}
                    <NavItem
                        navItems={navItems}
                    />
                </div>
            </div>
        </div>
    );
};

export default Navbar;
import React from 'react';
import Navbar from './index';

import BookmarkSVGIcon from '../../../svgs/BookmarkSVGIcon.svg';
import ShoppingBagSVGIcon from '../../../svgs/ShoppingBagSVGIcon.svg';
import UserSVGIcon from '../../../svgs/UserSVGIcon.svg';
import NotificationBellSVGIcon from '../../../svgs/NotificationBellSVGIcon.svg';

export default {
    title: 'Header',
    component: Navbar
}

const Template = arguments_ => <Navbar {...arguments_} />
export const NavigationBar = Template.bind({})

NavigationBar.args = {    
    iconsNav: [
        {icon: UserSVGIcon, mobileView: 'hide',
            NavbarIconNavigation: '/Login'
        },
        {icon: BookmarkSVGIcon, num: 10, mobileView: 'show',
            NavbarIconNavigation: '/Bookmarks' 
        }, 
        {icon: ShoppingBagSVGIcon, num: 17, mobileView: 'hide',
            NavbarIconNavigation: '/Cart'
        },
        {icon: NotificationBellSVGIcon, notification: true,
            mobileView: 'show', 
            NavbarIconNavigation: '/Notification'
        }
    ],


    menuItems: [
        {
            name:'Apparel',
            menuItemNavigation: '/Apparel',
            subMenu: {
                altTag: 'Apparel',
                img: 'https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG',
                subMenuItems: [
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    }
                ]
            }
        },
        {
            name: 'Home',
            menuItemNavigation: '/',
            subMenu: {
                altTag: 'Home',
                img: 'https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG',
                subMenuItems: [
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    }
                ]
    
            }
        },
        {
            name: 'Bags',
            menuItemNavigation: '/Bags',
            subMenu: {
                altTag: 'Bags',
                img: 'https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG',
                subMenuItems: [
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    },
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    },
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    },
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    }
                ]
                
            }
        }, 
        {
            name: 'Gifting',
            menuItemNavigation: '/Gifting',
            subMenu: {
                altTag: 'Gifting',
                img: 'https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG',
                subMenuItems: [
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    },
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    }
                ]
                 
            }
        }, 
        {
            name:'Accessories',
            menuItemNavigation: '/Accessories',
            subMenu: {
                altTag: 'Accessories',
                img: 'https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG',
                subMenuItems: [
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    },
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    }
                ]
                 
            }
        }, 
        {
            name: 'Jewelery',
            menuItemNavigation: '/Jewelery',
            subMenu: {
                altTag: 'Jewelery',
                img: 'https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG',
                subMenuItems: [
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    },
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    }
                ]
                 
            }
        }, 
        {
            name:'Travel',
            menuItemNavigation: '/Travel',
            subMenu: {
                altTag: 'Travel',
                img: 'https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG',
                subMenuItems: [
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    },
                    {   
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png',
                        item: 'Vanity Pouch',
                        subMenuItemNavigation: 'Bags/VanityPouch'
                        
                    },
                    {
                        itemImg: 'https://tann.gumlet.io/media/gen/image/Tote_Unselected.png',
                        item: 'Toto Bag',
                        subMenuItemNavigation: 'Bags/ToteBag'
                    }
                ]
                 
            }
        }
    ],
}
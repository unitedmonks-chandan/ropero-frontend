import React from "react";

function AboutUsHero({
  container,
  image,
  imgSize,
  position,
  heading1,
  heading1Style,
  heading2,
  heading2Style,
}) {
  return (
    <div className={container}>
      <img src={image} alt="hero" className={imgSize} />
      <div className={position}>
        <p className={heading1Style}>{heading1}</p>
        <p className={heading2Style}>{heading2}</p>
      </div>
    </div>
  );
}

export default AboutUsHero;

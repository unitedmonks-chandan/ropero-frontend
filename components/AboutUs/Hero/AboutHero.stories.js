import AboutUsHero from "./AboutUsHero";

export default {
  title: "Roperro / About us / Hero",
};

import heroImage from "../../../public/assets/images/about-us/hero.jpg";

const Template = (args) => <AboutUsHero {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  container: "relative w-full",
  image: heroImage,
  position: "absolute perfect-center text-center",
  heading1: "About",
  heading1Style: "text-6xl text-white font-primary font-light uppercase",
  heading2: "Roperro",
  heading2Style: "text-9xl text-white font-cinzel uppercase tracking-widest",
};

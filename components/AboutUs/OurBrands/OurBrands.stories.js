import OurBrands from "./OurBrands";
import brand1 from "../../../public/assets/images/about-us/brand1.jpg";
import brand2 from "../../../public/assets/images/about-us/brand2.jpg";
import brand3 from "../../../public/assets/images/about-us/brand3.jpg";
import brand1Logo from "../../../public/assets/svg/zimba/logo.svg";
import brand2Logo from "../../../public/assets/svg/monadaa.svg";
import brand3Logo from "../../../public/assets/svg/lerida/logo2.svg";

export default {
  title: "Roperro / About us / Our Brands",
};

const Template = (args) => <OurBrands {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  container: "bg-black py-20 px-16",
  textData: {
    container: "text-center",
    heading1: "FROM THE HOUSE OF ROPERRO",
    heading1Style: "text-white text-2xl font-primay uppercase mb-4",
    heading2: "Our Brands",
    heading2Style: "text-hoverYellow text-7xl font-cinzel uppercase mb-16",
  },
  carouselData: {
    customSlidesPerView: 3,
    customSlidesPerGroup: 3,
    customSpaceBetween: 0.01,
    customStyle:
      "hide-inbuilt-arrow left-white-arrow right-white-arrow white-pagination-bullet golden-pagination-bullet-active roperro-brands-slider reset-swiper-container reset-swiper-slide px-28 pb-20 relative",
    carouselData: [
      {
        image: brand1,
        btnData: {
          position: "absolute brands-logo-position z-50",
          logo: brand1Logo,
          logoSize: "slider-logo-size",
          btnLink: "/",
          btn: "hidden",
          btnText: "hidden",
        },
      },
      {
        image: brand2,
        btnData: {
          position: "absolute brands-logo-position z-50",
          logo: brand2Logo,
          logoSize: "slider-logo-size",
          btnLink: "/",
          btn: "hidden",
          btnText: "",
        },
      },
      {
        image: brand3,
        btnData: {
          position: "absolute brands-logo-position z-50",
          logo: brand3Logo,
          logoSize: "slider-logo-size",
          btnLink: "",
          btn: "hidden",
          btnText: "",
        },
      },
      {
        image: brand2,
        btnData: {
          position: "absolute brands-logo-position z-50",
          logo: brand2Logo,
          logoSize: "slider-logo-size",
          btnLink: "/",
          btn: "hidden",
          btnText: "hidden",
        },
      },
      {
        image: brand3,
        btnData: {
          position: "absolute brands-logo-position z-50",
          logo: brand3Logo,
          logoSize: "slider-logo-size",
          btnLink: "/",
          btn: "hidden",
          btnText: "",
        },
      },
      {
        image: brand1,
        btnData: {
          position: "absolute brands-logo-position z-50",
          logo: brand1Logo,
          logoSize: "slider-logo-size",
          btnLink: "",
          btn: "hidden",
          btnText: "",
        },
      },
    ],
  },
};

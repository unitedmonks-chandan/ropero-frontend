import React from "react";
import MultipleHeading from "../../MultipleHeading/MultipleHeading";
import HeroNewCarousel from "../../HeroNewCarousel/HeroNewCarousel";

function OurBrands({ container, textData, carouselData }) {
  return (
    <div className={container}>
      <MultipleHeading {...textData} />
      <HeroNewCarousel {...carouselData} />
    </div>
  );
}

export default OurBrands;

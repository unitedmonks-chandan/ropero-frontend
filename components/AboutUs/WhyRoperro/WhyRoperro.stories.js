import WhyRoperro from "./WhyRoperro";
import logo from "../../../public/assets/svg/roperro-golden-logo.svg";
import image from "../../../public/assets/images/about-us/why-roperro.jpg";

export default {
  title: "Roperro / About us / Why Roperro",
};

const Template = (args) => <WhyRoperro {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  container: "bg-black py-28 pl-32",
  display: "flex gap-24 mb-24",
  logo,
  logoSize: "w-44",
  textData: {
    container: "pl-20 pr-40 left-golden-border relative",
    heading1:
      "Our wardrobe to us means more than a physical space for our clothes & accessories.It reflects who we are, our style, what de-fines us, ourselections&carefully picked pieces.Celebrating this part of our personality, is Roperro.",
    heading1Style: "text-gray300 text-2xl  font-primay mb-4",
    heading2:
      "Presenting firstof its kind multibrande-commerce platform for your styleneeds only.Each piececarefully created & selected for you to be able tmake easychoices of what speaks‘You’the best.Right  from  bags  to  all   year  wear, Roperro  has  you covered.Whether it’s a  stylized  gifting box or something you desire for your own use, versatile,high-quality products. Something fromdaily to luxury,depending on what you desire you will find here.So, the next time you’re  looking for  quality &  style no need to hunt many  places  just to find  that  one piece,  drop by on our website and we promise you we will spoil you for options. If it is style, it is Roperro.",
    heading2Style: "text-gray300 text-2xl  font-primay mb-12",
    heading3: "The Best of Global Trends",
    heading3Style: "text-hoverYellow text-4xl font-cinzel uppercase",
    heading4: "Packed into one just for you",
    heading4Style: "text-hoverYellow text-4xl font-cinzel uppercase",
  },
  position: "relative",
  image,
  headingsBg:
    "bg-hoverYellow p-6 max-w-2xl absolute center-vertically -left-20",
  textData2: {
    container: "py-20 px-16 text-center border border-lightGolden",
    heading1: "Why",
    heading1Style: "text-lightGolden text-9xl font-cinzel uppercase -mb-14",
    heading2: "Roperro",
    heading2Style: "text-black text-7xl uppercase font-primary mb-12",
    heading3:
      "Our wardrobe to us means more than a physical space for our clothes & accessories. It reflects who we are, our style, what defines us, our selections & carefully picked pieces.",
    heading3Style: "text-white text-2xl font-primay mb-12",
    heading4:
      " Each piece carefully created & selected for you to be able to make easy choices of what speaks ‘You’ the best. So, the next time you’re looking for quality & style no need thunt many places just to find that one piece.",
    heading4Style: "text-white text-2xl font-primay",
  },
};

import React from "react";
import MultipleHeading from "../../MultipleHeading/MultipleHeading";
// import "./custom.css";

function WhyRoperro({
  container,
  display,
  logo,
  logoSize,
  textData,
  position,
  image,
  imgSize,
  headingsBg,
  textData2,
}) {
  return (
    <div className={container}>
      <div className={display}>
        <img src={logo} alt="logo" className={logoSize} />
        <MultipleHeading {...textData} />
      </div>
      <div className={position}>
        <img src={image} alt="" className={imgSize} />
        <div className={headingsBg}>
          <MultipleHeading {...textData2} />
        </div>
      </div>
    </div>
  );
}

export default WhyRoperro;

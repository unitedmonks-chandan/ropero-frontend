import DebitCard from "./index";
import VisaSVGIcon from '../../public/assets/svg/checkout/visa-svg-icon.svg';
import DotSVGIcon from '../../public/assets/svg/checkout/dot-svg-icon.svg';

export default {
    title: "Roperro/Payment Card"
}

const Template = args => <DebitCard {...args} />;

export const Visa = Template.bind({});

Visa.args = {
    tailwind: 'bg-white pt-6 pl-10 pb-10',
    css: 'width-488px debit-card-shadow',

    logo: {
        svg: VisaSVGIcon,
        tailwind: 'w-24 w-14 mb-2'
    }, 

    bankName: {
        text: 'Kotak Mahindra Debit Card',
        tailwind: 'text-xl font-medium text-colorGray30 mb-2.5'
    },

    cardNumber: {
        dot: {
            svg: DotSVGIcon,
            dotMargin: '4px',
            //  substract dot margin from dotGropMargin. 
            dotGroupMargin: '10px',
        },

        lastFourDigit: {
            digits: 7845,
            tailwind: 'text-xl text-colorGray30 font-medium'
        }
    },
    expiry: {
        expiryDate: '09/2024',
        tailwind: 'text-lg font-medium text-colorGray30  mt-3'
    },

    cvv: {
        tailwind: 'flex items-center mt-4',


        cvvInput: {



            tailwind: '',
            css: '',
            label: {
                tailwind: '', 
                text: ''
            },
            input: {
                required: true,
                placeholder: 'Enter CVV',
                type: 'number', 
                tailwind: 'bg-white text-xs text-colorGray30 pt-1.5 pb-1.5 pl-2.5 font-medium border border-solid border-colorBrown44  mr-20 w-20',
                css: ''
            }
        },
    
        cvvButton: {
            buttonText: 'What is CVV ?',
            tailwind: 'font-medium text-colorGray30',
            css: 'font-size-9px line-height-11px'
        }
    },

}
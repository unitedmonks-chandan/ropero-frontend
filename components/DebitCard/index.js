import { ReactSVG } from "react-svg";
import Para from '../Typography/Para';
import CardNumber from "../Typography/CardNumber";
import InputText from '../FormField/InputText';
import RoperroButton from '../RoperroButton';

const DebitCard = (props) => {

    const { tailwind, css, logo, bankName, cardNumber, expiry, cvv,  } = props;
    const {cvvInput, cvvButton} = cvv;

    return (
        <div className={`${tailwind} ${css}`}>
            <ReactSVG src={logo.svg} className={logo.tailwind} />
            <Para text={bankName.text} tailwind={bankName.tailwind} />
            <CardNumber
                dot={cardNumber.dot}
                lastFourDigit={cardNumber.lastFourDigit}
            />
            <Para text={`Expiry ${expiry.expiryDate}`} tailwind={expiry.tailwind} />

            <div className={cvv.tailwind}>
                <InputText
                    label={cvvInput.label}
                    input={cvvInput.input}
                />
                <RoperroButton buttonText={cvvButton.buttonText}
                    tailwind={cvvButton.tailwind}
                    css={cvvButton.css}
                />
            </div>
        </div>
    );
};


export default DebitCard;
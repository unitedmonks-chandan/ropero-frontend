import React from 'react'

const Statement3 = ({ text_statement3 }) => {
    return (
        <div className="shopping_text3 w-full mb-20">
            <p className='xl:mr-48 md:text-base lg:text-xl 2xl:text-xl 2xl:mr-96'>{text_statement3} </p>
        </div>
    )
}

export default Statement3
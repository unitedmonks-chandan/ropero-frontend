import React from 'react'

import Heading from "./heading/index";
import Statement1 from './statement1/index';
import Statement2 from './statement2/index';
import Statement3 from "./statement3/index";

const ShippingRest = (props) => {

    const { heading, statement_1, statement_2, statement_3 } = props.props;

    return (
        <div className="shopping_container mt-20 ml-28">
            <div>

                <Heading text_shooping={heading} />

                <Statement1 text_statement1={statement_1} />

                <Statement2 text_statement2={statement_2} />

                <Statement3 text_statement3={statement_3} />

            </div>
        </div>
    )
}

export default ShippingRest
import React from 'react';

const Statement1 = ({ text_statement1 }) => {
    return (
        <div className="shopping_text1 w-full ">
            <p className='lg:mr-20 xl:mr-44 md:text-base lg:text-xl 2xl:text-xl 2xl:mr-96 mb-8'> {text_statement1}</p>
        </div>
    )
}

export default Statement1;
import React from 'react';

const Heading = ({ text_shooping }) => {
    return (
        <div className="shopping_title">
            <h3 className='2xl:text-5xl'>{text_shooping}</h3>
        </div>
    )
}

export default Heading

const Statement2 = ({ text_statement2 }) => {
    return (
        <div className="shopping_text2 w-full">
            <p className='lg:mr-0 xl:mr-36 md:text-base lg:text-xl 2xl:text-xl 2xl:mr-80 mb-8'>{text_statement2} </p>
        </div>
    )
}

export default Statement2;
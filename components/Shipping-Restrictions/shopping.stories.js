import ShippingRest from "./index"

export default {
    title: "ShipingRestriction/Ui"
}
const Template = arguments_ => <ShippingRest {...arguments_} />

export const shopping = Template.bind({})

shopping.args = {
    props: {

        heading: "Shipping Restrictions",

        statement_1: " The delivery address of your order must match the country site in which you place your order. Orders made from a different country site cannot be made. Please select the correct country site for your order from the link in the top-left corner of every page.",

        statement_2: "At this time we are unable to ship orders to General Delivery, P.O. Boxes, Azores, Madeira, the Faéroér Islands, Greenland, Aland Island, the Holland Antilles, Poste Restante, Canary Islands, St. Pierre et Miquelon, Livigno, Channel Islands, Ceuta and Melilla. Orders made to any of these addresses will be cancelled.",

        statement_3: "Please note that dangerous goods like fragrances can’t be shipped to the markets listed below: Australia, Canada, China, Japan, New Zealand, Qatar, Taiwan, Turkey"
    }
}


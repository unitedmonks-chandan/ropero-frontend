import Heading from '../Typography/Heading';
import { ReactSVG } from 'react-svg';
import Para from '../Typography/Para';
import RoperroLink from '../RoperroLink';

const AboutMonadaa = (props) => {

    const { image, sideContainer, tailwind } = props.props;
    const { textBox } = sideContainer;
    const { heading, textLogo, para1, para2, link, logo } = textBox;


    return (
        <div className={tailwind}>
            <img src={image.imageSource} alt={image.altTag} className={image.tailwind} />


            <div className={`${sideContainer.tailwind} ${sideContainer.css}`}>


                <div className={textBox.tailwind}>
                    <Heading text={heading.text} tailwind={heading.tailwind} />
                    <ReactSVG src={textLogo.svg} className={textLogo.svg.tailwind} />
                    <Para text={para1.text} tailwind={para1.tailwind} />
                    <Para text={para2.text} tailwind={para2.tailwind} />
                    <RoperroLink text={link.text} linkHref={link.linkHref} tailwind={link.tailwind} css={link.css} />
                    <ReactSVG src={logo.svg} className={logo.tailwind} />
                </div>
            </div>
        </div>
    );
};

export default AboutMonadaa;
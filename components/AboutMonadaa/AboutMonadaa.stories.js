import AboutMonadaa from "./index";

import aboutMonadaaImage from '../../public/assets/images/aboutMonadaaImage.jpg';
import MonadaaLogoBrownSVGIcon from '../../svgs/Ropero/MonadaaLogoBrownSVGIcon.svg';
import MonadaaIconLogoSVGIcon from '../../svgs/Ropero/MonadaaIconLogoSVGIcon.svg';


export default {
    title: "Roperro/About"
}

const Template = args => <AboutMonadaa {...args} />;

export const Monadaa = Template.bind({});

Monadaa.args = {

    props: {
        tailwind: 'w-full relative',

        image: {
            imageSource: aboutMonadaaImage,
            altTag: 'Women in Monadaa jacket',
            tailwind: 'w-full'
        },
        
        sideContainer: {

            tailwind: 'absolute right-0	top-0 bg-colorDust h-full  w-1/3 flex items-end',
            css: '',
            
            textBox: {
                
                tailwind: ' mb-24 ml-24  pr-28 flex flex-col items-start justify-end',
                
                heading: {
                    text: 'about',
                    tailwind: 'uppercase text-6xl text-colorGolden87 font-cinzel'
                },
                
                textLogo: {
                    svg: MonadaaLogoBrownSVGIcon,
                },
                
                para1: {
                    text: "Monadaa is a vision, Monadaa is beauty. Driven by passion, deep rooted in qualityand perfection",
                    
                    tailwind: 'mt-8 mb-8 text-2xl text-white'
                },
                
                para2: {
                    text: "Experts from world over collaborate to build a design that is style & functionality.",
            
                    tailwind: 'mt-8 mb-14 text-2xl text-white'
                },
                
                link: {
                    text: 'know more',
                    linkHref: '/',
                    tailwind: 'w-64 h-16 font-medium bg-colorBlack0 text-white uppercase text-2xl mb-14 uppercase flex justify-center items-center',
                    css: ''
                },
                
                logo: {
                    svg: MonadaaIconLogoSVGIcon,
                    tailwind: 'w-40 h-60'
                }        
            }
        }
    }
    
}


import NewCarousel from "./NewCarousel";

import img1 from "../../public/assets/images/arkar/new-stylesy1.jpg";
import img2 from "../../public/assets/images/arkar/new-stylesy2.jpg";
import img3 from "../../public/assets/images/arkar/new-stylesy3.jpg";
// import img4 from "../../public/assets/images/lerida-comp.png";
// import img5 from "../../public/assets/images/zimba-comp.png";
// import img6 from "../../public/assets/images/migear-comp.png";

export default {
  title: "Roperro / New Carousel",
};

const Template = (args) => <NewCarousel {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  customStyle: "single-arrow-slider bg-black pl-20 relative",
  images: [
    {
      img: img1,
      altTag: 'model'
    },
    {
      img: img2,
      altTag: 'model'
    },
    {
      img: img3,
      altTag: 'model'
    },
    {
      img: img2,
      altTag: 'model'
    },
    {
      img: img3,
      altTag: 'model'
    },
    {
      img: img1,
      altTag: 'model'
    },
  ],
};

import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation, Autoplay } from "swiper";
import SwiperCore from "swiper";
// import "./slider-arrow.css";

const NewCarousel = ({
  images,
  customStyle,
  customSlidersPerView,
  customSlidesPerGroup,
  customSpaceBetween,
}) => {
  return (
    <div className={customStyle}>
      <Swiper
        slidesPerView={customSlidersPerView ? customSlidersPerView : 3}
        slidesPerGroup={customSlidesPerGroup ? customSlidesPerGroup : 3}
        spaceBetween={customSpaceBetween ? customSpaceBetween : 8}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        loop={true}
        navigation
        modules={[Pagination]}
        className={`mySwiper`}
      >
        {images.map((item, i) => (
          <SwiperSlide key={i}>
            <div>
              <img src={item.img} alt={item.altTag} style={{width: '100%'}} />
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default NewCarousel;

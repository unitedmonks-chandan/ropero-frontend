import AddressForm from "./index";


export default {
    title: "Roperro/Address Form"
};

const Template = args => <AddressForm {...args} />;

export const ShippingAddress = Template.bind({});

ShippingAddress.args = {
    tailwind: '',
    formHeading: {
        text: '1. Shipping Address',
        tailwind: 'font-semibold text-3xl text-colorGray30 mb-10'
    }, 

    inputs: {
        tailwind: 'grid grid-rows-4 grid-cols-2 gap-x-11 gap-y-7',
        firstName: {
            tailwind: 'flex flex-col',
            css: '',
            label: {
                text: 'First Name',
                tailwind: 'font-light text-colorGray30 text-3xl mb-2',
                css: ''
            },
            input: {
                required: true,
                placeholder: '',
                type: 'text', //number, email
                tailwind: 'bg-colorGray91 text-2xl text-colorGray30 pt-4 pb-4 pl-2 font-light',
                css: ''
            }
        },

        lastName: {
            tailwind: 'flex flex-col',
            css: '',
            label: {
                text: 'Last Name',
                tailwind: 'font-light text-colorGray30 text-3xl mb-2',
                css: ''
            },
            input: {
                required: true,
                placeholder: '',
                type: 'text', //number, email
                tailwind: 'bg-colorGray91 text-2xl text-colorGray30 pt-4 pb-4 pl-2 font-light',
                css: ''
            }
        },

        address1: {
            tailwind: 'flex flex-col col-start-1 col-end-3',
            css: '',
            label: {
                text: 'Address 1',
                tailwind: 'font-light text-colorGray30 text-3xl mb-2',
                css: ''
            },
            input: {
                required: true,
                placeholder: '',
                type: 'text', //number, email
                tailwind: 'bg-colorGray91 text-2xl text-colorGray30 pt-4 pb-4 pl-2 font-light',
                css: ''
            }
        },  
        city: {
            tailwind: 'flex flex-col',
            css: '',
            label: {
                text: 'City',
                tailwind: 'font-light text-colorGray30 text-3xl mb-2',
                css: ''
            },
            input: {
                required: true,
                placeholder: '',
                type: 'text', //number, email
                tailwind: 'bg-colorGray91 text-2xl text-colorGray30 pt-4 pb-4 pl-2 font-light',
                css: ''
            }
        },
        state: {
            tailwind: 'flex flex-col',
            css: '',
            label: {
                text: 'State',
                tailwind: 'font-light text-colorGray30 text-3xl mb-2',
                css: ''
            },
            input: {
                required: true,
                placeholder: '',
                type: 'text', //number, email
                tailwind: 'bg-colorGray91 text-2xl text-colorGray30 pt-4 pb-4 pl-2 font-light',
                css: ''
            }
        },
        pincode: {
            tailwind: 'flex flex-col',
            css: '',
            label: {
                text: 'Pincode',
                tailwind: 'font-light text-colorGray30 text-3xl mb-2',
                css: ''
            },
            input: {
                required: true,
                placeholder: '',
                type: 'number', //number, email
                tailwind: 'bg-colorGray91 text-2xl text-colorGray30 pt-4 pb-4 pl-2 font-light appearance-none',
                css: ''
            }
        },
        phoneNumber: {
            tailwind: 'flex flex-col',
            css: '',
            label: {
                text: 'Phone Number',
                tailwind: 'font-light text-colorGray30 text-3xl mb-2',
                css: ''
            },
            input: {
                required: true,
                placeholder: '',
                type: 'number', //number, email
                tailwind: 'bg-colorGray91 text-2xl text-colorGray30 pt-4 pb-4 pl-2 font-light',
                css: ''
            }
        },


    },

    defaultAddressCheckbox: {
        tailwind: 'flex items-center mt-11 mb-7',
        css: '',
    
        input: {
            type:'checkbox',
            id:'address',
            name:'address',
            value:'default address',
            
    
            tailwind: 'mr-2.5',
            css: ''
        },
    
        label: {
            text: 'Make This My Default Address',
            htmlFor: 'address',
            tailwind: 'font-light text-colorGray30 text-2xl',
            css: ''
        },
    }, 

    giftHeading: {
        text: 'Is this a gift?',
        tailwind: 'text-xl text-colorGray30 font-light mb-3.5'
    },

    giftCheckbox: {
        tailwind: 'flex items-center mb-9',
        css: '',
    
        input: {
            type:'checkbox',
            id:'gift',
            name:'gift',
            value:'yes',
            tailwind: 'mr-2.5',
            css: ''
        },
    
        label: {
            text: 'Yes',
            htmlFor: 'gift',
            tailwind: 'font-light text-colorGray30 text-2xl',
            css: ''
        },
    },

    useThisAddressButton: {
        buttonText: 'use this address',
        tailwind: 'uppercase bg-hoverYellow text-white text-2xl font-semibold w-80 h-16',
        css: ''
    }
}


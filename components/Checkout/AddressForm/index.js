import Heading from '../../Typography/Heading';
import InputText from '../../FormField/InputText';
import InputSelect from '../../FormField/InputSelect';
import RoperroButton from '../../RoperroButton';

const AddressForm = (props) => {
    const { tailwind, formHeading, inputs, defaultAddressCheckbox, giftHeading, giftCheckbox, useThisAddressButton } = props;
    const { firstName, lastName, address1, city, state, pincode, phoneNumber } = inputs;

    return (
        <div className={tailwind}>
            <Heading
                text={formHeading.text}
                tailwind={formHeading.tailwind}
            />

            <div className={inputs.tailwind}>
                <InputText
                    label={firstName.label}
                    input={firstName.input}
                    tailwind={firstName.tailwind}
                    css={firstName.css}
                />
                <InputText
                    label={lastName.label}
                    input={lastName.input}
                    tailwind={lastName.tailwind}
                    css={lastName.css}
                />
                <InputText
                    label={address1.label}
                    input={address1.input}
                    tailwind={address1.tailwind}
                    css={address1.css}
                />
                <InputText
                    label={city.label}
                    input={city.input}
                    tailwind={city.tailwind}
                    css={city.css}
                />
                <InputText
                    label={state.label}
                    input={state.input}
                    tailwind={state.tailwind}
                    css={state.css}
                />
                <InputText
                    label={pincode.label}
                    input={pincode.input}
                    tailwind={pincode.tailwind}
                    css={pincode.css}
                />
                <InputText
                    label={phoneNumber.label}
                    input={phoneNumber.input}
                    tailwind={phoneNumber.tailwind}
                    css={phoneNumber.css}
                />
            </div>

            <InputSelect
                tailwind={defaultAddressCheckbox.tailwind}
                css={defaultAddressCheckbox.css}
                input={defaultAddressCheckbox.input}
                label={defaultAddressCheckbox.label}
            />

            <Heading
                text={giftHeading.text}
                tailwind={giftHeading.tailwind}
            />

            <InputSelect
                tailwind={giftCheckbox.tailwind}
                css={defaultAddressCheckbox.css}
                input={defaultAddressCheckbox.input}
                label={giftCheckbox.label}
            />

            <RoperroButton 
                buttonText={useThisAddressButton.buttonText} 
                tailwind={useThisAddressButton.tailwind}
                css={useThisAddressButton.css}
            />
        </div>
    );
};

export default AddressForm;

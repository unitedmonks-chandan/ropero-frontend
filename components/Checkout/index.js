import Heading from '../Typography/Heading';
import AddressForm from './AddressForm';
import PaymentMethod from './PaymentMethod';
import OrderSummery from '../OrderSummery/index';
import RoperroLink from '../RoperroLink';


const Checkout = (props) => {

    const { tailwind, checkoutAndOrdersContainerCss, checkoutContainerCss, checkoutHeading, addressForm, paymentMethod, orderContainerCss, orderHeading, orderSummery, linksContainer } = props;

    const { backToShopping, makePayment } = linksContainer;

    return (
        <div className={tailwind}>
            <div className={checkoutAndOrdersContainerCss}>
                <div className={checkoutContainerCss}>
                    <Heading
                        text={checkoutHeading.text}
                        tailwind={checkoutHeading.tailwind}
                        css={checkoutHeading.css}
                    />

                    <AddressForm
                        tailwind={addressForm.tailwind}
                        formHeading={addressForm.formHeading}
                        inputs={addressForm.inputs}
                        defaultAddressCheckbox={addressForm.defaultAddressCheckbox}
                        giftHeading={addressForm.giftHeading}
                        giftCheckbox={addressForm.giftCheckbox}
                        useThisAddressButton={addressForm.useThisAddressButton}
                    />

                    <PaymentMethod
                        tailwind={paymentMethod.tailwind}
                        heading={paymentMethod.heading}
                        paymentMethods={paymentMethod.paymentMethods}
                        cardsContainerTailwind={paymentMethod.cardsContainerTailwind}
                        creditOrDebitCard={paymentMethod.creditOrDebitCard}
                        addNewCard={paymentMethod.addNewCard}
                        paraSafeInformation={paymentMethod.paraSafeInformation}
                    />
                </div>

                <div className={orderContainerCss}>

                    <Heading
                        text={orderHeading.text}
                        tailwind={orderHeading.tailwind}
                        css={orderHeading.css}
                    />

                    <OrderSummery
                        tailwind={orderSummery.tailwind}
                        css={orderSummery.css}
                        headingContainer={orderSummery.headingContainer}
                        cartItems={orderSummery.cartItems}
                        totalAmount={orderSummery.totalAmount}
                    />
                </div>
            </div>

            <div className={linksContainer.tailwind}>
                <RoperroLink
                    text={backToShopping.text}
                    backgroundTailwind={backToShopping.backgroundTailwind}
                    linkHref={backToShopping.linkHref}
                    tailwind={backToShopping.tailwind}
                    css={backToShopping.css}
                />
                <RoperroLink
                    text={makePayment.text}
                    backgroundTailwind={makePayment.backgroundTailwind}
                    linkHref={makePayment.linkHref}
                    tailwind={makePayment.tailwind}
                    css={makePayment.css}
                />
            </div>
        </div>
    );
};

export default Checkout;
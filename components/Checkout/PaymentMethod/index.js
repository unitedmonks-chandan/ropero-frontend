import Heading from '../../Typography/Heading';
import InputSelect from '../../FormField/InputSelect';
import DebitCard from '../../DebitCard/index';
import AddNewCard from '../../AddNewCard';
import Para from '../../Typography/Para';




const PaymentMethod = (props) => {
    const {tailwind, heading, paymentMethods, cardsContainerTailwind, creditOrDebitCard, addNewCard, paraSafeInformation} = props;
    const {card, upi, netBanking, cashOnDelivery, payPal} = paymentMethods;

    return (
        <div className={tailwind}>
           <Heading 
                text={heading.text}
                tailwind={heading.tailwind}
           />

           <div className={paymentMethods.tailwind}>
               <InputSelect 
                   tailwind={card.tailwind}
                   css={card.css}
                   input={card.input}
                   label={card.label}
               />
               <InputSelect 
                   tailwind={upi.tailwind}
                   css={upi.css}
                   input={upi.input}
                   label={upi.label}
               />
               <InputSelect 
                   tailwind={netBanking.tailwind}
                   css={netBanking.css}
                   input={netBanking.input}
                   label={netBanking.label}
               />
               <InputSelect 
                   tailwind={cashOnDelivery.tailwind}
                   css={cashOnDelivery.css}
                   input={cashOnDelivery.input}
                   label={cashOnDelivery.label}
               />
               <InputSelect 
                   tailwind={payPal.tailwind}
                   css={payPal.css}
                   input={payPal.input}
                   label={payPal.label}
               />
           </div>

           <div className={cardsContainerTailwind}>
                <DebitCard 
                    tailwind={creditOrDebitCard.tailwind}
                    css={creditOrDebitCard.css}
                    logo={creditOrDebitCard.logo}
                    bankName={creditOrDebitCard.bankName}
                    cardNumber={creditOrDebitCard.cardNumber}
                    expiry={creditOrDebitCard.expiry}
                    cvv={creditOrDebitCard.cvv}
                />

                <AddNewCard 
                    tailwind={addNewCard.tailwind}
                    css={addNewCard.css}
                    plus={addNewCard.plus}
                    heading={addNewCard.heading}
                />
            </div>

            <Para
                text={paraSafeInformation.text} 
                tailwind={paraSafeInformation.tailwind} 
                css={paraSafeInformation.css} 
            />
        </div>
    );
};

export default PaymentMethod;
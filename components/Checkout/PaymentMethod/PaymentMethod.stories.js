import PaymentMethod from "./index";
import PayPalSVGIcon from '../../../public/assets/svg/checkout/pay-pal-svg-icon.svg';

import VisaSVGIcon from '../../../public/assets/svg/checkout/visa-svg-icon.svg';
import DotSVGIcon from '../../../public/assets/svg/checkout/dot-svg-icon.svg';

export default {
    title: "Roperro/Payment Method"
};

const Template = args => <PaymentMethod {...args} />;

export const Default = Template.bind({});

Default.args = {
    tailwind: '',

    heading: {
        text: '2. Payment Method',
        tailwind: 'font-semibold text-3xl text-colorGray30 mb-10'
    },

    paymentMethods: {
        tailwind: 'flex justify-between',

        card: {
            tailwind: 'flex items-center',
            css: '',
        
            input: {
                type:'radio',
                id:'card',
                name:'payment-method',
                value:'card',
                tailwind: 'mr-2.5',
                css: ''
            },
        
            label: {
                text: 'Credit/Debit Card',
                htmlFor: 'card',
                tailwind: 'font-light text-colorGray30 text-2xl',
                css: ''
            },
        },
        upi: {
            tailwind: 'flex items-center',
            css: '',
        
            input: {
                type:'radio',
                id:'upi',
                name:'payment-method',
                value:'upi',
                tailwind: 'mr-2.5',
                css: ''
            },
        
            label: {
                text: 'UPI',
                htmlFor: 'upi',
                tailwind: 'font-light text-colorGray30 text-2xl',
                css: ''
            },
        },
        netBanking: {
            tailwind: 'flex items-center',
            css: '',
        
            input: {
                type:'radio',
                id:'net-banking',
                name:'payment-method',
                value:'net-banking',
                tailwind: 'mr-2.5',
                css: ''
            },
        
            label: {
                text: 'Net Banking',
                htmlFor: 'net-banking',
                tailwind: 'font-light text-colorGray30 text-2xl',
                css: ''
            },
        },
        cashOnDelivery: {
            tailwind: 'flex items-center',
            css: '',
        
            input: {
                type:'radio',
                id:'cash-on-delivery',
                name:'payment-method',
                value:'cash-on-delivery',
                tailwind: 'mr-2.5',
                css: ''
            },
        
            label: {
                text: 'Cash On Delivery',
                htmlFor: 'cash-on-delivery',
                tailwind: 'font-light text-colorGray30 text-2xl',
                css: ''
            },
        },
        payPal: {
            tailwind: 'flex items-center',
            css: '',
        
            input: {
                type:'radio',
                id:'pay-pal',
                name:'payment-method',
                value:'pay-pal',
                tailwind: 'mr-2.5',
                css: ''
            },
        
            label: {
                text: '',
                htmlFor: 'pay-pal',
                tailwind: 'font-light text-colorGray30 text-2xl',
                css: '',
                svg: {
                    icon: PayPalSVGIcon,
                    tailwind: ''  //width and height of svg icon
                }
            }
        }

    },

    cardsContainerTailwind: 'flex items-end',

    creditOrDebitCard: {
        tailwind: 'bg-white pt-6 pl-10 pb-10 mt-11 mr-12',
        css: 'width-488px debit-card-shadow',
    
        logo: {
            svg: VisaSVGIcon,
            tailwind: 'w-24 w-14 mb-1.5'
        }, 
    
        bankName: {
            text: 'Kotak Mahindra Debit Card',
            tailwind: 'text-xl font-medium text-colorGray30 mb-1.5'
        },
    
        cardNumber: {
            dot: {
                svg: DotSVGIcon,
                dotMargin: '4px',
                //  substract dot margin from dotGropMargin. 
                dotGroupMargin: '10px',
            },
    
            lastFourDigit: {
                digits: 7845,
                tailwind: 'text-xl text-colorGray30 font-medium'
            }
        },
        expiry: {
            expiryDate: '09/2024',
            tailwind: 'text-lg font-medium text-colorGray30  mt-1.5'
        },
    
        cvv: {
            tailwind: 'flex items-center mt-4',
    
            cvvInput: {
                tailwind: '',
                css: '',
                label: {
                    tailwind: '', 
                    text: ''
                },
                input: {
                    required: true,
                    placeholder: 'Enter CVV',
                    type: 'number', 
                    tailwind: 'bg-white text-xs text-colorGray30 pt-1.5 pb-1.5 pl-2.5 font-medium border border-solid border-colorBrown44  mr-20 w-20',
                    css: ''
                }
            },
        
            cvvButton: {
                buttonText: 'What is CVV ?',
                tailwind: 'font-medium text-colorGray30',
                css: 'font-size-9px line-height-11px'
            }
        }
    },

    addNewCard: {
        tailwind: 'bg-colorGray95 flex flex-col items-center justify-center',
        css: 'width-488px height-268px',
    
        plus: {
            tailwind: 'bg-colorGray79 flex items-center justify-center',
            css: 'width-81px height-81px border-radius-50percent', 
    
            link: {
                text: '+',
                linkHref: '/',
                tailwind: 'text-colorGray37 flex items-baseline -mr-.5 -mb-1',
                css: '  font-size-64px line-height-16px'
            }
        },
    
        heading: {
            text: 'Add New Card',
            tailwind: 'text-3xl text-colorGray30 mt-7',
            css: ''
        }
    },

    paraSafeInformation: {
        text: "Your Information Is Safe",
    
        tailwind: 'text-xl text-colorGray30 mt-3.5 ml-10',
    }
};
import InputText from "./index";

export default {
    title: "Roperro/Form Fields"
}

const Template = args => <InputText {...args} />

export const FirstName = Template.bind({});

FirstName.args = {
    tailwind: 'flex flex-col',
    css: '',
    label: {
        text: 'First Name',
        tailwind: 'font-light text-colorGray30 text-3xl mb-2',
        css: ''
    },
    input: {
        required: true,
        placeholder: 'Enter first name',
        type: 'text', //number, email
        tailwind: 'bg-colorGray91 text-2xl text-colorGray30 pt-4 pb-4 pl-2 font-light',
        css: ''
    }

}
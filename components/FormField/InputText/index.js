
const InputText = (props) => {
    const { label, input, tailwind='', css='' } = props;
    const {required=false, placeholder='', type='text'} = input;

    return (
        <div className={`${tailwind} ${css}`}>  

            <label 
                className={`${label.tailwind} ${label.css}`}
            >
                {label.text}
            </label>

            <input
                type={type} 
                className={`${input.tailwind} ${input.css}`} 
                placeholder={placeholder} 
                required={required} 
            />
        </div>
    );
};

export default InputText;
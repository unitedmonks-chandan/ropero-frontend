import {ReactSVG} from 'react-svg';


const InputSelect = (props) => {
    const { tailwind, css, input, label } = props;

    const renderIconLabel = () => {
        if(label.svg) {
            return (
                <ReactSVG src={label.svg.icon} className={label.svg.tailwind} />
            )
        }
    }

    return (
        <div className={`${tailwind} ${css}`}>
            <input className={`${input.tailwind} ${input.css}`}
                type={input.type}
                id={input.id}
                name={input.name}
                value={input.value}
            />

            <label className={`${label.tailwind} ${label.css}`} htmlFor={label.htmlFor}>
                {label.text}
                {renderIconLabel()}
            </label>
        </div>
    );
};

export default InputSelect;
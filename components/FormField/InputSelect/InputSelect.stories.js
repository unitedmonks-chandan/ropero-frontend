import InputSelect from "./index";
import PayPalSVGIcon from '../../../public/assets/svg/checkout/pay-pal-svg-icon.svg';

export default {
    title: "Roperro/Form Fields"
}

const Template = args => <InputSelect {...args} />

export const DefaultAddress = Template.bind({});

DefaultAddress.args = {
    tailwind: 'flex items-center',
    css: '',

    input: {
        type:'checkbox',
        id:'address',
        name:'address',
        value:'default address',
        

        tailwind: 'mr-2.5',
        css: ''
    },

    label: {
        text: 'Make This My Default Address',
        htmlFor: 'address',
        tailwind: 'font-light text-colorGray30 text-2xl',
        css: ''
    },

}


export const CheckboxIsThisAGift = Template.bind({});
// Note: use Heading component before InputCheckbox for headings like "Is this a gift?" 
CheckboxIsThisAGift.args = {
    tailwind: 'flex items-center',
    css: '',

    input: {
        type:'checkbox',
        id:'gift',
        name:'gift',
        value:'yes',
        

        tailwind: 'mr-2.5',
        css: ''
    },

    label: {
        text: 'Yes',
        htmlFor: 'gift',
        tailwind: 'font-light text-colorGray30 text-2xl',
        css: ''
    },

}


export const RadioCreditDebitCard = Template.bind({});

RadioCreditDebitCard.args = {
    tailwind: 'flex items-center',
    css: '',

    input: {
        type:'radio',
        id:'address',
        name:'address',
        value:'default address',
        

        tailwind: 'mr-2.5',
        css: ''
    },

    label: {
        text: 'Credit/Debit Card',
        htmlFor: 'address',
        tailwind: 'font-light text-colorGray30 text-2xl',
        css: ''
    },

}


export const RadioPaypal = Template.bind({});

RadioPaypal.args = {
    tailwind: 'flex items-center',
    css: '',

    input: {
        type:'radio',
        id:'address',
        name:'address',
        value:'default address',
        

        tailwind: 'mr-2.5',
        css: ''
    },

    label: {
        text: '',
        htmlFor: 'address',
        tailwind: 'font-light text-colorGray30 text-2xl',
        css: '',
        svg: {
            icon: PayPalSVGIcon,
            tailwind: ''  //width and height of svg icon
        }
    },

}
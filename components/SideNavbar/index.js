import { useState } from 'react';
import Heading from '../Typography/Heading';
import SideNavLink from '../Typography/SideNavLink';

const SideNavbar = (props) => {

    const { tailwind, css, heading, linksContainer } = props;
    const { links, defaultSelectedLink } = linksContainer;

    const [selected, setSelected] = useState(defaultSelectedLink);


    const renderLinks = () => {
        return links.map((item, i) => <SideNavLink
            key={i}
            tailwind={item.tailwind}
            link={item.link}
            icon={item.icon}
            active={item.link.text === selected}
            onClick={() => setSelected(item.link.text)}
        />
        )
    }

    return (
        <div className={`${tailwind} ${css}`}>
            <Heading text={heading.text} tailwind={heading.tailwind} css={heading.css} />

            <div className={`${linksContainer.tailwind} ${linksContainer.css}`}>

                {renderLinks()}

            </div>
        </div>
    );
};

export default SideNavbar;
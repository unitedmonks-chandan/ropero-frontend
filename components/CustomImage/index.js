import React, { useRef, useLayoutEffect, useState } from 'react';
import LazyLoad from "react-lazyload";

const CustomImage = ({ imageSource, altTag, borderTopRadius=0, borderBottomRadius=0 }) => {
    const targetRef = useRef();
    const [dimensions, setDimensions] = useState({ width: 0, height: 0, dpr:1 });

    useLayoutEffect(() => {
      if (targetRef.current) {
        setDimensions({
          width: targetRef.current.offsetWidth,
          dpr: window.devicePixelRatio
        });
      }
    }, []);

    return (
        <div ref={targetRef}>
          <LazyLoad height={"300px"} offset={100}>
            <img
                src={imageSource + "?width=" + dimensions.width+"&dpr="+dimensions.dpr}
                style={{borderTopLeftRadius: borderTopRadius,
                    borderTopRightRadius: borderTopRadius,
                    borderBottomLeftRadius: borderBottomRadius,
                    borderBottomRightRadius: borderBottomRadius
                }}
                alt={altTag} />
          </LazyLoad>
        </div>
    );
};

export default CustomImage;

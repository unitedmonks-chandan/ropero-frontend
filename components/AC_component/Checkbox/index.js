import React from 'react'

const CheckBox = ({ labelText }) => {
    return (
        <div className="Account-input_group">
            <div className="child_item ">
                <label className="content">
                    <span className="sp1 mx-10">{labelText}</span>
                    <input type="radio" name='option' />
                    <span className="checkmark"></span>
                </label>
            </div>
        </div>
    )
}

export default CheckBox
import React from 'react'

const Forgot = ({ forgot_text }) => {
    return (
        <div className='mx-2 my-2'>
            <div className="Forgotten_msg">
                <button className='border-0 bg-transparent ' type='submit'>
                    <p>{forgot_text} </p>
                </button>
            </div>
        </div>
    )
}

export default Forgot;
import React from 'react';

const Heading = ({ text }) => {
    return (
        <div className="text_heading text-4xl">
            <span>{text}</span>
        </div>
    )
}

export default Heading;
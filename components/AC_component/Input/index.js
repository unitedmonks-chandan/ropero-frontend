import React from 'react'

const CustomInput = (props) => {
    const { labelText, TypeAttributes, placeholderText, nameAttributes } = props;
    return (

        <div className="w-full px-2 md:w-1/1 acForm">
            <label
                className="block mt-10 mb-4 text-sm md:text-xl lg:text-xl 2xl:text-2xl font-light	"
                htmlFor="formGridCode_name">{labelText}</label>

            <input className="w-full h-10 2xl:h-14 px-3 "
                type={TypeAttributes}
                placeholder={`${placeholderText}`}
                name={nameAttributes}
                autoComplete="off" />
        </div>

    )
}

export default CustomInput;
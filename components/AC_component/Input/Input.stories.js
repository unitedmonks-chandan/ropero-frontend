

import CustomInput from './index';

export default {
    title: "CustomInput"
}

export const Template = arguments_ => <CustomInput {...arguments_} /> //creating a template

export const Primary = Template.bind({})



Primary.args = {
    labelText: "First Name*",
    TypeAttributes: "text",
    nameAttributes: "fname",
    placeholderText: "CHANGE NAME"
}







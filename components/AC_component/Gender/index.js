import React from 'react'

const Gender = ({ headText }) => {
    return (
        <div className='ac_g mx-2 font-light my-10 text-sm md:text-xl lg:text-xl 2xl:text-2xl'>
            <h5>{headText}</h5>
        </div>
    )
}

export default Gender
import Button from "./index"

export default {
    title: "AC/button"
}
const Template = argument => <Button {...argument} /> //creating a template

export const Submit = Template.bind({})
Submit.args = {
    text_button: "SUBMIT"
}
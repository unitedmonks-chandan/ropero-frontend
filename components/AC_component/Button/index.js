import React from 'react';

const Button = ({ text_button }) => {
    return (
        <div className="flex flex-wrap sm:mx-44 md:mx-20 lg:mx-20 xl:mx-24 2xl:mx-32 ac_btn">
            <div className="w-full">
                <button
                    className="mt-10 text-white  border-0 px-10 text-sm md:text-base lg:text-base xl:text-2xl">
                    {text_button}
                </button>
            </div>
        </div>
    )
}

export default Button;
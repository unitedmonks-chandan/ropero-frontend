import Decription from './index';
export default {
    title: "Decription"
}

const Template = arguments_ => <Decription {...arguments_} />
export const Para = Template.bind({})

Para.args = {
    description: "Please tell us more about your inquiry using the form below and click to submit your information."
}


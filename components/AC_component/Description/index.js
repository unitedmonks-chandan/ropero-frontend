import React from 'react';

const Description = ({ description }) => {
    return (
        <div className=" w-full text_description">
            <span className=' 2xl:text-2xl'>{description}</span>
        </div>
    )
}

export default Description
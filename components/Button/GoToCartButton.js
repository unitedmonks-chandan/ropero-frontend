import { ReactSVG } from 'react-svg'
import Button from './index';
import ShoppingBagSVGIcon from '../../svgs/ShoppingBagSVGIcon.svg';


const GoToCartButton = ({ btn, size}) => {
  return (
    <Button btn={btn} size={size} >
      <ReactSVG src={ShoppingBagSVGIcon} />
    </Button>
  )
}

export default GoToCartButton;
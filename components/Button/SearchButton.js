import { ReactSVG } from 'react-svg';
import Button from './index';
import SearchSVGIcon from '../../public/assets/svg/search-svg-icon.svg';


const SearchButton = ({ btn, size }) => {
    return (
        <Button btn={btn} size={size} >
            <ReactSVG src={SearchSVGIcon} />
        </Button>
    )
}

export default SearchButton;
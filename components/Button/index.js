

const Button = ({btn, size, children}) => {
    return( <button className={`${btn} ${size}`} >
        {children}
    </button>)
}



export default Button;




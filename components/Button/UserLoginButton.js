import { ReactSVG } from 'react-svg';
import Button from './index';
import UserSVGIcon from '../../svgs/UserSVGIcon.svg';


const UserLoginButton = ({ btn, size }) => {
    return (
        <Button btn={btn} size={size} >
            <ReactSVG src={UserSVGIcon} />
        </Button>
    )
}

export default UserLoginButton;
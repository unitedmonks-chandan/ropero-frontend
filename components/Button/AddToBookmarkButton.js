import { ReactSVG } from 'react-svg';
import Button from './index';
import BookmarkSVGIcon from '../../svgs/BookmarkSVGIcon.svg';


const AddToBookmarkButton = ({ btn="btn__icon__white", size="btn__icon__medium" }) => {
    return (
        <Button btn={btn} size={size} >
            <ReactSVG src={BookmarkSVGIcon} />
        </Button>
    )
}

export default AddToBookmarkButton;
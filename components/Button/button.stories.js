import Button from './index'; //importing the button component

export default {
    title: "My First Button" //title for our storybook
}

const Template = (args) => <Button {...args} /> //creating a template

export const Primary = Template.bind({})

//Passing the props to the component

Primary.args ={
    btn: "bg-red-400 font-bold text-white px-6 py-2",
    size: "h-10 w-56",
    children: "Primary Button"
}



export const Secondary = Template.bind({});

//Passing the props to the component

Secondary.args = {
    btn: "btn__dark",
    size: "btn__medium",
    children: "Shop now"
}



//these arguments will later be the control on the storybook UI and you can change them in the storybook without coming back to the editor.

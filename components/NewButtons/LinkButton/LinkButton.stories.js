import LinkButton from "./LinkButton";

export default {
  title: "Roperro / Link Button",
};

const Template = (args) => <LinkButton {...args} />;

export const Primary = Template.bind({});

Primary.args = {
  btnLink: "/lerida",
  btn: "inline-block bg-white text-black text-lg uppercase border border-black px-7 py-1",
  text: "Shop now",
  size: " ",
};

export const Secondary = Template.bind({});

Secondary.args = {
  btnLink: "/zimba",
  btn: "inline-block bg-black text-white text-lg uppercase px-7 py-1",
  text: "Shop now",
  size: " ",
};

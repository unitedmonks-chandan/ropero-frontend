import React from "react";
import Link from "next/link";

function LinkButton({ btnLink, btn, size, text }) {
  return (
    <Link href={btnLink}>
      <a className={`${btn} ${size}`}>{text}</a>
    </Link>
  );
}

export default LinkButton;

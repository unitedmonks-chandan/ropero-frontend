import React from "react";
import Link from "next/link";

function LinkButtonWithLogo({
  position,
  logo,
  logoSize,
  btnLink,
  btn,
  btnText,
}) {
  return (
    <div className={position}>
      <img src={logo} alt="logo" className={logoSize} />
      <Link href={btnLink}>
        <a className={btn}>{btnText}</a>
      </Link>
    </div>
  );
}

export default LinkButtonWithLogo;

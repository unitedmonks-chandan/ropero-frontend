import React from "react";
import Link from "next/link";
import { ReactSVG } from "react-svg";

function LinkButtonWithHeading({
  container,
  position,
  headingStyle='',
  heading1,
  heading1Style,
  currencySvgTailwind,
  priceContainer,
  heading2,
  heading2Style = '',
  currencySvg,
  heading3,
  heading3Style='',
  heading4,
  heading4Style='',
  btnContainer,
  btnLink,
  btn,
  size,
  btnText,
  btn2,
  btn2Link,
  size2,
  btn2Text,
}) {
  return (
    <div className={`${container} ${position ? position : ""}`}>
      <div className={headingStyle}>
        <p className={heading1Style ? heading1Style : ""}>{heading1}</p>
        <div className={priceContainer}>
          <ReactSVG src={currencySvg} className={currencySvgTailwind} />
          <p className={heading2Style}>{heading2}</p>
        </div>
        {heading3 && (
          <p className={heading3Style}>{heading3}</p>
        )}
        {heading4 && (
          <p className={heading4Style}>{heading4}</p>
        )}
      </div>
      <div className={btnContainer}>
        <Link href={btnLink}>
          <a className={`${btn} ${size}`}>{btnText}</a>
        </Link>
        {btn2 && (
          <Link href={btn2Link}>
            <a className={`${btn2} ${size2}`}>{btn2Text}</a>
          </Link>
        )}
      </div>
    </div>
  );
}

export default LinkButtonWithHeading;

import LinkButtonWithHeading from "./LinkButtonWithHeading";

export default {
  title: "Roperro / Link Button With Heading",
};

const Template = (args) => <LinkButtonWithHeading {...args} />;

export const SingleBtn = Template.bind({});

SingleBtn.args = {
  container: "",
  position: "",
  headingStyle: "",
  heading1: "Trendy",
  heading1Style: "text-2xl text-black font-primary font-medium uppercase",
  heading2: "Styles",
  heading2Style: "text-2xl text-black font-primary font-medium uppercase",
  btnLink: "/view-all",
  btn: "inline-block bg-black text-white text-lg font-primary px-7 py-1",
  size: "",
  btnText: "View All",
};

export const DoubleBtn = Template.bind({});

DoubleBtn.args = {
  container: "",
  position: "",
  headingStyle: "",
  heading1: "Winter",
  heading1Style: "text-6xl text-black font-primary font-medium uppercase mb-12",
  heading2: "Collection",
  heading2Style: "text-6xl text-black font-primary font-medium uppercase mb-12",
  btnContainer: "inline-flex flex-col gap-4",
  btnLink: "/men",
  btn: "inline-block text-black text-lg font-primary border border-black px-7 py-1 uppercase",
  size: "",
  btnText: "Shop for him",
  btn2Link: "/women",
  btn2: "inline-block bg-hoverYellow text-black text-lg font-primary px-7 py-1 uppercase",
  size2: "",
  btn2Text: "Shop for her",
};

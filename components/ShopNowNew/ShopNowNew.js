import React from "react";
import LinkButtonWithHeading from "../NewButtons/LinkButtonWithHeading/LinkButtonWithHeading";

// function ShopNowNew({
//   bgImg,
//   colSpan,
//   container,
//   position,
//   headingStyle,
//   heading1,
//   heading2,
//   btnContainer,
//   btnLink,
//   btn,
//   size,
//   btnText,
// }) {
//   return (
//     <div className={`relative w-full ${colSpan}`}>
//       <img src={bgImg} className="w-full" alt="" />
//       <LinkButtonWithHeading
//         container={container}
//         position={position}
//         headingStyle={headingStyle}
//         heading1={heading1}
//         heading2={heading2}
//         btnContainer={btnContainer}
//         btnLink={btnLink}
//         btn={btn}
//         size={size}
//         btnText={btnText}
//       />
//     </div>
//   );
// }

// method 2
function ShopNowNew({colSpan,bgImg,altTag,btnData}) {
  return (
    <div className={`relative w-full ${colSpan}`}>
    
      <img src={bgImg} className="w-full" alt={altTag} />

      <LinkButtonWithHeading
       {...btnData}
      />
    </div>
  );
}

export default ShopNowNew;

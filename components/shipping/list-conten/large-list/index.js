import React from 'react'


const ListConten = ({ list_contens }) => {
    return (
        <div className='shipp_list-conten1'>
            <ul>
                <li className='md:mr-0 2xl:mr-20'><span className='md:text-base lg:text-xl 2xl:text-xl'>{list_contens}</span></li>
            </ul>
        </div>
    )
}

export default ListConten
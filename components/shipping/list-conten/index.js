import React from 'react'

const List = ({ list_conten }) => {
    return (
        <div className='shipp_list-conten'>
            <ul>
                <li className=' 2xl:mr-48'><span className=' md:text-base lg:text-xl 2xl:text-xl'>{list_conten}</span></li>
            </ul>
        </div>
    )
}

export default List;
import React from 'react';

const TitleImage = ({ ShoppingImg }) => {
    return (

        <div className="shipping_banner">
            <div className="h-full flex justify-center items-center ">
                <div className="content">
                    <img src={ShoppingImg} alt="" />
                </div>
            </div>
        </div>

    )
}

export default TitleImage;
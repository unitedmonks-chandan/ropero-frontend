import ShopImg from "../../../public/assets/images/shipping/Group.png"
import TitleImage from "./index"

export default {
    title: "Shooping"
}
const Template = argument => <TitleImage {...argument} />
export const img = Template.bind({})
img.args = {
    ShoppingImg: ShopImg
}

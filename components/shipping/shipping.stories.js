import Shipping from "./index"
import ShopImg from "../../public/assets/images/shipping/Group.png"

export default {
    title: "Main/UI"
}
const Template = agurment => <Shipping {...agurment} />
export const Ui = Template.bind({})
Ui.args = {

    props: {
        Img: ShopImg,
        title_text: "Shipping Time And Costs",
        text_statement1: "Express Shipping – Free ",
        text_statement2: "All orders are shipped using DHL Express and shipping is free of charge. ",
        text_statement3: "Our courier usually delivers from Monday to Friday during business hours and will make three delivery attempts. After the third failed attempt, your order will be returned to our warehouse. Please note that once you have placed an order, it is no longer possible to modify your shipping address. Please contact Customer Service should you have any questions about your order.",
        text_statement4: "All orders are processed automatically and we are unable to expedite or delay shipping times. Our warehouse does not operate on certain public holidays.",
        text_statement5: "When pre-ordering, the number of days for shipping will be calculated at the time the items become available in the warehouse. ",
    }

}
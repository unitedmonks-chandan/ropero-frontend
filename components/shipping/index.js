import React from 'react';
import Heading from "./typography/index"
import TitleImage from "./Title-image/index"
import ListConten from "./list-conten/large-list/index"
import List from "./list-conten/index"


const Shipping = (props) => {

    const { Img, title_text, text_statement1, text_statement2, text_statement3,
        text_statement4, text_statement5 } = props.props;

    return (
        <div className="shipMain_container">

            <TitleImage ShoppingImg={Img} />
            <div className='shipping_child'>
                <Heading typography_text={title_text} />
                <div>
                    <List list_conten={text_statement1} />
                </div>
                <div>
                    <List list_conten={text_statement2} />
                </div>
                <div>
                    <ListConten list_contens={text_statement3} />
                </div>
                <div>
                    <List list_conten={text_statement4} />
                </div>
                <div>
                    <List list_conten={text_statement5} />
                </div>
            </div>
        </div>
    )
}

export default Shipping;
import Heading from "./index"
export default {
    title: "shipping/heading"
}
const Template = argument => <Heading {...argument} />
export const image = Template.bind({})
image.args = {
    typography_text: "Shipping Time And Costs"
}
import React from 'react'

const Heading = ({ typography_text }) => {
    return (
        <div className='shipping_heading'>
            <h5 className='2xl:text-5xl'>
                {typography_text}
            </h5>
        </div>
    )
}

export default Heading
import React from "react";
import Link from "next/link";
import Button from "../../Button/index";

// const MenuItem = ({ menu }) => {
//   const { name, menuItemNavigation, subMenu } = menu;
//   const { subMenuItems } = subMenu;

//   const itemName = name === "Home" ? "Items" : name;

//   const renderSubMenuItems = (Arr) => {
//     return Arr.map((e, i) => {
//       return (
//         <div className="submenu__items-item" key={i}>
//           <Link href={e.subMenuItemNavigation}>
//             <a>
//               {/* <img src={e.itemImg} alt={e.item} /> */}
//               <p className="submenu__items-item-name text-lg">{e.item}</p>
//             </a>
//           </Link>
//         </div>
//       );
//     });
//   };

//   return (
//     <div className="menu-item">
//       <div className="submenu">
//         <div className="submenu__items">{renderSubMenuItems(subMenuItems)}</div>
//         <div className="submenu__image">
//           {/* <CustomImage
//                         imageSource={subMenu.img}
//                         altTag={subMenu.name}
//                     /> */}

//           <img src={subMenu.img} alt={subMenu.altTag} />
//         </div>
//         {/* <div className="submenu__button">
//                     <Link href={menuItemNavigation}>
//                         <a>
//                             <Button btn='' size='btn__large'>View all {itemName}</Button>
//                         </a>
//                     </Link>
//                 </div> */}
//       </div>
//     </div>
//   );
// };

// export default MenuItem;

const MenuItem = ({ menu }) => {
  const { name, menuItemNavigation, subMenu } = menu;
  const { subMenuItems } = subMenu;

  const itemName = name === "Home" ? "Items" : name;

  const renderSubMenuItems = (Arr) => {
    return Arr.map((e, i) => {
      return (
        <div className="" key={i}>
          <Link href={e.subMenuItemNavigation}>
            <a>
              {/* <img src={e.itemImg} alt={e.item} /> */}
              <p className="px-3 py-2 text-center hover:bg-hoverYellow font-normal transition-colors duration-300">
                {e.item}
              </p>
            </a>
          </Link>
        </div>
      );
    });
  };

  return (
    <div>
      {renderSubMenuItems(subMenuItems)}
      {/* <div className="">
          <img src={subMenu.img} alt={subMenu.altTag} />
        </div> */}
    </div>
  );
};

export default MenuItem;

import NewLogo from "./index";

export default {
  title: "New Logo",
};

const Template = (args) => <NewLogo {...args} />;

export const LargeLogo = Template.bind({});
LargeLogo.args = {
  size: "w-44",
};

export const SmallLogo = Template.bind({});
SmallLogo.args = {
  size: "w-32",
};

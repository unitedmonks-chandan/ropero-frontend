import React from "react";
import { ReactSVG } from "react-svg";
import logoSvg from "../../../svgs/NewLogo.svg";

const NewLogo = ({ size, logo }) => {
  return (
    <>
      <div className={`${size}`}>
        <ReactSVG src={logo ? logo.src : logoSvg.src}></ReactSVG>
      </div>
    </>
  );
};

export default NewLogo;

import Link from "next/link";
import MenuItem from "../MenuItem";

const NavItem = ({ navItems }) => {
  const renderNavItems = () => {
    return navItems.map((navItem) => {
      return (
        // <li className="navbar__list-item" key={navItem.name}>
        //   <Link href={navItem.menuItemNavigation}>
        //     <a className="text-midGray hover:text-hoverYellow font-medium uppercase">
        //       {navItem.name}
        //     </a>
        //   </Link>
        //   <div>
        //     <MenuItem menu={navItem} />
        //   </div>
        // </li>

        <li className="list-none px-5" key={navItem.name}>
          <Link href={navItem.menuItemNavigation}>
            <a className="text-midGray hover:text-hoverYellow font-medium uppercase myBrand relative pb-10 transition-colors duration-300">
              {navItem.name}
              <div className="new-sub-item absolute top-10 text-white  sub-item-pos sub-item-box z-20">
                <MenuItem menu={navItem} />
              </div>
            </a>
          </Link>
        </li>
      );
    });
  };

  return <>{renderNavItems()}</>;
};

export default NavItem;

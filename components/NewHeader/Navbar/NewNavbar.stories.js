import React from "react";
import NewNavbar from "./index";


import shoppingBagNewSvg from "../../../public/assets/svg/shopping-bag-new.svg";
import searchIconSVG from '../../../public/assets/svg/search-svg-icon.svg';
import userNewSvg from "../../../public/assets/svg/user-new.svg";
import heartNewSvg from "../../../public/assets/svg/heart-new.svg";


export default {
  title: "Roperro/New Header"
}

const Template = (arguments_) => <NewNavbar {...arguments_} />;
export const NavigationBar = Template.bind({});

NavigationBar.args = {
  iconsNav: [
    { icon: userNewSvg, mobileView: "hide", NavbarIconNavigation: "/Login" },
    {
      icon: heartNewSvg,
      mobileView: "show",
      NavbarIconNavigation: "/Favorite",
    },
    // {
    //   icon: BookmarkSVGIcon,
    //   num: 10,
    //   mobileView: "show",
    //   NavbarIconNavigation: "/Bookmarks",
    // },
    {
      icon: shoppingBagNewSvg,
      // num: 17,
      mobileView: "hide",
      NavbarIconNavigation: "/Cart",
    },
    // {
    //   icon: NotificationBellSVGIcon,
    //   notification: true,
    //   mobileView: "show",
    //   NavbarIconNavigation: "/Notification",
    // },
  ],

  searchIcon: searchIconSVG,
  menuItems: [
    // {
    //   name: "Home",
    //   menuItemNavigation: "/",
    //   subMenu: {
    //     // altTag: "",
    //     // img: "",
    //     subMenuItems: [
    //       {
    //         // itemImg:
    //         //   "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
    //         item: "",
    //         subMenuItemNavigation: "",
    //       },
    //       {
    //         // itemImg:
    //         //   "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
    //         item: "",
    //         subMenuItemNavigation: "",
    //       },
    //       {
    //         // itemImg:
    //         //   "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
    //         item: "",
    //         subMenuItemNavigation: "",
    //       },
    //     ],
    //   },
    // },
    {
      name: "Brands",
      menuItemNavigation: "/Brands",
      subMenu: {
        // altTag: "Brands",
        // img: "https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG",
        subMenuItems: [
          {
            // itemImg:
            //   "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
            item: "Lerida",
            subMenuItemNavigation: "brands/lerida",
          },
          {
            // itemImg:
            //   "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
            item: "Migear",
            subMenuItemNavigation: "brands/migear",
          },
          {
            // itemImg:
            //   "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
            item: "Arkar",
            subMenuItemNavigation: "brands/arkar",
          },
          {
            // itemImg:
            //   "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
            item: "Monada",
            subMenuItemNavigation: "brands/monada",
          },
          {
            // itemImg:
            //   "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
            item: "Zimba",
            subMenuItemNavigation: "brands/zimba",
          },
        ],
      },
    },
    {
      name: "Men",
      menuItemNavigation: "/Men",
      subMenu: {
        subMenuItems: [
          {
            item: "Lerida",
            subMenuItemNavigation: "brands/lerida",
          },
          {
            item: "Migear",
            subMenuItemNavigation: "brands/migear",
          },
          {
            item: "Arkar",
            subMenuItemNavigation: "brands/arkar",
          },
          {
            item: "Monada",
            subMenuItemNavigation: "brands/monada",
          },
          {
            item: "Zimba",
            subMenuItemNavigation: "brands/zimba",
          },
        ],
      },
    },
    {
      name: "Women",
      menuItemNavigation: "/Women",
      subMenu: {
        subMenuItems: [
          {
            item: "Lerida",
            subMenuItemNavigation: "brands/lerida",
          },
          {
            item: "Migear",
            subMenuItemNavigation: "brands/migear",
          },
          {
            item: "Arkar",
            subMenuItemNavigation: "brands/arkar",
          },
          {
            item: "Monada",
            subMenuItemNavigation: "brands/monada",
          },
          {
            item: "Zimba",
            subMenuItemNavigation: "brands/zimba",
          },
        ],
      },
    },
    {
      name: "Collections",
      menuItemNavigation: "/Collections",
      subMenu: {
        subMenuItems: [
          {
            item: "Lerida",
            subMenuItemNavigation: "brands/lerida",
          },
          {
            item: "Migear",
            subMenuItemNavigation: "brands/migear",
          },
          {
            item: "Arkar",
            subMenuItemNavigation: "brands/arkar",
          },
          {
            item: "Monada",
            subMenuItemNavigation: "brands/monada",
          },
          {
            item: "Zimba",
            subMenuItemNavigation: "brands/zimba",
          },
        ],
      },
    },
    {
      name: "Sale",
      menuItemNavigation: "/Sale",
      subMenu: {
        subMenuItems: [
          {
            item: "Lerida",
            subMenuItemNavigation: "brands/lerida",
          },
          {
            item: "Migear",
            subMenuItemNavigation: "brands/migear",
          },
          {
            item: "Arkar",
            subMenuItemNavigation: "brands/arkar",
          },
          {
            item: "Monada",
            subMenuItemNavigation: "brands/monada",
          },
          {
            item: "Zimba",
            subMenuItemNavigation: "brands/zimba",
          },
        ],
      },
    },
  ],
  // menuItems: [
  //   {
  //     name: "Home",
  //     menuItemNavigation: "/",
  //     subMenu: {
  //       altTag: "Home",
  //       img: "https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG",
  //       subMenuItems: [
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //       ],
  //     },
  //   },
  //   {
  //     name: "Brands",
  //     menuItemNavigation: "/Brands",
  //     subMenu: {
  //       altTag: "Brands",
  //       img: "https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG",
  //       subMenuItems: [
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //       ],
  //     },
  //   },
  //   {
  //     name: "Men",
  //     menuItemNavigation: "/Men",
  //     subMenu: {
  //       altTag: "Men",
  //       img: "https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG",
  //       subMenuItems: [
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //       ],
  //     },
  //   },
  //   {
  //     name: "Women",
  //     menuItemNavigation: "/Women",
  //     subMenu: {
  //       altTag: "Women",
  //       img: "https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG",
  //       subMenuItems: [
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //       ],
  //     },
  //   },
  //   {
  //     name: "Collections",
  //     menuItemNavigation: "/Collections",
  //     subMenu: {
  //       altTag: "Collections",
  //       img: "https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG",
  //       subMenuItems: [
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //       ],
  //     },
  //   },
  //   {
  //     name: "Sale",
  //     menuItemNavigation: "/Sale",
  //     subMenu: {
  //       altTag: "Sale",
  //       img: "https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG",
  //       subMenuItems: [
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //           item: "Toto Bag",
  //           subMenuItemNavigation: "Bags/ToteBag",
  //         },
  //         {
  //           itemImg:
  //             "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //           item: "Vanity Pouch",
  //           subMenuItemNavigation: "Bags/VanityPouch",
  //         },
  //       ],
  //     },
  //   },
  //   // {
  //   //   name: "Sale",
  //   //   menuItemNavigation: "/Sale",
  //   //   subMenu: {
  //   //     altTag: "Sale",
  //   //     img: "https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG",
  //   //     subMenuItems: [
  //   //       {
  //   //         itemImg:
  //   //           "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //   //         item: "Vanity Pouch",
  //   //         subMenuItemNavigation: "Bags/VanityPouch",
  //   //       },
  //   //       {
  //   //         itemImg:
  //   //           "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //   //         item: "Toto Bag",
  //   //         subMenuItemNavigation: "Bags/ToteBag",
  //   //       },
  //   //       {
  //   //         itemImg:
  //   //           "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //   //         item: "Vanity Pouch",
  //   //         subMenuItemNavigation: "Bags/VanityPouch",
  //   //       },
  //   //     ],
  //   //   },
  //   // },
  //   // {
  //   //   name: "Travel",
  //   //   menuItemNavigation: "/Travel",
  //   //   subMenu: {
  //   //     altTag: "Travel",
  //   //     img: "https://tann.gumlet.io/media/gen/image/tantrimm_day_21155.JPG",
  //   //     subMenuItems: [
  //   //       {
  //   //         itemImg:
  //   //           "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //   //         item: "Vanity Pouch",
  //   //         subMenuItemNavigation: "Bags/VanityPouch",
  //   //       },
  //   //       {
  //   //         itemImg:
  //   //           "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //   //         item: "Toto Bag",
  //   //         subMenuItemNavigation: "Bags/ToteBag",
  //   //       },
  //   //       {
  //   //         itemImg:
  //   //           "https://tann.gumlet.io/media/gen/image/Vanity_Pouch_Unselected.png",
  //   //         item: "Vanity Pouch",
  //   //         subMenuItemNavigation: "Bags/VanityPouch",
  //   //       },
  //   //       {
  //   //         itemImg:
  //   //           "https://tann.gumlet.io/media/gen/image/Tote_Unselected.png",
  //   //         item: "Toto Bag",
  //   //         subMenuItemNavigation: "Bags/ToteBag",
  //   //       },
  //   //     ],
  //   //   },
  //   // },
  // ],
};

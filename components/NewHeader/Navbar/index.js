import React, { useState } from 'react'
import Link from 'next/link'
import { ReactSVG } from 'react-svg'
import NewLogo from '../Logo'
import NavIconButtons from '../NavIconButtons'
import NavItem from '../NavItem/index'
import MenuBarsSVGIcon from '../../../svgs/MenuBarsSVGIcon.svg'
import MenuCloseSVGIcon from '../../../svgs/MenuCloseSVGIcon.svg'

const NewNavbar = ({ menuItems, iconsNav, searchIcon }) => {
  const [NavIcons, setNavIcons] = useState(iconsNav)

  // CSS class controls the menu icon in mobile view bars or X
  const [menuIcon, setMenuIcon] = useState(MenuBarsSVGIcon)

  // CSS class show and hide the menu items in mobile view when bars icon or times icon is clicked
  const [isActive, setIsActive] = useState('inactive')

  const menu = () => {
    // Changes the menu icon states
    menuIcon === MenuBarsSVGIcon
      ? setMenuIcon(MenuCloseSVGIcon)
      : setMenuIcon(MenuBarsSVGIcon)
    // Show and hide the menu items
    isActive === 'inactive' ? setIsActive('active') : setIsActive('inactive')
  }

  // const navItems = menuItems;

  return (
    <>
      <div className="bg-white h-28 px-12 font-primary">
        <div className="flex justify-between items-center w-full h-full">
          {/* click changes the state of menu icon bars <=> X and show/hide the menu item  */}
          <div
            className="navbar__icon navbar__icon-menu icon__large"
            onClick={menu}
          >
            <ReactSVG src={menuIcon.src} />
          </div>

          <div className="navbar__logo-box">
            <Link href="/">
              <a>
                <NewLogo size="w-32 sm:hidden" />
                <NewLogo size="w-48 hidden sm:block" />
              </a>
            </Link>
          </div>
          <div className="flex">
            {/*Navbar items like Home,Men,Sale etc and their submenu  */}
            <Link href="/">
              <a
                href=""
                className="text-midGray hover:text-hoverYellow font-medium uppercase myBrand relative px-5 transition-colors duration-300"
              >
                Home
              </a>
            </Link>
            <NavItem navItems={menuItems} />
          </div>
          <div className="navbar__icons-container">
            <NavIconButtons
              icons={NavIcons}
              btn="icon btn__icon__white"
              size="btn__icon__medium"
            />
          </div>
          {/* <div className={`navbar__items-container ${isActive}`}></div> */}
        </div>
      </div>
      <div className="w-full bg-black px-24 xl:px-40 h-20 font-primary">
        <div className="flex justify-between text-white text-lg w-full h-full items-center">
          <Link href="/aboutus">
            <a>About us</a>
          </Link>
          <Link href="#">
            <a>Product</a>
          </Link>
          <Link href="#">
            <a>Deals of the day</a>
          </Link>
          <Link href="#">
            <a>Fresh arrivals</a>
          </Link>
          <Link href="#">
            <a>Combo</a>
          </Link>
          <div className="bg-midGray flex gap-4 items-start px-2 py-1 rounded-full text-sm">
            {/* <input
              type="text"
              name=""
              id="search-item"
              className="bg-midGray outline-none px-2  rounded-md"
            /> */}
            <ReactSVG src={searchIcon} />
            <p>what are you looking for?</p>
          </div>
        </div>
      </div>
    </>
  )
}

export default NewNavbar

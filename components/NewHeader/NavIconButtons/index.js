import Button from "../../Button";
import Link from "next/link";
import { ReactSVG } from "react-svg";
import SearchSVGIcon from "../../../public/assets/svg/search-svg-icon.svg";

const renderNotification = (e) => {
  if (e.notification) {
    return <span className={"icon__dot"}></span>;
  }
};

const renderSubscriptNumber = (e) => {
  if (e.num) {
    return (
      <span className="icon__tiny icon__subscript-number">
        {e.num > 0 ? e.num : ""}
      </span>
    );
  }
};

const renderIcons = (icons, btn, size) => {
  return icons.map((e, i) => {
    return (
      <div className={`icon ${e.mobileView}`} key={i}>
        <Link href={e.NavbarIconNavigation}>
          <a>
            <Button btn={btn} size={size}>
              <ReactSVG
                src={e.icon}
                style={{ width: "22px", height: "22px" }}
                beforeInjection={(svg) => {
                  svg.setAttribute("style", "height: 22px");
                }}
              />
              {renderSubscriptNumber(e)}
              {renderNotification(e)}
            </Button>
          </a>
        </Link>
      </div>
    );
  });
};

const NavIconButtons = ({ icons, btn, size }) => {
  return (
    <div className="icons">
      {/* For search icon only to avoid navigation */}
      {/* <div className={`icon ${icons.mobileView}`} key="search">
                <Button btn={btn} size={size}  >
                    <ReactSVG src={SearchSVGIcon}
                        style={{ width: "22px", height: "22px" }}
                        beforeInjection={(svg) => {
                            svg.setAttribute('style', 'height: 22px')
                        }}
                    />
                </Button>
            </div> */}

      {renderIcons(icons, btn, size)}
    </div>
  );
};

export default NavIconButtons;

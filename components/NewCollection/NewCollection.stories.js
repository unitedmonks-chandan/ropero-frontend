import NewCollection from './index';

import bag from '../../public/assets/images/new-leather-collection/bag.jpg';

export default {
    title: "Roperro/New Collection"
}


const Template = args => <NewCollection {...args} />

export const LeatherCollection = Template.bind({});

LeatherCollection.args = {
    props: {
        section: {
            tailwind: 'flex items-center pt-28 pl-4 2xl:pt-52 2xl:pl-64 2xl:pr-72'
        },
        imageContainer: {
            image: bag,
            tailwind: '',
            css: ''
        },
        beforeHeading: {
            text: 'House of Roperro',
            tailwind: 'uppercase text-2xl text-colorGray38 mb-3'
        },
        heading: {
            text: 'new leather collections',
            tailwind: 'uppercase text-8xl text-hoverYellow font-cinzel mb-4'
        },
        para: {
            text: "The online store presents selected successful collections, fashionable women’s clothing which is sewn exclusively from quality material.",
            
            tailwind: 'text-2xl text-colorGray38 mb-20'
        },
        link: {
            text: 'view all',
            linkHref: '/',
            tailwind: 'w-64 h-16 bg-colorBlack0 text-white text-2xl uppercase flex items-center justify-center',
            css: ''
        }
}

}
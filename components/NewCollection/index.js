import BeforeHeading from '../Typography/BeforeHeading';
import Heading from '../Typography/Heading';
import Para from '../Typography/Para';
import RoperroLink from '../RoperroLink';

const NewCollection = (props) => {
    

    const { section, imageContainer, beforeHeading, heading, para, link, textContainerCss, textContainerTailwind } = props.props;
    
    return (
        <div className={`${section.tailwind}`}>
            <div className={`${imageContainer.tailwind} ${imageContainer.css}`}>
                <img src={imageContainer.image} alt={imageContainer.altTag} />
            </div>
            <div className={`${textContainerCss} ${textContainerTailwind}`}>
                <BeforeHeading text={beforeHeading.text} tailwind={beforeHeading.tailwind} />
                <Heading text={heading.text} tailwind={heading.tailwind} />
                <Para text={para.text} tailwind={para.tailwind} />
                <RoperroLink
                    text={link.text}
                    linkHref={link.linkHref}
                    tailwind={link.tailwind}
                    css={link.css}
                />
            </div>
        </div>
    );
};

export default NewCollection;
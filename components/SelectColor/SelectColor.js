
const SelectColor = ({ hexCode }) => {
    return (
        <div className="select-color" style={{ backgroundColor: hexCode }}></div>
    )
}

export default SelectColor;

import { ReactSVG } from 'react-svg';
import Heading from '../Typography/Heading';
import Para from '../Typography/Para';

const OrderItem = (props) => {

    const { tailwind, css, image, productNameAndDescription, deliveryDate, productQuantity, rightBorder, productPrice } = props;
    const { productName, productDescription, } = productNameAndDescription;
    const { colorTag, color, sizeTag, size } = productDescription;


    return (
        <div className={`${tailwind} ${css}`}>
            <img
                className={image.tailwind}
                src={image.imageSource}
            />

            <div className={productNameAndDescription.tailwind}>
                <Heading
                    text={productName.text}
                    tailwind={productName.tailwind}
                    css={productName.css}
                />
                <div className={productDescription.tailwind}>
                    <span className={colorTag.tailwind}>{colorTag.text}</span>
                    <span className={color.tailwind}>{color.text}</span>
                    <span className={sizeTag.tailwind}>{sizeTag.text}</span>
                    <span className={size.tailwind}>{size.text}</span>
                </div>
            </div>

            <span className={rightBorder.tailwind}>{rightBorder.content}</span>
            <div className={deliveryDate.tailwind}>
                <Para
                    text={deliveryDate.text}
                />
                <Para
                    text={deliveryDate.date}
                />
            </div>
            <span className={rightBorder.tailwind}>{rightBorder.content}</span>
            <Para
                text={`${productQuantity.text} ${productQuantity.quantity}`}
                tailwind={productQuantity.tailwind}
            />
            <span className={rightBorder.tailwind}>{rightBorder.content}</span>
            <div className={productPrice.tailwind}>
                <ReactSVG src={productPrice.priceSvg} className={productPrice.priceSvgTailwind} />
                <span className={productPrice.priceTailwind}>{productPrice.price}</span>
            </div>
        </div>
    );
};

export default OrderItem;
import OrderItem from './index';

import orderImageTwo from '../../public/assets/images/My-orders/12.png';
import IndianRupeeSVGIcon from '../../public/assets/svg/orders/indian-rupee-svg-icon.svg';

export default {
    title: "Roperro/Order Item"
};

const Template = args => <OrderItem {...args} />;

export const Default = Template.bind({});

Default.args = {
    tailwind: 'py-8 flex items-center bg-colorGray90 w-full h-60 rounded-3xl ',
    css: 'padding-left-7percent',

    rightBorder: {
        content: '',
        tailwind: 'h-full border-r border-solid border-colorGray84'
    },
    
    image: {
        imageSource: orderImageTwo,
        tailwind: 'w-44'
    }, 

    productNameAndDescription: {
        tailwind: 'border-r border-solid border-colorGray84  flex-1',
        productName: {
            text: 'Dark green hoody Sweatshirt',
            tailwind: 'text-base font-medium text-colorGray30 mb-4 ml-9'
        },

        productDescription: {
            tailwind: 'grid grid-cols-2 grid-rows-2 gap-y-2 ml-9',
            colorTag: {
                text: 'Color:',
                tailwind: 'text-base font-normal',
            },
    
            color: {
                text: 'Grey', 
                tailwind: 'text-base font-normal'
            },
    
            sizeTag: {
                text: 'Size:',
                tailwind: 'text-base font-normal',
            },
    
            size: {
                text: 'Large', 
                tailwind: 'text-base font-normal'
            },
        }


    }, 

    deliveryDate: {
        text: 'Delivered On',
        date: '25 December 2021', 
        tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex flex-col justify-start items-center'
    }, 
    
    productQuantity: {
        text: 'Quantity: ',
        quantity: 1, 
        tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex justify-center'
    },

    productPrice: {
        tailwind: 'flex-1 flex items-center justify-center text-3xl font-medium',
        priceSvg: IndianRupeeSVGIcon,
        priceSvgTailwind: 'w-3.5 mr-2',
        price: 5000,
        priceTailwind: '' 
    }
}
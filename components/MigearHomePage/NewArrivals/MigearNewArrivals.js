import React from "react";

import MultipleHeading from "../../MultipleHeading/MultipleHeading";

function MigearNewArrivals({ container, textData, display, images }) {
  return (
    <div className={container}>
      <MultipleHeading {...textData} />
      <div className={display}>
        {images &&
          images.map(({ image, rowSpan }, i) => {
            return <img src={image} alt="" key={i} className={rowSpan} />;
          })}
      </div>
    </div>
  );
}

export default MigearNewArrivals;

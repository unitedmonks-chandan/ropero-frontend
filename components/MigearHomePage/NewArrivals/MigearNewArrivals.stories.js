import MigearNewArrivals from "./MigearNewArrivals";

import img1 from "../../../public/assets/images/migear/new-arrivals1.jpg";
import img2 from "../../../public/assets/images/migear/new-arrivals2.jpg";
import img3 from "../../../public/assets/images/migear/new-arrivals3.jpg";

export default {
  title: "Roperro / MiGear / New Arrivals",
};

const Template = (args) => <MigearNewArrivals {...args} />;

export const Desing1 = Template.bind({});

Desing1.args = {
  container: "bg-black pl-20 py-24 grid grid-cols-3 gap-8 items-center",
  textData: {
    heading1: "New",
    heading1Style: "text-white text-6xl font-cinzel uppercase mb-1",
    heading2: "Arrivals",
    heading2Style: "text-white text-6xl font-cinzel uppercase mb-8",
    heading3:
      "The online store presents selected successful collections, fashionable women’s clothing which is sewn exclusively from fquality material.",
    heading3Style: "text-gray100 text-2xl",
  },
  display: "col-span-2 grid grid-cols-2 gap-4",
  images: [
    {
      image: img1,
      rowSpan: "row-span-2 h-full",
    },
    {
      image: img2,
      rowSpan: "",
    },
    {
      image: img3,
      rowSpan: "",
    },
  ],
};

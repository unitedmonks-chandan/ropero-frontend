import MigearBestSellers from "./MigearBestSellers";
import img1 from "../../../public/assets/images/migear/best-sellers1.jpg";
import img2 from "../../../public/assets/images/migear/best-sellers2.jpg";
import img3 from "../../../public/assets/images/migear/best-sellers3.jpg";
import img4 from "../../../public/assets/images/migear/best-sellers4.jpg";

export default {
  title: "Roperro / MiGear / Best Sellers",
};

const Template = (args) => <MigearBestSellers {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  display: "bg-black",
  textData: {
    container: "py-20 text-center",
    position: "",
    heading1: "Best Sellers",
    heading1Style: "text-white text-6xl font-cinzel uppercase mb-10",
    heading2:
      "The online store presents selected successful collections, fashionable",
    heading2Style: "text-gray100 text-2xl font-primary uppercase mb-3",
    heading3: "women’s clothing which is sewn exclusively from quality material.",
    heading3Style: "text-gray100 text-2xl font-primary uppercase mb-3",
  },
  carouselData: {
    customSlidersPerView: 4,
    customSlidesPerGroup: 4,
    customSpaceBetween: 16,
    customStyle: "hide-inbuilt-arrow hide-left-hidden-arrow hide-right-hidden-arrow migear-best-sellers-slider",
    images: [
      {img: img1, altTag: 'backpack'},
      {img: img2, altTag: 'backpack'},
      {img: img3, altTag: 'backpack'},
      {img: img4, altTag: 'backpack'},
      {img: img4, altTag: 'backpack'},
      {img: img3, altTag: 'backpack'},
      {img: img2, altTag: 'backpack'},
      {img: img1, altTag: 'backpack'},
    ],
  },
};

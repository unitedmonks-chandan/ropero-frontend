import MigearNewCollection from "./MigearNewCollection";
import img1 from "../../../public/assets/images/migear/new-collection.jpg";
import img2 from "../../../public/assets/images/migear/new-collection.jpg";

export default {
  title: "Roperro / MiGear / New Collection",
};

const Template = (args) => <MigearNewCollection {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  display: "grid grid-cols-1 xl:grid-cols-4",
  textData: {
    container: "pl-4 lg:pl-28 pt-16 pb-16 lg:pb-0 lg:pt-60",
    position: "",
    heading1: "New",
    heading1Style: "text-midGray text-5xl 2xl:text-6xl font-cinzel uppercase",
    heading2: "Collection",
    heading2Style: "text-midGray text-5xl 2xl:text-6xl font-cinzel uppercase mb-3",
    heading3:
      "The online store presents selected successful collections, fashionable women’s clothing which is sewn fexclusively from quality material.",
    heading3Style: "text-midGray text-xl mb-5",
    heading4: "50% OFF",
    heading4Style: "text-lightRed100 text-5xl font-primary uppercase",
  },
  carouselData: {
    customSlidersPerView: 1,
    customSlidesPerGroup: 1,
    customSpaceBetween: 0.1,
    customStyle:
      "hide-inbuilt-arrow hide-left-hidden-arrow hide-pagination-bullets right-white-arrow py-20 pr-64 migear-new-collection-bg col-span-2 relative reset-swiper-container migear-new-collection-slider",
    images: [
      {img: img1, altTag: 'hiker backpack'}, 
      {img: img2, altTag: 'hiker backpack'}
    ],
  },
};
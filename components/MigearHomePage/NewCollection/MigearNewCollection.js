import React from "react";

import MultipleHeading from "../../MultipleHeading/MultipleHeading";
import NewCarousel from "../../NewCarousel/NewCarousel";
//import "./custom.css";

function MigearNewCollection({ display, textData, carouselData }) {
  return (
    <div className={display}>
      <MultipleHeading {...textData} />
      <div className="invisible">hidden</div>
      <NewCarousel {...carouselData} />
    </div>
  );
}

export default MigearNewCollection;

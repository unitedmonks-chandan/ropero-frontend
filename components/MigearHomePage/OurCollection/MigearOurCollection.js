import react from "react";
import Link from "next/link";
import LinkButton from "../../NewButtons/LinkButton/LinkButton";

function MigearOurCollection({
  container,
  heading,
  headingStyle,
  display,
  imagesData,
  btnData,
}) {
  return (
    <div className={container}>
      <p className={headingStyle}>{heading}</p>
      <div className={display}>
        {imagesData &&
          imagesData.map(({ position, image, imgLink, text, textStyle }, i) => {
            return (
              <div key={i} className={position}>
                <Link href={imgLink}>
                  <a>
                    <img src={image} alt={text} />
                  </a>
                </Link>
                <p className={textStyle}>{text}</p>
              </div>
            );
          })}
      </div>
      <LinkButton {...btnData} />
    </div>
  );
}

export default MigearOurCollection;

import MigearOurCollection from "./MigearOurCollection";

import img1 from "../../../public/assets/images/migear/our-collection1.jpg";
import img2 from "../../../public/assets/images/migear/our-collection2.jpg";
import img3 from "../../../public/assets/images/migear/our-collection3.jpg";
import img4 from "../../../public/assets/images/migear/our-collection4.jpg";

export default {
  title: "Roperro / MiGear / Our Collection",
};

const Template = (args) => <MigearOurCollection {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  container: "bg-black py-24 px-16 text-center",
  heading: "Our Collection",
  headingStyle:
    "text-lightRed text-6xl font-cinzel uppercase mb-16 text-center",
  display: "grid grid-cols-4 gap-4",
  imagesData: [
    {
      position: "relative",
      image: img1,
      imgLink: "/rucksack",
      text: "Rucksack",
      textStyle:
        "text-white text-2xl font-primary font-medium uppercase absolute bottom-5 left-20",
    },
    {
      position: "relative",
      image: img2,
      imgLink: "/backpack",
      text: "Backpack",
      textStyle:
        "text-white text-2xl font-primary font-medium uppercase absolute bottom-5 left-20",
    },
    {
      position: "relative",
      image: img3,
      imgLink: "duffle",
      text: "Duffle",
      textStyle:
        "text-white text-2xl font-primary font-medium uppercase absolute bottom-5 left-20",
    },
    {
      position: "relative",
      image: img4,
      imgLink: "/laptop-bags",
      text: "Laptop bags",
      textStyle:
        "text-white text-2xl font-primary font-medium uppercase absolute bottom-5 left-20",
    },
  ],
  btnData: {
    btnLink: "/view-collection",
    btn: "inline-block bg-white text-black text-lg uppercase border border-black px-5 py-1 mt-8",
    text: "View collection",
    size: " ",
  },
};

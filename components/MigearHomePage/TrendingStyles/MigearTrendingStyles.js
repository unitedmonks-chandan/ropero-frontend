import React from "react";

import LinkButtonWithHeading from "../../NewButtons/LinkButtonWithHeading/LinkButtonWithHeading";
import NewCarousel from "../../NewCarousel/NewCarousel";
//import "./custom.css";

function MigearTrendingStyles({ display, btnData, carouselData }) {
  return (
    <div className={display}>
      <LinkButtonWithHeading {...btnData} />
      <NewCarousel {...carouselData} />
    </div>
  );
}

export default MigearTrendingStyles;

import MigearTrendingStyles from "./MigearTrendingStyles";
import img1 from "../../../public/assets/images/migear/trending-styles.jpg";
import img2 from "../../../public/assets/images/migear/trending-styles.jpg";

export default {
  title: "Roperro / MiGear / Trending Styles",
};

const Template = (args) => <MigearTrendingStyles {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  display: "grid grid-cols-1 lg:grid-cols-3 ",
  btnData: {
    container:
      "migear-trending-styles-bg pl-4 lg:pl-28 pt-16 pb-16 lg:pb-0 lg:pt-60",
    position: "",
    headingStyle: "text-white text-7xl font-cinzel uppercase mb-8",
    heading1: "Trendy",
    heading2: "Styles",
    btnLink: "/view-all",
    btn: "inline-block bg-white text-black text-lg font-primary px-7 py-1",
    size: "",
    btnText: "View All",
  },
  carouselData: {
    customSlidersPerView: 1,
    customSlidesPerGroup: 1,
    customSpaceBetween: 0.1,
    customStyle:
      "col-span-2 bg-black pr-20 hide-inbuilt-arrow hide-left-hidden-arrow hide-pagination-bullets right-white-arrow reset-swiper-container relative",
    images: [
      {img: img1, altTag: 'backpack'},
      {img: img2, altTag: 'backpack'}
    ],
  },
};

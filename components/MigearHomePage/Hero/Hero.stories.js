import HeroNewCarousel from "../../HeroNewCarousel/HeroNewCarousel";

import img1 from "../../../public/assets/images/migear/hero.jpg";
import img2 from "../../../public/assets/images/migear/hero.jpg";
import logo from "../../../public/assets/svg/migear-logo.svg";

export default {
  title: "Roperro / MiGear / Hero",
};

const Template = (args) => <HeroNewCarousel {...args} />;

export const Design1 = Template.bind({});

Design1.args = {
  customStyle:
    "relative h-screen hide-inbuilt-arrow hide-left-hidden-arrow right-white-arrow2 white-pagination-bullet red-pagination-bullet-active migear-hero-slider",
  carouselData: [
    {
      image: img1,
      altTag: 'Man with MiGEAR bag',
      btnData: {
        position: "absolute btn-position z-50 flex flex-col gap-8",
        logo,
        logoSize: "slider-logo-size",
        btnLink: "/migear/shop-now",
        btn: "bg-white text-black text-lg font-primary px-7 py-1 uppercase self-center",
        btnText: "Shop now",
      },
    },
    {
      image: img2,
      altTag: 'Man with MiGEAR bag',
      btnData: {
        position: "absolute btn-position z-50 flex flex-col gap-8",
        logo,
        logoSize: "slider-logo-size",
        btnLink: "/migear/view-all",
        btn: "bg-midRed text-white text-lg font-primary px-7 py-1 uppercase self-center",
        btnText: "View all",
      },
    },
  ],
};

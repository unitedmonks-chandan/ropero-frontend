import React from "react";
import { ReactSVG } from "react-svg";
import ShopNowNew from "../ShopNowNew/ShopNowNew";
// import "./our-collection.css";

function OurCollection({
  stripContainer,
  stripText1,
  stripText2,
  stripText3,
  stripText1Style,
  stripText2Style,
  stripText3Style,
  display,
  shopNowCompsData,
}) {
  return (
    <div className="w-full">
      {stripContainer && (
        <div className={stripContainer}>
          {stripText1 && <p className={stripText1Style}>{stripText1}</p>}
          {stripText2 && <p className={stripText2Style}>{stripText2}</p>}
          {stripText3 && <p className={stripText3Style}>{stripText3}</p>}
        </div>
      )}
      {/* <div className={display}>
        {shopNowCompsData &&
          shopNowCompsData.map(
            (
              {
                bgImg,
                colSpan,
                container,
                position,
                headingStyle,
                heading1,
                heading2,
                btnContainer,
                btnLink,
                btn,
                size,
                btnText,
              },
              index
            ) => (
              <ShopNowNew
                key={index}
                colSpan={colSpan}
                bgImg={bgImg}
                container={container}
                position={position}
                headingStyle={headingStyle}
                heading1={heading1}
                heading2={heading2}
                btnContainer={btnContainer}
                btnLink={btnLink}
                btn={btn}
                size={size}
                btnText={btnText}
              />
            )
          )}
      </div> */}
      {/* method 2 */}
      <div className={display}>
        {shopNowCompsData &&
          shopNowCompsData.map((shopNowData, index) => (
            <ShopNowNew {...shopNowData} key={index} />
          ))}
      </div>
    </div>
  );
}

export default OurCollection;

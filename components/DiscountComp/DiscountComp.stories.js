import DiscountComp from "./DiscountComp";
import discountImg from "../../public/assets/images/discount-comp.png";
import arkarDiscountImg from "../../public/assets/images/arkar/discount.jpg";
import arkarLogo from "../../public/assets/images/arkar/arkar-logo.svg";

export default {
  title: "Roperro / Homepage / Discount Comp",
};

const Template = (args) => <DiscountComp {...args} />;

export const withoutLogo = Template.bind({});

withoutLogo.args = {
  image: discountImg,
  container: "font-cinzel",
  containerPosition: "absolute left-20 heading-position",
  heading1: "HOUSE OF ROPERRO",
  heading1Style: "text-white text-lg uppercase mb-2",
  heading2: "Leather",
  heading2Style: "text-white text-6xl uppercase",
  heading3: "Collection",
  heading3Style: "text-white text-6xl uppercase mb-8",
  heading4: "50% off",
  heading4Style: "text-hoverYellow text-8xl uppercase",
};

export const withLogo = Template.bind({});
withLogo.args = {
  image: arkarDiscountImg,
  container: "bg-midRed text-white font-primary px-16 pt-8 pb-10",
  containerPosition: "absolute left-16 heading-position",
  logo: arkarLogo,
  logoSize: "w-24 mb-7",
  heading1: "HOUSE OF ROPERRO",
  heading1Style: "text-lg mb-1",
  heading2: "Leather",
  heading2Style: "text-4xl",
  heading3: "Collection",
  heading3Style: "text-5xl mb-5",
  heading4: "50% off",
  heading4Style: "text-5xl",
};

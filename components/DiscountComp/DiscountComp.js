import React from "react";

function DiscountComp({
  image,
  container,
  containerPosition,
  logo,
  logoSize,
  heading1,
  heading1Style,
  heading2,
  heading2Style,
  heading3,
  heading3Style,
  heading4,
  heading4Style,
}) {
  return (
    <div className="relative">
      <img src={image} alt="" className="w-full" />
      <div
        className={`${container} ${containerPosition ? containerPosition : ""}`}
      >
        {logo && <img src={logo} alt="" className={logoSize} />}
        <p className={heading1Style}>{heading1}</p>
        <p className={heading2Style}>{heading2}</p>
        <p className={heading3Style}>{heading3}</p>
        <p className={heading4Style}>{heading4}</p>
      </div>
    </div>
  );
}

export default DiscountComp;

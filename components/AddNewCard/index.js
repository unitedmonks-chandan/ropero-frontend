import Heading from '../Typography/Heading';
import RoperroLink from '../RoperroLink';

const AddNewCard = (props) => {

    const { tailwind, css, plus, heading } = props;
    const { link } = plus;

    return (
        <div className={`${tailwind} ${css}`} >
            <div className={`${plus.tailwind} ${plus.css}`}>
                <RoperroLink
                    text={link.text}
                    tailwind={link.tailwind}
                    linkHref={link.linkHref}
                    css={link.css}
                />
            </div>

            <Heading
                text={heading.text}
                tailwind={heading.tailwind}
                css={heading.css}
            />
        </div>
    );
};

export default AddNewCard;


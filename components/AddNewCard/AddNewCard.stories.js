import AddNewCard from './index';

export default {
    title: "Roperro"
}

const Template = args => <AddNewCard {...args} />;

export const AddCard = Template.bind({});

AddCard.args = {
    tailwind: 'bg-colorGray95 flex flex-col items-center justify-center',
    css: 'width-488px height-268px',

    plus: {
        tailwind: 'bg-colorGray79 flex items-center justify-center',
        css: 'width-81px height-81px border-radius-50percent', 

        link: {
            text: '+',
            linkHref: '/',
            tailwind: 'text-colorGray37 flex items-baseline -mr-.5 -mb-1',
            css: '  font-size-64px line-height-16px'
        }
    },

    heading: {
        text: 'Add New Card',
        tailwind: 'text-3xl text-colorGray30 mt-7',
        css: ''
    }

}
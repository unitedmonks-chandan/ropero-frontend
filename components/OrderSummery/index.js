import Heading from '../Typography/Heading';
import RoperroLink from '../RoperroLink';
import CartItem from '../CartItem';
import Para from '../Typography/Para';
import { ReactSVG } from 'react-svg';
import { useState, useEffect } from 'react';

const OrderSummery = (props) => {

    // Getting data from parent 
    const { tailwind, css, headingContainer, cartItems, totalAmount } = props;

    // Destructuing data
    const { orderHeading, editLink } = headingContainer;
    const { currency, subTotal, shippingCharge, totalToPay } = totalAmount;

    // Initial sub total amount of all products. Products subtotal amount will add for every iteration of cart item loop.
    let initialTotal = 0;

    // Product sub total amount. It will be set for the first time when cart item loop completed and all products values added to initialTotal i.e. in useEffect.
    const [subTotalAmount, setSubTotalAmount] = useState(0);

    // Shipping charges amount is coming from parent
    const shippingChargesAmount = shippingCharge.amount.value;

    // Total amount is sum of sub total and shipping charge amount
    const TotalToPayAmount = shippingChargesAmount > 0? subTotalAmount + shippingChargesAmount: subTotalAmount;

    // This function is passed in cart item loop and is called for each iteration. It is bringing initial sum of every product sub total amount. 
    const handleInitialTotal = (productInitialAmountSum) => {
        initialTotal = initialTotal + productInitialAmountSum;
    }

    // Setting initial sub total of all products to sub total amount
    useEffect(() => {
        setSubTotalAmount(initialTotal);
    },[]);


    // Incrementing or decrementing sub total if any product quantity is changed.
    const handleSubTotalOnQuantityChange = (updateAmountBy) => {
        setSubTotalAmount(subTotalAmount + updateAmountBy);
    }

    
    // Cart item loop. 
    // Rendering all products and bringing  sum of every product value. For eg. if a product is Rs. 500 with 2 quantity then it will bring Rs. 1000 in handleInitialTotal. 
    // It also bring the product value with + or - if quantity of a product is increases or decreases in handleSubTotalOnQuantityChange. 
    const renderCartItems = () => {
        return cartItems.map((item, i) => {
            return (
                <CartItem key={i}
                    tailwind={item.tailwind}
                    css={item.css}
                    image={item.image}
                    textContainer={item.textContainer}
                    handleInitialTotal={handleInitialTotal}
                    handleSubTotalOnQuantityChange={handleSubTotalOnQuantityChange}
                />
            )
        })
    }


    // Rendering shipping charge. It will be either free i.e. 0 or more than 0
    const renderShippingCharge = () => {
        if (shippingChargesAmount > 0) {
            return (
                <>
                    <ReactSVG src={currency.svg} className={currency.tailwind} />
                    <Para text={shippingChargesAmount} tailwind={shippingCharge.amount.tailwind} />
                </>
            )
        } else {
            return (
                <>
                    <Para text="Free" tailwind={shippingCharge.amount.freeTextTailwind} />
                </> 
            )
        }
    }

    return (
        <div className={`${tailwind} ${css}`}>

            {/* Rendering Total Items and Edit Button i.e. first line */}
            <div className={headingContainer.tailwind}>
                <Heading text={`${orderHeading.text} Items`} tailwind={orderHeading.tailwind} />
                <RoperroLink
                    text={editLink.text}
                    backgroundTailwind={editLink.backgroundTailwind}
                    linkHref={editLink.linkHref}
                    tailwind={editLink.tailwind}
                    css={editLink.css}
                />
            </div>


            {/* Rendering Cart Items */}
            {renderCartItems()}

            {/* Rendering amounts breaks or order summery */}
            {/* Rendering SubTotal */}
            <div className={`${totalAmount.tailwind} ${totalAmount.css}`}>
                <div className={`${subTotal.tailwind} ${subTotal.css}`}>
                    <Para text={subTotal.costText.text} tailwind={subTotal.costText.tailwind} />
                    <ReactSVG src={currency.svg} className={currency.tailwind} />
                    <Para text={subTotalAmount} tailwind={subTotal.amount.tailwind} />
                </div>

                {/* Rendering Shipping Charge */}
                <div className={`${shippingCharge.tailwind} ${shippingCharge.css}`}>
                    <Para text={shippingCharge.costText.text} tailwind={subTotal.costText.tailwind} />

                    {/* Shipping may be Free so it is rendered by function */}
                    {renderShippingCharge()}
                </div>

                {/* Rendering Total Amount to be paid by customer for this order */}
                <div className={`${totalToPay.tailwind} ${totalToPay.css}`}>
                    <Para text={totalToPay.costText.text} tailwind={totalToPay.costText.tailwind} />
                    <ReactSVG src={currency.svg} className={currency.tailwind} />
                    <Para text={TotalToPayAmount} tailwind={totalToPay.amount.tailwind} />
                </div>
            </div>

        </div>
    );
};

export default OrderSummery;
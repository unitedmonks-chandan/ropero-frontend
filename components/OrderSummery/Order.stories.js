import OrderSummery from "./index";

import itemOne from '../../public/assets/images/cart/itemOne.jpg';
import itemTwo from '../../public/assets/images/cart/itemTwo.jpg';
import RupeeSVGIcon from '../../public/assets/svg/checkout/rupee-svg-icon.svg';
import MinusSVGIcon from '../../public/assets/svg/checkout/minus-svg-icon.svg';
import PlusSVGIconBlack from '../../public/assets/svg/checkout/plus-svg-icon-black.svg';


export default {
    title: "Roperro/Order"
}

const Template = args => <OrderSummery {...args} />;

export const Order = Template.bind({});

Order.args = {

    tailwind: 'pt-10 pl-6 pr-6 pb-10',
    css: 'width-468px height-600px black-border-point5px overflow-auto',

    headingContainer: {
        tailwind: 'flex items-center justify-between  mb-10',

        orderHeading: {
            text: 2,
            tailwind: 'font-semibold text-xl text-colorBlack5'
        },

        editLink: {
            text: 'Edit',
            linkHref: '/', 
            backgroundTailwind: '', 
            tailwind: 'font-medium text-xl text-hoverYellow', 
            css: ''
        }
    },

    //////////////////////// Cart Items 

    cartItems: [
                    
        {
            tailwind: 'flex items-start mb-10',
            css: '',

            image: {
                imageSource: itemOne,
                altTag: 'Girl in Grey Sweatshirt',
                imageTailwind: '',
                imageCss: '',
                imageWrapperTailwind: '',
                imageWrapperCss: ''
            },
                        
            textContainer: {
                tailwind: 'ml-6 -mt-1.5',
                css: '',
                
                heading: {
                    text: 'Light Grey Hoody Sweatshirt',
                    tailwind: 'text-base font-medium text-colorGray30',
                    css: ''
                },

                price: {
                    tailwind: 'flex items-center mt-1.5',
                    css: '',
                    
                    icon: {
                        currency: RupeeSVGIcon,
                        tailwind: 'w-2 h-3 mr-1',
                        css: ''
                    },
                        
                    cost: {
                        cost: 5000,
                        tailwind: 'text-base font-medium text-colorBlack5',
                        css: ''
                    }
                },

                color: {
                    value: 'Grey',
                    tailwind: 'text-normal font-normal text-colorGray30 mt-1.5',
                    css: ''
                },
                    
                size: {
                    value: 'Medium',
                    tailwind: 'text-normal font-normal text-colorGray30 mt-1.5',
                    css: ''
                },
                    
                quantity: {
                    value: 2,
                    tailwind: 'text-normal font-normal text-colorGray30 mt-1.5',
                    css: ''
                },

                buttons: {
                    tailwind: 'flex items-center mt-1',
                    css: '',
                    
                    minus: {
                        svg: MinusSVGIcon,
                        tailwind: 'w-5 h-5 bg-colorGray88 flex items-center justify-center rounded-full',
                        css: ''
                    },
                    
                    quantityInBetweenbuttons: {
                        tailwind: 'ml-1.5 mr-1.5',
                        css: ''
                    },
                    
                    plus: {
                        svg: PlusSVGIconBlack,
                        tailwind: 'w-5 h-5 bg-hoverYellow flex items-center justify-center rounded-full',
                        css: ''
                    }
                }
            }
        },

        /////////////////// Second Item of cart
        {
            tailwind: 'flex items-start mb-10',
            css: '',

            image: {
                imageSource: itemTwo,
                altTag: 'Girl in Grey Sweatshirt',
                imageTailwind: '',
                imageCss: '',
                imageWrapperTailwind: '',
                imageWrapperCss: ''
            },
                        
            textContainer: {
                tailwind: 'ml-6 -mt-1.5',
                css: '',
                
                heading: {
                    text: 'Dark Green Hoody Sweatshirt',
                    tailwind: 'text-base font-medium text-colorGray30',
                    css: ''
                },

                price: {
                    tailwind: 'flex items-center mt-1.5',
                    css: '',
                    
                    icon: {
                        currency: RupeeSVGIcon,
                        tailwind: 'w-2 h-3 mr-1',
                        css: ''
                    },
                        
                    cost: {
                        cost: 7500,
                        tailwind: 'text-base font-medium text-colorBlack5',
                        css: ''
                    }
                },

                color: {
                    value: 'Green',
                    tailwind: 'text-normal font-normal text-colorGray30 mt-1.5',
                    css: ''
                },
                    
                size: {
                    value: 'Large',
                    tailwind: 'text-normal font-normal text-colorGray30 mt-1.5',
                    css: ''
                },
                    
                quantity: {
                    value: 1,
                    tailwind: 'text-normal font-normal text-colorGray30 mt-1.5',
                    css: ''
                },

                buttons: {
                    tailwind: 'flex items-center mt-1',
                    css: '',
                    
                    minus: {
                        svg: MinusSVGIcon,
                        tailwind: 'w-5 h-5 bg-colorGray88 flex items-center justify-center rounded-full',
                        css: ''
                    },
                    
                    quantityInBetweenbuttons: {
                        tailwind: 'ml-1.5 mr-1.5',
                        css: ''
                    },
                    
                    plus: {
                        svg: PlusSVGIconBlack,
                        tailwind: 'w-5 h-5 bg-hoverYellow flex items-center justify-center rounded-full',
                        css: ''
                    }
                }
            }
        }
    ],

    //////////////////////////////////// order summery

    totalAmount: {
        tailwind: 'text-lg font-medium text-colorGray30',
        css: 'black-top-border-point5px',
            
        currency: {
            svg: RupeeSVGIcon,
            tailwind: 'w-2 h-3 mr-1'
        },

        subTotal: {
            tailwind: 'flex items-baseline justify-start mt-10',
            
            costText: {
                text: 'Sub Total',
                tailwind: 'mr-auto'
            },
            amount: {
                tailwind: 'text-base font-medium text-colorGray30'
            }
        },

        shippingCharge: {
            tailwind: 'flex items-baseline justify-start mt-2.5', 

            costText: {
                text: 'Shipping Charge',
                tailwind: 'mr-auto'
            },
            amount: {
                value: 0,
                tailwind: 'text-base font-medium text-colorGray30',
                freeTextTailwind: 'text-base font-semibold text-hoverYellow uppercase'
            }
        },

        totalToPay: {
            tailwind: 'flex items-baseline justify-start mt-2.5', 

            costText: {
                text: 'Total To Pay',
                tailwind: 'mr-auto'
            },
            amount: {
                tailwind: 'text-base font-medium text-colorGray30'
            }
        }
    }

}
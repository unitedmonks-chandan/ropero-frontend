import CustomForm from "./index"
import textImg from "../../public/assets/images/Customer-Form/assets/cs-text.svg";
import pn from "../../public/assets/images/Customer-Form/assets/phone-cs.svg";
import mail from "../../public/assets/images/Customer-Form/assets/mail-cs.svg";
import map from "../../public/assets/images/Customer-Form/assets/map-cs.svg";


export default {
    title: "Custom/Main-page"
}

const Template = (argument) => <CustomForm {...argument} />
export const mainPage = Template.bind({});

mainPage.args = {

    props: {
        text_img: textImg,
        get_text: "Get In Touch",
        incude_text1: " We will respond to every email within 24 hours, Monday to Friday, excluding",
        incude_text2: "holidays. You can also call us at our toll-free number +0000-000-0000.",
        first_Name: "First Name",
        text: "text",
        last_Name: "Last Name",
        phone_no: "Phone",
        number: "number",
        email_text: "Email",
        email: "email",
        subject: "Subject",
        msg_text: "Message",
        btn: "Send",
        phoneImage: pn,
        phone: "Phone",
        num1: "1800 0000 000",
        num2: "1800 0000 000",
        mail: mail,
        mail_text: "E-Mail",
        emailId: "reporre@.com",
        map: map,
        address: "Address",
        addres_text1: "Lorem ipsum dolor",
        addres_text2: "sit amet,"

    }

}
import Msg from "./index";
export default {
    title: "Custom/msg"
}
const Templete = (argument) => <Msg {...argument} />
export const msg = Templete.bind({});
msg.args = {
    msgText: "Message",
    TypeAttribute: "text"
}
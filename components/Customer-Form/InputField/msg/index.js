import React from 'react'

const Msg = ({ msgText, TypeAttribute }) => {
    return (
        <div className="flex flex-wrap msg_style">
            <div className="w-full">
                <label
                    className="block mt-10 mb-4  text-sm md:text-xl lg:text-2xl xl:text-2xl font-300 font-Montserrat"
                    htmlFor="formGridCode_card">{msgText}</label>
                <textarea
                    className="w-full px-3 pt-3 bg-transparent border border-[#474747] text-[#D1D1D1] text-sm md:text-xl lg:text-xl xl:text-xl font-300 font-Montserrat placeholder-[#D1D1D1] focus:shadow-outline"
                    id="message" type={TypeAttribute} rows="5" placeholder=""></textarea>
            </div>
        </div>
    )
}

export default Msg
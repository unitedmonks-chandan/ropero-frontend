import Input from "./index"

export default {
    title: "Custom/input"
}
const Template = (argument) => <Input {...argument} />
export const inputData = Template.bind({});
inputData.args = {
    labelText: "First Name",
    TypeAttribute: "text"
}
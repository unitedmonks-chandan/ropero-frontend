import React from 'react';

const Subject = ({ subText, TypeAttribute }) => {
    return (
        <div className="flex flex-wrap">
            <div className="w-full sub_style">
                <label
                    className="block mt-10 mb-4 text-sm md:text-xl lg:text-2xl xl:text-2xl font-light "
                    htmlFor="formGridCode_card">{subText}</label>

                <input className="w-full h-10 px-3 bg-transparent border border-[#474747]" type={TypeAttribute} id="subject" autoComplete='off' required />

                <input className="w-full h-10 px-3 bg-transparent text-white border border-[#474747]" type="text" id="subject" required />

            </div>
        </div>
    )
}

export default Subject
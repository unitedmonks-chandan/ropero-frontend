import Subject from "./index"

export default {
    title: "Custom/subject"
}
const Template = (argument) => <Subject {...argument} />
export const sub = Template.bind({})
sub.args = {
    subText: "Subject",
    TypeAttribute: "text"
}
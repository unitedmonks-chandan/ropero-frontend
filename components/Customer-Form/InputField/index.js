import React from 'react';

const Input = ({ labelText, TypeAttribute }) => {
    return (
        <div className="w-full px-2 md:w-1/2 inputFelid">
            <label
                className="block mt-10 mb-4 text-sm md:text-xl lg:text-2xl xl:text-2xl font-light	"
                htmlFor="formGridCode_name">{labelText}</label>

            <input className="w-full h-10 px-3 bg-transparent"
                type={TypeAttribute}
                autoComplete='off'
                required />
        </div>
    )
}

export default Input
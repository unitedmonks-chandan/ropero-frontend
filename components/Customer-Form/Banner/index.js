import React from 'react';

const Banner = ({ bgImg }) => {
    return (
        <div
            className="banner  
            bg-black bg-no-repeat bg-center  pb-24 w-full text-center text-white flex">
            <h3 className="m-auto pt-10">
                <img src={bgImg} className="w-auto h-auto sm:object-center sm:h-10	" alt="" />
            </h3>

        </div>
    )
}

export default Banner;
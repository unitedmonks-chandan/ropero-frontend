import Banner from "./index"
import textImg from "../../../public/assets/images/Customer-Form/assets/cs-text.svg"

export default {
    title: "Custom/banner"
}

const Template = (argument) => <Banner {...argument} />
export const banner = Template.bind({})
banner.args = {
    TextImage: textImg
}
import Social from "./index";
import pn from "../../../public/assets/images/Customer-Form/assets/phone-cs.svg"
export default {
    title: "Custom/phone"
}
const Template = (argument) => <Social {...argument} />
export const phoneNo = Template.bind({});
phoneNo.args = {
    phone_text: "Phone",
    phoneImg: pn
}
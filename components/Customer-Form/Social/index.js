import React from 'react'

const Social = ({ phoneImg, phone_text, add_text1, add_text2 }) => {
    return (
        <div className="border-r border-[#474747] social">
            <div className="md:flex md:items-start md:-mx-4">
                <span className="inline-block p-0 md:p-2 md:mx-2">
                    <img src={phoneImg} className="h-7 md:h-8 lg:h-8 xl:h-full w-auto" alt="" />
                </span>
                <div className="mt-4 md:mx-4 md:mt-0">
                    <h3 className="mb-2 lg:mb-6 text-sm md:text-xl lg:text-2xl
                     xl:text-2xl">{phone_text} </h3>
                    <p
                        className="mt-3 text-[#D1D1D1] text-sm md:text-xl lg:text-xl xl:text-2xl md:leading-5 lg:leading-[34px] xl:leading-[38px] font-[300] font-Montserrat">
                        {add_text1}<br />{add_text2}</p>
                </div>
            </div>
        </div>
    )
}

export default Social
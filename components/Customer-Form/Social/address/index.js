import React from 'react'

const Address = ({ phoneImg, address_text, addres_text1, addres_text2 }) => {
    return (
        <div className="Address">
            <div className="md:flex md:items-start md:-mx-4">
                <span className="inline-block p-0 md:p-2 md:mx-2">
                    <img src={phoneImg} className="h-7 md:h-8 lg:h-8 xl:h-full w-auto" alt="" />
                </span>
                <div className="mt-4 md:mx-4 md:mt-0">
                    <h3 className="mb-2 lg:mb-6 text-sm md:text-xl lg:text-2xl
                     xl:text-2xl">{address_text} </h3>
                    <p
                        className="mt-3 text-[#D1D1D1] text-sm md:text-xl lg:text-xl xl:text-2xl md:leading-5 lg:leading-[34px] xl:leading-[38px] font-[300] font-Montserrat">
                        {addres_text1}<br />{addres_text2}</p>
                </div>
            </div>
        </div>
    )
}

export default Address
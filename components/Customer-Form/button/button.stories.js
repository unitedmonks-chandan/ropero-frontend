import Button from "./index"


export default {
    title: "Custom/button"
}
const Template = (argument) => <Button {...argument} />
export const button = Template.bind({})
button.args = {
    btn_text: "Send"
}
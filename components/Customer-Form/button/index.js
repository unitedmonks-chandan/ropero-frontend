import React from 'react'

const Button = ({ btn_text }) => {
    return (
        <div className="flex flex-wrap sm:mx-8 md:mx-12 lg:mx-24 customF_btn">
            <div className="w-full">
                <button
                    className="mt-10 text-black  border-0 px-10 
                text-sm md:text-xl lg:text-xl xl:text-3xl">{btn_text} </button>
            </div>
        </div>
    )
}

export default Button
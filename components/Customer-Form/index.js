import React from 'react'
import Banner from './Banner/index';
import Title from "./Title/index";
import Input from "./InputField/index";
import Subject from "./InputField/sub/index";
import Msg from "./InputField/msg/index";
import Button from "./button/index"
import Social from "./Social/index";
import Address from "./Social/address/index"

const CustomForm = (props) => {

    const { text_img, get_text, incude_text1, incude_text2, first_Name, text, last_Name,
        phone_no, email_text, email, subject, msg_text, btn, phoneImage, phone, number, num1, num2,
        mail, mail_text, emailId, map, address, addres_text1, addres_text2 } = props.props;

    return (
        <div className="bg-black">

            <Banner bgImg={text_img} />

            <div className=" sm:mx-8 md:mx-12 lg:mx-24 flex justify-between ">

                <Title
                    get_text={get_text}
                    incude_text1={incude_text1}
                    incude_text2={incude_text2} />
            </div>

            <form className="sm:mx-8 md:mx-12 lg:mx-24">
                <div className="flex flex-wrap -mx-2 space-y-4 md:space-y-0">

                    <Input labelText={first_Name}
                        TypeAttribute={text} />

                    <Input labelText={last_Name}
                        TypeAttribute={text} />

                </div>
                <div className="flex flex-wrap -mx-2 space-y-4 md:space-y-0">
                    <Input labelText={phone_no}
                        TypeAttribute={number} />

                    <Input labelText={email_text}
                        TypeAttribute={email} />
                </div>
                <Subject subText={subject}
                    TypeAttribute={text} />

                <Msg msgText={msg_text}
                    TypeAttribute={text} />
            </form>

            <Button btn_text={btn} />

            <div>
                <div className="sm:mx-8 md:mx-12 lg:mx-24 py-10  grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-8">
                    <Social
                        phoneImg={phoneImage}
                        phone_text={phone}
                        add_text1={num1}
                        add_text2={num2} />

                    <Social
                        phoneImg={mail}
                        phone_text={mail_text}
                        add_text1={emailId} />

                    <Address
                        phoneImg={map}
                        address_text={address}
                        addres_text1={addres_text1}
                        addres_text2={addres_text2} />
                </div>

            </div>
        </div>

    )
}

export default CustomForm;
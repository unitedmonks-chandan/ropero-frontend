import React from 'react'

const index = ({ get_text, incude_text1, incude_text2 }) => {

    return (
        <div className="text-left Title_text">
            <h3
                className="text-sm md:text-xl lg:text-2xl xl:text-5xl pt-10">
                {get_text}</h3>
            <p></p>
            <p
                className=" mt-10 mb-12 text-sm  md:text-base lg:text-xl xl:text-xl
                md:leading-8 lg:leading-8 xl:leading-9 font-light">
                {incude_text1}
                <br className="hidden xl:block" /> {incude_text2}
            </p>
        </div>
    )
}

export default index;
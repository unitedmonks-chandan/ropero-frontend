import BeforeHeading from '../Typography/BeforeHeading';
import Heading from '../Typography/Heading';
import Para from '../Typography/Para';
import RoperroLink from '../RoperroLink';


// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation, Autoplay } from "swiper";
import SwiperCore from 'swiper';


SwiperCore.use([Autoplay, Pagination, Navigation])



const Slider = (props) => {

    const {carousel, tailwind, css} = props;
    const {slides, customCss} = carousel;


    const renderSlides = () => {
        return slides.map((item, i) => {
            return <SwiperSlide key={i}><img src={item.image.img} alt={item.image.altTag} />
               
                <div className={item.textContainer.tailwind}>
                    <div className={item.textContainer.textBox.tailwind}>
                        {console.log(item.textContainer.textBox)}
                        <BeforeHeading text={item.textContainer.textBox.beforeHeading.text} tailwind={item.textContainer.textBox.beforeHeading.tailwind} />
                        <Heading text={item.textContainer.textBox.heading.text} tailwind={item.textContainer.textBox.heading.tailwind} />
                        <Para text={item.textContainer.textBox.para.text} tailwind={item.textContainer.textBox.para.tailwind} />
                        <RoperroLink text={item.textContainer.textBox.link.text} 
                            linkHref={item.textContainer.textBox.link.linkHref}
                            tailwind={item.textContainer.textBox.link.tailwind}
                            css={item.textContainer.textBox.link.css} 
                        />
                    </div>
                </div>
            </SwiperSlide>
        })
    }


    return (
        <div className={`${tailwind} ${css}`}>
            <div className="carousel">
                <Swiper
                    autoplay={{
                        delay: 2500,
                        disableOnInteraction: false,
                    }}
                    pagination={{
                        clickable: true
                    }}
                    loop={true}
                    navigation
                    modules={[Pagination]}
                    className={`mySwiper ${customCss}`}
                >
                    {renderSlides()}
                </Swiper >
        </div>
        </div>
    );
};

export default Slider;











// const Carousel = (props) => {

//     const { carousel } = props;
//     const { slides, customCss = '' } = carousel;
   




//     return (
//         <div className="carousel">
//             <Swiper
//                 // autoplay={{
//                 //     delay: 2500,
//                 //     disableOnInteraction: false,
//                 // }}
//                 pagination={{
//                     clickable: true
//                 }}
//                 loop={true}
//                 navigation
//                 modules={[Pagination]}
//                 className={`mySwiper ${customCss}`}
//             >
//                 {renderSlides()}
//             </Swiper >
//         </div>
//     );
// }

// export default Carousel;




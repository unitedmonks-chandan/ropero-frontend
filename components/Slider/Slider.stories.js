import Slider from './index';

import watch from '../../public/assets/images/carousel/watch.jpg';

export default {
    title: "Roperro/Slider"
}

const Template = args => <Slider {...args} />;

export const GiftingCollection = Template.bind({});

GiftingCollection.args = {
    
    tailwind: 'relative',
    css: '',    
    
    carousel: {
        slides: [
            {
                image: {img: watch, altTag: 'watch'},
                textContainer: {
                    tailwind: 'absolute left-0 top-0 z-10 w-5/12 h-full bg-colorBlack21 opacity-60 flex items-end',
                    
                    textBox: {
                        tailwind: 'ml-28 mb-80 flex flex-col items-start justify-start',
                        
                        beforeHeading: {
                            text: 'EXQUISITE',
                            tailwind: 'uppercase text-2xl text-hoverYellow mb-4'
                        },
        
                        heading: {
                            text: 'gifting collection',
                            tailwind: 'uppercase text-8xl text-white font-cinzel mb-7 text-left'
                        },
        
                        para: {
                            text: "A priceless gift for your loved ones",
                            tailwind: 'text-2xl text-colorGray73 mb-16'
                        },
                        
                        link: {
                            text: 'shop now',
                            linkHref: '',
                            tailwind: "uppercase text-white text-2xl bg-hoverYellow w-64 h-16 font-medium flex justify-center items-center ",
                            css: ''
                        }
                    }
                    
                }
            },
            
            // second slide

            {
                image: {img: watch, altTag: 'watch'},
                textContainer: {
                    tailwind: 'absolute left-0 top-0 z-10 w-5/12 h-full bg-colorBlack21 opacity-60 flex items-end',
                    
                    textBox: {
                        tailwind: 'ml-28 mb-80 flex flex-col items-start justify-start',
                        
                        beforeHeading: {
                            text: 'EXQUISITE',
                            tailwind: 'uppercase text-2xl text-hoverYellow mb-4'
                        },
        
                        heading: {
                            text: 'gifting collection',
                            tailwind: 'uppercase text-8xl text-white font-cinzel mb-7 text-left'
                        },
        
                        para: {
                            text: "A priceless gift for your loved ones",
                            tailwind: 'text-2xl text-colorGray73 mb-16'
                        },
                        
                        link: {
                            text: 'shop now',
                            linkHref: '',
                            tailwind: "uppercase text-white text-2xl bg-hoverYellow w-64 h-16 font-medium flex justify-center items-center ",
                            css: ''
                        }
                    }
                    
                }
            },
            
            // third slide

            {
                image: {img: watch, altTag: 'watch'},
                textContainer: {
                    tailwind: 'absolute left-0 top-0 z-10 w-5/12 h-full bg-colorBlack21 opacity-60 flex items-end',
                    
                    textBox: {
                        tailwind: 'ml-28 mb-80 flex flex-col items-start justify-start',
                        
                        beforeHeading: {
                            text: 'EXQUISITE',
                            tailwind: 'uppercase text-2xl text-hoverYellow mb-4'
                        },
        
                        heading: {
                            text: 'gifting collection',
                            tailwind: 'uppercase text-8xl text-white font-cinzel mb-7 text-left'
                        },
        
                        para: {
                            text: "A priceless gift for your loved ones",
                            tailwind: 'text-2xl text-colorGray73 mb-16'
                        },
                        
                        link: {
                            text: 'shop now',
                            linkHref: '',
                            tailwind: "uppercase text-white text-2xl bg-hoverYellow w-64 h-16 font-medium flex justify-center items-center ",
                            css: ''
                        }
                    }
                    
                }
            },

            // fourth slide

            {
                image: {img: watch, altTag: 'watch'},
                textContainer: {
                    tailwind: 'absolute left-0 top-0 z-10 w-5/12 h-full bg-colorBlack21 opacity-60 flex items-end',
                    
                    textBox: {
                        tailwind: 'ml-28 mb-80 flex flex-col items-start justify-start',
                        
                        beforeHeading: {
                            text: 'EXQUISITE',
                            tailwind: 'uppercase text-2xl text-hoverYellow mb-4'
                        },
        
                        heading: {
                            text: 'gifting collection',
                            tailwind: 'uppercase text-8xl text-white font-cinzel mb-7 text-left'
                        },
        
                        para: {
                            text: "A priceless gift for your loved ones",
                            tailwind: 'text-2xl text-colorGray73 mb-16'
                        },
                        
                        link: {
                            text: 'shop now',
                            linkHref: '',
                            tailwind: "uppercase text-white text-2xl bg-hoverYellow w-64 h-16 font-medium flex justify-center items-center ",
                            css: ''
                        }
                    }
                    
                }
            },
        ],
        customCss: 'single-right-arrow-slider pagination-start bullet-margin-left-112px white-and-golden-bullets'
    }, 

}
import React from "react";
import {ReactSVG} from 'react-svg';
import RoperroLink from "../../components/RoperroLink";


// import './arrow.css';

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation, Autoplay } from "swiper";
import SwiperCore from 'swiper';


SwiperCore.use([Autoplay, Pagination, Navigation])

const Carousel = (props) => {

    const { carousel } = props;
    const { slides, customCss = '' } = carousel;
   

    const renderSlides = () => {
        return slides.map((item, i) => {
            return <SwiperSlide key={i}><img src={item.image.img} alt={item.image.altTag} />
                <div className={item.container}>

                    {item.heading? <h1 className={`${item.heading.tailwind} ${item.heading.css}`}>{item.heading.text}</h1> : ''}

                    {/* If there is svg then show it else show nothing. Ternary operator */}
                    {item.logo? <ReactSVG src={item.logo.svg} className={`${item.logo.tailwind} ${item.logo.css}`} /> : ''
                    }
                    <RoperroLink
                        text={item.link.text}
                        tailwind={item.link.tailwind}
                        css={item.link.css}
                        linkHref={item.link.linkHref}
                    />
                </div>
            </SwiperSlide>
        })
    }


    return (
        <div className="carousel">
            <Swiper
                // autoplay={{
                //     delay: 2500,
                //     disableOnInteraction: false,
                // }}
                pagination={{
                    clickable: true
                }}
                loop={true}
                navigation
                modules={[Pagination]}
                className={`mySwiper ${customCss}`}
            >
                {renderSlides()}
            </Swiper >
        </div>
    );
}

export default Carousel;








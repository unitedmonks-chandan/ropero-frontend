import Carousel from "./index";

import imageOne from '../../public/assets/images/carousel/1.jpg';
import roperroBlackTextLogo from '../../public/assets/svg/roperro-black-text-logo.svg';
import MonadaaBrownLogo from '../../public/assets/svg/monadaa/MonadaaBrownLogo.svg';

import imageTwo from '../../public/assets/images/carousel/Black2.jpg';
import monadaaWhiteLogo from '../../public/assets/svg/monadaa/MonadaaWhiteLogo.svg';


export default {
    title: "Roperro/Carousel"
}

const Template = args => <Carousel {...args} />;

export const RoperroHeroSlider = Template.bind({});

RoperroHeroSlider.args = {
  carousel: {
    slides: [
      {
        image: {img: imageOne, altTag: 'Woman in white dress'},

        // Container css is handling the position of logo, text and link button
        container: 'absolute bottom-35percent z-10 center-to-width flex flex-col items-center',
        heading: {
          text: '', //Roperro
          tailwind: '', 
          css: 'carousel-heading font-black'
        },
        logo: {
          svg: roperroBlackTextLogo,
          tailwind: 'mb-12',
          css: ''
        },
        link: {
          text: 'shop now',
          linkHref: '/',
          tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center',
          css: '',    
        }
      },
      {
        image: {img: imageOne, altTag: 'Woman in white dress'},

        // Container css is handling the position of logo, text and link button
        container: ' absolute bottom-35percent z-10 center-to-width flex flex-col items-center',
        heading: {
          text: '', //Monadaa
          tailwind: '', 
          css: 'carousel-heading font-black'
        },
        logo: {
          svg: MonadaaBrownLogo,
          tailwind: 'mb-12',
          css: ''
        },
        link: {
          text: 'shop now',
          linkHref: '/',
          tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center',
          css: '',    
        }
      }
    ],
    customCss: 'hide-inbuilt-arrow'
  },
};

export const MonadaaHeroCarousel = Template.bind({});

MonadaaHeroCarousel.args = {
  tailwind: 'relative',
  css: '',

  carousel: {
      slides: [
          {
              image: {img: imageTwo, altTag: 'Woman wearing black glass'},
              container: ' absolute left-0 bottom-35percent z-10 flex flex-col items-start ml-28',
              heading: {
                  text: '', //Roperro
                  tailwind: '', 
                  css: 'carousel-heading font-black'
                }, 
              logo: {
                  svg: monadaaWhiteLogo, 
                  tailwindCss: 'mb-14',
                  iconTailwindCss: '',
                  customCss: ''
              },
              link: {
                  text: 'shop now',
                  linkHref: '/',
                  tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center mt-14',
                  css: '',    
              }
          },

          {
              image: {img: imageTwo, altTag: 'Woman wearing black glass'},
              container: ' absolute left-0 bottom-35percent z-10 flex flex-col items-start ml-28',
              heading: {
                  text: '', //Roperro
                  tailwind: '', 
                  css: 'carousel-heading font-black'
                }, 
              logo: {
                  svg: monadaaWhiteLogo, 
                  tailwindCss: 'mb-14',
                  iconTailwindCss: '',
                  customCss: ''
              },
              link: {
                  text: 'shop now',
                  linkHref: '/',
                  tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center mt-14',
                  css: '',    
              }
          },

          {
              image: {img: imageTwo, altTag: 'Woman wearing black glass'},
              container: ' absolute left-0 bottom-35percent z-10 flex flex-col items-start ml-28',
              heading: {
                  text: '', //Roperro
                  tailwind: '', 
                  css: 'carousel-heading font-black'
                }, 
              logo: {
                  svg: monadaaWhiteLogo, 
                  tailwindCss: 'mb-14',
                  iconTailwindCss: '',
                  customCss: ''
              },
              link: {
                  text: 'shop now',
                  linkHref: '/',
                  tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center mt-14',
                  css: '',    
              }
          },

          {
              image: {img: imageTwo, altTag: 'Woman wearing black glass'},
              container: ' absolute left-0 bottom-35percent z-10 flex flex-col items-start ml-28',
              heading: {
                  text: '', //Roperro
                  tailwind: '', 
                  css: 'carousel-heading font-black'
                }, 
              logo: {
                  svg: monadaaWhiteLogo, 
                  tailwindCss: 'mb-14',
                  iconTailwindCss: '',
                  customCss: ''
              },
              link: {
                  text: 'shop now',
                  linkHref: '/',
                  tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center mt-14',
                  css: '',    
              }
          },

      ],
      customCss: 'hide-inbuilt-arrow single-right-arrow-slider pagination-start bullet-margin-left-112px white-and-golden-bullets'
  }   
};
import React from 'react'
// import "./style.css"
const SearchBox = ({ placeholder_text, icon }) => {
    return (

        <div className="MyOrd_searchbox w-72  h-8 relative top-0">
            <input className='h-8 px-14 rounded-2xl focus:outline-none text-xs' type="text"
                placeholder={placeholder_text} />

            <img className=' absolute top-2 left-5' src={icon} />
        </div>
    )
}

export default SearchBox;
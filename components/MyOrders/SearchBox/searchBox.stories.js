import SearchBox from "./index"
import searchIcon from "../../../public/assets/images/My-orders/Group.png";
export default {
    title: "My orders/searchbox"
}

const Template = (argument) => <SearchBox {...argument} />
export const search = Template.bind({})
search.args = {
    placeholder_text: "what are you looking for?",
    icon: searchIcon
}
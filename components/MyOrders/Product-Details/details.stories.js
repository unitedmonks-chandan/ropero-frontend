import Details from "."

export default {
    title: "My-orders/product-details"
}
const Template = (arg) => <Details {...arg} />
export const productdetails = Template.bind({})
productdetails.args = {
    product_details: "Light grey hoody Sweatshirt ",
    item_color: "red",
    item_size: 35
}
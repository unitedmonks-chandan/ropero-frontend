import React from 'react';
// import "./style.css";
const Details = ({ product_details, item_color, item_size }) => {
    return (
        <div className=' lg:w-56 xl:w-72 h-44 flex justify-center items-center productDetails '>
            <div className=' mx-12'>
                <h6 className='font-light text-base'>
                    {product_details}
                </h6>

                <div className='flex justify-between w-28'>
                    <div className='font-light text-sm'>
                        <span className=' block'>Color:</span>
                        <span className=' block'>Size:</span>
                    </div>
                    <div className='font-light text-sm'>

                        <span className='block'>{item_color}</span>
                        <span className='block'>{item_size}</span>
                    </div>
                </div>

            </div>

        </div >

    )
}

export default Details
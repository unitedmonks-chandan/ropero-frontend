import React from 'react'
import "./style.css"
const Nav = (props) => {
    const { address = '/', nav_text } = props;

    return (
        <li className=' w-full text-center text-xl flex py-0 myOrderNav'>
            {/* <Link to={`${address}`} > */}
            <a>{nav_text}</a>
            {/* </Link> */}
        </li>
    )
}

export default Nav
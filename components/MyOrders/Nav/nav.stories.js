import Nav from "."

export default {
    title: "My-orders/nav"
}
const Template = (arg) => <Nav {...arg} />
export const nav = Template.bind({})
nav.args = {
    nav_text: "Delivered"
}
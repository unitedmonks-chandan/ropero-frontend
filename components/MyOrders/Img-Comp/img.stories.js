import Image from "./index"
import orderImg from "../../../public/assets/images/My-orders/14.png"
export default {
    title: "My-orders/img"
}
const Template = (arg) => <Image {...arg} />
export const img = Template.bind({})
img.args = {
    img_file: orderImg
}
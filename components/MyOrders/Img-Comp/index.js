import React from 'react'

const Image = ({ img_file }) => {
    return (
        <div className='w-full h-64 flex justify-center items-center'>
            <img className='object-cover ' src={img_file} />
        </div>
    )
}

export default Image;
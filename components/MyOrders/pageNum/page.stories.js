import Page from "./index"
import blackRightArrow from '../../../public/assets/svg/orders/black-right-arrow-small.svg';
import blackLeftArrow from '../../../public/assets/svg/orders/black-left-arrow-small.svg';

export default {
    title: "My-orders/page"
}
const Template = (arg) => <Page {...arg} />
export const page = Template.bind({})
page.args = {
    page_text: "Page",
    buttonNumber: 1, 
    arrowLeft: blackLeftArrow,
    arrowRight: blackRightArrow
}
import React from 'react';
import {ReactSVG} from 'react-svg';

const Page = ({ page_text, buttonNumber, arrowLeft, arrowRight }) => {
    return (
        <div>
            <div className='flex items-center'>
                <h6 className='uppercase'>{page_text}</h6>

                <div className='px-2 flex items-center'>
                    <button className='border-b border-solid border-black'>{buttonNumber}</button>

                    <button className='' type='submit'>
                        <ReactSVG src={arrowLeft} className="w-2 h-3.5 m-2" />
                    </button>
                    <button className='' type='submit'>
                        <ReactSVG src={arrowRight} className="w-2 h-3.5" />
                    </button>
                </div>
            </div>

        </div>
    )
}

export default Page
import React from 'react'
// import "./style.css"
const Header = ({ order_text }) => {
    return (
        <div className=' w-full h-48 xl:h-60 2xl:h-80 myOrders_header'>
            <div className='h-full flex justify-center items-center'>
                <h1 className='text-4xl 2xl:text-6xl font-medium'>{order_text}</h1>
            </div>
        </div>
    )
}

export default Header
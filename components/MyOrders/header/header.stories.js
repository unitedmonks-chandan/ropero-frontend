import Header from "."

export default {
    title: "My Orders/header"
}
const Template = (argument) => <Header{...argument} />
export const header = Template.bind({})
header.args = {
    order_text: "MY ORDERS"
}
import React from 'react'
import Header from "./header/index";
import Filter from "./filter/index"
import SearchBox from "./SearchBox/index";
const MyOrders = (props) => {

    const { text, filter, placeholder, iconImg } = props;

    return (<>
        <Header order_text={text} />

        <div className='flex justify-between mt-5 mx-20'>

            <div>
                <Filter filter_text={filter} />
            </div>

            <div>
                <SearchBox placeholder_text={placeholder}
                    icon={iconImg} />
            </div>
        </div>
    </>
    )
}

export default MyOrders
import MyOrders from "./index";
import searchIcon from "../../public/assets/images/My-orders/Group.png"

export default {
    title: "My-orders/Main-page"
}
const Template = (arg) => <MyOrders{...arg} />
export const myOrders = Template.bind({})
myOrders.args = {
    text: "MY ORDERS",
    filter: "Filters",
    placeholder: "what are you looking for?",
    iconImg: searchIcon
}
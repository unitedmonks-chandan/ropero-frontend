import Price from "./index"

export default {
    title: "My-orders/prices"
}
const Template = (args) => <Price {...args} />
export const price = Template.bind({})
price.args = {
    prices: 5000
} 
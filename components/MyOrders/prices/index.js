import React from 'react'
// import "./style.css"
const Price = ({ prices }) => {
    return (
        <div className='md:w-28 lg:w-28 xl:w-36 Myordprice'>
            <div className='w-full'>
                <h5 className='text-2xl'><span className='font-light'>&#x20B9;</span>{prices}</h5>
            </div>
        </div>
    )
}

export default Price
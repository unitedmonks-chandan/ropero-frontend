import Button from "."

export default {
    title: "My orders/btn"
}
const Template = (arg) => <Button {...arg} />
export const btn = Template.bind({})
btn.args = {
    view: "VIEW ALL"
}
import React from 'react'
// import "./style.css"
const Button = ({ view }) => {
    return (
        <div className='mr-auto' style={{marginLeft: '50%', transform: 'translateX(-50%)'}}>
            <button className=' w-32  h-8 mx-auto bg-black text-white font-light' type='submit'>{view}</button>
        </div>
    )
}
export default Button
import Filter from "../filter";
import SearchBox from "../SearchBox";
import SideNavbar from "../../SideNavbar/index";
import Button from "../button";
import Page from '../pageNum';
import OrderItem from '../../OrderItem';

const Orders = ({topBar, orderStatusNavbar, orderTimeNavbar, sideNavandOrdersContainer, sideNavContainerTailwind, orderItemsContainer, orderItems, buttonData, pagination, buttonAndPageContainer }) => {

    const {tailwind, filter_text, placeholder_text, icon, filterTailwind} = topBar;


    const renderOrders = () => {
        return orderItems.map((item, i) => <OrderItem 
                key={i}
                image={item.image} 
                tailwind={item.tailwind}
                css={item.css}
                productNameAndDescription={item.productNameAndDescription}
                deliveryDate={item.deliveryDate}
                productQuantity={item.productQuantity}
                rightBorder={item.rightBorder}
                productPrice={item.productPrice}
            />
        );
    }

    return (
        <div>
            <div className={tailwind}>
                <Filter filter_text={filter_text} filterTailwind={filterTailwind} />
                <SearchBox placeholder_text={placeholder_text} icon={icon} />
            </div>

            <div className={sideNavandOrdersContainer}>
                <div className={sideNavContainerTailwind}>
                    <SideNavbar 
                        tailwind={orderStatusNavbar.tailwind}
                        css={orderStatusNavbar.css}
                        heading={orderStatusNavbar.heading}
                        linksContainer={orderStatusNavbar.linksContainer}
                    />

                    <SideNavbar 
                        tailwind={orderTimeNavbar.tailwind}
                        css={orderTimeNavbar.css}
                        heading={orderTimeNavbar.heading}
                        linksContainer={orderTimeNavbar.linksContainer}
                    />
                </div>
                <div className={orderItemsContainer}>
                    {renderOrders()}


                    <div className={buttonAndPageContainer}>
                        <Button view={buttonData.view} />
                        <Page 
                            page_text={pagination.page_text} 
                            buttonNumber={pagination.buttonNumber} 
                            arrowLeft={pagination.arrowLeft}
                            arrowRight={pagination.arrowRight}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Orders;
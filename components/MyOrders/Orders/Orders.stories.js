import Orders from './index';
import searchIcon from "../../../public/assets/images/My-orders/Group.png";
import RightArrowBlackSVGIcon from '../../../public/assets/svg/orders/black-right-arrow-svg-icon.svg';

import blackRightArrowSmall from '../../../public/assets/svg/orders/black-right-arrow-small.svg';
import blackLeftArrowSmall from '../../../public/assets/svg/orders/black-left-arrow-small.svg';

import orderImageOne from "../../../public/assets/images/My-orders/11.png"
import orderImageTwo from '../../../public/assets/images/My-orders/12.png';
import orderImageThree from "../../../public/assets/images/My-orders/13.png"
import orderImageFour from "../../../public/assets/images/My-orders/14.png"
import IndianRupeeSVGIcon from '../../../public/assets/svg/orders/indian-rupee-svg-icon.svg';

export default {
    title: "Roperro/Orders"
};

const Template = args => <Orders {...args} />;

export const Default = Template.bind({});

Default.args = {
    topBar: {
        tailwind: 'flex justify-between mt-12 ml-44 mr-32 mb-14',
        filter_text: "Filters",
        filterTailwind: 'colorGray24',
        placeholder_text: "what are you looking for?",
        icon: searchIcon
    }, 

    sideNavandOrdersContainer: 'flex margin-right-7percent',
    sideNavContainerTailwind: ' margin-right-7percent',
    orderStatusNavbar: {
        tailwind: 'bg-colorGray98 pl-28 mb-16',
        css: 'width-486px pt-12',

        heading: {
            text: 'Order Status', 
            tailwind: 'text-3xl text-colorBlack21 font-semibold pb-12 border-b border-solid border-colorGray69 text-right pr-24',
            css: ''
        },

        linksContainer: {
            tailwind: 'pr-12 mt-12 ml-16 pb-12',
            css: '',
            defaultSelectedLink: 'In Transit',

            links: [
                {
                    tailwind: ' flex items-baseline justify-between mb-11',
                    link: {
                        text: 'Delivered',
                        linkHref: '/',
                        tailwind: ' text-2xl font-light text-colorGray24',
                        css: '',
                        onClickCss: 'color-golden45 bold600'
                    },
                    
                    icon: {
                        svg: RightArrowBlackSVGIcon,
                        tailwind: ' w-3 h-5',
                    }
                },

                {
                    tailwind: ' flex items-baseline justify-between mb-11',
            
                    link: {
                        text: 'In Transit',
                        linkHref: '/',
                        tailwind: ' text-2xl font-light text-colorGray24',
                        css: '',
                        onClickCss: 'color-golden45 bold600'
                    },
                    
                    icon: {
                        svg: RightArrowBlackSVGIcon,
                        tailwind: ' w-3 h-5',
                    }
                },

            
                {
                    tailwind: ' flex items-baseline justify-between mb-11',
            
                    link: {
                        text: 'Cancelled',
                        linkHref: '/',
                        tailwind: ' text-2xl font-light text-colorGray24',
                        css: '',
                        onClickCss: 'color-golden45 bold600'
                    },
                    
                    icon: {
                        svg: RightArrowBlackSVGIcon,
                        tailwind: ' w-3 h-5',
                    }
                },

            
                {
                    tailwind: ' flex items-baseline justify-between',
            
                    link: {
                        text: 'Returned',
                        linkHref: '/',
                        tailwind: ' text-2xl font-light text-colorGray24',
                        css: '',
                        onClickCss: 'color-golden45 bold600'
                    },
                    
                    icon: {
                        svg: RightArrowBlackSVGIcon,
                        tailwind: ' w-3 h-5',
                    }
                },
            ]

        }
    },

    orderTimeNavbar: {
        tailwind: 'bg-colorGray98 pl-28',
        css: 'width-486px pt-12',

        heading: {
            text: 'Order Time', 
            tailwind: 'text-3xl text-colorBlack21 font-semibold pb-12 border-b border-solid border-colorGray69 text-right pr-24',
            css: ''
        },

        linksContainer: {
            tailwind: 'pr-12 mt-12 ml-16 pb-12',
            css: '',
            defaultSelectedLink: 'Last 30 Days',

            links: [
                {
                    tailwind: ' flex items-baseline justify-between mb-11',
                    link: {
                        text: 'Last 30 Days',
                        linkHref: '/',
                        tailwind: ' text-2xl font-light text-colorGray24',
                        css: '',
                        onClickCss: ''
                    },
                    
                    icon: {
                        svg: RightArrowBlackSVGIcon,
                        tailwind: ' w-3 h-5',
                    }
                },

                {
                    tailwind: ' flex items-baseline justify-between mb-11',
            
                    link: {
                        text: '2020',
                        linkHref: '/',
                        tailwind: ' text-2xl font-light text-colorGray24',
                        css: '',
                        onClickCss: ''
                    },
                    
                    icon: {
                        svg: RightArrowBlackSVGIcon,
                        tailwind: ' w-3 h-5',
                    }
                },

            
                {
                    tailwind: ' flex items-baseline justify-between mb-11',
            
                    link: {
                        text: '2019',
                        linkHref: '/',
                        tailwind: ' text-2xl font-light text-colorGray24',
                        css: '',
                        onClickCss: ''
                    },
                    
                    icon: {
                        svg: RightArrowBlackSVGIcon,
                        tailwind: ' w-3 h-5',
                    }
                },

            
                {
                    tailwind: ' flex items-baseline justify-between',
            
                    link: {
                        text: 'Older',
                        linkHref: '/',
                        tailwind: ' text-2xl font-light text-colorGray24',
                        css: '',
                        onClickCss: ''
                    },
                    
                    icon: {
                        svg: RightArrowBlackSVGIcon,
                        tailwind: ' w-3 h-5',
                    }
                },
            ]

        }
    },

    orderItemsContainer: 'flex-1',
    orderItems: [
        {
            tailwind: 'py-8 flex items-center bg-colorGray98 w-full h-60 rounded-3xl ',
            css: 'padding-left-7percent mb-5',
        
            rightBorder: {
                content: '',
                tailwind: 'h-full border-r border-solid border-colorGray91'
            },
            
            image: {
                imageSource: orderImageOne,
                tailwind: 'w-44'
            }, 
        
            productNameAndDescription: {
                tailwind: 'border-r border-solid border-colorGray84  flex-1',
                productName: {
                    text: 'Dark green hoody Sweatshirt',
                    tailwind: 'text-base font-medium text-colorGray30 mb-4 ml-9'
                },
        
                productDescription: {
                    tailwind: 'grid grid-cols-2 grid-rows-2 gap-y-2 ml-9',
                    colorTag: {
                        text: 'Color:',
                        tailwind: 'text-base font-normal',
                    },
            
                    color: {
                        text: 'Grey', 
                        tailwind: 'text-base font-normal'
                    },
            
                    sizeTag: {
                        text: 'Size:',
                        tailwind: 'text-base font-normal',
                    },
            
                    size: {
                        text: 'Large', 
                        tailwind: 'text-base font-normal'
                    },
                }
        
        
            }, 
        
            deliveryDate: {
                text: 'Delivered On',
                date: '25 December 2021', 
                tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex flex-col justify-start items-center'
            }, 
            
            productQuantity: {
                text: 'Quantity: ',
                quantity: 1, 
                tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex justify-center'
            },
        
            productPrice: {
                tailwind: 'flex-1 flex items-center justify-center text-3xl font-medium',
                priceSvg: IndianRupeeSVGIcon,
                priceSvgTailwind: 'w-3.5 mr-2',
                price: 5000,
                priceTailwind: '' 
            }
        }, 

        {
            tailwind: 'py-8 flex items-center bg-colorGray90 w-full h-60 rounded-3xl ',
            css: 'padding-left-7percent mb-5',
        
            rightBorder: {
                content: '',
                tailwind: 'h-full border-r border-solid border-colorGray84'
            },
            
            image: {
                imageSource: orderImageTwo,
                tailwind: 'w-44'
            }, 
        
            productNameAndDescription: {
                tailwind: 'border-r border-solid border-colorGray84  flex-1',
                productName: {
                    text: 'Dark green hoody Sweatshirt',
                    tailwind: 'text-base font-medium text-colorGray30 mb-4 ml-9'
                },
        
                productDescription: {
                    tailwind: 'grid grid-cols-2 grid-rows-2 gap-y-2 ml-9',
                    colorTag: {
                        text: 'Color:',
                        tailwind: 'text-base font-normal',
                    },
            
                    color: {
                        text: 'Grey', 
                        tailwind: 'text-base font-normal'
                    },
            
                    sizeTag: {
                        text: 'Size:',
                        tailwind: 'text-base font-normal',
                    },
            
                    size: {
                        text: 'Large', 
                        tailwind: 'text-base font-normal'
                    },
                }
        
        
            }, 
        
            deliveryDate: {
                text: 'Delivered On',
                date: '25 December 2021', 
                tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex flex-col justify-start items-center'
            }, 
            
            productQuantity: {
                text: 'Quantity: ',
                quantity: 1, 
                tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex justify-center'
            },
        
            productPrice: {
                tailwind: 'flex-1 flex items-center justify-center text-3xl font-medium',
                priceSvg: IndianRupeeSVGIcon,
                priceSvgTailwind: 'w-3.5 mr-2',
                price: 5000,
                priceTailwind: '' 
            }
        },


        {
            tailwind: 'py-8 flex items-center bg-colorGray98 w-full h-60 rounded-3xl ',
            css: 'padding-left-7percent mb-5',
        
            rightBorder: {
                content: '',
                tailwind: 'h-full border-r border-solid border-colorGray91'
            },
            
            image: {
                imageSource: orderImageThree,
                tailwind: 'w-44'
            }, 
        
            productNameAndDescription: {
                tailwind: 'border-r border-solid border-colorGray84  flex-1',
                productName: {
                    text: 'Dark green hoody Sweatshirt',
                    tailwind: 'text-base font-medium text-colorGray30 mb-4 ml-9'
                },
        
                productDescription: {
                    tailwind: 'grid grid-cols-2 grid-rows-2 gap-y-2 ml-9',
                    colorTag: {
                        text: 'Color:',
                        tailwind: 'text-base font-normal',
                    },
            
                    color: {
                        text: 'Grey', 
                        tailwind: 'text-base font-normal'
                    },
            
                    sizeTag: {
                        text: 'Size:',
                        tailwind: 'text-base font-normal',
                    },
            
                    size: {
                        text: 'Large', 
                        tailwind: 'text-base font-normal'
                    },
                }
        
        
            }, 
        
            deliveryDate: {
                text: 'Delivered On',
                date: '25 December 2021', 
                tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex flex-col justify-start items-center'
            }, 
            
            productQuantity: {
                text: 'Quantity: ',
                quantity: 1, 
                tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex justify-center'
            },
        
            productPrice: {
                tailwind: 'flex-1 flex items-center justify-center text-3xl font-medium',
                priceSvg: IndianRupeeSVGIcon,
                priceSvgTailwind: 'w-3.5 mr-2',
                price: 5000,
                priceTailwind: '' 
            }
        },

        {
            tailwind: 'py-8 flex items-center bg-colorGray98 w-full h-60 rounded-3xl ',
            css: 'padding-left-7percent mb-5',
        
            rightBorder: {
                content: '',
                tailwind: 'h-full border-r border-solid border-colorGray91'
            },
            
            image: {
                imageSource: orderImageFour,
                tailwind: 'w-44'
            }, 
        
            productNameAndDescription: {
                tailwind: 'border-r border-solid border-colorGray84  flex-1',
                productName: {
                    text: 'Dark green hoody Sweatshirt',
                    tailwind: 'text-base font-medium text-colorGray30 mb-4 ml-9'
                },
        
                productDescription: {
                    tailwind: 'grid grid-cols-2 grid-rows-2 gap-y-2 ml-9',
                    colorTag: {
                        text: 'Color:',
                        tailwind: 'text-base font-normal',
                    },
            
                    color: {
                        text: 'Grey', 
                        tailwind: 'text-base font-normal'
                    },
            
                    sizeTag: {
                        text: 'Size:',
                        tailwind: 'text-base font-normal',
                    },
            
                    size: {
                        text: 'Large', 
                        tailwind: 'text-base font-normal'
                    },
                }
        
        
            }, 
        
            deliveryDate: {
                text: 'Delivered On',
                date: '25 December 2021', 
                tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex flex-col justify-start items-center'
            }, 
            
            productQuantity: {
                text: 'Quantity: ',
                quantity: 1, 
                tailwind: 'text-lg font-medium text-colorGray30 flex-1 flex justify-center'
            },
        
            productPrice: {
                tailwind: 'flex-1 flex items-center justify-center text-3xl font-medium',
                priceSvg: IndianRupeeSVGIcon,
                priceSvgTailwind: 'w-3.5 mr-2',
                price: 5000,
                priceTailwind: '' 
            }
        },
    ],

    buttonAndPageContainer: 'flex justify-center mt-20 mb-20', 

    buttonData: {
        view: "VIEW ALL"
    }, 

    pagination: {
        page_text: "Page: ", 
        buttonNumber: '01', 
        arrowLeft: blackLeftArrowSmall,
        arrowRight: blackRightArrowSmall
    }
}
import React from 'react'
// import "./style.css"
const Quantity = ({ quanty, num }) => {
    return (<>
        <div className=' md:w-32 lg:w-32  xl:w-44 h-44 flex justify-center items-center font-light quntity'>
            <div className='w-full'>
                <span>{quanty}:</span>
                <span className='pl-2'>{num}</span>
            </div>
        </div>
    </>
    )
}

export default Quantity
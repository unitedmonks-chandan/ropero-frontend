import React from 'react'
// import "./style.css"
const Filter = ({ filter_text, filterTailwind }) => {
    return (
        <div className={filterTailwind}>
            <h5 className='text-xl 2xl:font-27px line-height-33px colorGray24 font-light'>{filter_text}</h5>
        </div>
    )
}

export default Filter;
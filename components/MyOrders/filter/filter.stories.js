import Filter from "./index"

export default {
    title: "My Orders/filter"
}
const Template = (argument) => <Filter {...argument} />
export const filter = Template.bind({})
filter.args = {
    filter_text: "Filters",
    filterTailwind: 'mt-12 colorGray24',
}
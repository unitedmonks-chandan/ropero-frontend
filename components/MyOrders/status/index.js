import React from 'react'
import "./style.css"
const OrderStatus = ({ order_status }) => {
    return (
        <div>
            <div className='orderStatus'>
                <h5 className='text-xl pl-14 font-semibold'>{order_status}</h5>
            </div>
        </div>
    )
}

export default OrderStatus
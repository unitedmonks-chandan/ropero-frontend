import OrderStatus from "./index"

export default {
    title: "My-orders/Status"
}
const Template = (argument) => <OrderStatus {...argument} />
export const status = Template.bind({})
status.args = {
    order_status: "Order Status"
}
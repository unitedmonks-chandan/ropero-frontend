import React from 'react'
// import "./style.css"
const Delivery = ({ delivery, date }) => {
    return (
        <div className='md:w-32 lg:w-32 xl:w-44 h-44 flex justify-center items-center delivery'>
            <div className='w-full'>
                <h6 className=' font-light text-base'>
                    {delivery}
                </h6>
                {/* <div className='font-light'> */}
                <span className='font-light text-base '>{date}</span>
                {/* </div> */}
            </div>
        </div>
    )
}

export default Delivery
import Details from "./index"
export default {
    title: "My orders/delivery"
}
const Template = (arg) => <Details {...arg} />
export const delivery = Template.bind({})
delivery.args = {
    delivery: "Delivered On",
    date: "5 Jan 2021"
}
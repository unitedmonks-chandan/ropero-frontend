import OrderList from "./index"
import orderImg from "../../../public/assets/images/My-orders/14.png"
export default {
    title: "My-orders/Item"
}
const Template = (arg) => <OrderList {...arg} />
export const listItem = Template.bind({})
listItem.args = {
    image: orderImg,
    product_data: "Light grey hoody Sweatshirt",
    color: "Red",
    size: "Medium",
    status: "Delivery on",
    date: "5 Jan 2020",
    quanty: "Quantity",

    prics: "1",
    price: "5000",
    backgroundColor:'bg-colorGray90',
    tailwindCss: ''
}
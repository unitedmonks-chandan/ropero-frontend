import React from 'react'
import Image from "../Img-Comp/index";
import Details from "../Product-Details/index";
import Delivery from "../delivery-date/index"
import Quantity from "../Quantity/index";
import Price from "../prices/index";

const OrderList = (props) => {
    const { image, product_data, color, size, status,
        date, quanty, prics, price, backgroundColor='', tailwindCss='' } = props;
    return (
        <div className={`${backgroundColor} ${tailwindCss} w-full h-64 rounded-3xl`}>
            <div className=' flex justify-around items-center' >
                <div>
                    <Image img_file={image} alt="model" />
                </div>
                <div>
                    <Details product_details={product_data} item_color={color} item_size={size} />
                </div>
                <div>
                    <Delivery delivery={status} date={date} />
                </div>
                <div>
                    <Quantity quanty={quanty} num={prics} />
                </div>
                <div>
                    <Price prices={price} />
                </div>
            </div>
        </div>
    )
}

export default OrderList;
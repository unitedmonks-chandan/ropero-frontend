// Import the global style enabling tailwind classes

import '../styles/globals.css';
import 'tailwindcss/tailwind.css';

///////////////////////////// For Monadaa 
// for single right arrow
import '../components/Carousel/arrow.css';

/////////////////////////////  For Zimba 
import '../components/HeroNewCarousel/double-arrow-slider.css';

//Zimba page our collection 
import "../components/OurCollection/our-collection.css";

//Zimba page new arrivals
import "../components/ZimbaHomePage/NewArrivals/bg-style.css";
import "../components/NewCarousel/slider-arrow.css";

// Zimba best seller 
import "../components/CarouselCustomSyling/carousel-custom-styling.css";


export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

import Layout from '../components/layout'
import SecondComp from '../components/NewHomePage/SecondComp'
import NewArrival from '../components/NewHomePage/NewArrivals/NewArrival'
import DiscountComp from '../components/DiscountComp/DiscountComp'
import Carousel from '../components/Carousel'

const Home = () => {
  const hero_data = {
    carousel: {
      slides: [
        {
          image: {
            img: '/assets/images/carousel/1.jpg',
            altTag: 'Woman in white dress',
          },

          // Container css is handling the position of logo, text and link button
          container:
            'absolute bottom-35percent z-10 center-to-width flex flex-col items-center',
          heading: {
            text: '', //Roperro
            tailwind: '',
            css: 'carousel-heading font-black',
          },
          logo: {
            svg: '/assets/svg/roperro-black-text-logo.svg',
            tailwind: 'mb-12',
            css: '',
          },
          link: {
            text: 'shop now',
            linkHref: '/',
            tailwind:
              'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center',
            css: '',
          },
        },
        {
          image: {
            img: '/assets/images/carousel/1.jpg',
            altTag: 'Woman in white dress',
          },

          // Container css is handling the position of logo, text and link button
          container:
            ' absolute bottom-35percent z-10 center-to-width flex flex-col items-center',
          heading: {
            text: '', //Monadaa
            tailwind: '',
            css: 'carousel-heading font-black',
          },
          logo: {
            svg: '/assets/svg/monadaa/MonadaaBrownLogo.svg',
            tailwind: 'mb-12',
            css: '',
          },
          link: {
            text: 'shop now',
            linkHref: '/',
            tailwind:
              'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center',
            css: '',
          },
        },
      ],
      customCss: 'hide-inbuilt-arrow',
    },
  }

  const our_brands = {
    bg1: 'bg-black w-full flex flex-col gap-4 items-center justify-center h-48',
    font: 'text-xl font-primay text-hoverYellow',
    blackBoxLogo: '/assets/svg/home/our-brands.svg',
    blackBoxLogoSize: 'w-96',
    display: 'grid grid-cols-1 md:grid-cols-2',
    text: 'From The House Of Roperro',
    blackContainer: true,
    // logoPosition: "shopNow-logo",
    shopNowCompsData: [
      {
        logoSize: 'w-32',
        logoPosition: 'shopNow-logo',
        btnLink: '#',
        // btnLink: '/lerida',
        shopNowBtn:
          'bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary',
        logo: '/assets/svg/home/lerida-logo.svg',
        btnText: 'Shop now',
        position: 'absolute shopNow-comp right-10 inline-block',
        bgImg: '/assets/images/lerida-comp.png',
        altTag: 'Woman in creme color shirt',
        colSpan: '',
      },
      {
        logoSize: 'w-20',
        logoPosition: 'shopNow-logo',
        btnLink: '#',
        // btnLink: '/zimba',
        shopNowBtn:
          'bg-black text-white px-5 py-2 flext items center justify-center uppercase text-xl font-primary',
        logo: '/assets/svg/home/zimba-logo.svg',
        btnText: 'Shop now',
        position: 'absolute shopNow-comp right-10 inline-block',
        bgImg: '/assets/images/zimba-comp.png',
        altTag: 'Woman in white shirt',
        colSpan: '',
      },
      {
        logoSize: 'w-80',
        btnLink: '#',
        // btnLink: '/monadaa',
        shopNowBtn:
          'bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary self-center',
        logo: '/assets/svg/home/monadaa-logo.svg',
        btnText: 'Shop now',
        position:
          'absolute shopNow-comp-type2 flex flex-col gap-8 justify-center',
        bgImg: '/assets/images/monadaa-comp.png',
        altTag: 'Woman in white shirt',
        colSpan: 'col-span-full',
        logoPosition: '',
      },
      {
        logoSize: 'w-32',
        logoPosition: 'shopNow-logo',
        // btnLink: '/arkar',
        btnLink: '#',
        shopNowBtn:
          'bg-white text-black px-5 py-2 flext items center justify-center uppercase text-xl font-primary',
        logo: '/assets/svg/home/arkar-logo.svg',
        btnText: 'Shop now',
        position: 'absolute shopNow-comp right-10 inline-block',
        bgImg: '/assets/images/arkar-comp.png',
        altTag: 'Woman wearing watch',
        colSpan: '',
      },
      {
        logoSize: 'w-32',
        logoPosition: 'shopNow-logo',
        // btnLink: '/migear',
        btnLink: '#',
        shopNowBtn:
          'bg-black text-white px-5 py-2 flext items center justify-center uppercase text-xl font-primary',
        logo: '/assets/svg/home/migear-logo.svg',
        btnText: 'Shop now',
        position: 'absolute shopNow-comp right-10 inline-block',
        bgImg: '/assets/images/migear-comp.png',
        altTag: 'Man with MiGear backpack',
        colSpan: '',
      },
    ],
  }

  const new_arrival_and_winter_collection = {
    spacing: 'py-16 pl-24 2xl:pl-44 pr-52 2xl:pr-60',

    firstCompData: [
      {
        spacing: '',
        // spacing: "py-16 pl-12 pr-60 2xl:pr-64",
        display:
          'grid grid-cols-1 xl:grid-cols-2 xl:max-w-screen-lg gap-10 xl:gap-14',
        subHeadingFont: 'text-xl font-primary',
        subHeading: 'HOUSE OF ROPERRO',
        headingFont: 'text-hoverYellow font-cinzel text-8xl uppercase',
        heading1: 'New',
        heading2: 'Arrivals',
        spanDisplay: 'block',
        paraFont: 'text-lg font-primary self-center',
        para: 'The online store presents selected suc- cessful collections, fashionable women’s clothing which is sewn exclusively from quality material.',
      },
    ],
    btnData: {
      container: 'pt-44 pl-8',
      position: '',
      headingStyle:
        'text-2xl text-black font-cinzel font-medium uppercase mb-4',
      heading1: 'Trendy',
      heading2: 'Styles',
      // btnLink: '/view-all',
      btnLink: '#',
      btn: 'inline-block bg-black text-white text-lg font-primary px-7',
      size: '',
      btnText: 'View All',
    },
    display: 'grid grid-cols-4 mt-12',
    carouselData: {
      customSpaceBetween: 0.1,
      customStyle:
        'relative w-full col-span-3 hide-inbuilt-arrow right-black-arrow hide-pagination-bullets',
      images: [
        {
          img: '/assets/images/new-arrivals-img1.png',
          altTag: 'Woman in light brown dress',
        },
        {
          img: '/assets/images/new-arrivals-img2.png',
          altTag: 'Woman in blue sweatshirt',
        },
        {
          img: '/assets/images/new-arrivals-img3.png',
          altTag: 'Woman in black coat',
        },
        {
          img: '/assets/images/new-arrivals-img2.png',
          altTag: 'Woman in blue sweatshirt',
        },
        {
          img: '/assets/images/new-arrivals-img3.png',
          altTag: 'Woman in black coat',
        },
        {
          img: '/assets/images/new-arrivals-img1.png',
          altTag: 'Woman in light brown dress',
        },
      ],
    },
    winterCollectionData: [
      {
        containerCl: 'grid grid-cols-1 md:grid-cols-2',
        image1: '/assets/images/winter-collection-img1.png',
        altTag1: 'Man in black orange sweatshirt',
        image2: '/assets/images/winter-collection-img2.png',
        altTag2: 'Woman in light brown jacket',
        positionCl: 'absolute two-btn-position flex flex-col gap-12',
        headingCl:
          'text-5xl font-cinzel text-black flex gap-2 flex-col justify-center items-center uppercase',
        heading1: 'Winter',
        heading2: 'collection',
        btnContainerCl: 'flex flex-col gap-6 justify-center items-center',
        // btn1Link: "/men",
        btn1Link: '#',
        btn1Cl: 'px-7 py-1 border border-black flex items-center uppercase',
        btn1Text: 'Shop for him',
        btn2Link: '#',
        // btn2Link: "/women",
        btn2Cl:
          'px-7 py-1 border-2 border-transparent bg-hoverYellow flex items-center uppercase',
        btn2Text: 'Shop for her',
      },
    ],
  }

  const discount_comp = {
    image: '/assets/images/discount-comp.png',
    container: '',
    containerPosition: 'absolute left-20 heading-position',
    heading1: 'HOUSE OF ROPERRO',
    heading1Style: 'text-white text-lg uppercase mb-2',
    heading2: 'Leather',
    heading2Style: 'text-white text-6xl uppercase font-cinzel',
    heading3: 'Collection',
    heading3Style: 'text-white text-6xl uppercase mb-8 font-cinzel',
    heading4: '50% off',
    heading4Style: 'text-hoverYellow font-cinzel text-8xl uppercase ',
  }

  return (
    <Layout>
      <div>
        <Carousel
          carousel={hero_data.carousel}
          // tailwind={hero_data.tailwind}
          // css={hero_data.css}
        />

        <SecondComp
          bg1={our_brands.bg1}
          blackBoxLogo={our_brands.blackBoxLogo}
          blackBoxLogoSize={our_brands.blackBoxLogoSize}
          blackContainer={our_brands.blackContainer}
          display={our_brands.display}
          font={our_brands.font}
          shopNowCompsData={our_brands.shopNowCompsData}
          text={our_brands.text}
        />

        <NewArrival
          spacing={new_arrival_and_winter_collection.spacing}
          firstCompData={new_arrival_and_winter_collection.firstCompData}
          display={new_arrival_and_winter_collection.display}
          carouselData={new_arrival_and_winter_collection.carouselData}
          btnData={new_arrival_and_winter_collection.btnData}
          winterCollectionData={
            new_arrival_and_winter_collection.winterCollectionData
          }
        />

        <DiscountComp
          image={discount_comp.image}
          container={discount_comp.container}
          containerPosition={discount_comp.containerPosition}
          logo={discount_comp.logo}
          logoSize={discount_comp.logoSize}
          heading1={discount_comp.heading1}
          heading1Style={discount_comp.heading1Style}
          heading2={discount_comp.heading2}
          heading2Style={discount_comp.heading2Style}
          heading3={discount_comp.heading3}
          heading3Style={discount_comp.heading3Style}
          heading4={discount_comp.heading4}
          heading4Style={discount_comp.heading4Style}
        />
      </div>
    </Layout>
  )
}

export default Home

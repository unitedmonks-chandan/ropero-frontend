import Layout from '../../components/layout';
import CustomForm from "../../components/Customer-Form/index";



const ContactForm = () => {
    let contact = {
        text_img: "assets/images/Customer-Form/assets/cs-text.svg",
        get_text: "Get In Touch",
        incude_text1: " We will respond to every email within 24 hours, Monday to Friday, excluding",
        incude_text2: "holidays. You can also call us at our toll-free number +0000-000-0000.",
        first_Name: "First Name",
        text: "text",
        last_Name: "Last Name",
        phone_no: "Phone",
        number: "number",
        email_text: "Email",
        email: "email",
        subject: "Subject",
        msg_text: "Message",
        btn: "Send",
        phoneImage: "assets/images/Customer-Form/assets/phone-cs.svg",
        phone: "Phone",
        num1: "1800 0000 000",
        num2: "1800 0000 000",
        mail: "assets/images/Customer-Form/assets/mail-cs.svg",
        mail_text: "E-Mail",
        emailId: "reporre@.com",
        map: "assets/images/Customer-Form/assets/map-cs.svg",
        address: "Address",
        addres_text1: "Lorem ipsum dolor",
        addres_text2: "sit amet,"
    }


    return (

        <div>
            <Layout>

                <CustomForm

                    props={contact}
                />

            </Layout>

        </div>
    )

}

export default ContactForm;
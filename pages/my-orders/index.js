import Layout from '../../components/layout';
import Header from '../../components/MyOrders/header/index';
import Orders from '../../components/MyOrders/Orders';

const MyOrders = () => {

    const heading = {
        order_text: "MY ORDERS"
    };

    const orders = {
        topBar: {
            tailwind: 'flex justify-between mt-12 ml-24 xl:ml-44 mr-14 xl:mr-32 mb-14',
            filter_text: "Filters",
            filterTailwind: 'colorGray24',
            placeholder_text: "what are you looking for?",
            icon: '/assets/images/My-orders/Group.png'
        }, 
    
        sideNavandOrdersContainer: 'flex mr-12 1800px:mr-36',
        sideNavContainerTailwind: 'mr-12 1800px:mr-32',

        orderStatusNavbar: {
            tailwind: 'mb-8 2xl:mb-16 pt-6 2xl:pt-12 bg-colorGray98 pl-10 1800px:pl-28',
            css: 'width-486px',
    
            heading: {
                text: 'Order Status', 
                tailwind: 'pb-4 xl:pb-6 2xl:pb-12 text-lg xl:text-2xl 2xl:text-3xl pl-8 xl:pl-16 text-colorBlack21 font-semibold border-b border-solid border-colorGray69 text-left',
                css: ''
            },
    
            linksContainer: {
                tailwind: 'mt-6 2xl:mt-12 pr-12 ml-8 xl:ml-16 pb-12',
                css: '',
                defaultSelectedLink: 'In Transit',
    
                links: [
                    {
                        tailwind: 'mb-8 2xl:mb-11 flex items-center justify-between',
                        link: {
                            text: 'Delivered',
                            linkHref: '/my-orders/delivered',
                            tailwind: 'text-xl 2xl:text-2xl font-light text-colorGray24',
                            css: '',
                            onClickCss: 'color-golden45 bold600'
                        },
                        
                        icon: {
                            svg: '/assets/svg/orders/black-right-arrow-svg-icon.svg',
                            tailwind: 'w-2.5 2xl:w-3 h-5',
                        }
                    },
    
                    {
                        tailwind: 'mb-8 2xl:mb-11 flex items-center justify-between',
                
                        link: {
                            text: 'In Transit',
                            linkHref: '/my-orders/in-transit',
                            tailwind: 'text-xl 2xl:text-2xl font-light text-colorGray24',
                            css: '',
                            onClickCss: 'color-golden45 bold600'
                        },
                        
                        icon: {
                            svg: '/assets/svg/orders/black-right-arrow-svg-icon.svg',
                            tailwind: 'w-2.5 2xl:w-3 h-5',
                        }
                    },
    
                
                    {
                        tailwind: 'mb-8 2xl:mb-11 flex items-center justify-between',
                
                        link: {
                            text: 'Cancelled',
                            linkHref: '/my-orders/cancelled',
                            tailwind: 'text-xl 2xl:text-2xl font-light text-colorGray24',
                            css: '',
                            onClickCss: 'color-golden45 bold600'
                        },
                        
                        icon: {
                            svg: '/assets/svg/orders/black-right-arrow-svg-icon.svg',
                            tailwind: 'w-2.5 2xl:w-3 h-5',
                        }
                    },
    
                
                    {
                        tailwind: ' flex items-center justify-between',
                
                        link: {
                            text: 'Returned',
                            linkHref: '/my-orders/returned',
                            tailwind: 'text-xl 2xl:text-2xl font-light text-colorGray24',
                            css: '',
                            onClickCss: 'color-golden45 bold600'
                        },
                        
                        icon: {
                            svg: '/assets/svg/orders/black-right-arrow-svg-icon.svg',
                            tailwind: 'w-2.5 2xl:w-3 h-5',
                        }
                    },
                ]
    
            }
        },
    
        orderTimeNavbar: {
            tailwind: 'pt-6 2xl:pt-12 bg-colorGray98 pl-10 1800px:pl-28',
            css: 'width-486px ',
    
            heading: {
                text: 'Order Time', 
                tailwind: 'pb-4 xl:pb-6 2xl:pb-12 text-lg xl:text-2xl 2xl:text-3xl text-colorBlack21 font-semibold border-b border-solid border-colorGray69 text-left pl-8 xl:pl-16',
                css: ''
            },
    
            linksContainer: {
                tailwind: 'mt-6 2xl:mt-12 pr-12 ml-8 xl:ml-16 pb-12',
                css: '',
                defaultSelectedLink: 'Last 30 Days',
    
                links: [
                    {
                        tailwind: 'mb-8 2xl:mb-11 flex items-center justify-between',
                        link: {
                            text: 'Last 30 Days',
                            linkHref: 'last-30-days',
                            tailwind: 'text-xl 2xl:text-2xl font-light text-colorGray24',
                            css: '',
                            onClickCss: ''
                        },
                        
                        icon: {
                            svg: '/assets/svg/orders/black-right-arrow-svg-icon.svg',
                            tailwind: 'w-2.5 2xl:w-3 h-5',
                        }
                    },
    
                    {
                        tailwind: 'mb-8 2xl:mb-11 flex items-center justify-between',
                
                        link: {
                            text: '2020',
                            linkHref: '2020',
                            tailwind: 'text-xl 2xl:text-2xl font-light text-colorGray24',
                            css: '',
                            onClickCss: ''
                        },
                        
                        icon: {
                            svg: '/assets/svg/orders/black-right-arrow-svg-icon.svg',
                            tailwind: 'w-2.5 2xl:w-3 h-5',
                        }
                    },
    
                
                    {
                        tailwind: 'mb-8 2xl:mb-11 flex items-center justify-between ',
                
                        link: {
                            text: '2019',
                            linkHref: '2019',
                            tailwind: 'text-xl 2xl:text-2xl font-light text-colorGray24',
                            css: '',
                            onClickCss: ''
                        },
                        
                        icon: {
                            svg: '/assets/svg/orders/black-right-arrow-svg-icon.svg',
                            tailwind: 'w-2.5 2xl:w-3 h-5',
                        }
                    },
    
                
                    {
                        tailwind: ' flex items-center justify-between',
                
                        link: {
                            text: 'Older',
                            linkHref: 'older',
                            tailwind: 'text-xl 2xl:text-2xl font-light text-colorGray24',
                            css: '',
                            onClickCss: ''
                        },
                        
                        icon: {
                            svg: '/assets/svg/orders/black-right-arrow-svg-icon.svg',
                            tailwind: 'w-2.5 2xl:w-3 h-5',
                        }
                    },
                ]
    
            }
        },
        
        orderItemsContainer: 'flex-1',
        orderItems: [
            {
                tailwind: 'h-40 xl:h-44 2xl:h-60 mb-5 w-full py-8 flex items-center bg-colorGray98  rounded-2xl xl:rounded-3xl ',
                css: 'padding-left-7percent',
            
                rightBorder: {
                    content: '',
                    tailwind: 'h-full border-r border-solid border-colorGray91'
                },
                
                image: {
                    imageSource: '/assets/images/My-orders/11.png',
                    tailwind: 'w-28 xl:w-36 2xl:w-44'
                }, 
            
                productNameAndDescription: {
                    tailwind: 'w-40 xl:w-52 2xl:w-56',
                    productName: {
                        text: 'Dark green hoody Sweatshirt',
                        tailwind: 'text-sm 2xl:text-base font-medium text-colorGray30 mb-4 ml-4 xl:ml-9'
                    },
            
                    productDescription: {
                        tailwind: 'grid grid-cols-2 grid-rows-2 gap-y-2 ml-4 xl:ml-9',
                        colorTag: {
                            text: 'Color:',
                            tailwind: 'text-sm 2xl:text-base font-normal',
                        },
                
                        color: {
                            text: 'Grey', 
                            tailwind: 'text-sm 2xl:text-base font-normal'
                        },
                
                        sizeTag: {
                            text: 'Size:',
                            tailwind: 'text-sm 2xl:text-base font-normal',
                        },
                
                        size: {
                            text: 'Large', 
                            tailwind: 'text-sm 2xl:text-base font-normal'
                        },
                    }
            
            
                }, 
            
                deliveryDate: {
                    text: 'Delivered On',
                    date: '25 December 2021', 
                    tailwind: 'flex-auto 2xl:flex-1 text-sm 2xl:text-lg font-medium text-colorGray30  flex flex-col justify-start items-center'
                }, 
                
                productQuantity: {
                    text: 'Quantity: ',
                    quantity: 1, 
                    tailwind: 'flex-auto 2xl:flex-1 text-sm 2xl:text-lg font-medium text-colorGray30  flex justify-center'
                },
            
                productPrice: {
                    tailwind: 'flex-auto 2xl:flex-1 flex items-center justify-center text-lg xl:text-xl 2xl:text-3xl font-medium',
                    priceSvg: '/assets/svg/orders/indian-rupee-svg-icon.svg',
                    priceSvgTailwind: 'w-2.5 2xl:w-3.5 mr-2',
                    price: 5000,
                    priceTailwind: '' 
                }
            }, 
    
            {
                tailwind: 'h-40 xl:h-44 2xl:h-60 py-8 flex items-center bg-colorGray90 w-full rounded-2xl xl:rounded-3xl ',
                css: 'padding-left-7percent mb-5',
            
                rightBorder: {
                    content: '',
                    tailwind: 'h-full border-r border-solid border-colorGray84'
                },
                
                image: {
                    imageSource: '/assets/images/My-orders/12.png',
                    tailwind: 'w-28 xl:w-36 2xl:w-44'
                }, 
            
                productNameAndDescription: {
                    tailwind: 'w-40 xl:w-52 2xl:w-56',
                    productName: {
                        text: 'Dark green hoody Sweatshirt',
                        tailwind: 'text-sm 2xl:text-base font-medium text-colorGray30 mb-4 ml-4 xl:ml-9'
                    },
            
                    productDescription: {
                        tailwind: 'grid grid-cols-2 grid-rows-2 gap-y-2 ml-4 xl:ml-9',
                        colorTag: {
                            text: 'Color:',
                            tailwind: 'text-sm 2xl:text-base font-normal',
                        },
                
                        color: {
                            text: 'Grey', 
                            tailwind: 'text-sm 2xl:text-base font-normal'
                        },
                
                        sizeTag: {
                            text: 'Size:',
                            tailwind: 'text-sm 2xl:text-base font-normal',
                        },
                
                        size: {
                            text: 'Large', 
                            tailwind: 'text-sm 2xl:text-base font-normal'
                        },
                    }
            
            
                }, 
            
                deliveryDate: {
                    text: 'Delivered On',
                    date: '25 December 2021', 
                    tailwind: 'flex-auto 2xl:flex-1 text-sm 2xl:text-lg font-medium text-colorGray30 flex flex-col justify-start items-center'
                }, 
                
                productQuantity: {
                    text: 'Quantity: ',
                    quantity: 1, 
                    tailwind: 'flex-auto 2xl:flex-1 text-sm 2xl:text-lg font-medium text-colorGray30 flex justify-center'
                },
            
                productPrice: {
                    tailwind: 'flex-auto 2xl:flex-1 flex items-center justify-center text-lg xl:text-xl 2xl:text-3xl font-medium',
                    priceSvg: '/assets/svg/orders/indian-rupee-svg-icon.svg',
                    priceSvgTailwind: 'w-2.5 2xl:w-3.5 mr-2',
                    price: 5000,
                    priceTailwind: '' 
                }
            },
    
    
            {
                tailwind: 'h-40 xl:h-44 2xl:h-60 py-8 flex items-center bg-colorGray98 w-full rounded-2xl xl:rounded-3xl ',
                css: 'padding-left-7percent mb-5',
            
                rightBorder: {
                    content: '',
                    tailwind: 'h-full border-r border-solid border-colorGray91'
                },
                
                image: {
                    imageSource: '/assets/images/My-orders/13.png',
                    tailwind: 'w-28 xl:w-36 2xl:w-44'
                }, 
            
                productNameAndDescription: {
                    tailwind: 'w-40 xl:w-52 2xl:w-56',
                    productName: {
                        text: 'Dark green hoody Sweatshirt',
                        tailwind: 'text-sm 2xl:text-base font-medium text-colorGray30 mb-4 ml-4 xl:ml-9'
                    },
            
                    productDescription: {
                        tailwind: 'grid grid-cols-2 grid-rows-2 gap-y-2 ml-4 xl:ml-9',
                        colorTag: {
                            text: 'Color:',
                            tailwind: 'text-sm 2xl:text-base font-normal',
                        },
                
                        color: {
                            text: 'Grey', 
                            tailwind: 'text-sm 2xl:text-base font-normal'
                        },
                
                        sizeTag: {
                            text: 'Size:',
                            tailwind: 'text-sm 2xl:text-base font-normal',
                        },
                
                        size: {
                            text: 'Large', 
                            tailwind: 'text-sm 2xl:text-base font-normal'
                        },
                    }
            
            
                }, 
            
                deliveryDate: {
                    text: 'Delivered On',
                    date: '25 December 2021', 
                    tailwind: 'flex-auto 2xl:flex-1 text-sm 2xl:text-lg font-medium text-colorGray30 flex flex-col justify-start items-center'
                }, 
                
                productQuantity: {
                    text: 'Quantity: ',
                    quantity: 1, 
                    tailwind: 'flex-auto 2xl:flex-1 text-sm 2xl:text-lg font-medium text-colorGray30 flex justify-center'
                },
            
                productPrice: {
                    tailwind: 'flex-auto 2xl:flex-1 flex items-center justify-center text-lg xl:text-xl 2xl:text-3xl font-medium',
                    priceSvg: '/assets/svg/orders/indian-rupee-svg-icon.svg',
                    priceSvgTailwind: 'w-2.5 2xl:w-3.5 mr-2',
                    price: 5000,
                    priceTailwind: '' 
                }
            },
    
            {
                tailwind: 'h-40 xl:h-44 2xl:h-60 py-8 flex items-center bg-colorGray98 w-full rounded-2xl xl:rounded-3xl mb-5',
                css: 'padding-left-7percent ',
            
                rightBorder: {
                    content: '',
                    tailwind: 'h-full border-r border-solid border-colorGray91'
                },
                
                image: {
                    imageSource: '/assets/images/My-orders/14.png',
                    tailwind: 'w-28 xl:w-36 2xl:w-44'
                }, 
            
                productNameAndDescription: {
                    tailwind: 'w-40 xl:w-52 2xl:w-56',
                    productName: {
                        text: 'Dark green hoody Sweatshirt',
                        tailwind: 'text-sm 2xl:text-base font-medium text-colorGray30 mb-4 ml-4 xl:ml-9'
                    },
            
                    productDescription: {
                        tailwind: 'grid grid-cols-2 grid-rows-2 gap-y-2 ml-4 xl:ml-9',
                        colorTag: {
                            text: 'Color:',
                            tailwind: 'text-sm 2xl:text-base font-normal',
                        },
                
                        color: {
                            text: 'Grey', 
                            tailwind: 'text-sm 2xl:text-base font-normal'
                        },
                
                        sizeTag: {
                            text: 'Size:',
                            tailwind: 'text-sm 2xl:text-base font-normal',
                        },
                
                        size: {
                            text: 'Large', 
                            tailwind: 'text-sm 2xl:text-base font-normal'
                        },
                    }
            
            
                }, 
            
                deliveryDate: {
                    text: 'Delivered On',
                    date: '25 December 2021', 
                    tailwind: 'flex-auto 2xl:flex-1 text-sm 2xl:text-lg font-medium text-colorGray30 flex flex-col justify-start items-center'
                }, 
                
                productQuantity: {
                    text: 'Quantity: ',
                    quantity: 1, 
                    tailwind: 'flex-auto 2xl:flex-1 text-sm 2xl:text-lg font-medium text-colorGray30 flex justify-center'
                },
            
                productPrice: {
                    tailwind: 'flex-auto 2xl:flex-1 flex items-center justify-center text-lg xl:text-xl 2xl:text-3xl font-medium',
                    priceSvg: '/assets/svg/orders/indian-rupee-svg-icon.svg',
                    priceSvgTailwind: 'w-2.5 2xl:w-3.5 mr-2',
                    price: 5000,
                    priceTailwind: '' 
                }
            },
        ], 
    
        buttonAndPageContainer: 'flex justify-center mt-20 mb-20', 
    
        buttonData: {
            view: "VIEW ALL"
        }, 
    
        pagination: {
            page_text: "Page: ", 
            buttonNumber: '01', 
            arrowLeft: '/assets/svg/orders/black-left-arrow-small.svg',
            arrowRight: '/assets/svg/orders/black-right-arrow-small.svg'
        }
    };

    return (
        <Layout>
            <Header 
                order_text={heading.order_text}
            />
            <Orders 
                topBar={orders.topBar} 
                orderStatusNavbar={orders.orderStatusNavbar} 
                orderTimeNavbar={orders.orderTimeNavbar} 
                sideNavandOrdersContainer={orders.sideNavandOrdersContainer} 
                sideNavContainerTailwind={orders.sideNavContainerTailwind} 
                orderItems={orders.orderItems} 
                buttonData={orders.buttonData} 
                pagination={orders.pagination} 
                buttonAndPageContainer={orders.buttonAndPageContainer}
                orderItemsContainer={orders.orderItemsContainer}
            />
        </Layout>
    );
};

export default MyOrders;
import '../styles/globals.css';

import "../components/Customer-Form/Banner/banner.css";
import "../components/shipping/Title-image/TitleImg.css";
import "../components/MigearHomePage/TrendingStyles/custom.css";
import "../components/MigearHomePage/NewCollection/custom.css";

// used in monadaa hero section for single right arrow
import '../components/Carousel/arrow.css';
import '../components/NewCarousel/slider-arrow.css';

//For HeroNewSlider component
// used in zimba hero section, arker
import "../components/HeroNewCarousel/double-arrow-slider.css";

//Zimba page our collection 
import "../components/OurCollection/our-collection.css";

//Zimba page new arrivals
import "../components/ZimbaHomePage/NewArrivals/bg-style.css";
import "../components/NewCarousel/slider-arrow.css";

// Zimba best seller 
import "../components/CarouselCustomSyling/carousel-custom-styling.css";


// Faqs-background image 
import "../components/FAQs/Background/Faqsbg.css"

// About us
import '../components/AboutUs/WhyRoperro/custom.css';




function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp

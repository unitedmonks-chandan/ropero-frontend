import Layout from "@components/layout";
import AboutUsHero from "@components/AboutUs/Hero/AboutUsHero";
import WhyRoperro from "@components/AboutUs/WhyRoperro/WhyRoperro";
import OurBrands from "@components/AboutUs/OurBrands/OurBrands";

const AboutUs = () => {

    const hero = {
        container: "relative w-full",
        image: '/assets/images/about-us/hero.jpg',
        position: "absolute perfect-center text-center",
        heading1: "About",
        heading1Style: "text-6xl text-white font-primary font-light uppercase",
        heading2: "Roperro",
        heading2Style: "text-8xl xl:text-9xl text-white font-cinzel uppercase tracking-widest",
    };


    const why_roperro = {
        container: "bg-black py-20 xl:py-28 pl-32",
        display: "flex gap-24 mb-24",
        logo: '/assets/svg/roperro-golden-logo.svg',
        logoSize: "w-44",
        textData: {
            container: "pl-10 xl:pl-20 pr-20 2xl:pr-40 left-golden-border relative",
            heading1:
            "Our wardrobe to us means more than a physical space for our clothes & accessories.It reflects who we are, our style, what de-fines us, ourselections&carefully picked pieces.Celebrating this part of our personality, is Roperro.",
            heading1Style: "text-gray300 text-base xl:text-lg 2xl:text-2xl  font-primay mb-4",
            heading2:
            "Presenting firstof its kind multibrande-commerce platform for your styleneeds only.Each piececarefully created & selected for you to be able tmake easychoices of what speaks‘You’the best.Right  from  bags  to  all   year  wear, Roperro  has  you covered.Whether it’s a  stylized  gifting box or something you desire for your own use, versatile,high-quality products. Something fromdaily to luxury,depending on what you desire you will find here.So, the next time you’re  looking for  quality &  style no need to hunt many  places  just to find  that  one piece,  drop by on our website and we promise you we will spoil you for options. If it is style, it is Roperro.",
            heading2Style: "text-gray300 text-base xl:text-lg 2xl:text-2xl  font-primay mb-12",
            heading3: "The Best of Global Trends",
            heading3Style: "text-hoverYellow text-2xl xl:text-3xl 2xl:text-4xl font-cinzel uppercase",
            heading4: "Packed into one just for you",
            heading4Style: "text-hoverYellow text-2xl xl:text-3xl 2xl:text-4xl font-cinzel uppercase",
        },
        position: "relative",
        image: '/assets/images/about-us/why-roperro.jpg',
        headingsBg: "bg-hoverYellow p-6 max-w-sm xl:max-w-lg 2xl:max-w-2xl absolute center-vertically -left-20",
        textData2: {
            container: "py-2 px-8 xl:px-16 text-center border border-lightGolden",
            heading1: "Why",
            heading1Style: "text-lightGolden text-6xl xl:text-8xl 2xl:text-9xl font-cinzel uppercase -mb-7 xl:-mb-10 2xl:-mb-14",
            heading2: "Roperro",
            heading2Style: "text-black text-3xl xl:text-5xl 2xl:text-7xl uppercase font-primary mb-6 xl:mb-12",
            heading3:
            "Our wardrobe to us means more than a physical space for our clothes & accessories. It reflects who we are, our style, what defines us, our selections & carefully picked pieces.",
            heading3Style: "text-white text-sm xl:text-base 2xl:text-2xl font-primay mb-6 xl:mb-12",
            heading4:
            " Each piece carefully created & selected for you to be able to make easy choices of what speaks ‘You’ the best. So, the next time you’re looking for quality & style no need to hunt many places just to find that one piece.",
            heading4Style: "text-white text-sm xl:text-base 2xl:text-2xl font-primay",
        }
    }

    const our_brands = {
        container: "bg-black py-10 xl:py-20 px-16",
        textData: {
            container: "text-center",
            heading1: "FROM THE HOUSE OF ROPERRO",
            heading1Style: "text-white text-xl xl:text-2xl font-primay uppercase mb-4",
            heading2: "Our Brands",
            heading2Style: "text-hoverYellow text-6xl xl:text-7xl font-cinzel uppercase mb-16",
        },
        carouselData: {
            customSlidesPerView: 3,
            customSlidesPerGroup: 1,
            customSpaceBetween: 0.01,
            customStyle:
            "hide-inbuilt-arrow left-white-arrow right-white-arrow white-pagination-bullet golden-pagination-bullet-active roperro-brands-slider reset-swiper-container reset-swiper-slide px-28 pb-20 relative",
            carouselData: [
            {
                image: '/assets/images/about-us/brand1.jpg',
                altTag: 'model',
                btnData: {
                position: "absolute brands-logo-position z-50",
                logo: '/assets/svg/zimba/logo.svg',
                logoSize: "slider-logo-size",
                btnLink: "/",
                btn: "hidden",
                btnText: "hidden",
                },
            },
            {
                image: '/assets/images/about-us/brand2.jpg',
                altTag: 'model',
                btnData: {
                position: "absolute brands-logo-position z-50",
                logo: '/assets/svg/monadaa.svg',
                logoSize: "slider-logo-size",
                btnLink: "/",
                btn: "hidden",
                btnText: "",
                },
            },
            {
                image: '/assets/images/about-us/brand3.jpg',
                altTag: 'model',
                btnData: {
                position: "absolute brands-logo-position z-50",
                logo: '/assets/svg/lerida/logo2.svg',
                logoSize: "slider-logo-size",
                btnLink: "",
                btn: "hidden",
                btnText: "",
                },
            },
            {
                image: '/assets/images/about-us/brand2.jpg',
                altTag: 'model',
                btnData: {
                position: "absolute brands-logo-position z-50",
                logo: '/assets/svg/monadaa.svg',
                logoSize: "slider-logo-size",
                btnLink: "/",
                btn: "hidden",
                btnText: "hidden",
                },
            },
            {
                image: '/assets/images/about-us/brand3.jpg',
                altTag: 'model',
                btnData: {
                position: "absolute brands-logo-position z-50",
                logo: '/assets/svg/lerida/logo2.svg',
                logoSize: "slider-logo-size",
                btnLink: "/",
                btn: "hidden",
                btnText: "",
                },
            },
            {
                image: '/assets/images/about-us/brand1.jpg',
                altTag: 'model',
                btnData: {
                position: "absolute brands-logo-position z-50",
                logo: '/assets/svg/zimba/logo.svg',
                logoSize: "slider-logo-size",
                btnLink: "",
                btn: "hidden",
                btnText: "",
                },
            },
            ],
        }
    };


    return (
        <Layout>
            <AboutUsHero 
                container={hero.container}
                image={hero.image}
                imgSize={hero.imgSize}
                position={hero.position}
                heading1={hero.heading1}
                heading1Style={hero.heading1Style}
                heading2={hero.heading2}
                heading2Style={hero.heading2Style}
            />

            <WhyRoperro 
                container={why_roperro.container}
                display={why_roperro.display}
                logo={why_roperro.logo}
                logoSize={why_roperro.logoSize}
                textData={why_roperro.textData}
                position={why_roperro.position}
                image={why_roperro.image}
                imgSize={why_roperro.imgSize}
                headingsBg={why_roperro.headingsBg}
                textData2={why_roperro.textData2}
            />

            <OurBrands 
                container={our_brands.container}
                textData={our_brands.textData}
                carouselData={our_brands.carouselData}
            />
        </Layout>
    );
};

export default AboutUs;
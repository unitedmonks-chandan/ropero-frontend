import React from 'react'
import Layout from '../../components/layout'
import Shipping from '../../components/shipping/index'
import ShippingRest from "../../components/Shipping-Restrictions/index"

const ShippingCard = () => {


    let shipping_data = {
        Img: "assets/images/shipping/Group.png",
        title_text: "Shipping Time And Costs",
        text_statement1: "Express Shipping – Free ",
        text_statement2: "All orders are shipped using DHL Express and shipping is free of charge. ",
        text_statement3: "Our courier usually delivers from Monday to Friday during business hours and will make three delivery attempts. After the third failed attempt, your order will be returned to our warehouse. Please note that once you have placed an order, it is no longer possible to modify your shipping address. Please contact Customer Service should you have any questions about your order.",
        text_statement4: "All orders are processed automatically and we are unable to expedite or delay shipping times. Our warehouse does not operate on certain public holidays.",
        text_statement5: "When pre-ordering, the number of days for shipping will be calculated at the time the items become available in the warehouse. ",
    }
    let shipRest = {
        heading: "Shipping Restrictions",

        statement_1: " The delivery address of your order must match the country site in which you place your order. Orders made from a different country site cannot be made. Please select the correct country site for your order from the link in the top-left corner of every page.",

        statement_2: "At this time we are unable to ship orders to General Delivery, P.O. Boxes, Azores, Madeira, the Faéroér Islands, Greenland, Aland Island, the Holland Antilles, Poste Restante, Canary Islands, St. Pierre et Miquelon, Livigno, Channel Islands, Ceuta and Melilla. Orders made to any of these addresses will be cancelled.",

        statement_3: "Please note that dangerous goods like fragrances can’t be shipped to the markets listed below: Australia, Canada, China, Japan, New Zealand, Qatar, Taiwan, Turkey"
    }

    return (
        <div>
            <Layout>

                <Shipping
                    props={shipping_data}
                />
                <ShippingRest props={shipRest} />
            </Layout>

        </div>
    )
}


export default ShippingCard


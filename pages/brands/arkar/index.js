// importing components
import Layout from '../../../components/layout'
import HeroNewCarousel from '../../../components/HeroNewCarousel/HeroNewCarousel'
import ArkarDiscount from '../../../components/ArkarHomePage/Discount/ArkarDiscount'
import BestSeller from '../../../components/ArkarHomePage/BestSeller/BestSeller'
import OurCollectionWithNewStyles from '../../../components/ArkarHomePage/OurCollectionWithNewStyles'
import { useEffect, useState } from 'react'

const Arkar = () => {
  const [newStylesSlides, setNewStylesSlides] = useState(3)

  const hero_carousel = {
    customStyle: 'arker-hero-slider  relative h-screen',
    carouselData: [
      {
        image: '/assets/images/arkar/hero.jpg',
        altTag: 'model',
        btnData: {
          position: 'absolute arkar-hero-btn-position z-50 flex flex-col gap-8',
          logo: '/assets/svg/arkar/arkar-logo-new.svg',
          logoSize: 'slider-logo-size',
          btnLink: '#',
          // btnLink: "/arkar",
          btn: 'bg-midRed text-white text-xl 2xl:text-2xl font-primary uppercase px-4 lg:px-7 2xl:px-11 py-1.5 lg:py-2 2xl:py-3.5 self-center',
          btnText: 'Shop now',
        },
      },
      {
        image: '/assets/images/monadaa-comp.png',
        altTag: 'model',
        btnData: {
          position: 'absolute arkar-hero-btn-position z-50 flex flex-col gap-8',
          logo: '/assets/svg/arkar/arkar-logo-new.svg',
          logoSize: 'slider-logo-size',
          btnLink: '#',
          // btnLink: "/arkar/view-all",
          btn: 'bg-white text-black text-xl 2xl:text-2xl font-primary px-4 lg:px-7 2xl:px-11 py-1.5 lg:py-2 2xl:py-3.5 uppercase self-center',
          btnText: 'View all',
        },
      },
    ],
  }

  const our_collection_with_new_stylesy = {
    containerStyle:
      'bg-gradient-to-t	from-type2ColorBrown18 to-type2ColorBrown32',

    ourCollection: {
      stripContainer:
        'font-primary text-center pt-20 lg:pt-28 2xl:pt-36 pb-24 lg:pb-28 2xl:pb-32 arkar-strip-bg bg-cover',
      stripText1: 'Our Collection',
      stripText1Style:
        'text-white text-4xl lg:text-5xl mb-7 lg:mb-6 uppercase font-cinzel',
      stripText2:
        'The online store presents selected successful collections, fashionable ',
      stripText2Style: 'text-lightGray text-xl 2xl:text-3xl text-center',
      stripText3:
        'women’s clothing which is sewn exclusively from quality material.',
      stripText3Style: 'text-lightGray text-xl 2xl:text-3xl text-center',
      display: 'grid grid-cols-1 lg:grid-cols-2 px-32 lg:px-0',
      shopNowCompsData: [
        {
          bgImg: '/assets/images/arkar/ourCollectionWomen.jpg',
          altTag: 'Woman in blue jacket',
          colSpan: '1',
          btnData: {
            container: '',
            position: 'absolute left-24 2xl:left-28 bottom-24 2xl:bottom-28',
            headingStyle: '',
            heading1: 'For',
            heading1Style:
              'text-3xl 2xl:text-4xl text-white uppercase font-cinzel',
            heading2: 'Women',
            heading2Style:
              'text-4xl 2xl:text-5xl text-white uppercase mb-5 2xl:mb-6 font-cinzel',
            btnContainer: '',
            btnLink: '#',
            // btnLink: '/arkar/women',
            btn: 'bg-white text-colorBlack2 px-10 2xl:px-12 py-3 2xl:py-3.5 uppercase text-2xl',
            size: '',
            btnText: 'Shop now',
          },
        },
        {
          bgImg: '/assets/images/arkar/ourCollectionMen.jpg',
          altTag: 'Man in brown jacket',
          colSpan: '1',
          btnData: {
            container: '',
            position: 'absolute left-24 2xl:left-28 bottom-24 2xl:bottom-28',
            headingStyle: '',
            heading1: 'For',
            heading1Style:
              'text-3xl 2xl:text-4xl text-white uppercase font-cinzel',
            heading2: 'Men',
            heading2Style:
              'text-4xl 2xl:text-5xl text-white uppercase mb-5 2xl:mb-6 font-cinzel',
            btnContainer: '',
            btnLink: '#',
            // btnLink: '/arkar/men',
            btn: 'bg-white text-colorBlack2 px-10 2xl:px-12 py-3 2xl:py-3.5 uppercase text-2xl',
            size: '',
            btnText: 'Shop now',
          },
        },
        {
          bgImg: '/assets/images/arkar/ourCollectionLifestyle.jpg',
          altTag: 'Woman in black coat',
          colSpan: '1',
          btnData: {
            container: '',
            position: 'absolute left-24 2xl:left-24 bottom-16 2xl:bottom-28',
            headingStyle: '',
            heading1: 'Best in women',
            heading1Style:
              'text-3xl 2xl:text-4xl text-white uppercase font-cinzel',
            heading2: 'lifeStyle',
            heading2Style:
              'text-4xl 2xl:text-5xl text-white uppercase mb-5 2xl:mb-6 font-cinzel',
            btnContainer: '',
            btnLink: '#',
            // btnLink: '/arkar/lifestyle',
            btn: 'bg-white text-colorBlack2 px-10 2xl:px-12 py-3 2xl:py-3.5 uppercase text-2xl',
            size: '',
            btnText: 'Shop now',
          },
        },
        {
          bgImg: '/assets/images/arkar/ourCollectionJackets.jpg',
          altTag: 'Man in jacket',
          colSpan: '1',
          btnData: {
            container: '',
            position: 'absolute left-24 2xl:left-28 bottom-24 2xl:bottom-28',
            headingStyle: '',
            heading1: 'Custom varsity',
            heading1Style:
              'text-3xl 2xl:text-4xl text-white uppercase font-cinzel',
            heading2: 'Jackets',
            heading2Style:
              'text-4xl 2xl:text-5xl text-white uppercase mb-5 2xl:mb-6 font-cinzel',
            btnContainer: '',
            // btnLink: '/arkar/jackets',
            btnLink: '#',
            btn: 'bg-white text-colorBlack2 px-10 2xl:px-12 py-3 2xl:py-3.5 uppercase text-2xl',
            size: '',
            btnText: 'Shop now',
          },
        },
      ],
    },

    newStyles: {
      container:
        'pt-28 lg:pt-20 2xl:pt-40 pb-20 2xl:pb-36 brownImgBg bg-center flex flex-col items-center single-arrow-slider',
      textData: {
        display:
          'flex flex-col lg:flex-row gap-9 lg:gap-16 2xl:gap-20 items-center pl-0 lg:pl-24 2xl:pl-32 mb-28 lg:mb-16 2xl:mb-40',
        headings: ['New Styles'],
        headingStyle:
          'text-midRed w-90 lg:w-64 2xl:w-96 text-6xl 2xl:text-7xl uppercase font-cinzel tracking-wide',
        paras: [
          'The online store presents selected successful collections, fashionable women’s clothing which is sewn exclusively from quality material.',
        ],
        paraStyle:
          'text-white text-xl 2xl:text-2xl w-3/5 2xl:w-1/3 text-center lg:text-left',
      },
      btnData: {
        btnLink: '#',
        // btnLink: '/arkar/view-all',
        btn: 'bg-midRed text-white text-lg 2xl:text-2xl px-10 2xl:px-14 py-1 2xl:py-2.5 self-end justify-self-center',
        text: 'View All',
        size: '',
      },
      display: 'grid grid-cols-4',
      carouselData: {
        customSlidersPerView: newStylesSlides,
        customSlidesPerGroup: newStylesSlides,
        customStyle:
          'arkar-new-styles pl-20 relative lg:w-full col-start-2 col-end-4 lg:col-span-3',
        images: [
          {
            img: '/assets/images/arkar/new-stylesy1.jpg',
            altTag: 'model',
          },
          {
            img: '/assets/images/arkar/new-stylesy2.jpg',
            altTag: 'model',
          },
          {
            img: '/assets/images/arkar/new-stylesy3.jpg',
            altTag: 'model',
          },
          {
            img: '/assets/images/arkar/new-stylesy2.jpg',
            altTag: 'model',
          },
          {
            img: '/assets/images/arkar/new-stylesy3.jpg',
            altTag: 'model',
          },
          {
            img: '/assets/images/arkar/new-stylesy1.jpg',
            altTag: 'model',
          },
        ],
      },
    },
  }

  const arkar_discount = {
    image: '/assets/images/arkar/discount.jpg',
    container:
      'bg-midRed text-white font-primary px-16 2xl:px-24 pt-8 2xl:pt-16 pb-10 2xl:pb-20', //pt-8 px-16 pb-10
    containerPosition: 'absolute left-16 2xl:left-20 heading-position',
    logo: '/assets/images/arkar/arkar-logo.svg',
    logoSize: 'w-24 2xl:w-40 mb-7 2xl:mb-12',
    heading1: 'HOUSE OF ROPERRO',
    heading1Style: 'text-lg 2xl:text-xl mb-1 uppercase',
    heading2: 'Leather',
    heading2Style: 'text-4xl 2xl:text-6xl font-cinzel uppercase font-light',
    heading3: 'Collection',
    heading3Style: 'text-5xl 2xl:text-6xl mb-5 2xl:mb-6 font-cinzel uppercase',
    heading4: '50% off',
    heading4Style: 'text-5xl 2xl:text-7xl uppercase',
  }

  const best_seller = {
    display:
      'bg-black pt-12 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 md:gap-x-4 md:gap-y-3 place-items-center text-white',
    img1: {
      imageSource: '/assets/images/arkar/best-seller1.jpg',
      altTag: 'Man with Brown Coat',
    },
    img2: {
      imageSource: '/assets/images/arkar/best-seller2.jpg',
      altTag: 'Woman in blue coat pent',
    },
    img3: {
      imageSource: '/assets/images/arkar/best-seller3.jpg',
      altTag: 'Woman with grey purse',
    },
    img4: {
      imageSource: '/assets/images/arkar/best-seller4.jpg',
      altTag: 'Man in green shirt',
    },
    img5: {
      imageSource: '/assets/images/arkar/best-seller5.jpg',
      altTag: 'Woman in black dress',
    },

    colSpan: 'md:col-span-2 md:self-stretch',
    mainHeadingStyle:
      'text-white text-4xl 2xl:text-6xl uppercase mb-2 font-cinzel', //text-3xl
    textStyle: 'md:col-span-2 text-white text-lg 2xl:text-2xl px-36 py-32', //text-lg
    text: {
      mainHeading: 'Best Seller',
      textData: [
        'The online store presents selected',
        'successful collections, fashionable',
        'women’s clothing which is sewn',
        'exclusively from quality material.',
      ],
    },
  }

  useEffect(() => {
    const width = window.screen.width
    console.log(width)
    if (width < 1024) {
      setNewStylesSlides(1)
    } else {
      setNewStylesSlides(3)
    }
  }, [])

  return (
    <Layout>
      <div>
        <HeroNewCarousel
          customStyle={hero_carousel.customStyle}
          customSlidesPerView={hero_carousel.customSlidesPerView}
          customSlidesPerGroup={hero_carousel.customSlidesPerGroup}
          customSpaceBetween={hero_carousel.customSpaceBetween}
          carouselData={hero_carousel.carouselData}
        />

        <OurCollectionWithNewStyles
          containerStyle={our_collection_with_new_stylesy.containerStyle}
          ourCollection={our_collection_with_new_stylesy.ourCollection}
          newStylesy={our_collection_with_new_stylesy.newStyles}
        />

        <ArkarDiscount
          image={arkar_discount.image}
          container={arkar_discount.container}
          containerPosition={arkar_discount.containerPosition}
          logo={arkar_discount.logo}
          logoSize={arkar_discount.logoSize}
          heading1={arkar_discount.heading1}
          heading1Style={arkar_discount.heading1Style}
          heading2={arkar_discount.heading2}
          heading2Style={arkar_discount.heading2Style}
          heading3={arkar_discount.heading3}
          heading3Style={arkar_discount.heading3Style}
          heading4={arkar_discount.heading4}
          heading4Style={arkar_discount.heading4Style}
        />

        <BestSeller
          display={best_seller.display}
          img1={best_seller.img1}
          img2={best_seller.img2}
          img3={best_seller.img3}
          img4={best_seller.img4}
          img5={best_seller.img5}
          colSpan={best_seller.colSpan}
          mainHeadingStyle={best_seller.mainHeadingStyle}
          text={best_seller.text}
          textStyle={best_seller.textStyle}
        />
      </div>
    </Layout>
  )
}

export default Arkar

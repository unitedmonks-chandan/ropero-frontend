import Layout from '../../../components/layout'
import HeroNewCarousel from '../../../components/HeroNewCarousel/HeroNewCarousel'
import LeridaNewArrivals from '@components/LeridaHomePage/NewArrivals/LeridaNewArrivals'
import OurCollection from '../../../components/OurCollection/OurCollection'
import LeridaBestSellers from '@components/LeridaHomePage/BestSellers/LeridaBestSellers'
import LeridaTrendingStyles from '@components/LeridaHomePage/TrendingStyles/LeridaTrendingStyles'
import ImageBtnIconsCarousel from '@components/ImageBtnIconsCarousel/ImageBtnIconsCarousel'

const Lerida = () => {
  const hero = {
    customStyle:
      'relative h-screen hide-inbuilt-arrow hide-left-hidden-arrow right-white-arrow white-pagination-bullet golden-pagination-bullet-active lerida-hero-slider',
    carouselData: [
      {
        image: '/assets/images/lerida/hero.jpg',
        altTag: 'model',
        btnData: {
          position:
            'absolute lerida-hero-btn-position z-50 flex flex-col gap-8 items-center justify-center',
          logo: '/assets/svg/lerida/logo.svg',
          logoSize: 'slider-logo-size',
          btnLink: '#',
          // btnLink: "/lerida",
          btn: 'bg-white text-black text-lg font-primary px-7 py-1 uppercase hidden',
          btnText: 'Shop now',
        },
      },
      {
        image: '/assets/images/lerida/hero.jpg',
        altTag: 'model',
        btnData: {
          position:
            'absolute lerida-hero-btn-position z-50 flex flex-col gap-8 items-center justify-center',
          logo: '/assets/svg/lerida/logo2.svg',
          logoSize: 'slider-logo-size',
          btnLink: '#',
          //   btnLink: '/lerida/view-all',
          btn: 'bg-midRed text-white text-lg font-primary px-7 py-1 uppercase hidden',
          btnText: 'View all',
        },
      },
    ],
  }

  const new_arrivals = {
    container: 'bg-black py-24 pl-16',
    display: 'grid grid-cols-3 gap-16',
    btnData: {
      container: 'pt-44 pl-8',
      position: '',
      headingStyle: 'text-5xl text-white font-cinzel uppercase mb-6',
      heading1: 'New',
      heading2: 'Arrivals',
      heading3:
        'The online store presents selected successful collections, fashionable women’s clothing which is sewn exclusively from high quality material',
      heading3Style: 'text-lg text-gray100 font-primary mt-6',
      btnLink: '#',
      //   btnLink: '/view-all',
      btn: 'inline-block bg-lightOrange text-black text-lg font-primary px-7 font-medium uppercase',
      size: '',
      btnText: 'Explore',
    },
    carouselData: {
      customSlidersPerView: 2,
      customSlidesPerGroup: 2,
      customSpaceBetween: 0.1,
      customStyle:
        'relative w-full col-span-2 hide-inbuilt-arrow hide-left-hidden-arrow right-white-arrow hide-pagination-bullets lerida-new-arrivals-slider',
      images: [
        {
          img: '/assets/images/lerida/new-arrivals1.jpg',
          altTag: 'model',
        },
        {
          img: '/assets/images/lerida/new-arrivals2.jpg',
          altTag: 'model',
        },
        {
          img: '/assets/images/lerida/new-arrivals2.jpg',
          altTag: 'model',
        },
        {
          img: '/assets/images/lerida/new-arrivals1.jpg',
          altTag: 'model',
        },
      ],
    },
    heading: 'LEATHER JACKETS',
    headingStyle:
      'text-xl text-white font-primary font-medium text-center uppercase mt-10',
  }

  const our_collection = {
    display: 'grid grid-cols-1',
    shopNowCompsData: [
      {
        bgImg: '/assets/images/lerida/for-her.jpg',
        colSpan: '1',
        btnData: {
          container: '',
          position:
            'lerida-our-collection-btn bottom-72 flex flex-col justify-center items-center',
          headingStyle: 'text-5xl text-white uppercase mb-4 font-cinzel',
          heading1: 'For her',
          btnContainer: '',
          btnLink: '#',
          //   btnLink: '/lerida/women',
          btn: 'bg-white text-black font-primary font-medium px-7 py-2 uppercase',
          size: '',
          btnText: 'Shop now',
        },
      },
      {
        bgImg: '/assets/images/lerida/for-him.jpg',
        colSpan: '1',
        btnData: {
          container: '',
          position:
            'lerida-our-collection-btn bottom-72 flex flex-col justify-center items-center',
          headingStyle: 'text-5xl text-white uppercase mb-12 font-cinzel',
          heading1: 'For him',
          btnContainer: '',
          //   btnLink: '/lerida/men',
          btnLink: '#',
          btn: 'bg-white text-black font-primary font-medium px-7 py-2 uppercase',
          size: '',
          btnText: 'Shop now',
        },
      },
    ],
  }

  const best_seller = {
    spacing: 'pt-20',
    textData: {
      container: 'font-primary text-center pb-16',
      heading1: 'HOUSE OF LERIDA',
      heading1Style:
        'text-midGray text-2xl font-primary font-medium uppercase mb-2',
      heading2: 'Best Sellers',
      heading2Style: ' text-black text-6xl font-cinzel uppercase',
    },
    carouselData: {
      customSlidesPerView: 3,
      customSlidesPerGroup: 1,
      customSpaceBetween: 0.1,
      customStyle:
        'relative pb-32 hide-inbuilt-arrow hide-left-hidden-arrow right-black-arrow golden-pagination-bullet lerida-best-sellers-slider reset-swiper-container',
      carouselData: [
        {
          customSlide: 'flex flex-col',
          btnIcons: 'relative border border-midGray',
          image: '/assets/images/lerida/best-sellers1.jpg',
          altTag: 'model',
          btnData: {
            container: '',
            position: '',
            headingStyle:
              'text-midGray text-2xl text-left font-primary flex flex-col justify-center items-center gap-2 py-4 ',
            heading1: 'LEATHER JACKET',
            heading1Style: 'font-medium',
            priceContainer:
              'flex items-baseline self-start ml-14 xl:self-center xl:ml-0',
            currencySvg: '/assets/svg/checkout/rupee-svg-icon.svg',
            currencySvgTailwind: 'w-3 mr-1',
            heading2: '5000',
            heading2Style: '',
            btnLink: '/',
            btn: 'hidden',
          },
          icons: [
            {
              icon: '/assets/svg/red-heart-fill.svg',
              iconSize: 'icon-size',
            },
            {
              icon: '/assets/svg/bag.svg',
              iconSize: 'icon-size',
            },
          ],
          iconsPosition: 'flex gap-6 absolute right-10 bottom-4',
        },
        {
          customSlide: 'flex flex-col',
          btnIcons: 'relative border border-midGray',
          image: '/assets/images/lerida/best-sellers2.jpg',
          altTag: 'model',
          btnData: {
            container: '',
            position: '',
            headingStyle:
              'text-midGray text-2xl text-left font-primary flex flex-col justify-center items-center gap-2 py-4 ',
            heading1: 'TURTENECK SWEATSHIRT',
            heading1Style: 'font-medium',
            priceContainer:
              'flex items-baseline self-start ml-14 xl:self-center xl:ml-0',
            currencySvg: '/assets/svg/checkout/rupee-svg-icon.svg',
            currencySvgTailwind: 'w-3 mr-1',
            heading2: '7000',
            heading2Style: '',
            btnLink: '/',
            btn: 'hidden',
          },
          icons: [
            {
              icon: '/assets/svg/red-heart-fill.svg',
              iconSize: 'icon-size',
            },
            {
              icon: '/assets/svg/bag.svg',
              iconSize: 'icon-size',
            },
          ],
          iconsPosition: 'flex gap-6 absolute right-10 bottom-4',
        },
        {
          customSlide: 'flex flex-col',
          btnIcons: 'relative border border-midGray',
          image: '/assets/images/lerida/best-sellers1.jpg',
          altTag: 'model',
          btnData: {
            container: '',
            position: '',
            headingStyle:
              'text-midGray text-2xl text-left font-primary flex flex-col justify-center items-center gap-2 py-4 ',
            heading1: 'FORMAL GREY SUIT',
            heading1Style: 'font-medium',
            priceContainer:
              'flex items-baseline self-start ml-14 xl:self-center xl:ml-0',
            currencySvg: '/assets/svg/checkout/rupee-svg-icon.svg',
            currencySvgTailwind: 'w-3 mr-1',
            heading2: '8000',
            heading2Style: '',
            btnLink: '/',
            btn: 'hidden',
          },
          icons: [
            {
              icon: '/assets/svg/red-heart-fill.svg',
              iconSize: 'icon-size',
            },
            {
              icon: '/assets/svg/bag.svg',
              iconSize: 'icon-size',
            },
          ],
          iconsPosition: 'flex gap-6 absolute right-10 bottom-4',
        },
        {
          customSlide: 'flex flex-col',
          btnIcons: 'relative border border-midGray',
          image: '/assets/images/lerida/best-sellers2.jpg',
          altTag: 'model',
          btnData: {
            container: '',
            position: '',
            headingStyle:
              'text-midGray text-2xl text-left font-primary flex flex-col justify-center items-center gap-2 py-4 ',
            heading1: 'LEATHER JACKET',
            heading1Style: 'font-medium',
            priceContainer:
              'flex items-baseline self-start ml-14 xl:self-center xl:ml-0',
            currencySvg: '/assets/svg/checkout/rupee-svg-icon.svg',
            currencySvgTailwind: 'w-3 mr-1',
            heading2: '5000',
            heading2Style: '',
            btnLink: '/',
            btn: 'hidden',
          },
          icons: [
            {
              icon: '/assets/svg/red-heart-fill.svg',
              iconSize: 'icon-size',
            },
            {
              icon: '/assets/svg/bag.svg',
              iconSize: 'icon-size',
            },
          ],
          iconsPosition: 'flex gap-6 absolute right-10 bottom-4',
        },
      ],
    },
  }

  const trending_styles = {
    container: 'bg-lightGray2 pt-32 pb-12 pr-20 grid grid-cols-3',
    heading1: 'Trending',
    heading2: 'Styles',
    headingStyle:
      'text-gray200 text-6xl 2xl:text-9xl font-cinzel uppercase self-center rotate-negative-90',
    image: '/assets/images/lerida/trending-styles.jpg',
    imageSize: '',
    //   heading2Style: "ml-28",
    carouselData: {
      customSlidesPerView: 1,
      customSlidesPerGroup: 1,
      customSpaceBetween: 0.1,
      customStyle:
        'relative pb-32 hide-inbuilt-arrow left-black-arrow right-black-arrow hide-pagination-bullets lerida-trending-styles-slider',
      carouselData: [
        {
          customSlide: 'self-start relative',
          btnIcons: 'absolute rotate-negative-90 -right-12 bottom-32',
          image: '/assets/images/lerida/bag.jpg',
          altTag: 'brown bag',
          btnData: {
            container: '',
            position: '',
            heading1: 'BROWN LEATHER PURSE',
            heading1Style: 'text-lg text-midGray ',
            btnLink: '/',
            btn: 'hidden',
          },
          icons: [
            {
              icon: '',
              iconSize: '',
            },
            {
              icon: '',
              iconSize: '',
            },
          ],
          iconsPosition: 'hidden',
        },
        {
          customSlide: 'self-start relative',
          btnIcons: 'absolute rotate-negative-90 -right-12 bottom-32',
          image: '/assets/images/lerida/bag.jpg',
          altTag: 'brown bag',
          btnData: {
            container: '',
            position: '',
            heading1: 'BROWN LEATHER PURSE',
            heading1Style: 'text-lg text-hoverYellow',
            btnLink: '/',
            btn: 'hidden',
          },
          icons: [
            {
              icon: '',
              iconSize: '',
            },
            {
              icon: '',
              iconSize: '',
            },
          ],
          iconsPosition: 'hidden',
        },
      ],
    },
  }

  const discount = {
    customSlidesPerView: 1,
    customSlidesPerGroup: 1,
    customSpaceBetween: 0.1,
    customStyle:
      ' relative h-screen hide-inbuilt-arrow hide-left-hidden-arrow right-white-arrow hide-pagination-bullets lerida-discount-slider',
    carouselData: [
      {
        customSlide: 'flex',
        btnIcons: 'absolute left-40 1800px:left-52 bottom-12 1800px:bottom-6',
        image: '/assets/images/lerida/discount.jpg',
        altTag: 'model wearing Lerida jewellery',
        btnData: {
          container: 'inline-flex flex-col items-start gap-8',
          position: '',
          headingStyle:
            'text-white font-primary text-left text-5xl 1800px:text-7xl',
          heading1: 'HOUSE OF LERIDA',
          heading1Style: 'text-xl font-medium uppercase mb-2',
          heading2: 'Best of',
          heading2Style: 'font-cinzel uppercase',
          heading3: 'Jewellery',
          heading3Style: 'font-cinzel uppercase',
          heading4: '50% off',
          heading4Style:
            'text-lightOrange font-roboto mt-4 uppercase text-6xl 1800px:text-8xl',
          btnLink: '#',
          //   btnLink: '/view-all',
          btn: 'hidden',
        },
      },
      {
        customSlide: 'flex',
        btnIcons: 'absolute left-40 1800px:left-52 bottom-12 1800px:bottom-6',
        image: '/assets/images/lerida/discount.jpg',
        altTag: 'model wearing Lerida jewellery',
        btnData: {
          container: 'inline-flex flex-col items-start gap-8',
          position: '',
          headingStyle:
            'text-lightOrange font-primary text-left text-5xl 1800px:text-7xl',
          heading1: 'HOUSE OF LERIDA',
          heading1Style: 'text-xl font-medium uppercase mb-2',
          heading2: 'Best of',
          heading2Style: 'font-cinzel uppercase',
          heading3: 'Jewellery',
          heading3Style: 'font-cinzel uppercase',
          heading4: '49% off',
          heading4Style:
            'text-white font-roboto mt-4 uppercase  text-6xl 1800px:text-8xl',
          btnLink: '#',
          //   btnLink: '/view-all',
          btn: 'hidden',
        },
      },
    ],
  }

  return (
    <Layout>
      <div>
        <HeroNewCarousel
          customStyle={hero.customStyle}
          customSlidesPerView={hero.customSlidesPerView}
          customSlidesPerGroup={hero.customSlidesPerGroup}
          customSpaceBetween={hero.customSpaceBetween}
          carouselData={hero.carouselData}
        />

        <LeridaNewArrivals
          container={new_arrivals.container}
          display={new_arrivals.display}
          btnData={new_arrivals.btnData}
          carouselData={new_arrivals.carouselData}
          heading={new_arrivals.heading}
          headingStyle={new_arrivals.headingStyle}
        />

        <OurCollection
          stripContainer={our_collection.stripContainer}
          stripText1={our_collection.stripText1}
          stripText2={our_collection.stripText2}
          stripText3={our_collection.stripText3}
          stripText1Style={our_collection.stripText1Style}
          stripText2Style={our_collection.stripText2Style}
          stripText3Style={our_collection.stripText3Style}
          display={our_collection.display}
          shopNowCompsData={our_collection.shopNowCompsData}
        />

        <LeridaBestSellers
          spacing={best_seller.spacing}
          textData={best_seller.textData}
          carouselData={best_seller.carouselData}
        />

        <LeridaTrendingStyles
          container={trending_styles.container}
          heading1={trending_styles.heading1}
          heading2={trending_styles.heading2}
          headingStyle={trending_styles.headingStyle}
          carouselData={trending_styles.carouselData}
          image={trending_styles.image}
          imageSize={trending_styles.imageSize}
        />

        <ImageBtnIconsCarousel
          customStyle={discount.customStyle}
          carouselData={discount.carouselData}
          customSlidesPerView={discount.customSlidesPerView}
          customSlidesPerGroup={discount.customSlidesPerGroup}
          customSpaceBetween={discount.customSpaceBetween}
        />
      </div>
    </Layout>
  )
}

export default Lerida

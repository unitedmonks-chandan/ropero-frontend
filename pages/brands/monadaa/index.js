// importing components
import Layout from '../../../components/layout';
import Hero from '../../../components/Hero';
import Carousel from '../../../components/Carousel';
import NewCollection from '../../../components/NewCollection';
import Trendy from '@components/Trendy';
import AboutMonadaa from '@components/AboutMonadaa';
import Slider from '@components/Slider';

const Monadaa = () => {

    const hero = {
        tailwind: 'relative',
        css: '',

        carousel: {
            slides: [
                {
                    image: {img: '/assets/images/carousel/Black2.jpg', altTag: 'Woman wearing black glass'},
                    container: ' absolute left-0 bottom-35percent z-10 flex flex-col items-start ml-28',
                    heading: {
                        text: '', //Roperro
                        tailwind: '', 
                        css: 'carousel-heading font-black'
                      }, 
                    logo: {
                        svg: '/assets/svg/monadaa/MonadaaWhiteLogo.svg', 
                        tailwindCss: 'mb-14',
                        iconTailwindCss: '',
                        customCss: ''
                    },
                    link: {
                        text: 'shop now',
                        linkHref: '/',
                        tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center mt-14',
                        css: '',    
                    }
                },

                {
                    image: {img: '/assets/images/carousel/Black2.jpg', altTag: 'Woman wearing black glass'},
                    container: ' absolute left-0 bottom-35percent z-10 flex flex-col items-start ml-28',
                    heading: {
                        text: '', //Roperro
                        tailwind: '', 
                        css: 'carousel-heading font-black'
                      }, 
                    logo: {
                        svg: '/assets/svg/monadaa/MonadaaWhiteLogo.svg', 
                        tailwindCss: 'mb-14',
                        iconTailwindCss: '',
                        customCss: ''
                    },
                    link: {
                        text: 'shop now',
                        linkHref: '/',
                        tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center mt-14',
                        css: '',    
                    }
                },

                {
                    image: {img: '/assets/images/carousel/Black2.jpg', altTag: 'Woman wearing black glass'},
                    container: ' absolute left-0 bottom-35percent z-10 flex flex-col items-start ml-28',
                    heading: {
                        text: '', //Roperro
                        tailwind: '', 
                        css: 'carousel-heading font-black'
                      }, 
                    logo: {
                        svg: '/assets/svg/monadaa/MonadaaWhiteLogo.svg', 
                        tailwindCss: 'mb-14',
                        iconTailwindCss: '',
                        customCss: ''
                    },
                    link: {
                        text: 'shop now',
                        linkHref: '/',
                        tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center mt-14',
                        css: '',    
                    }
                },

                {
                    image: {img: '/assets/images/carousel/Black2.jpg', altTag: 'Woman wearing black glass'},
                    container: ' absolute left-0 bottom-35percent z-10 flex flex-col items-start ml-28',
                    heading: {
                        text: '', //Roperro
                        tailwind: '', 
                        css: 'carousel-heading font-black'
                      }, 
                    logo: {
                        svg: '/assets/svg/monadaa/MonadaaWhiteLogo.svg', 
                        tailwindCss: 'mb-14',
                        iconTailwindCss: '',
                        customCss: ''
                    },
                    link: {
                        text: 'shop now',
                        linkHref: '/',
                        tailwind: 'w-64 h-16 font-medium text-2xl uppercase text-colorBlack0 bg-hoverYellow flex justify-center items-center mt-14',
                        css: '',    
                    }
                },

            ],
            customCss: 'hide-inbuilt-arrow single-right-arrow-slider pagination-start bullet-margin-left-112px white-and-golden-bullets'
        }   
    };


    const new_collection = {

        section:{
            tailwind: 'flex items-center justify-center mb-20 mt-16 2xl:mt-32' 
        },
        imageContainer: {
            image: '/assets/images/new-leather-collection/bag.jpg',
            tailwind: '',
            css: 'new-collection-flex-basis-35percent',
            altTag: 'black bag'
        },
        textContainerCss: 'new-collection-flex-basis-39percent',
        textContainerTailwind: '',
        beforeHeading:{
            text: 'House of Roperro',
            tailwind: 'uppercase text-base xl:text-xl 2xl:text-2xl text-colorGray38 mb-3'
        },
        heading:{
            text: 'new leather collections',
            tailwind: 'uppercase text-5xl xl:text-6xl 2xl:text-7xl 1800px:text-8xl text-hoverYellow font-cinzel mb-4'
        },
        para:{
            text: "The online store presents selected successful collections, fashionable women’s clothing which is sewn exclusively from quality material.",
    
            tailwind: 'text-base xl:text-xl 2xl:text-2xl text-colorGray38 mb-8 xl:mb-20'
        },
        link:{
            text: 'view all',
            linkHref: '/',
            tailwind: 'w-40 xl:w-52 2xl:w-64 h-10 xl:h-14 2xl:h-16 bg-colorBlack0 text-white text-base xl:text-xl 2xl:text-2xl uppercase flex items-center justify-center',
            css: ''
        }
    };



    const trendy = {
            image: {
                textImage: {
                imageSource: '/assets/images/trendy/TRENDY.jpg',
                altTag: "Trendy Text",
                tailwindChild: 'ml-16 w-2/5',
                tailwind: 'flex justify-end mr-6'
            },
            
            
            image1: {
                imageSource: '/assets/images/trendy/imageOne.jpg',
                altTag: "Boy in Black Jacket"
            },
            
            image2: {
                imageSource: '/assets/images/trendy/imageTwo.jpg',
                altTag: "Girl with Black Bag"
            },
            imagebag: {
                imageSource: '/assets/images/trendy/bag.jpg',
                altTag: "Black Bag"
            },

            imageTall: {
                imageSource: '/assets/images/trendy/imageThree.jpg',
                altTag: "Girl with White Bag",
                tailwind: 'row-span-2'
            },
            
            tailwind: 'grid grid-cols-3 -mt-6'
        },

        text: {
            heading: {
                text: 'trendy',
                tailwind: 'uppercase text-7xl 2xl:text-8xl text-hoverYellow font-cinzel text-center mt-9'
            },
            subHeading: {
                text: 'monday',
                tailwind: 'uppercase text-5xl 2xl:text-7xl text-colorBlack0 text-center -mt-8 2xl:-mt-12'
            },
            para: {
                text: "The online store presents selected successful collections",
                tailwind: 'text-sm xl:text-lg 2xl:text-2xl text-colorGray38 text-center mt-2.5 mb-auto'
            },
            link: {
                text: 'shop now',
                tailwind: 'w-40 xl:w-52 2xl:w-64 h-10 xl:h-14 2xl:h-16 font-medium bg-colorBlack0 text-white text-base xl:text-xl 2xl:text-2xl font-medium flex justify-center items-center uppercase',
                linkHref: '/'
            },

            tailwind: 'mt-4 xl:mt-6 2xl:mt-6 1800px:mt-36 ml-14 2xl:ml-28 mr-14 2xl:mr-28 mb-20 flex flex-col items-center'
        }
    };



    const about_monadaa = {
        tailwind: 'w-full relative',
        image: {
            imageSource: '/assets/images/aboutMonadaaImage.jpg',
            altTag: 'Women in Monadaa jacket',
            tailwind: 'w-full'
        },
        sideContainer: {
            tailwind: 'absolute right-0	top-0 bg-colorDust h-full  w-1/3 flex items-end',
            css: '',
            textBox: {
                tailwind: 'mb-12 2xl:mb-24 ml-10 xl:ml-16 2xl:ml-24 pr-10 2xl:pr-28 flex flex-col items-start justify-end',
                heading: {
                    text: 'about',
                    tailwind: 'uppercase text-4xl 2xl:text-6xl text-colorGolden87 font-cinzel'
                },
                textLogo: {
                    svg: '/assets/svg/monadaa/MonadaaBrownLogo.svg',
                    tailwind: ''
                },
                para1: {
                    text: "Monadaa is a vision, Monadaa is beauty. Driven by passion, deep rooted in qualityand perfection",
                    tailwind: 'mt-8 mb-4 2xl:mb-8 text-sm xl:texl-base 2xl:text-xl 1800px:text-2xl text-white'
                },
                para2: {
                    text: "Experts from world over collaborate to build a design that is style & functionality.",
                    tailwind: 'mt-4 xl:mt-8 mb-8 xl:mb-14 text-sm xl:text-base 2xl:text-xl 1800px:text-2xl text-white'
                },
        
                link: {
                    text: 'know more',
                    linkHref: '/',
                    tailwind: 'w-40 xl:w-52 2xl:w-64 h-10 xl:h-14 2xl:h-16 font-medium bg-colorBlack0 text-white uppercase text-base xl:text-xl 2xl:text-2xl mb-14 uppercase flex justify-center items-center',
                    css: ''
                },
        
                logo: {
                    svg: '/assets/svg/monadaa/MonadaaLogoIcon.svg',
                    tailwind: 'w-14 xl:w-20 1800px:w-40'
                }        
            }

        }
    }


    const slider_gifting_collection = {   
        tailwind: 'relative',
        css: '',    
        
        carousel: {
            slides: [
                {
                    image: {img: '/assets/images/carousel/watch.jpg', altTag: 'watch'},
                    textContainer: {
                        tailwind: 'absolute left-0 top-0 z-10 w-5/12 h-full bg-colorBlack21 opacity-60 flex items-end',
                        
                        textBox: {
                            tailwind: 'ml-28 mb-80 flex flex-col items-start justify-start',
                            
                            beforeHeading: {
                                text: 'EXQUISITE',
                                tailwind: 'uppercase text-2xl text-hoverYellow mb-4'
                            },
            
                            heading: {
                                text: 'gifting collection',
                                tailwind: 'uppercase text-8xl text-white font-cinzel mb-7 text-left'
                            },
            
                            para: {
                                text: "A priceless gift for your loved ones",
                                tailwind: 'text-2xl text-colorGray73 mb-16'
                            },
                            
                            link: {
                                text: 'shop now',
                                linkHref: '',
                                tailwind: "uppercase text-white text-2xl bg-hoverYellow w-64 h-16 font-medium flex justify-center items-center ",
                                css: ''
                            }
                        }
                        
                    }
                },
                
                // second slide

                {
                    image: {img: '/assets/images/carousel/watch.jpg', altTag: 'watch'},
                    textContainer: {
                        tailwind: 'absolute left-0 top-0 z-10 w-5/12 h-full bg-colorBlack21 opacity-60 flex items-end',
                        
                        textBox: {
                            tailwind: 'ml-28 mb-80 flex flex-col items-start justify-start',
                            
                            beforeHeading: {
                                text: 'EXQUISITE',
                                tailwind: 'uppercase text-2xl text-hoverYellow mb-4'
                            },
            
                            heading: {
                                text: 'gifting collection',
                                tailwind: 'uppercase text-8xl text-white font-cinzel mb-7 text-left'
                            },
            
                            para: {
                                text: "A priceless gift for your loved ones",
                                tailwind: 'text-2xl text-colorGray73 mb-16'
                            },
                            
                            link: {
                                text: 'shop now',
                                linkHref: '',
                                tailwind: "uppercase text-white text-2xl bg-hoverYellow w-64 h-16 font-medium flex justify-center items-center ",
                                css: ''
                            }
                        }
                        
                    }
                },
                
                // third slide

                {
                    image: {img: '/assets/images/carousel/watch.jpg', altTag: 'watch'},
                    textContainer: {
                        tailwind: 'absolute left-0 top-0 z-10 w-5/12 h-full bg-colorBlack21 opacity-60 flex items-end',
                        
                        textBox: {
                            tailwind: 'ml-28 mb-80 flex flex-col items-start justify-start',
                            
                            beforeHeading: {
                                text: 'EXQUISITE',
                                tailwind: 'uppercase text-2xl text-hoverYellow mb-4'
                            },
            
                            heading: {
                                text: 'gifting collection',
                                tailwind: 'uppercase text-8xl text-white font-cinzel mb-7 text-left'
                            },
            
                            para: {
                                text: "A priceless gift for your loved ones",
                                tailwind: 'text-2xl text-colorGray73 mb-16'
                            },
                            
                            link: {
                                text: 'shop now',
                                linkHref: '',
                                tailwind: "uppercase text-white text-2xl bg-hoverYellow w-64 h-16 font-medium flex justify-center items-center ",
                                css: ''
                            }
                        }
                        
                    }
                },

                // fourth slide

                {
                    image: {img: '/assets/images/carousel/watch.jpg', altTag: 'watch'},
                    textContainer: {
                        tailwind: 'absolute left-0 top-0 z-10 w-5/12 h-full bg-colorBlack21 opacity-60 flex items-end',
                        
                        textBox: {
                            tailwind: 'ml-28 mb-80 flex flex-col items-start justify-start',
                            
                            beforeHeading: {
                                text: 'EXQUISITE',
                                tailwind: 'uppercase text-2xl text-hoverYellow mb-4'
                            },
            
                            heading: {
                                text: 'gifting collection',
                                tailwind: 'uppercase text-8xl text-white font-cinzel mb-7 text-left'
                            },
            
                            para: {
                                text: "A priceless gift for your loved ones",
                                tailwind: 'text-2xl text-colorGray73 mb-16'
                            },
                            
                            link: {
                                text: 'shop now',
                                linkHref: '',
                                tailwind: "uppercase text-white text-2xl bg-hoverYellow w-64 h-16 font-medium flex justify-center items-center ",
                                css: ''
                            }
                        }
                        
                    }
                },
            ],
            customCss: 'single-right-arrow-slider pagination-start bullet-margin-left-112px white-and-golden-bullets'
        }, 
    }


    return (
        <Layout >
            <div>
                <Carousel 
                    tailwind={hero.tailwind}
                    css={hero.css}
                    carousel={hero.carousel} 
                />
                <NewCollection props={new_collection} />
                <Trendy props={trendy} />
                <AboutMonadaa  props={about_monadaa} />
                <Slider 
                    carousel={slider_gifting_collection.carousel} 
                    tailwind={slider_gifting_collection.tailwind}
                    css={slider_gifting_collection.css}
                />
            </div>
        </Layout>
    );
};

export default Monadaa;
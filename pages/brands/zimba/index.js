// importing components
import Layout from '../../../components/layout'
import HeroNewCarousel from '../../../components/HeroNewCarousel/HeroNewCarousel'
import OurCollection from '@components/OurCollection/OurCollection'
import ZimbaNewArrivals from '@components/ZimbaHomePage/NewArrivals/ZimbaNewArrivals'
import ZimbaBestSellers from '@components/ZimbaHomePage/BestSellers/ZimbaBestSellers'

const Zimba = () => {
  const zimba_hero = {
    customStyle: 'zimba-hero-slider relative h-screen',
    carouselData: [
      {
        image: '/assets/images/zimba/hero1.jpg',
        altTag: 'Woman in black jacket',
        btnData: {
          position:
            'absolute zimba-hero-btn-position z-50 flex flex-col gap-8 items-center justify-center',
          logo: '/assets/svg/zimba/logo.svg',

          logoSize: 'slider-logo-size',
          btnLink: '#',
          // btnLink: "/zimba",
          btn: 'bg-white text-black text-lg font-primary px-7 py-1 uppercase',
          btnText: 'Shop now',
        },
      },
      {
        image: '/assets/images/monadaa-comp.png',
        altTag: 'Prada Purse',
        btnData: {
          position:
            'absolute zimba-hero-btn-position z-50 flex flex-col gap-8 items-center justify-center',
          logo: '/assets/svg/zimba/logo.svg',

          logoSize: 'slider-logo-size',
          btnLink: '#',
          // btnLink: '/zimba/view-all',
          btn: 'bg-midRed text-white text-lg font-primary px-7 py-1 uppercase',
          btnText: 'View all',
        },
      },
    ],
  }

  const our_collection = {
    stripContainer: 'font-primary text-center py-10 zimba-strip-bg bg-cover',
    stripText1: 'Our Collection',
    stripText1Style: 'text-white text-6xl mb-6',
    stripText2:
      'The online store presents selected successful collections, fashionable ',
    stripText2Style: 'text-lightGray text-lg text-center',
    stripText3:
      'women’s clothing which is sewn exclusively from quality material.',
    stripText3Style: 'text-lightGray text-lg text-center',
    display: 'grid grid-cols-1 md:grid-cols-2',

    shopNowCompsData: [
      {
        bgImg: '/assets/images/zimba/our-collection1.jpg',
        altTag: 'Woman in brown coat',
        colSpan: '1',
        btnData: {
          container: '',
          position:
            'zimba-our-collection-btn bottom-20 flex flex-col justify-center items-center',
          headingStyle: 'text-5xl text-white uppercase mb-12',
          heading1: 'Women',
          btnContainer: '',
          btnLink: '#',
          // btnLink: '/zimba/women',
          btn: 'bg-white text-black px-6 py-1 uppercase',
          size: '',
          btnText: 'Shop now',
        },
      },
      {
        bgImg: '/assets/images/zimba/our-collection2.jpg',
        altTag: 'Man in black leather jacket',
        colSpan: '1',
        btnData: {
          container: '',
          position:
            'zimba-our-collection-btn bottom-20 flex flex-col justify-center items-center',
          headingStyle: 'text-5xl text-white uppercase mb-12',
          heading1: 'Men',
          btnContainer: '',
          btnLink: '#',
          // btnLink: '/zimba/men',
          btn: 'bg-white text-black px-6 py-1 uppercase',
          size: '',
          btnText: 'Shop now',
        },
      },
    ],
  }

  const trending_styles = {
    customStyle:
      'zimba-hero-slider zimba-trending-styles-slider relative bg-black pr-24 py-32',
    carouselData: [
      {
        image: '/assets/images/zimba/trending-styles.jpg',
        altTag: 'Woman with Grey Bag',
        btnData: {
          position:
            'flex flex-col gap-8 items-start justify-center w-96  pl-24 2xl:p-0 mr-32',
          logo: '/assets/svg/zimba/trending-styles.svg',

          logoSize: 'slider-logo-size',
          btnLink: '#',
          // btnLink: '/zimba/view-all',
          btn: 'bg-white text-black text-lg font-primary px-7 py-1 uppercase',
          btnText: 'View all',
        },
      },
      {
        image: '/assets/images/zimba/trending-styles.jpg',
        altTag: 'Woman with Grey Bag',
        btnData: {
          position:
            'flex flex-col gap-8 items-start justify-center w-96  pl-24 2xl:p-0 mr-32',
          logo: '/assets/svg/zimba/trending-styles.svg',

          logoSize: 'slider-logo-size',
          btnLink: '#',
          // btnLink: '/zimba',
          btn: 'bg-midRed text-white text-lg font-primary px-7 py-1 uppercase',
          btnText: 'Shop now',
        },
      },
    ],
  }

  const new_arrivals = {
    topBg: 'top-bg',
    heading: 'New Arrivals',
    headingStyle:
      'font-cinzel text-7xl text-white pt-20 text-center uppercase mb-96',
    midBg: 'bg-black',

    images: [
      {
        img: '/assets/images/zimba/new-arrivals1.jpg',
        altTag: 'Woman in black leather coat',
      },
      {
        img: '/assets/images/zimba/new-arrivals2.jpg',
        altTag: 'Grey black purse',
      },
      {
        img: '/assets/images/zimba/new-arrivals3.jpg',
        altTag: 'Man in brown jacket',
      },
      {
        img: '/assets/images/zimba/new-arrivals2.jpg',
        altTag: 'Grey black purse',
      },
      {
        img: '/assets/images/zimba/new-arrivals3.jpg',
        altTag: 'Man in brown jacket',
      },
      {
        img: '/assets/images/zimba/new-arrivals1.jpg',
        altTag: 'Woman in black leather coat',
      },
    ],
    customStyle:
      'relative pl-52 pr-56 hide-inbuilt-arrow left-white-arrow right-white-arrow reset-swiper-container zimba-new-arrivals-slider hide-pagination-bullets',
    bottomBg: 'bottom-bg grid place-items-center pt-2 pb-52 relative',
    image: '/assets/images/zimba/summer-collection-removebg.png',

    imagePosition: 'absolute bottom-0 left-24',
    btnData: {
      container: '',
      position: 'relative top-20 left-44 xl:left-0',
      heading1: 'HOUSE OF ZIMBA',
      heading1Style: 'text-white text-xl font-primary uppercase mb-2',
      heading2: 'Summer',
      heading2Style: 'font-cinzel text-5xl text-white uppercase',
      heading3: 'Collection',
      heading3Style: 'font-cinzel text-5xl text-white uppercase mb-8',
      btnLink: '#',
      // btnLink: '/shop-now',
      btn: 'inline-block bg-lightOrange text-black text-lg font-primary px-7 py-1 uppercase',
      size: '',
      btnText: 'Shop Now',
    },
  }

  const best_seller = {
    textData: {
      container: 'font-primary text-center pb-12',
      heading1: 'best sellers',
      heading1Style: 'bg-black text-white text-6xl uppercase mb-4',
      heading2:
        'The online store presents selected successful collections, fashionable',
      heading2Style: 'text-gray100 text-xl uppercase',
      heading3:
        'women’s clothing which is sewn exclusively from quality material.',
      heading3Style: 'text-gray100 text-xl uppercase mb-8',
    },

    carouselData: {
      customSpaceBetween: 20,
      customStyle:
        ' relative zimba-best-sellers-slider hide-arrow white-pagination-bullet golden-pagination-bullet-active reset-swiper-container',
      carouselData: [
        {
          customSlide: 'flex',
          btnIcons:
            'bg-lightCream px-6 2xl:px-12 grid place-items-center relative -ml-1 flex-1',

          image: '/assets/images/zimba/best-sellers1.jpg',
          imageTailwind: 'w-2/3',
          altTag: 'Woman in White Coat',
          btnData: {
            container: 'inline-flex flex-col items-start gap-8',
            position: '',
            headingStyle: 'text-midGray font-primary text-left',
            heading1: 'LINEN JACKET',
            heading1Style: 'text-xl 2xl:text-3xl mb-1',
            priceContainer: 'flex items-baseline',
            currencySvg: '/assets/svg/checkout/rupee-svg-icon.svg',
            currencySvgTailwind: 'w-2.5 2xl:w-3 mr-1',
            heading2: '5000',
            heading2Style: 'text-lg 2xl:text-2xl mb-2',
            heading3:
              'Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.',
            heading3Style: 'text-xs 2xl:text-sm 1800px:text-lg',
            btnLink: '#',
            // btnLink: '/view-all',
            btn: 'inline-block bg-black text-white text-base 2xl:text-lg font-primary px-10 py-1 mt-6',
            size: '',
            btnText: 'View',
          },

          icons: [
            {
              icon: '/assets/svg/red-heart-fill.svg',
              iconSize: 'icon-size',
            },
            {
              icon: '/assets/svg/bag.svg',

              iconSize: 'icon-size',
            },
          ],
          iconsPosition:
            'flex gap-4 absolute right-8 1800px:right-20 bottom-8 1800px:bottom-24',
        },
        {
          customSlide: 'flex',
          btnIcons:
            'bg-lightCream px-6 2xl:px-12 grid place-items-center relative -ml-1 flex-1',

          image: '/assets/images/zimba/best-sellers2.jpg',
          imageTailwind: 'w-2/3',

          btnData: {
            container: 'inline-flex flex-col items-start gap-8',
            position: '',
            headingStyle: 'text-midGray font-primary text-left',
            heading1: 'LINEN JACKET',
            heading1Style: 'text-xl 2xl:text-3xl mb-1',
            priceContainer: 'flex items-baseline',
            currencySvg: '/assets/svg/checkout/rupee-svg-icon.svg',
            currencySvgTailwind: 'w-2.5 2xl:w-3 mr-1',
            heading2: '5000',
            heading2Style: 'text-lg 2xl:text-2xl mb-2',
            heading3:
              'Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.',
            heading3Style: 'text-xs 2xl:text-sm 1800px:text-lg',
            btnLink: '#',
            // btnLink: '/view-all',
            btn: 'inline-block bg-black text-white text-base 2xl:text-lg font-primary px-10 py-1 mt-6',
            size: '',
            btnText: 'View',
          },
          icons: [
            {
              icon: '/assets/svg/red-heart-fill.svg',
              iconSize: 'icon-size',
            },
            {
              icon: '/assets/svg/bag.svg',

              iconSize: 'icon-size',
            },
          ],
          iconsPosition:
            'flex gap-4 absolute right-8 1800px:right-20 bottom-8 1800px:bottom-24',
        },
        {
          customSlide: 'flex',
          btnIcons:
            'bg-lightCream px-6 2xl:px-12 grid place-items-center relative -ml-1 flex-1',

          image: '/assets/images/zimba/best-sellers2.jpg',
          imageTailwind: 'w-2/3',
          altTag: 'Man in Green Shirt',
          btnData: {
            container: 'inline-flex flex-col items-start gap-8',
            position: '',
            headingStyle: 'text-midRed font-primary text-left',
            heading1: 'LINEN JACKET',
            heading1Style: 'text-xl 2xl:text-3xl mb-1',
            priceContainer: 'flex items-baseline',
            currencySvg: '/assets/svg/checkout/rupee-svg-icon.svg',
            currencySvgTailwind: 'w-2.5 2xl:w-3 mr-1',
            heading2: '5000',
            heading2Style: 'text-lg 2xl:text-2xl mb-2',
            heading3:
              'Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.',
            heading3Style: 'text-xs 2xl:text-sm 1800px:text-lg',
            // btnLink: '/view-all',
            btnLink: '#',
            btn: 'inline-block bg-white text-black text-base 2xl:text-lg font-primary px-10 py-1 mt-6',
            size: '',
            btnText: 'View',
          },
          icons: [
            {
              icon: '/assets/svg/red-heart-fill.svg',
              iconSize: 'icon-size',
            },
            {
              icon: '/assets/svg/bag.svg',

              iconSize: 'icon-size',
            },
          ],
          iconsPosition:
            'flex gap-4 absolute right-8 1800px:right-20 bottom-8 1800px:bottom-24',
        },
        {
          customSlide: 'flex',
          btnIcons:
            'bg-lightCream px-6 2xl:px-12 grid place-items-center relative -ml-1 flex-1',

          image: '/assets/images/zimba/best-sellers1.jpg',
          imageTailwind: 'w-2/3',
          btnData: {
            container: 'inline-flex flex-col items-start gap-8',
            position: '',
            headingStyle: 'text-midRed font-primary text-left',
            heading1: 'LINEN JACKET',
            heading1Style: 'text-xl 2xl:text-3xl mb-1',
            priceContainer: 'flex items-baseline',
            currencySvg: '/assets/svg/checkout/rupee-svg-icon.svg',
            currencySvgTailwind: 'w-2.5 2xl:w-3 mr-1',
            heading2: '5000',
            heading2Style: 'text-lg 2xl:text-2xl mb-2',
            heading3:
              'Deconstructed regular fit single breasted peak lapel jacket with front flap pockets. Enriched by tonal top stitching details. Crafted in heavy twill and half lined.',
            heading3Style: 'text-xs 2xl:text-sm 1800px:text-lg',
            btnLink: '#',
            // btnLink: '/view-all',
            btn: 'inline-block bg-white text-black text-base 2xl:text-lg font-primary px-10 py-1 mt-6',
            size: '',
            btnText: 'View',
          },
          icons: [
            {
              icon: '/assets/svg/red-heart-fill.svg',
              iconSize: 'icon-size',
            },
            {
              icon: '/assets/svg/bag.svg',

              iconSize: 'icon-size',
            },
          ],
          iconsPosition:
            'flex gap-4 absolute right-8 1800px:right-20 bottom-8 1800px:bottom-24',
        },
      ],
    },
  }

  return (
    <Layout>
      <div>
        <HeroNewCarousel
          customStyle={zimba_hero.customStyle}
          customSlidesPerView={zimba_hero.customSlidesPerView}
          customSlidesPerGroup={zimba_hero.customSlidesPerGroup}
          customSpaceBetween={zimba_hero.customSpaceBetween}
          carouselData={zimba_hero.carouselData}
        />

        <OurCollection
          stripContainer={our_collection.stripContainer}
          stripText1={our_collection.stripText1}
          stripText2={our_collection.stripText2}
          stripText3={our_collection.stripText3}
          stripText1Style={our_collection.stripText1Style}
          stripText2Style={our_collection.stripText2Style}
          stripText3Style={our_collection.stripText3Style}
          display={our_collection.display}
          shopNowCompsData={our_collection.shopNowCompsData}
        />

        <HeroNewCarousel
          customStyle={trending_styles.customStyle}
          carouselData={trending_styles.carouselData}
        />

        <ZimbaNewArrivals
          topBg={new_arrivals.topBg}
          midBg={new_arrivals.midBg}
          bottomBg={new_arrivals.bottomBg}
          heading={new_arrivals.heading}
          headingStyle={new_arrivals.headingStyle}
          images={new_arrivals.images}
          customStyle={new_arrivals.customStyle}
          image={new_arrivals.image}
          imagePosition={new_arrivals.imagePosition}
          btnData={new_arrivals.btnData}
        />

        <ZimbaBestSellers
          textData={best_seller.textData}
          carouselData={best_seller.carouselData}
        />
      </div>
    </Layout>
  )
}

export default Zimba

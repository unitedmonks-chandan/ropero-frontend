import Layout from '../../../components/layout'
import HeroNewCarousel from '../../../components/HeroNewCarousel/HeroNewCarousel'
import MigearOurCollection from '@components/MigearHomePage/OurCollection/MigearOurCollection'
import MigearTrendingStyles from '@components/MigearHomePage/TrendingStyles/MigearTrendingStyles'
import MigearNewCollection from '@components/MigearHomePage/NewCollection/MigearNewCollection'
import MigearNewArrivals from '@components/MigearHomePage/NewArrivals/MigearNewArrivals'
import MigearBestSellers from '@components/MigearHomePage/BestSellers/MigearBestSellers'

const Migear = () => {
  const hero = {
    customStyle:
      'relative h-screen hide-inbuilt-arrow hide-left-hidden-arrow right-white-arrow2 white-pagination-bullet red-pagination-bullet-active migear-hero-slider',
    carouselData: [
      {
        image: '/assets/images/migear/hero.jpg',
        altTag: 'Man with MiGEAR bag',
        btnData: {
          position: 'absolute btn-position z-50 flex flex-col gap-8',
          logo: '/assets/svg/migear-logo.svg',
          logoSize: 'slider-logo-size',
          btnLink: '#',
          // btnLink: "/migear/shop-now",
          btn: 'bg-white text-black text-lg font-primary px-7 py-1 uppercase self-center',
          btnText: 'Shop now',
        },
      },
      {
        image: '/assets/images/migear/hero.jpg',
        altTag: 'Man with MiGEAR bag',
        btnData: {
          position: 'absolute btn-position z-50 flex flex-col gap-8',
          logo: '/assets/svg/migear-logo.svg',
          logoSize: 'slider-logo-size',
          btnLink: '#',
          // btnLink: '/migear/view-all',
          btn: 'bg-midRed text-white text-lg font-primary px-7 py-1 uppercase self-center',
          btnText: 'View all',
        },
      },
    ],
  }

  const our_collection = {
    container: 'bg-black py-24 px-16 text-center',
    heading: 'Our Collection',
    headingStyle:
      'text-lightRed text-6xl font-cinzel uppercase mb-16 text-center',
    display: 'grid grid-cols-4 gap-4',
    imagesData: [
      {
        position: 'relative',
        image: '/assets/images/migear/our-collection1.jpg',
        imgLink: '/rucksack',
        text: 'Rucksack',
        textStyle:
          'text-white text-2xl font-primary font-medium uppercase absolute bottom-5 left-20',
      },
      {
        position: 'relative',
        image: '/assets/images/migear/our-collection2.jpg',
        imgLink: '/backpack',
        text: 'Backpack',
        textStyle:
          'text-white text-2xl font-primary font-medium uppercase absolute bottom-5 left-20',
      },
      {
        position: 'relative',
        image: '/assets/images/migear/our-collection3.jpg',
        imgLink: 'duffle',
        text: 'Duffle',
        textStyle:
          'text-white text-2xl font-primary font-medium uppercase absolute bottom-5 left-20',
      },
      {
        position: 'relative',
        image: '/assets/images/migear/our-collection4.jpg',
        imgLink: '/laptop-bags',
        text: 'Laptop bags',
        textStyle:
          'text-white text-2xl font-primary font-medium uppercase absolute bottom-5 left-20',
      },
    ],
    btnData: {
      btnLink: '#',
      // btnLink: '/view-collection',
      btn: 'inline-block bg-white text-black text-lg uppercase border border-black px-5 py-1 mt-8',
      text: 'View collection',
      size: ' ',
    },
  }

  const trending_styles = {
    display: 'grid grid-cols-1 lg:grid-cols-3 ',
    btnData: {
      container:
        'migear-trending-styles-bg pl-24 2xl:pl-28 pt-16 pb-16 lg:pb-0 lg:pt-60',
      position: '',
      headingStyle:
        'text-white text-5xl 2xl:text-7xl font-cinzel uppercase mb-8',
      heading1: 'Trending',
      heading2: 'Styles',
      btnLink: '#',
      // btnLink: '/view-all',
      btn: 'inline-block bg-white text-black text-lg font-primary px-7 py-1',
      size: '',
      btnText: 'View All',
    },
    carouselData: {
      customSlidersPerView: 1,
      customSlidesPerGroup: 1,
      customSpaceBetween: 0.1,
      customStyle:
        'col-span-2 bg-black pr-20 hide-inbuilt-arrow hide-left-hidden-arrow hide-pagination-bullets right-white-arrow reset-swiper-container relative',
      images: [
        {
          img: '/assets/images/migear/trending-styles.jpg',
          altTag: 'backpack',
        },
        {
          img: '/assets/images/migear/trending-styles.jpg',
          altTag: 'backpack',
        },
      ],
    },
  }

  const new_arrivals = {
    container: 'bg-black pl-20 py-24 grid grid-cols-3 gap-8 items-center',
    textData: {
      heading1: 'New',
      heading1Style:
        'text-white text-5xl xl:text-6xl font-cinzel uppercase mb-1',
      heading2: 'Arrivals',
      heading2Style:
        'text-white text-5xl xl:text-6xl font-cinzel uppercase mb-8',
      heading3:
        'The online store presents selected successful collections, fashionable women’s clothing which is sewn exclusively from fquality material.',
      heading3Style: 'text-gray100 text-xl 2xl:text-2xl',
    },
    display: 'col-span-2 grid grid-cols-2 gap-4',
    images: [
      {
        image: '/assets/images/migear/new-arrivals1.jpg',
        rowSpan: 'row-span-2 h-full',
      },
      {
        image: '/assets/images/migear/new-arrivals2.jpg',
        rowSpan: '',
      },
      {
        image: '/assets/images/migear/new-arrivals3.jpg',
        rowSpan: '',
      },
    ],
  }

  const new_collection = {
    display: 'grid grid-cols-1 xl:grid-cols-4',
    textData: {
      container: 'pl-4 pl-24 pt-20 xl:pt-40 pb-16 lg:pb-0 2xl:pt-60 2xl:pl-28',
      position: '',
      heading1: 'New',
      heading1Style: 'text-midGray text-5xl 2xl:text-6xl font-cinzel uppercase',
      heading2: 'Collection',
      heading2Style:
        'text-midGray text-5xl 2xl:text-6xl font-cinzel uppercase mb-3',
      heading3:
        'The online store presents selected successful collections, fashionable women’s clothing which is sewn fexclusively from quality material.',
      heading3Style: 'text-midGray text-xl mb-5',
      heading4: '50% OFF',
      heading4Style: 'text-lightRed100 text-5xl font-primary uppercase',
    },
    carouselData: {
      customSlidersPerView: 1,
      customSlidesPerGroup: 1,
      customSpaceBetween: 0.1,
      customStyle:
        'hide-inbuilt-arrow hide-left-hidden-arrow hide-pagination-bullets right-white-arrow py-20 pr-64 migear-new-collection-bg col-span-2 relative reset-swiper-container migear-new-collection-slider',
      images: [
        { img: '/assets/images/migear/new-collection.jpg', altTag: 'backpack' },
        { img: '/assets/images/migear/new-collection.jpg', altTag: 'backpack' },
      ],
    },
  }

  const best_sellers = {
    display: 'bg-black',
    textData: {
      container: 'py-14 xl:py-20 text-center',
      position: '',
      heading1: 'Best Sellers',
      heading1Style:
        'text-white text-5xl xl:text-6xl font-cinzel uppercase mb-10',
      heading2:
        'The online store presents selected successful collections, fashionable',
      heading2Style:
        'text-gray100 text-base xl:text-xl 2xl:text-2xl font-primary uppercase mb-3',
      heading3:
        'women’s clothing which is sewn exclusively from quality material.',
      heading3Style:
        'text-gray100 text-base xl:text-xl 2xl:text-2xl font-primary uppercase mb-0 xl:mb-3',
    },
    carouselData: {
      customSlidersPerView: 4,
      customSlidesPerGroup: 4,
      customSpaceBetween: 16,
      customStyle:
        'hide-inbuilt-arrow hide-left-hidden-arrow hide-right-hidden-arrow migear-best-sellers-slider',
      images: [
        { img: '/assets/images/migear/best-sellers3.jpg', altTag: 'backpack' },
        { img: '/assets/images/migear/best-sellers2.jpg', altTag: 'backpack' },
        { img: '/assets/images/migear/best-sellers2.jpg', altTag: 'backpack' },
        { img: '/assets/images/migear/best-sellers3.jpg', altTag: 'backpack' },
        { img: '/assets/images/migear/best-sellers3.jpg', altTag: 'backpack' },
        { img: '/assets/images/migear/best-sellers3.jpg', altTag: 'backpack' },
        { img: '/assets/images/migear/best-sellers2.jpg', altTag: 'backpack' },
        { img: '/assets/images/migear/best-sellers3.jpg', altTag: 'backpack' },
      ],
    },
  }

  return (
    <Layout>
      <div>
        <HeroNewCarousel
          customStyle={hero.customStyle}
          customSlidesPerView={hero.customSlidesPerView}
          customSlidesPerGroup={hero.customSlidesPerGroup}
          customSpaceBetween={hero.customSpaceBetween}
          carouselData={hero.carouselData}
        />

        <MigearOurCollection
          container={our_collection.container}
          heading={our_collection.heading}
          headingStyle={our_collection.headingStyle}
          display={our_collection.display}
          imagesData={our_collection.imagesData}
          btnData={our_collection.btnData}
        />

        <MigearTrendingStyles
          display={trending_styles.display}
          btnData={trending_styles.btnData}
          carouselData={trending_styles.carouselData}
        />

        <MigearNewArrivals
          container={new_arrivals.container}
          textData={new_arrivals.textData}
          display={new_arrivals.display}
          images={new_arrivals.images}
        />

        <MigearNewCollection
          display={new_collection.display}
          textData={new_collection.textData}
          carouselData={new_collection.carouselData}
        />

        <MigearBestSellers
          display={best_sellers.display}
          textData={best_sellers.textData}
          carouselData={best_sellers.carouselData}
        />
      </div>
    </Layout>
  )
}

export default Migear

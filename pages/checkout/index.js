import Layout from "../../components/layout";
import Checkout from '../../components/Checkout';


const AddressAndCheckout = () => {

    const addressAndCheckout = {
        tailwind: 'pt-28 pr-8 2xl:pr-16 1800px:pr-28 pb-36 pl-12 2xl:pl-16 1800px:pl-28',

        checkoutAndOrdersContainerCss: 'flex items-start justify-between',
        checkoutContainerCss: 'flex-basis-62',
    
        checkoutHeading: {
            text: 'checkout',
            tailwind: 'uppercase font-semibold text-3xl xl:text-4xl text-hoverYellow mb-16'
        },
    
        addressForm: {
            tailwind: 'mb-20 2xl:mb-28',
            formHeading: {
                text: '1. Shipping Address',
                tailwind: 'font-semibold text-2xl xl:text-3xl text-colorGray30 mb-10'
            }, 
        
            inputs: {
                tailwind: 'grid grid-rows-4 grid-cols-2 gap-x-8 2xl:gap-x-11 gap-y-7',
                firstName: {
                    tailwind: 'flex flex-col',
                    css: '',
                    label: {
                        text: 'First Name',
                        tailwind: 'font-light text-colorGray30 text-xl xl:text-2xl 2xl:text-3xl mb-2',
                        css: ''
                    },
                    input: {
                        required: true,
                        placeholder: '',
                        type: 'text', //number, email
                        tailwind: 'bg-colorGray91 text-lg xl:text-xl 2xl:text-2xl text-colorGray30 pt-1 xl:pt-2 2xl:pt-4 pb-1 xl:pb-2 2xl:pb-4 pl-2 font-light',
                        css: ''
                    }
                },
        
                lastName: {
                    tailwind: 'flex flex-col',
                    css: '',
                    label: {
                        text: 'Last Name',
                        tailwind: 'font-light text-colorGray30 text-xl xl:text-2xl 2xl:text-3xl mb-2',
                        css: ''
                    },
                    input: {
                        required: true,
                        placeholder: '',
                        type: 'text', //number, email
                        tailwind: 'bg-colorGray91 text-lg xl:text-xl 2xl:text-2xl text-colorGray30 pt-1 xl:pt-2 2xl:pt-4 pb-1 xl:pb-2 2xl:pb-4 pl-2 font-light',
                        css: ''
                    }
                },
        
                address1: {
                    tailwind: 'flex flex-col col-start-1 col-end-3',
                    css: '',
                    label: {
                        text: 'Address 1',
                        tailwind: 'font-light text-colorGray30 text-xl xl:text-2xl 2xl:text-3xl mb-2',
                        css: ''
                    },
                    input: {
                        required: true,
                        placeholder: '',
                        type: 'text', //number, email
                        tailwind: 'bg-colorGray91 text-lg xl:text-xl 2xl:text-2xl text-colorGray30 pt-1 xl:pt-2 2xl:pt-4 pb-1 xl:pb-2 2xl:pb-4 pl-2 font-light',
                        css: ''
                    }
                },  
                city: {
                    tailwind: 'flex flex-col',
                    css: '',
                    label: {
                        text: 'City',
                        tailwind: 'font-light text-colorGray30 text-xl xl:text-2xl 2xl:text-3xl mb-2',
                        css: ''
                    },
                    input: {
                        required: true,
                        placeholder: '',
                        type: 'text', //number, email
                        tailwind: 'bg-colorGray91 text-lg xl:text-xl 2xl:text-2xl text-colorGray30 pt-1 xl:pt-2 2xl:pt-4 pb-1 xl:pb-2 2xl:pb-4 pl-2 font-light',
                        css: ''
                    }
                },
                state: {
                    tailwind: 'flex flex-col',
                    css: '',
                    label: {
                        text: 'State',
                        tailwind: 'font-light text-colorGray30 text-xl xl:text-2xl 2xl:text-3xl mb-2',
                        css: ''
                    },
                    input: {
                        required: true,
                        placeholder: '',
                        type: 'text', //number, email
                        tailwind: 'bg-colorGray91 text-lg xl:text-xl 2xl:text-2xl text-colorGray30 pt-1 xl:pt-2 2xl:pt-4 pb-1 xl:pb-2 2xl:pb-4 pl-2 font-light',
                        css: ''
                    }
                },
                pincode: {
                    tailwind: 'flex flex-col',
                    css: '',
                    label: {
                        text: 'Pincode',
                        tailwind: 'font-light text-colorGray30 text-xl xl:text-2xl 2xl:text-3xl mb-2',
                        css: ''
                    },
                    input: {
                        required: true,
                        placeholder: '',
                        type: 'number', //number, email
                        tailwind: 'bg-colorGray91 text-lg xl:text-xl 2xl:text-2xl text-colorGray30 pt-1 xl:pt-2 2xl:pt-4 pb-1 xl:pb-2 2xl:pb-4 pl-2 font-light appearance-none',
                        css: ''
                    }
                },
                phoneNumber: {
                    tailwind: 'flex flex-col',
                    css: '',
                    label: {
                        text: 'Phone Number',
                        tailwind: 'font-light text-colorGray30 text-xl xl:text-2xl 2xl:text-3xl mb-2',
                        css: ''
                    },
                    input: {
                        required: true,
                        placeholder: '',
                        type: 'number', //number, email
                        tailwind: 'bg-colorGray91 text-lg xl:text-xl 2xl:text-2xl text-colorGray30 pt-1 xl:pt-2 2xl:pt-4 pb-1 xl:pb-2 2xl:pb-4 pl-2 font-light',
                        css: ''
                    }
                },
        
        
            },
        
            defaultAddressCheckbox: {
                tailwind: 'flex items-center mt-11 mb-7',
                css: '',
            
                input: {
                    type:'checkbox',
                    id:'address',
                    name:'address',
                    value:'default address',
                    
            
                    tailwind: 'mr-2.5',
                    css: ''
                },
            
                label: {
                    text: 'Make This My Default Address',
                    htmlFor: 'address',
                    tailwind: 'font-light text-colorGray30 text-xl xl:text-2xl',
                    css: ''
                },
            }, 
        
            giftHeading: {
                text: 'Is this a gift?',
                tailwind: 'text-lg xl:text-xl text-colorGray30 font-light mb-3.5'
            },
        
            giftCheckbox: {
                tailwind: 'flex items-center mb-9',
                css: '',
            
                input: {
                    type:'checkbox',
                    id:'gift',
                    name:'gift',
                    value:'yes',
                    tailwind: 'mr-2.5',
                    css: ''
                },
            
                label: {
                    text: 'Yes',
                    htmlFor: 'gift',
                    tailwind: 'font-light text-colorGray30 text-xl xl:text-2xl',
                    css: ''
                },
            },
        
            useThisAddressButton: {
                buttonText: 'use this address',
                tailwind: 'uppercase bg-hoverYellow text-white text-lg xl:text-xl 2xl:text-2xl font-semibold w-56 xl:w-64 2xl:w-80 h-14 xl:h-16',
                css: ''
            }
        },
    
        paymentMethod: {
            tailwind: '',
    
            heading: {
                text: '2. Payment Method',
                tailwind: 'font-semibold text-2xl xl:text-3xl text-colorGray30 mb-10'
            },
        
            paymentMethods: {
                tailwind: 'flex content-between justify-start xl:justify-between flex-wrap xl:flex-nowrap h-20',
        
                card: {
                    tailwind: 'flex items-center',
                    css: '',
                
                    input: {
                        type:'radio',
                        id:'card',
                        name:'payment-method',
                        value:'card',
                        tailwind: 'mr-1 2xl:mr-2.5',
                        css: ''
                    },
                
                    label: {
                        text: 'Credit/Debit Card',
                        htmlFor: 'card',
                        tailwind: 'font-light text-colorGray30 text-xl 1800px:text-2xl mr-5',
                        css: ''
                    },
                },
                upi: {
                    tailwind: 'flex items-center',
                    css: '',
                
                    input: {
                        type:'radio',
                        id:'upi',
                        name:'payment-method',
                        value:'upi',
                        tailwind: 'mr-1 2xl:mr-2.5',
                        css: ''
                    },
                
                    label: {
                        text: 'UPI',
                        htmlFor: 'upi',
                        tailwind: 'font-light text-colorGray30 text-lg 1800px:text-2xl mr-5',
                        css: ''
                    },
                },
                netBanking: {
                    tailwind: 'flex items-center',
                    css: '',
                
                    input: {
                        type:'radio',
                        id:'net-banking',
                        name:'payment-method',
                        value:'net-banking',
                        tailwind: 'mr-2.5',
                        css: ''
                    },
                
                    label: {
                        text: 'Net Banking',
                        htmlFor: 'net-banking',
                        tailwind: 'font-light text-colorGray30 text-lg 1800px:text-2xl mr-5',
                        css: ''
                    },
                },
                cashOnDelivery: {
                    tailwind: 'flex items-center',
                    css: '',
                
                    input: {
                        type:'radio',
                        id:'cash-on-delivery',
                        name:'payment-method',
                        value:'cash-on-delivery',
                        tailwind: 'mr-2.5',
                        css: ''
                    },
                
                    label: {
                        text: 'Cash On Delivery',
                        htmlFor: 'cash-on-delivery',
                        tailwind: 'font-light text-colorGray30 text-lg 1800px:text-2xl mr-8',
                        css: ''
                    },
                },
                payPal: {
                    tailwind: 'flex items-center',
                    css: '',
                
                    input: {
                        type:'radio',
                        id:'pay-pal',
                        name:'payment-method',
                        value:'pay-pal',
                        tailwind: 'mr-2.5',
                        css: ''
                    },
                
                    label: {
                        text: '',
                        htmlFor: 'pay-pal',
                        tailwind: 'font-light text-colorGray30 text-2xl',
                        css: '',
                        svg: {
                            icon: '/assets/svg/checkout/pay-pal-svg-icon.svg',
                            tailwind: 'w-16'  //width and height of svg icon
                        }
                    }
                }
        
            },
        
            cardsContainerTailwind: 'flex flex-col items-start xl:flex-row xl:items-end',
        
            creditOrDebitCard: {
                tailwind: 'bg-white pt-1 2xl:pt-3 1800px:pt-6 pl-10 pb-4 1800px:pb-10 mt-11 mr-12',
                css: 'width-488px debit-card-shadow mb-10 xl:mb-0',
            
                logo: {
                    svg: '/assets/svg/checkout/visa-svg-icon.svg',
                    tailwind: 'w-20 2xl:w-24 mb-1.5'
                }, 
            
                bankName: {
                    text: 'Kotak Mahindra Debit Card',
                    tailwind: 'text-base 2xl:text-xl font-medium text-colorGray30 mb-1.5'
                },
            
                cardNumber: {
                    dot: {
                        svg: '/assets/svg/checkout/dot-svg-icon.svg',
                        dotMargin: '4px',
                        //  substract dot margin from dotGropMargin. 
                        dotGroupMargin: 'mr-1.5 2xl:mr-2.5',
                    },
            
                    lastFourDigit: {
                        digits: 7845,
                        tailwind: 'text-base 2xl:text-xl text-colorGray30 font-medium'
                    }
                },
                expiry: {
                    expiryDate: '09/2024',
                    tailwind: 'text-base 2xl:text-lg font-medium text-colorGray30  mt-1.5'
                },
            
                cvv: {
                    tailwind: 'flex items-center mt-4',
            
                    cvvInput: {
                        tailwind: '',
                        css: '',
                        label: {
                            tailwind: '', 
                            text: ''
                        },
                        input: {
                            required: true,
                            placeholder: 'Enter CVV',
                            type: 'number', 
                            tailwind: 'bg-white text-xs text-colorGray30 pt-1.5 pb-1.5 pl-2.5 font-medium border border-solid border-colorBrown44  mr-20 w-20',
                            css: ''
                        }
                    },
                
                    cvvButton: {
                        buttonText: 'What is CVV ?',
                        tailwind: 'font-medium text-colorGray30',
                        css: 'font-size-9px line-height-11px'
                    }
                }
            },
        
            addNewCard: {
                tailwind: 'bg-colorGray95 flex flex-col items-center justify-center',
                css: 'width-488px height-268px',
            
                plus: {
                    tailwind: 'bg-colorGray79 flex items-center justify-center',
                    css: 'width-81px height-81px border-radius-50percent', 
            
                    link: {
                        text: '+',
                        linkHref: '/',
                        tailwind: 'text-colorGray37 flex items-baseline -mr-.5 -mb-3.5',
                        css: '  font-size-64px line-height-16px'
                    }
                },
            
                heading: {
                    text: 'Add New Card',
                    tailwind: 'text-3xl text-colorGray30 mt-7',
                    css: ''
                }
            },
        
            paraSafeInformation: {
                text: "Your Information Is Safe",
            
                tailwind: 'text-xl text-colorGray30 mt-3.5 ml-10',
            }
        },
    
        orderContainerCss: 'flex-basis-28',
    
        orderHeading: {
            text: 'order',
            tailwind: 'uppercase font-semibold text-3xl xl:text-4xl text-hoverYellow mb-16'
        },
        orderSummery: {
            tailwind: 'pt-10 pl-6 pr-6 pb-10',
            css: 'width-468px height-600px black-border-point5px overflow-auto',
    
            headingContainer: {
                tailwind: 'flex items-center justify-between  mb-10',
    
                orderHeading: {
                    text: 2,
                    tailwind: 'font-semibold text-xl text-colorBlack5'
                },
    
                editLink: {
                    text: 'Edit',
                    linkHref: '/', 
                    backgroundTailwind: '', 
                    tailwind: 'font-medium text-xl text-hoverYellow', 
                    css: ''
                }
            },
    
            //////////////////////// Cart Items 
    
            cartItems: [
                            
                {
                    tailwind: 'flex items-start mb-10',
                    css: '',
    
                    image: {
                        imageSource: '/assets/images/cart/itemOne.jpg',
                        altTag: 'Girl in Grey Sweatshirt',
                        imageTailwind: '',
                        imageCss: 'w-32 xl:w-44',
                        imageWrapperTailwind: '',
                        imageWrapperCss: ''
                    },
                                
                    textContainer: {
                        tailwind: 'ml-6 -mt-1.5',
                        css: '',
                        
                        heading: {
                            text: 'Light Grey Hoody Sweatshirt',
                            tailwind: 'text-sm xl:text-base font-medium text-colorGray30',
                            css: ''
                        },
    
                        price: {
                            tailwind: 'flex items-center mt-1.5',
                            css: '',
                            
                            icon: {
                                currency: '/assets/svg/checkout/rupee-svg-icon.svg',
                                tailwind: 'w-2 h-3 mr-1',
                                css: ''
                            },
                                
                            cost: {
                                cost: 5000,
                                tailwind: 'text-base font-medium text-colorBlack5',
                                css: ''
                            }
                        },
    
                        color: {
                            value: 'Grey',
                            tailwind: 'text-sm xl:text-normal font-normal text-colorGray30 mt-1.5',
                            css: ''
                        },
                            
                        size: {
                            value: 'Medium',
                            tailwind: 'text-sm xl:text-normal font-normal text-colorGray30 mt-1.5',
                            css: ''
                        },
                            
                        quantity: {
                            value: 2,
                            tailwind: 'text-sm xl:text-normal font-normal text-colorGray30 mt-1.5',
                            css: ''
                        },
    
                        buttons: {
                            tailwind: 'flex items-center mt-1',
                            css: '',
                            
                            minus: {
                                svg: '/assets/svg/checkout/minus-svg-icon.svg',
                                tailwind: 'w-5 h-5 bg-colorGray88 flex items-center justify-center rounded-full',
                                css: ''
                            },
                            
                            quantityInBetweenbuttons: {
                                tailwind: 'ml-1.5 mr-1.5',
                                css: ''
                            },
                            
                            plus: {
                                svg: '/assets/svg/checkout/plus-svg-icon-black.svg',
                                tailwind: 'w-5 h-5 bg-hoverYellow flex items-center justify-center rounded-full',
                                css: ''
                            }
                        }
                    }
                },
    
                /////////////////// Second Item of cart
                {
                    tailwind: 'flex items-start mb-10',
                    css: '',
    
                    image: {
                        imageSource: '/assets/images/cart/itemTwo.jpg',
                        altTag: 'Girl in Grey Sweatshirt',
                        imageTailwind: '',
                        imageCss: 'w-32 xl:w-44',
                        imageWrapperTailwind: '',
                        imageWrapperCss: ''
                    },
                                
                    textContainer: {
                        tailwind: 'ml-6 -mt-1.5',
                        css: '',
                        
                        heading: {
                            text: 'Dark Green Hoody Sweatshirt',
                            tailwind: 'text-sm xl:text-base font-medium text-colorGray30',
                            css: ''
                        },
    
                        price: {
                            tailwind: 'flex items-center mt-1.5',
                            css: '',
                            
                            icon: {
                                currency: '/assets/svg/checkout/rupee-svg-icon.svg',
                                tailwind: 'w-2 h-3 mr-1',
                                css: ''
                            },
                                
                            cost: {
                                cost: 7500,
                                tailwind: 'text-base font-medium text-colorBlack5',
                                css: ''
                            }
                        },
    
                        color: {
                            value: 'Green',
                            tailwind: 'text-sm xl:text-base font-normal text-colorGray30 mt-1.5',
                            css: ''
                        },
                            
                        size: {
                            value: 'Large',
                            tailwind: 'text-sm xl:text-base font-normal text-colorGray30 mt-1.5',
                            css: ''
                        },
                            
                        quantity: {
                            value: 1,
                            tailwind: 'text-sm xl:text-base font-normal text-colorGray30 mt-1.5',
                            css: ''
                        },
    
                        buttons: {
                            tailwind: 'flex items-center mt-1',
                            css: '',
                            
                            minus: {
                                svg: '/assets/svg/checkout/minus-svg-icon.svg',
                                tailwind: 'w-5 h-5 bg-colorGray88 flex items-center justify-center rounded-full',
                                css: ''
                            },
                            
                            quantityInBetweenbuttons: {
                                tailwind: 'ml-1.5 mr-1.5',
                                css: ''
                            },
                            
                            plus: {
                                svg: '/assets/svg/checkout/plus-svg-icon-black.svg',
                                tailwind: 'w-5 h-5 bg-hoverYellow flex items-center justify-center rounded-full',
                                css: ''
                            }
                        }
                    }
                }
            ],
    
            //////////////////////////////////// order summery
    
            totalAmount: {
                tailwind: 'text-lg font-medium text-colorGray30',
                css: 'black-top-border-point5px',
                    
                currency: {
                    svg: '/assets/svg/checkout/rupee-svg-icon.svg',
                    tailwind: 'w-2 h-3 mr-1'
                },
    
                subTotal: {
                    tailwind: 'flex items-baseline justify-start mt-10',
                    
                    costText: {
                        text: 'Sub Total',
                        tailwind: 'mr-auto'
                    },
                    amount: {
                        tailwind: 'text-base font-medium text-colorGray30'
                    }
                },
    
                shippingCharge: {
                    tailwind: 'flex items-baseline justify-start mt-2.5', 
    
                    costText: {
                        text: 'Shipping Charge',
                        tailwind: 'mr-auto'
                    },
                    amount: {
                        value: 0,
                        tailwind: 'text-base font-medium text-colorGray30',
                        freeTextTailwind: 'text-base font-semibold text-hoverYellow uppercase'
                    }
                },
    
                totalToPay: {
                    tailwind: 'flex items-baseline justify-start mt-2.5', 
    
                    costText: {
                        text: 'Total To Pay',
                        tailwind: 'mr-auto'
                    },
                    amount: {
                        tailwind: 'text-base font-medium text-colorGray30'
                    }
                }
            }
        },
    
        linksContainer: {
            tailwind: 'flex items-center justify-center mt-32',
            backToShopping: {
                text: 'back to shopping',
                linkHref: '/',
                tailwind: 'text-xl 2xl:text-2xl uppercase  text-hoverYellow',
                css: '',
                backgroundTailwind: 'w-64 2xl:w-80 h-16 bg-white border-2 border-solid border-hoverYellow flex items-center justify-center mr-12 2xl:mr-16'
            }, 
    
            makePayment: {
                text: 'make payment',
                linkHref: '/',
                tailwind: 'text-xl 2xl:text-2xl uppercase text-white ',
                css: '',
                backgroundTailwind: 'w-64 2xl:w-80 h-16 bg-hoverYellow border-2 border-solid border-hoverYellow flex items-center justify-center'
            }
        }
    }



    return (
        <Layout >
            <Checkout 
                tailwind={addressAndCheckout.tailwind} 
                checkoutAndOrdersContainerCss={addressAndCheckout.checkoutAndOrdersContainerCss} 
                checkoutContainerCss={addressAndCheckout.checkoutContainerCss} 
                checkoutHeading={addressAndCheckout.checkoutHeading} 
                addressForm={addressAndCheckout.addressForm} 
                paymentMethod={addressAndCheckout.paymentMethod} 
                orderContainerCss={addressAndCheckout.orderContainerCss} 
                orderHeading={addressAndCheckout.orderHeading} 
                orderSummery={addressAndCheckout.orderSummery} 
                linksContainer={addressAndCheckout.linksContainer} 
            />
        </Layout>
    );
};

export default AddressAndCheckout;